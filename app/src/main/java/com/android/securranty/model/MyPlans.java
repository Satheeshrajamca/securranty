
package com.android.securranty.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyPlans {

    @SerializedName("AdPage")
    private List<AdPage> adPage;
    @Expose
    private String status;
    @Expose
    private int statusCode;

    public List<AdPage> getAdPage() {
        return adPage;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private List<AdPage> adPage;
        private String status;
        private int statusCode;

        public MyPlans.Builder withAdPage(List<AdPage> adPage) {
            this.adPage = adPage;
            return this;
        }

        public MyPlans.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public MyPlans.Builder withStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public MyPlans build() {
            MyPlans myPlan = new MyPlans();
            myPlan.adPage = adPage;
            myPlan.status = status;
            myPlan.statusCode = statusCode;
            return myPlan;
        }

    }

}
