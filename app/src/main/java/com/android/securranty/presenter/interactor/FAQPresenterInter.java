package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.FAQModel;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class FAQPresenterInter {

    BasePresenter basePresenter;

    public FAQPresenterInter(BasePresenter basePresenter) {
        this.basePresenter = basePresenter;
    }

    public void getFAQ() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getFAQDetails(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    "1").enqueue(new Callback<FAQModel>() {
                @Override
                public void onResponse(Call<FAQModel> call, Response<FAQModel> response) {
                    if (response.isSuccessful()) {
                        basePresenter.success(response.body());
                    } else {
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<FAQModel> call, Throwable t) {
                    if (t instanceof HttpException)
                        errorResponse(((HttpException) t).response().errorBody());
                    else
                        basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }
    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            basePresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
