
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Coverage {

    @SerializedName("Id")
    private String id;
    @SerializedName("Name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static class Builder {

        private String id;
        private String name;

        public Coverage.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Coverage.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Coverage build() {
            Coverage coverage = new Coverage();
            coverage.id = id;
            coverage.name = name;
            return coverage;
        }

    }

}
