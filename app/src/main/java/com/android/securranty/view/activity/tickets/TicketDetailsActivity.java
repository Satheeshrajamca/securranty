package com.android.securranty.view.activity.tickets;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.tickets.model.PostMessageSuccess;
import com.android.securranty.view.activity.tickets.model.SupportTicketMessage;
import com.android.securranty.view.activity.tickets.model.Ticket;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

import static com.android.securranty.view.activity.tickets.TicketListActivity.ticketsItem;

public class TicketDetailsActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textTitle)
    TextView textTitle;
    @BindView(R.id.textMessage)
    TextView textMessage;
    @BindView(R.id.textPriority)
    TextView textPriority;
    @BindView(R.id.textType)
    TextView textType;
    @BindView(R.id.textStatus)
    TextView textStatus;
    @BindView(R.id.textDateAdded)
    TextView textDateAdded;
    @BindView(R.id.textLastResponse)
    TextView textLastResponse;
    @BindView(R.id.textTicketClosed)
    TextView textTicketClosed;
    @BindView(R.id.textContactEmail)
    TextView textContactEmail;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    private ActionBar actionBar;
    HashMap<Integer, Object> map;

    public static int TICKET_DETAILS = 0;
    public static int MESSAGE = 1;
    List<SupportTicketMessage> listMessage;
    int ticketId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        int ticketPosition = getIntent().getIntExtra("TICKET_POSITION", 0);
        listMessage = new ArrayList<>();
        //listMessage.add("Testing");
        Ticket ticketList = ticketsItem.get(ticketPosition);
        for (SupportTicketMessage ticketMessage : ticketList.getSupportTicketMessage()) {
            listMessage.add(ticketMessage);
        }
        ticketId = ticketList.getId();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*new MaterialDialog.Builder(TicketDetailsActivity.this).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //finish();
                        //startActivityForResult(new Intent(ShippingActivity.this, AddShippingAddressActivity.class), 100);
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                        .canceledOnTouchOutside(false)
                        .title("Post Reply")
                        .positiveText("Send")
                        .negativeText("Cancel")
                        .show();*/

                new MaterialDialog.Builder(TicketDetailsActivity.this).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        View view = dialog.getCustomView();
                        EditText editText = (EditText) view.findViewById(R.id.editMessage);
                        Log.i("PostReply", editText.getText().toString().trim());
                        postingMessage(editText.getText().toString().trim());

                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).title("Post Reply")
                        .customView(R.layout.layout_post_reply, true)
                        .positiveText("Send")
                        .negativeText("Cancel")
                        .show();
            }
        });

        textTitle.setText(ticketList.getTitle() != null ? ticketList.getTitle() : "-");
        textMessage.setText("-");
        textPriority.setText(ticketList.getPriority() != null ? ticketList.getPriority() : "-");
        textType.setText(ticketList.getType() != null ? ticketList.getType().toString() : "-");
        textStatus.setText(ticketList.getStatus() != null ? ticketList.getStatus().toString() : "-");
        textDateAdded.setText(ticketList.getCreatedDate() != null ? ticketList.getCreatedDate().split("T")[0] : "-");
        textLastResponse.setText(ticketList.getLastModDate() != null ? ticketList.getLastModDate().split("T")[0] : "-");
        textTicketClosed.setText(ticketList.getDateClosed() != null ? ticketList.getDateClosed().toString() : "-");
        textContactEmail.setText(ticketList.getCreatedBy() != null ? ticketList.getCreatedBy() : "-");

        recyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this);
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setLayoutManager(linearLayoutManager);
        AdapterTicketDetails ticketListAdapter = new AdapterTicketDetails();
        ticketListAdapter.setData(listMessage);
        recyclerView.setAdapter(ticketListAdapter);
    }

    private void postingMessage(String message) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (!TextUtils.isEmpty(message)) {
                progressBar.setVisibility(View.VISIBLE);
                ApiClient.getClient().postMessage(SharePref.getInstance(this).getToken(), ticketId, message).enqueue(new Callback<PostMessageSuccess>() {
                    @Override
                    public void onResponse(Call<PostMessageSuccess> call, Response<PostMessageSuccess> response) {
                        if (response.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            PostMessageSuccess loginResponse = response.body();
                            if (loginResponse.getStatusCode() == 200) {
                                new iOSDialogBuilder(TicketDetailsActivity.this)
                                        .setTitle("Success")
                                        .setSubtitle(loginResponse.getMessage())
                                        .setBoldPositiveLabel(true)
                                        .setCancelable(false)
                                        .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                                            @Override
                                            public void onClick(iOSDialog dialog) {
                                                Intent intent = getIntent();
                                                setResult(RESULT_OK, intent);
                                                finish();
                                                dialog.dismiss();
                                            }
                                        }).build().show();
                            }

                        } else {
                            progressBar.setVisibility(View.GONE);
                            errorResponse(response.errorBody());
                        }
                    }

                    @Override
                    public void onFailure(Call<PostMessageSuccess> call, Throwable e) {
                        progressBar.setVisibility(View.GONE);
                        if (e instanceof HttpException)
                            errorResponse(((HttpException) e).response().errorBody());
                        else
                            Toast.makeText(TicketDetailsActivity.this, getString(R.string.err_back_end), Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                Toast.makeText(this, "Message do not empty", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.imageBackButton})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            Toast.makeText(this, message.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IllegalStateException | JsonSyntaxException exception) {
            Toast.makeText(this, getString(R.string.err_back_end), Toast.LENGTH_SHORT).show();
        }
    }
}
