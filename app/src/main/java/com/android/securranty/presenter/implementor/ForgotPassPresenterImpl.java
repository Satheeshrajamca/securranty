package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.ForgotPassPresenterInteractor;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshraja on 11/16/17.
 */

public class ForgotPassPresenterImpl implements BasePresenter {
    private BaseView forgotpassView;
    private ForgotPassPresenterInteractor signinPresenterInteractor;

    public ForgotPassPresenterImpl(BaseView forgotpassView) {
        this.forgotpassView = forgotpassView;
    }

    public void validateCredential(String email) {
        if (forgotpassView != null) {
            forgotpassView.showProgress();
            forgotpassView.hideKeyboard();
            signinPresenterInteractor = new ForgotPassPresenterInteractor(this);
        }
        signinPresenterInteractor.validateEmail(email);
    }

    @Override
    public void success(Object obj) {
        forgotpassView.success(obj);
    }

    @Override
    public void errorMsg(int statusCode, String errorMsg) {
        if (forgotpassView != null) {
            forgotpassView.hideProgress();
            forgotpassView.showErrorMsg(statusCode, errorMsg);
        }
    }

    @Override
    public void onDestroy() {
        forgotpassView = null;
    }

}
