package com.android.securranty.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.securranty.model.LoginResponse;
import com.android.securranty.model.UserInfo;

/**
 * Created by satheeshr on 24-11-2017.
 */

public class SharePref {
    private static SharePref sharePref = new SharePref();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static final String IS_LOGGEDIN = "IS_LOGGEDIN";
    private static final String USER_ID = "USER_ID";
    private static final String TOKEN = "TOKEN";
    private static final String USER_EMAIL = "USER_EMAIL";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String ACCOUNT_CATEGORY = "ACCOUNT_CATEGORY";
    private static final String PASSWORD = "PASSWORD";
    private static final String IS_LOGGEDIN_CURRENTLY = "PASSWORD";
    private static final String PROFILE_IMAGE = "PROFILE_IMAGE";

    private SharePref() {
    } //prevent creating multiple instances by making the constructor private

    //The context passed into the getInstance should be application level context.
    public static SharePref getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return sharePref;
    }

    public void storeLoginSession(boolean loggedIn) {
        editor.putBoolean(IS_LOGGEDIN, loggedIn);
        editor.apply();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGGEDIN, false);
    }

    public String getUserId() {
        return sharedPreferences.getString(USER_ID, "");
    }

    public String getToken() {
        return sharedPreferences.getString(TOKEN, "");
    }

    public String getUserEmail() {
        return sharedPreferences.getString(USER_EMAIL, "");
    }

    public String getFirstName() {
        return sharedPreferences.getString(FIRST_NAME, "");
    }

    public String getLastName() {
        return sharedPreferences.getString(LAST_NAME, "");
    }

    public String getPassword() {
        return sharedPreferences.getString(PASSWORD, "");
    }

    public String getAccountCategory() {
        return sharedPreferences.getString(ACCOUNT_CATEGORY, "");
    }

    public void clearCredential() {
        editor.putBoolean(IS_LOGGEDIN, false);
        editor.apply();
    }

    public boolean isLoggedinCurrently() {
        return sharedPreferences.getBoolean(IS_LOGGEDIN_CURRENTLY, false);
    }

    public void storeUserDetails(LoginResponse loginResponse, String password) {
        UserInfo userInfo = loginResponse.getUser();
        editor.putString(USER_ID, userInfo.getId());
        editor.putString(TOKEN, loginResponse.getToken());
        editor.putString(USER_EMAIL, userInfo.getUserName());
        editor.putString(FIRST_NAME, userInfo.getFirstName());
        editor.putString(LAST_NAME, userInfo.getLastName());
        editor.putString(ACCOUNT_CATEGORY, userInfo.getAccountCategory());
        editor.putString(PASSWORD, password);
        editor.putBoolean(IS_LOGGEDIN_CURRENTLY, true);
        editor.apply();
    }

    public void resetUserDetails() {
        editor.putString(USER_ID, "");
        editor.putString(TOKEN, "");
        editor.putString(USER_EMAIL, "");
        editor.putString(FIRST_NAME, "");
        editor.putString(LAST_NAME, "");
        editor.putString(ACCOUNT_CATEGORY, "");
        editor.putString(PASSWORD, "");
        editor.putBoolean(IS_LOGGEDIN_CURRENTLY, false);
        editor.apply();
    }

    public void setPassword(String password) {
        editor.putString(PASSWORD, password);
    }

    public void setProfileImage(String profileImage) {
        editor.putString(PROFILE_IMAGE, profileImage);
        editor.apply();
    }

    public String getProfileImage() {
        return sharedPreferences.getString(PROFILE_IMAGE, "");
    }

    public void setFirstName(String firstName) {
        editor.putString(FIRST_NAME, firstName);
        editor.apply();
    }

    public void setLastName(String lastName) {
        editor.putString(LAST_NAME, lastName);
        editor.apply();
    }

}
