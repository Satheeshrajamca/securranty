package com.android.securranty.view.fragment.fileclaim;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.view.activity.claims.FileClaimsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class DamageDescriptionFragment extends Fragment implements TextWatcher {
    @BindView(R.id.editDetailsOfDamage)
    EditText editDetailsOfDamage;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.previous)
    TextView previous;
    @BindView(R.id.textCount)
    TextView textCount;


    private FileClaimsActivity context;
    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.layout_details_failure,
                    container, false);
            ButterKnife.bind(this, view);
            previous.setVisibility(View.VISIBLE);
            next.setVisibility(View.GONE);
            editDetailsOfDamage.addTextChangedListener(this);
            editDetailsOfDamage.setImeOptions(EditorInfo.IME_ACTION_DONE);
            editDetailsOfDamage.setRawInputType(InputType.TYPE_CLASS_TEXT);
        }
        return view;
    }

    @OnClick({R.id.editDetailsOfDamage, R.id.next, R.id.previous})
    public void onclickClaimType(View view) {
        switch (view.getId()) {
            case R.id.next:
                daoFileClaim.setProblemDesc(problemDes);
                context.triggerClaim(new ClaimPreferenceFragment(), "Claim Service Preference");
                break;
            case R.id.previous:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
        }
    }

    private String problemDes;

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        problemDes=editable.toString();
        textCount.setText(String.valueOf(editable.length()));
        if (editable.length() > 99) {
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.GONE);
        }
    }

}
