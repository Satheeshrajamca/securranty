
package com.android.securranty.view.activity.tickets.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class PostMessageSuccess {

    @Expose
    private String message;
    @Expose
    private String status;
    @Expose
    private Long statusCode;
    @Expose
    private Long ticketId;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public static class Builder {

        private String message;
        private String status;
        private Long statusCode;
        private Long ticketId;

        public PostMessageSuccess.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public PostMessageSuccess.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public PostMessageSuccess.Builder withStatusCode(Long statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public PostMessageSuccess.Builder withTicketId(Long ticketId) {
            this.ticketId = ticketId;
            return this;
        }

        public PostMessageSuccess build() {
            PostMessageSuccess postMessageSuccess = new PostMessageSuccess();
            postMessageSuccess.message = message;
            postMessageSuccess.status = status;
            postMessageSuccess.statusCode = statusCode;
            postMessageSuccess.ticketId = ticketId;
            return postMessageSuccess;
        }

    }

}
