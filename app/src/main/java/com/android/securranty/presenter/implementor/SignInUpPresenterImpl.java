package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.SignInUpPresenterInteractor;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;

/**
 * Created by satheeshraja on 11/16/17.
 */

public class SignInUpPresenterImpl implements BasePresenter {
    private BaseView signinView;
    private SignInUpPresenterInteractor signInUpPresenterInteractor;

    public SignInUpPresenterImpl(BaseView signinView) {
        this.signinView = signinView;
    }

    public void validateCredential(String userName, String password, SignInActivity.SIGNING signing, boolean isCheckedRememberMe) {
        if (signinView != null) {
            signinView.showProgress();
            signinView.hideKeyboard();
            signInUpPresenterInteractor = new SignInUpPresenterInteractor(this);
        }
        signInUpPresenterInteractor.signIn(userName, password, signing,isCheckedRememberMe);
    }

    @Override
    public void success(Object obj) {
        signinView.success(obj);
    }

    @Override
    public void errorMsg(int statusCode, String errorMsg) {
        if (signinView != null) {
            signinView.hideProgress();
            signinView.showErrorMsg(statusCode, errorMsg);
        }
    }

    @Override
    public void onDestroy() {
        signinView = null;
    }

}
