package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.CreditcardListPresenterInter;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class CreditcardListPresenterImpl implements BasePresenter {
    BaseView baseView;
    CreditcardListPresenterInter creditcardListPresenterInter;
    public CreditcardListPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        creditcardListPresenterInter = new CreditcardListPresenterInter(this);
    }

    public void initCreditcardList() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        creditcardListPresenterInter.getCreditCardList();
    }

    @Override
    public void success(Object obj) {
        baseView.hideProgress();
        baseView.success(obj);
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.showErrorMsg(statusCode, msg);
        }
    }

    @Override
    public void onDestroy() {
        if (baseView != null) {
            baseView = null;
        }
    }


}
