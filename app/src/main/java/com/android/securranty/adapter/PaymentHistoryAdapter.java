package com.android.securranty.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.PaymentHistoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> implements Filterable {
    private List<PaymentHistoryModel.PaymentHistory> paymentList;
    private List<PaymentHistoryModel.PaymentHistory> filteredPaymentList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_payment_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textInvoiceId.setText(String.valueOf(filteredPaymentList.get(position).getInvoiceId()));
        holder.textDeviceType.setText(filteredPaymentList.get(position).getDeviceType());
        holder.textCoverageType.setText(filteredPaymentList.get(position).getCoverageType());
        holder.textInvoiceType.setText(filteredPaymentList.get(position).getInvoiceType());
        holder.textPaymentMethod.setText(filteredPaymentList.get(position).getPaymentMethod());
        holder.textCoverageTerm.setText(filteredPaymentList.get(position).getCoverageTerm());
        holder.textInvoiceDate.setText(filteredPaymentList.get(position).getPaymentDate());
        holder.textPaidAmt.setText(filteredPaymentList.get(position).getAmountPaid());
    }

    @Override
    public int getItemCount() {
        return filteredPaymentList.size();
    }

    public void setData(List<PaymentHistoryModel.PaymentHistory> paymentList) {
        this.paymentList = paymentList;
        filteredPaymentList = paymentList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textInvoiceId, textDeviceType, textCoverageType, textInvoiceType,
                textPaymentMethod, textCoverageTerm, textInvoiceDate, textPaidAmt;

        public ViewHolder(View itemView) {
            super(itemView);
            textInvoiceId = (TextView) itemView.findViewById(R.id.textInvoice);
            textDeviceType = (TextView) itemView.findViewById(R.id.textDeviceType);
            textCoverageType = (TextView) itemView.findViewById(R.id.textCoverageType);
            textInvoiceType = (TextView) itemView.findViewById(R.id.textInvoiceType);
            textPaymentMethod = (TextView) itemView.findViewById(R.id.textPaymentMethod);
            textCoverageTerm = (TextView) itemView.findViewById(R.id.textCoverageTerm);
            textInvoiceDate = (TextView) itemView.findViewById(R.id.textPaymentDate);
            textPaidAmt = (TextView) itemView.findViewById(R.id.textPaidAmt);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredPaymentList = paymentList;
                } else {
                    List<PaymentHistoryModel.PaymentHistory> filteredList = new ArrayList<>();
                    for (PaymentHistoryModel.PaymentHistory copyTicketList : paymentList) {
                        if (String.valueOf(copyTicketList.getInvoiceId()).toLowerCase().contains(charString.toLowerCase()) ||
                                copyTicketList.getInvoiceType().toLowerCase().contains(charString.toLowerCase()) ||
                                copyTicketList.getAmountPaid().toLowerCase().contains(charString.toLowerCase()) ||
                                copyTicketList.getPaymentDate().toLowerCase().contains(charString.toLowerCase())
                                ) {
                            filteredList.add(copyTicketList);
                        }
                    }
                    filteredPaymentList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredPaymentList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredPaymentList = (ArrayList<PaymentHistoryModel.PaymentHistory>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
