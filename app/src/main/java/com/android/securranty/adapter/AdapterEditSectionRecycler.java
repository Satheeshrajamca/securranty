package com.android.securranty.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.Child;
import com.android.securranty.model.SectionHeader;
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter;

import java.util.List;

/**
 * Created by satheeshraja on 4/1/18.
 */

public class AdapterEditSectionRecycler extends SectionRecyclerViewAdapter<SectionHeader, Child, AdapterEditSectionRecycler.SectionViewHolder,
        AdapterEditSectionRecycler.ChildViewHolder> {

    Context context;

    public AdapterEditSectionRecycler(Context context, List<SectionHeader> sectionItemList) {
        super(context, sectionItemList);
        this.context = context;
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_active_plan_header, sectionViewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_update_active_plan_child, childViewGroup, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, SectionHeader section) {
        if (sectionPosition == 0) {
            sectionViewHolder.textEditPlan.setVisibility(View.VISIBLE);
        } else {
            sectionViewHolder.textEditPlan.setVisibility(View.GONE);
        }
        sectionViewHolder.sectionName.setText(section.getSectionText());
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder childViewHolder, int sectionPosition, int childPosition, Child child) {
        childViewHolder.editPlanKey.setText(child.getKey());
        childViewHolder.editPlanValue.setText(child.getValue());
        if (child.isAutoComplete()) {
            childViewHolder.editPlanValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_down_arrow, 0);
        }
        else
        {
            childViewHolder.editPlanValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }


    public class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView sectionName, textEditPlan;

        public SectionViewHolder(View itemView) {
            super(itemView);
            sectionName = (TextView) itemView.findViewById(R.id.section);
            textEditPlan = (TextView) itemView.findViewById(R.id.textEditPlan);
            textEditPlan.setText("Update");
            Typeface openSansSemiBold = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_semibold.ttf");
            sectionName.setTypeface(openSansSemiBold);
            textEditPlan.setTypeface(openSansSemiBold);
        }
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {

        TextView editPlanKey;
        EditText editPlanValue;

        public ChildViewHolder(View itemView) {
            super(itemView);
            ///editPlanKey = (TextView) itemView.findViewById(R.id.editPlanKey);
            //editPlanValue = (EditText) itemView.findViewById(R.id.editPlanValue);
            Typeface openSansRegular = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_regular.ttf");
            editPlanKey.setTypeface(openSansRegular);
            editPlanValue.setTypeface(openSansRegular);
        }
    }
}
