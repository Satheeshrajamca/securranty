
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeviceTypesModel {

    @SerializedName("deviceTypeList")
    private List<com.android.securranty.model.DeviceTypeList> DeviceTypeList;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<com.android.securranty.model.DeviceTypeList> getDeviceTypeList() {
        return DeviceTypeList;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<com.android.securranty.model.DeviceTypeList> DeviceTypeList;
        private String Status;
        private Long StatusCode;

        public DeviceTypesModel.Builder withDeviceTypeList(List<com.android.securranty.model.DeviceTypeList> deviceTypeList) {
            DeviceTypeList = deviceTypeList;
            return this;
        }

        public DeviceTypesModel.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public DeviceTypesModel.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public DeviceTypesModel build() {
            DeviceTypesModel DeviceTypesModel = new DeviceTypesModel();
            DeviceTypesModel.DeviceTypeList = DeviceTypeList;
            DeviceTypesModel.Status = Status;
            DeviceTypesModel.StatusCode = StatusCode;
            return DeviceTypesModel;
        }

    }

}
