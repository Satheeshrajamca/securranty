package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.utility.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import android.text.TextUtils;

import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.presenter.implementor.ChangePassPresenterImp;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshraja on 11/30/17.
 */

public class ChangePassPresenterInter {
    ChangePassPresenterImp changePassPresenterImp;

    public ChangePassPresenterInter(ChangePassPresenterImp changePassPresenterImp) {
        this.changePassPresenterImp = changePassPresenterImp;
    }

    public void changePassword(String currentPassword, String newPassword, String repeatPassword) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (!TextUtils.isEmpty(currentPassword)) {
                if (!TextUtils.isEmpty(newPassword)) {
                    if (!TextUtils.isEmpty(repeatPassword)) {
                        if (newPassword.equals(repeatPassword)) {
                            apiCall(SharePref.getInstance(SecurrantyApp.context).getToken(), currentPassword, newPassword, repeatPassword);

                        } else {
                            changePassPresenterImp.errorMsg(400, "New password doesn't match the Repeat password");
                        }
                    } else {
                        changePassPresenterImp.errorMsg(400, "Repeat password doesn't empty");
                    }
                } else {
                    changePassPresenterImp.errorMsg(400, "New password doesn't empty");
                }
            } else {
                changePassPresenterImp.errorMsg(400, "Current password doesn't empty");
            }
        } else {
            changePassPresenterImp.errorMsg(401, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }

    }

    private void apiCall(String token, String currentPassword, String newPassword, String repeatPassword) {
        ApiClient.getClient().changePassword(token, currentPassword, newPassword, repeatPassword).enqueue(new Callback<APISuccessModel>() {
            @Override
            public void onResponse(Call<APISuccessModel> call, Response<APISuccessModel> response) {
                if (response.isSuccessful()) {
                    changePassPresenterImp.success(response);
                } else {
                    errorResponse(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<APISuccessModel> call, Throwable t) {
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    changePassPresenterImp.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            changePassPresenterImp.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            changePassPresenterImp.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
