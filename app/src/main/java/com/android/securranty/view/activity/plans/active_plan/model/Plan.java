
package com.android.securranty.view.activity.plans.active_plan.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class Plan {

    @SerializedName("Active")
    private Boolean active;
    @SerializedName("AutoRenew")
    private Boolean autoRenew;
    @SerializedName("CancellationDate")
    private Object cancellationDate;
    @SerializedName("CancelledBy")
    private Object cancelledBy;
    @SerializedName("Carrier")
    private String carrier;
    @SerializedName("CarrierId")
    private Long carrierId;
    @SerializedName("CostCenter")
    private Object costCenter;
    @SerializedName("CostCenterId")
    private Object costCenterId;
    @SerializedName("Country")
    private String country;
    @SerializedName("CountryId")
    private Long countryId;
    @SerializedName("CoverageLimit")
    private Double coverageLimit;
    @SerializedName("CoverageTerm")
    private String coverageTerm;
    @SerializedName("CoverageTermId")
    private Long coverageTermId;
    @SerializedName("CreatedBy")
    private String createdBy;
    @SerializedName("CreatedDate")
    private String createdDate;
    @SerializedName("Deductable")
    private List<Deductable> deductable;
    @SerializedName("DiscountRetail")
    private Object discountRetail;
    @SerializedName("Division")
    private Object division;
    @SerializedName("DivisionId")
    private Object divisionId;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("IMEI")
    private String iMEI;
    @SerializedName("Id")
    private Long id;
    @SerializedName("ImportantNotes")
    private String importantNotes;
    @SerializedName("IsOngoingClaim")
    private Long isOngoingClaim;
    @SerializedName("IsUpdatedMandatoryField")
    private Long isUpdatedMandatoryField;
    @SerializedName("ItemCondition")
    private String itemCondition;
    @SerializedName("ItemConditionId")
    private Long itemConditionId;
    @SerializedName("ItemPurchaseDate")
    private String itemPurchaseDate;
    @SerializedName("ItemPurchasePrice")
    private Double itemPurchasePrice;
    @SerializedName("ItemType")
    private String itemType;
    @SerializedName("ItemTypeId")
    private Long itemTypeId;
    @SerializedName("LastModBy")
    private String lastModBy;
    @SerializedName("LastModDate")
    private String lastModDate;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("Manufacturer")
    private String manufacturer;
    @SerializedName("ManufacturerId")
    private Long manufacturerId;
    @SerializedName("Model")
    private String model;
    @SerializedName("ModelId")
    private Long modelId;
    @SerializedName("PayForInsurance")
    private Object payForInsurance;
    @SerializedName("PhoneNumber")
    private Long phoneNumber;
    @SerializedName("PlanPurchasePrice")
    private Double planPurchasePrice;
    @SerializedName("PolicyExpirationDate")
    private String policyExpirationDate;
    @SerializedName("PolicyStartDate")
    private String policyStartDate;
    @SerializedName("PolicyStatus")
    private String policyStatus;
    @SerializedName("PolicyStatusId")
    private Long policyStatusId;
    @SerializedName("PolicyType")
    private String policyType;
    @SerializedName("Postage")
    private String postage;
    @SerializedName("ProofofPurchase")
    private List<ProofofPurchase> proofofPurchase;
    @SerializedName("ProofofPurchaseMessage")
    private String proofofPurchaseMessage;
    @SerializedName("ResellerId")
    private Object resellerId;
    @SerializedName("SKU")
    private Object sKU;
    @SerializedName("Serial")
    private String serial;
    @SerializedName("ServiceLevel")
    private String serviceLevel;
    @SerializedName("ServiceLevelId")
    private Long serviceLevelId;
    @SerializedName("SoldBy")
    private String soldBy;
    @SerializedName("TermsUrl")
    private String termsUrl;
    @SerializedName("TransferPolicyId")
    private Object transferPolicyId;
    @SerializedName("UserDeviceId")
    private Long userDeviceId;
    @SerializedName("UserId")
    private String userId;
    @SerializedName("UserPlanId")
    private Long userPlanId;
    @SerializedName("WarrantySummary")
    private String warrantySummary;
    @SerializedName("WarrantySummaryId")
    private Long warrantySummaryId;
    @SerializedName("IsCarrierVisible")
    private boolean isCarrierVisible;

    public boolean isCarrierVisible() {
        return isCarrierVisible;
    }

    public Boolean getActive() {
        return active;
    }

    public Boolean getAutoRenew() {
        return autoRenew;
    }

    public Object getCancellationDate() {
        return cancellationDate;
    }

    public Object getCancelledBy() {
        return cancelledBy;
    }

    public String getCarrier() {
        return carrier;
    }

    public Long getCarrierId() {
        return carrierId;
    }

    public Object getCostCenter() {
        return costCenter;
    }

    public Object getCostCenterId() {
        return costCenterId;
    }

    public String getCountry() {
        return country;
    }

    public Long getCountryId() {
        return countryId;
    }

    public Double getCoverageLimit() {
        return coverageLimit;
    }

    public String getCoverageTerm() {
        return coverageTerm;
    }

    public Long getCoverageTermId() {
        return coverageTermId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public List<Deductable> getDeductable() {
        return deductable;
    }

    public Object getDiscountRetail() {
        return discountRetail;
    }

    public Object getDivision() {
        return division;
    }

    public Object getDivisionId() {
        return divisionId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getIMEI() {
        return iMEI;
    }

    public Long getId() {
        return id;
    }

    public String getImportantNotes() {
        return importantNotes;
    }

    public Long getIsOngoingClaim() {
        return isOngoingClaim;
    }

    public Long getIsUpdatedMandatoryField() {
        return isUpdatedMandatoryField;
    }

    public String getItemCondition() {
        return itemCondition;
    }

    public Long getItemConditionId() {
        return itemConditionId;
    }

    public String getItemPurchaseDate() {
        return itemPurchaseDate;
    }

    public Double getItemPurchasePrice() {
        return itemPurchasePrice;
    }

    public String getItemType() {
        return itemType;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public String getLastModBy() {
        return lastModBy;
    }

    public String getLastModDate() {
        return lastModDate;
    }

    public String getLastName() {
        return lastName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public String getModel() {
        return model;
    }

    public Long getModelId() {
        return modelId;
    }

    public Object getPayForInsurance() {
        return payForInsurance;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public Double getPlanPurchasePrice() {
        return planPurchasePrice;
    }

    public String getPolicyExpirationDate() {
        return policyExpirationDate;
    }

    public String getPolicyStartDate() {
        return policyStartDate;
    }

    public String getPolicyStatus() {
        return policyStatus;
    }

    public Long getPolicyStatusId() {
        return policyStatusId;
    }

    public String getPolicyType() {
        return policyType;
    }

    public String getPostage() {
        return postage;
    }

    public List<ProofofPurchase> getProofofPurchase() {
        return proofofPurchase;
    }

    public String getProofofPurchaseMessage() {
        return proofofPurchaseMessage;
    }

    public Object getResellerId() {
        return resellerId;
    }

    public Object getSKU() {
        return sKU;
    }

    public String getSerial() {
        return serial;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public Long getServiceLevelId() {
        return serviceLevelId;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public String getTermsUrl() {
        return termsUrl;
    }

    public Object getTransferPolicyId() {
        return transferPolicyId;
    }

    public Long getUserDeviceId() {
        return userDeviceId;
    }

    public String getUserId() {
        return userId;
    }

    public Long getUserPlanId() {
        return userPlanId;
    }

    public String getWarrantySummary() {
        return warrantySummary;
    }

    public Long getWarrantySummaryId() {
        return warrantySummaryId;
    }

    public static class Builder {

        private Boolean active;
        private Boolean autoRenew;
        private Object cancellationDate;
        private Object cancelledBy;
        private String carrier;
        private Long carrierId;
        private Object costCenter;
        private Object costCenterId;
        private String country;
        private Long countryId;
        private Double coverageLimit;
        private String coverageTerm;
        private Long coverageTermId;
        private String createdBy;
        private String createdDate;
        private List<Deductable> deductable;
        private Object discountRetail;
        private Object division;
        private Object divisionId;
        private String firstName;
        private String iMEI;
        private Long id;
        private String importantNotes;
        private Long isOngoingClaim;
        private Long isUpdatedMandatoryField;
        private String itemCondition;
        private Long itemConditionId;
        private String itemPurchaseDate;
        private Double itemPurchasePrice;
        private String itemType;
        private Long itemTypeId;
        private String lastModBy;
        private String lastModDate;
        private String lastName;
        private String manufacturer;
        private Long manufacturerId;
        private String model;
        private Long modelId;
        private Object payForInsurance;
        private Long phoneNumber;
        private Double planPurchasePrice;
        private String policyExpirationDate;
        private String policyStartDate;
        private String policyStatus;
        private Long policyStatusId;
        private String policyType;
        private String postage;
        private List<ProofofPurchase> proofofPurchase;
        private String proofofPurchaseMessage;
        private Object resellerId;
        private Object sKU;
        private String serial;
        private String serviceLevel;
        private Long serviceLevelId;
        private String soldBy;
        private String termsUrl;
        private Object transferPolicyId;
        private Long userDeviceId;
        private String userId;
        private Long userPlanId;
        private String warrantySummary;
        private Long warrantySummaryId;

        public Plan.Builder withActive(Boolean active) {
            this.active = active;
            return this;
        }

        public Plan.Builder withAutoRenew(Boolean autoRenew) {
            this.autoRenew = autoRenew;
            return this;
        }

        public Plan.Builder withCancellationDate(Object cancellationDate) {
            this.cancellationDate = cancellationDate;
            return this;
        }

        public Plan.Builder withCancelledBy(Object cancelledBy) {
            this.cancelledBy = cancelledBy;
            return this;
        }

        public Plan.Builder withCarrier(String carrier) {
            this.carrier = carrier;
            return this;
        }

        public Plan.Builder withCarrierId(Long carrierId) {
            this.carrierId = carrierId;
            return this;
        }

        public Plan.Builder withCostCenter(Object costCenter) {
            this.costCenter = costCenter;
            return this;
        }

        public Plan.Builder withCostCenterId(Object costCenterId) {
            this.costCenterId = costCenterId;
            return this;
        }

        public Plan.Builder withCountry(String country) {
            this.country = country;
            return this;
        }

        public Plan.Builder withCountryId(Long countryId) {
            this.countryId = countryId;
            return this;
        }

        public Plan.Builder withCoverageLimit(Double coverageLimit) {
            this.coverageLimit = coverageLimit;
            return this;
        }

        public Plan.Builder withCoverageTerm(String coverageTerm) {
            this.coverageTerm = coverageTerm;
            return this;
        }

        public Plan.Builder withCoverageTermId(Long coverageTermId) {
            this.coverageTermId = coverageTermId;
            return this;
        }

        public Plan.Builder withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Plan.Builder withCreatedDate(String createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Plan.Builder withDeductable(List<Deductable> deductable) {
            this.deductable = deductable;
            return this;
        }

        public Plan.Builder withDiscountRetail(Object discountRetail) {
            this.discountRetail = discountRetail;
            return this;
        }

        public Plan.Builder withDivision(Object division) {
            this.division = division;
            return this;
        }

        public Plan.Builder withDivisionId(Object divisionId) {
            this.divisionId = divisionId;
            return this;
        }

        public Plan.Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Plan.Builder withIMEI(String iMEI) {
            this.iMEI = iMEI;
            return this;
        }

        public Plan.Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Plan.Builder withImportantNotes(String importantNotes) {
            this.importantNotes = importantNotes;
            return this;
        }

        public Plan.Builder withIsOngoingClaim(Long isOngoingClaim) {
            this.isOngoingClaim = isOngoingClaim;
            return this;
        }

        public Plan.Builder withIsUpdatedMandatoryField(Long isUpdatedMandatoryField) {
            this.isUpdatedMandatoryField = isUpdatedMandatoryField;
            return this;
        }

        public Plan.Builder withItemCondition(String itemCondition) {
            this.itemCondition = itemCondition;
            return this;
        }

        public Plan.Builder withItemConditionId(Long itemConditionId) {
            this.itemConditionId = itemConditionId;
            return this;
        }

        public Plan.Builder withItemPurchaseDate(String itemPurchaseDate) {
            this.itemPurchaseDate = itemPurchaseDate;
            return this;
        }

        public Plan.Builder withItemPurchasePrice(Double itemPurchasePrice) {
            this.itemPurchasePrice = itemPurchasePrice;
            return this;
        }

        public Plan.Builder withItemType(String itemType) {
            this.itemType = itemType;
            return this;
        }

        public Plan.Builder withItemTypeId(Long itemTypeId) {
            this.itemTypeId = itemTypeId;
            return this;
        }

        public Plan.Builder withLastModBy(String lastModBy) {
            this.lastModBy = lastModBy;
            return this;
        }

        public Plan.Builder withLastModDate(String lastModDate) {
            this.lastModDate = lastModDate;
            return this;
        }

        public Plan.Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Plan.Builder withManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public Plan.Builder withManufacturerId(Long manufacturerId) {
            this.manufacturerId = manufacturerId;
            return this;
        }

        public Plan.Builder withModel(String model) {
            this.model = model;
            return this;
        }

        public Plan.Builder withModelId(Long modelId) {
            this.modelId = modelId;
            return this;
        }

        public Plan.Builder withPayForInsurance(Object payForInsurance) {
            this.payForInsurance = payForInsurance;
            return this;
        }

        public Plan.Builder withPhoneNumber(Long phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Plan.Builder withPlanPurchasePrice(Double planPurchasePrice) {
            this.planPurchasePrice = planPurchasePrice;
            return this;
        }

        public Plan.Builder withPolicyExpirationDate(String policyExpirationDate) {
            this.policyExpirationDate = policyExpirationDate;
            return this;
        }

        public Plan.Builder withPolicyStartDate(String policyStartDate) {
            this.policyStartDate = policyStartDate;
            return this;
        }

        public Plan.Builder withPolicyStatus(String policyStatus) {
            this.policyStatus = policyStatus;
            return this;
        }

        public Plan.Builder withPolicyStatusId(Long policyStatusId) {
            this.policyStatusId = policyStatusId;
            return this;
        }

        public Plan.Builder withPolicyType(String policyType) {
            this.policyType = policyType;
            return this;
        }

        public Plan.Builder withPostage(String postage) {
            this.postage = postage;
            return this;
        }

        public Plan.Builder withProofofPurchase(List<ProofofPurchase> proofofPurchase) {
            this.proofofPurchase = proofofPurchase;
            return this;
        }

        public Plan.Builder withProofofPurchaseMessage(String proofofPurchaseMessage) {
            this.proofofPurchaseMessage = proofofPurchaseMessage;
            return this;
        }

        public Plan.Builder withResellerId(Object resellerId) {
            this.resellerId = resellerId;
            return this;
        }

        public Plan.Builder withSKU(Object sKU) {
            this.sKU = sKU;
            return this;
        }

        public Plan.Builder withSerial(String serial) {
            this.serial = serial;
            return this;
        }

        public Plan.Builder withServiceLevel(String serviceLevel) {
            this.serviceLevel = serviceLevel;
            return this;
        }

        public Plan.Builder withServiceLevelId(Long serviceLevelId) {
            this.serviceLevelId = serviceLevelId;
            return this;
        }

        public Plan.Builder withSoldBy(String soldBy) {
            this.soldBy = soldBy;
            return this;
        }

        public Plan.Builder withTermsUrl(String termsUrl) {
            this.termsUrl = termsUrl;
            return this;
        }

        public Plan.Builder withTransferPolicyId(Object transferPolicyId) {
            this.transferPolicyId = transferPolicyId;
            return this;
        }

        public Plan.Builder withUserDeviceId(Long userDeviceId) {
            this.userDeviceId = userDeviceId;
            return this;
        }

        public Plan.Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Plan.Builder withUserPlanId(Long userPlanId) {
            this.userPlanId = userPlanId;
            return this;
        }

        public Plan.Builder withWarrantySummary(String warrantySummary) {
            this.warrantySummary = warrantySummary;
            return this;
        }

        public Plan.Builder withWarrantySummaryId(Long warrantySummaryId) {
            this.warrantySummaryId = warrantySummaryId;
            return this;
        }

        public Plan build() {
            Plan plan = new Plan();
            plan.active = active;
            plan.autoRenew = autoRenew;
            plan.cancellationDate = cancellationDate;
            plan.cancelledBy = cancelledBy;
            plan.carrier = carrier;
            plan.carrierId = carrierId;
            plan.costCenter = costCenter;
            plan.costCenterId = costCenterId;
            plan.country = country;
            plan.countryId = countryId;
            plan.coverageLimit = coverageLimit;
            plan.coverageTerm = coverageTerm;
            plan.coverageTermId = coverageTermId;
            plan.createdBy = createdBy;
            plan.createdDate = createdDate;
            plan.deductable = deductable;
            plan.discountRetail = discountRetail;
            plan.division = division;
            plan.divisionId = divisionId;
            plan.firstName = firstName;
            plan.iMEI = iMEI;
            plan.id = id;
            plan.importantNotes = importantNotes;
            plan.isOngoingClaim = isOngoingClaim;
            plan.isUpdatedMandatoryField = isUpdatedMandatoryField;
            plan.itemCondition = itemCondition;
            plan.itemConditionId = itemConditionId;
            plan.itemPurchaseDate = itemPurchaseDate;
            plan.itemPurchasePrice = itemPurchasePrice;
            plan.itemType = itemType;
            plan.itemTypeId = itemTypeId;
            plan.lastModBy = lastModBy;
            plan.lastModDate = lastModDate;
            plan.lastName = lastName;
            plan.manufacturer = manufacturer;
            plan.manufacturerId = manufacturerId;
            plan.model = model;
            plan.modelId = modelId;
            plan.payForInsurance = payForInsurance;
            plan.phoneNumber = phoneNumber;
            plan.planPurchasePrice = planPurchasePrice;
            plan.policyExpirationDate = policyExpirationDate;
            plan.policyStartDate = policyStartDate;
            plan.policyStatus = policyStatus;
            plan.policyStatusId = policyStatusId;
            plan.policyType = policyType;
            plan.postage = postage;
            plan.proofofPurchase = proofofPurchase;
            plan.proofofPurchaseMessage = proofofPurchaseMessage;
            plan.resellerId = resellerId;
            plan.sKU = sKU;
            plan.serial = serial;
            plan.serviceLevel = serviceLevel;
            plan.serviceLevelId = serviceLevelId;
            plan.soldBy = soldBy;
            plan.termsUrl = termsUrl;
            plan.transferPolicyId = transferPolicyId;
            plan.userDeviceId = userDeviceId;
            plan.userId = userId;
            plan.userPlanId = userPlanId;
            plan.warrantySummary = warrantySummary;
            plan.warrantySummaryId = warrantySummaryId;
            return plan;
        }

    }

}
