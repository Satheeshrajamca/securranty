
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class MesClaimServPreferencesage {

    @Expose
    private int Id;
    @Expose
    private String Name;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public static class Builder {

        private int Id;
        private String Name;

        public MesClaimServPreferencesage.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public MesClaimServPreferencesage.Builder withName(String Name) {
            this.Name = Name;
            return this;
        }

        public MesClaimServPreferencesage build() {
            MesClaimServPreferencesage MesClaimServPreferencesage = new MesClaimServPreferencesage();
            MesClaimServPreferencesage.Id = Id;
            MesClaimServPreferencesage.Name = Name;
            return MesClaimServPreferencesage;
        }

    }

}
