
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class ConfirmationNote {

    @Expose
    private String BindFieldName;
    @Expose
    private String Value;

    public String getBindFieldName() {
        return BindFieldName;
    }

    public String getValue() {
        return Value;
    }

    public static class Builder {

        private String BindFieldName;
        private String Value;

        public ConfirmationNote.Builder withBindFieldName(String BindFieldName) {
            this.BindFieldName = BindFieldName;
            return this;
        }

        public ConfirmationNote.Builder withValue(String Value) {
            this.Value = Value;
            return this;
        }

        public ConfirmationNote build() {
            ConfirmationNote ConfirmationNote = new ConfirmationNote();
            ConfirmationNote.BindFieldName = BindFieldName;
            ConfirmationNote.Value = Value;
            return ConfirmationNote;
        }

    }

}
