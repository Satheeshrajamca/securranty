package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class CreateTicketModel {

    @SerializedName("message")
    private String message;

    @SerializedName("ticketId")
    private int ticketId;

    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("status")
    private String status;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
