package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountryList {

    @SerializedName("countries")
    private List<CountriesItem> countries;

    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("status")
    private String status;

    public List<CountriesItem> getCountries() {
        return countries;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatus() {
        return status;
    }

    public class CountriesItem {

        @SerializedName("Id")
        private int id;

        @SerializedName("Name")
        private String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
