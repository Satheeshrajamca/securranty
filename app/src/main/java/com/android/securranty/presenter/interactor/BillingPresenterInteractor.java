package com.android.securranty.presenter.interactor;

import android.util.Log;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.BillingModel;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import com.android.securranty.utility.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshr on 21-11-2017.
 */

public class BillingPresenterInteractor implements Callback<BillingModel> {
    private BasePresenter basePresenter;

    public BillingPresenterInteractor(BasePresenter myAccountPresenter) {
        this.basePresenter = myAccountPresenter;
    }

    public void getBillingAccount() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getBillingInfo(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(this);
        } else {
            basePresenter.errorMsg(401, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void updateBillingProfile(String billinngContactName, String billingContactPhone, String billingContactEmail, String billingEmail,
                                     String billingOrderNo, String billingAddress1, String billingAddress2, String billingCity, String billingState,
                                     String billingZip, String billingCountry, String billingMethod) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (Utils.isEmailValid(billingEmail)) {
                ApiClient.getClient().updateBillingDetails(SharePref.getInstance(SecurrantyApp.context).getToken(),
                        SharePref.getInstance(SecurrantyApp.context).getUserId(), billinngContactName, billingContactPhone, billingContactEmail, billingEmail, billingOrderNo, billingAddress1,
                        billingAddress2, billingCity, billingState, billingZip, billingCountry, billingMethod).enqueue(new Callback<APISuccessModel>() {
                    @Override
                    public void onResponse(Call<APISuccessModel> call, Response<APISuccessModel> response) {
                        if (response.isSuccessful()) {
                            APISuccessModel apiSuccessModel = response.body();
                            if (apiSuccessModel.getStatusCode() == 200) {
                                basePresenter.success(apiSuccessModel);
                            } else {
                                basePresenter.errorMsg(apiSuccessModel.getStatusCode(), "Invalid User");
                            }

                        } else {
                            errorResponse(response.errorBody());
                        }
                    }

                    @Override
                    public void onFailure(Call<APISuccessModel> call, Throwable t) {
                        if (t instanceof HttpException)
                            errorResponse(((HttpException) t).response().errorBody());
                        else
                            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                    }
                });
            } else {
                basePresenter.errorMsg(20, SecurrantyApp.context.getResources().getString(R.string.err_invalid_email));
            }
        } else {
            basePresenter.errorMsg(401, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    @Override
    public void onResponse(Call<BillingModel> call, Response<BillingModel> response) {
        if (response.isSuccessful()) {
            BillingModel myaccountResponse = response.body();
            if (myaccountResponse.getStatusCode() == 200) {
                basePresenter.success(myaccountResponse);
            } else {
                basePresenter.errorMsg(myaccountResponse.getStatusCode(), "Invalid User");
            }

        } else {
            errorResponse(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<BillingModel> call, Throwable t) {
        if (t instanceof HttpException)
            errorResponse(((HttpException) t).response().errorBody());
        else
            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            basePresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }


}
