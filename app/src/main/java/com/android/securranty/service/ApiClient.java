package com.android.securranty.service;

import com.google.gson.JsonObject;

import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.AdvertiseImages;
import com.android.securranty.model.BillingModel;
import com.android.securranty.model.BuyPlan;
import com.android.securranty.model.Carrier;
import com.android.securranty.model.ClaimsModel;
import com.android.securranty.model.ConditionModel;
import com.android.securranty.model.CostCenterModel;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.CreateTicketModel;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.model.DeviceModel;
import com.android.securranty.model.DeviceTypesModel;
import com.android.securranty.model.DivisionModel;
import com.android.securranty.model.FAQModel;
import com.android.securranty.model.GetCoverage;
import com.android.securranty.model.GetCoverageType;
import com.android.securranty.model.ItemCond;
import com.android.securranty.model.LoginResponse;
import com.android.securranty.model.Manufacturer;
import com.android.securranty.model.MyAccount;
import com.android.securranty.model.MyPlans;
import com.android.securranty.model.PaymentHistoryModel;
import com.android.securranty.model.PlanList;
import com.android.securranty.model.PolicyModel;
import com.android.securranty.model.StatesList;
import com.android.securranty.model.TicketModel;
import com.android.securranty.view.activity.account.AccountProfileModel;
import com.android.securranty.view.activity.plans.active_plan.model.ActivePlan;
import com.android.securranty.view.activity.plans.active_plan.model.UploadImageModel;
import com.android.securranty.view.activity.tickets.model.PostMessageSuccess;
import com.android.securranty.view.activity.tickets.model.TicketList;
import com.android.securranty.view.fragment.fileclaim.model.ClaimPreference;
import com.android.securranty.view.fragment.fileclaim.model.ClaimType;
import com.android.securranty.view.fragment.fileclaim.model.PaymentandReview;
import com.android.securranty.view.fragment.fileclaim.model.ShippingAddress;
import com.android.securranty.view.fragment.fileclaim.model.SuccessClaimModel;

import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by satheeshraja on 11/17/17.
 */

public class ApiClient {
    public static final String BASE_URL = "https://stage.securranty.com/api/";
    private static Retrofit retrofit = null;

    public static SecurrantyAPI getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS)
                    .addInterceptor(interceptor).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit.create(SecurrantyAPI.class);
    }

    public interface SecurrantyAPI {
        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("SignIn")
        Call<LoginResponse> getSigninResponse(@Field("email") String email, @Field("password") String password);

        @GET("TicketElements?")
        Call<TicketModel> getTicketDetails(@Header("Token") String token);

        @Multipart
        @POST("AccountProfile/UploadImage")
        Call<AccountProfileModel> updateProfileImage(@Query("UserId") String userId, @Header("token") String token, @Part MultipartBody.Part image,
                                                     @Part("photo_path") RequestBody name);

        @Multipart
        @POST("AccountProfile/UploadImage")
        Observable<AccountProfileModel> updateProfile(@Query("UserId") String userId, @Header("token") String token, @Part MultipartBody.Part image,
                                                      @Part("photo_path") RequestBody name);

        /*@Multipart
        @PUT("AccountProfile/UploadImage")
        Call<String> uploadImage(@Part("UserId") String userId, @Header("token") String token, @Part MultipartBody.Part image);*/

        @GET("AccountProfile?")
        Observable<MyAccount> getAccountInfo(@Header("Token") String token, @Query("UserId") String userId);

        @GET("Country?")
        Observable<CountryList> getCountryList(@Header("Token") String token);

        @FormUrlEncoded
        @Headers({"Accept: application/json"})
        @PUT("AccountProfile")
        Observable<APISuccessModel> updateAccountInfo(@Header("token") String token, @Field("Id") String userId, @Field("FirstName") String firstName,
                                                      @Field("LastName") String lastName, @Field("Email") String email, @Field("MobilePhone") String mobile,
                                                      @Field("Address1") String address1, @Field("State") String state, @Field("Country") String country,
                                                      @Field("Zip") String zip, @Field("SalesTax") String salesTax, @Field("SalesTaxCountry") String salesTaxCountry,
                                                      @Field("SalesTaxPercent") String salesTaxPercent, @Field("CompanyName") String companyName,
                                                      @Field("ContactEmail") String contactEmail, @Field("OfficePhone") String officePhone, @Field("Address2") String address2,
                                                      @Field("City") String city, @Field("ContactName") String contactName, @Field("ProfileImage") String profileImage);

        @GET("ConsumerPlan?")
        Call<MyPlans> getPlanDetails(@Query("PageId") String PageId);

        @GET("PurchasePlan/GetCoverage?")
        Call<GetCoverage> getCoverage(@Header("Token") String token, @Query("pageId") String PageId);

        @GET("PurchasePlan/GetItemCondition?")
        Call<ItemCond> getItemCondtion(@Header("Token") String token, @Query("pageId") String pageId, @Query("coverageType") String coverageType);

        @GET("PurchasePlan/GetCoverageType?")
        Call<GetCoverageType> getCoverageType(@Header("Token") String token, @Query("pageId") String pageId, @Query("coverage") String coverageType,
                                              @Query("condition") int condition);

        @GET("PurchasePlan/GetPlanList?")
        Call<PlanList> getPlanList(@Header("Token") String token, @Query("pageId") String pageId, @Query("coverage") String coverage,
                                       @Query("condition") int condition, @Query("coverageType") int coverageType);

        @GET("State?")
        Call<StatesList> getStates(@Header("Token") String token, @Query("CountryId") int userId);

        @GET("BillingProfile?")
        Call<BillingModel> getBillingInfo(@Header("Token") String token, @Query("UserId") String userId);

        @GET("AdvertisementImages?")
        Call<AdvertiseImages> getAdvertiseImages();

        @FormUrlEncoded
        @Headers({"Accept: application/json"})
        @PUT("BillingProfile")
        Call<APISuccessModel> updateBillingDetails(@Header("token") String token, @Field("Id") String userId, @Field("BillingContact") String billinngContactName,
                                                   @Field("BillingContactPhone") String billingContactPhone, @Field("BillingContactEmails") String billingContactEmail, @Field("BillingEmail") String billingEmail,
                                                   @Field("BillingPO") String billingOrderNo, @Field("BillingAddress1") String billingAddress1, @Field("BillingAddress2") String billingAddress2,
                                                   @Field("BillingCity") String billingCity, @Field("BillingState") String billingState, @Field("Billingzip") String billingZip,
                                                   @Field("BillingCountry") String billingCountry, @Field("BillingMethod") String billingMethod);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("SignUp")
        Call<LoginResponse> getSignupResponse(@Field("emailId") String email, @Field("password") String password);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("ForgotPassword")
        Call<LoginResponse> getForgotPassResponse(@Field("Email") String email);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("Ticket")
        Call<CreateTicketModel> createTicket(@Header("token") String token, @Field("UserId") String userId, @Field("Title") String subject, @Field("TypeId") String typeId,
                                             @Field("Priority") String priority, @Field("Message") String message);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("ChangePassword")
        Call<APISuccessModel> changePassword(@Header("token") String token, @Field("CurrentPassword") String currentPassword, @Field("NewPassword") String newPassword
                , @Field("ConfirmNewPassword") String confirmPassword);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("CreditCard")
        Call<APISuccessModel> updateCreditCard(@Header("token") String token, @Field("UserId") String userid, @Field("CardNumber") String creditCardNo, @Field("ExMonth") String expireMonth,
                                               @Field("ExYear") String expireYear, @Field("CCV") String cvv, @Field("FirstName") String firstName, @Field("LastName") String lastName, @Field("Email") String billingEmail,
                                               @Field("Phone") String phone, @Field("Address1") String billingAdd1, @Field("Address2") String billingAdd2,
                                               @Field("City") String city, @Field("State") String state, @Field("Country") String country, @Field("Zip") String zip,
                                               @Field("Default") boolean defaults, @Field("Active") boolean active);

        @GET("CreditCard?")
        Call<CreditCardList> getCreditcardList(@Header("Token") String token, @Query("UserId") String userId);

        @GET("Ticket?")
        Call<TicketList> getTicketList(@Header("Token") String token, @Query("UserId") String userId);

        @GET("Plan?")
        Call<ActivePlan> getActivePlans(@Header("Token") String token, @Query("UserId") String userId);

        @GET("Manufacturer?")
        Call<Manufacturer> getManufacturer(@Header("Token") String token, @Query("DeviceTypeId") Long itemTypeId);

        @GET("DeviceModel?")
        Call<PolicyModel> getModel(@Header("Token") String token, @Query("DeviceTypeId") Long itemTypeId, @Query("ManufacturerId") Long manufacturerId);

        @GET("Carrier?")
        Call<Carrier> getCarrier(@Header("Token") String token);

        @GET("DeviceType?")
        Call<DeviceTypesModel> getDeviceModel(@Header("Token") String token, @Query("UserId") String userId);

        @GET("DeviceCondition?")
        Call<ConditionModel> getConditionModel(@Header("Token") String token, @Query("UserId") String userId);

        @GET("Division?")
        Call<DivisionModel> getDivisionModel(@Header("Token") String token, @Query("UserId") String userId);

        @GET("CostCenter?")
        Call<CostCenterModel> getCostCenterModel(@Header("Token") String token, @Query("UserId") String userId);

        @GET("PaymentHistory?")
        Call<PaymentHistoryModel> getPaymentList(@Header("Token") String token, @Query("UserId") String userId);

        @GET("Country?")
        Call<CountryList> getCountryLists(@Header("Token") String token);

        @GET("Faq?")
        Call<FAQModel> getFAQDetails(@Header("Token") String token, @Query("MainCategory") String categoryId);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("Plan")
        Call<BuyPlan> buyPlan(@Field("Email") String email, @Field("Address1") String address1, @Field("Address2") String address2,
                              @Field("Country") String country, @Field("State") String state,
                              @Field("City") String city, @Field("Zip") String zip, @Field("CardNumber") String creditcardno,
                              @Field("ExMonth") String expMonth, @Field("ExYear") String expYear, @Field("CCV") String cvv, @Field("PlanId") String planId,
                              @Field("PaymentProfileID") String paymentProfileId);


        @Headers({"Accept: application/json"})
        @PUT("Plan")
        Call<APISuccessModel> updatePlan(@Header("Token") String token, @Body JsonObject jsonObject);

        @FormUrlEncoded
        @Headers({"Accept: application/json"})
        @PUT("Plan")
        Call<APISuccessModel> updatePlan(@Header("Token") String token, @Field("CarrierId") String carrierId, @Field("CostCenterId") String costcenterId,
                                         @Field("CountryId") String countryId,
                                         @Field("DivisionId") String divisionId, @Field("FirstName") String firstName, @Field("IMEI") String imei,
                                         @Field("ItemConditionId") String itemConditionId,
                                         @Field("ItemPurchaseDate") String itemPurchaseDate, @Field("ItemPurchasePrice") String itemPurchasePrice,
                                         @Field("LastName") String lastName,
                                         @Field("ManufactureId") String manufacturerId, @Field("ModelId") String modelId, @Field("PhoneNumber") String phoneNumber,
                                         @Field("PolicyId") Long policyId, @Field("Serial") String serial, @Field("File") String fileId);

        @GET("Claim?")
        Call<ClaimsModel> getClaims(@Header("Token") String token, @Query("UserId") String userId);

        @GET("FileClaim/GetDeviceInfo/{PolicyId}?")
        Call<DeviceModel> getDeviceInfo(@Header("Token") String token, @Path("PolicyId") String policyId);

        @GET("FileClaim/GetClaimType/{PolicyId}/0")
        Call<ClaimType> getClaimType(@Header("Token") String token, @Path("PolicyId") String policyId);

        @GET("FileClaim/GetClaimType/{PolicyId}/{ClaimId}")
        Call<ClaimType> getSubClaimType(@Header("Token") String token, @Path("PolicyId") String policyId, @Path("ClaimId") String claimId);

        @GET("FileClaim/GetClaimPreference/3/{ClaimId}")
        Call<ClaimPreference> getClaimPreference(@Header("Token") String token, @Path("ClaimId") int claimId);

        @GET("FileClaim/GetShippingAddress/{UserId}")
        Call<ShippingAddress> getShippingAddress(@Header("Token") String token, @Path("UserId") String userId);

        @GET("FileClaim/GetOtherInfo/{PolicyId}?")
        Observable<PaymentandReview> getPaymentAndReview(@Header("Token") String token, @Path("PolicyId") String policyId);

        @GET("CreditCard?")
        Observable<CreditCardList> getCreditcardInfo(@Header("Token") String token, @Query("UserId") String userId);

        @Headers({"Accept: application/json"})
        @POST("FileClaim/SubmitClaim")
        Call<SuccessClaimModel> submitClaim(@Header("token") String token, @Body JsonObject jsonBody);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("FileClaim/SaveShippingAddress")
        Call<APISuccessModel> addShippingAddress(@Header("token") String token,
                                                 @Field("Id") String id,
                                                 @Field("ShiptToName") String shippingName,
                                                 @Field("CompanyName") String companyName,
                                                 @Field("Email") String email,
                                                 @Field("PhoneNumber") String phone,
                                                 @Field("Address1") String address1,
                                                 @Field("Address2") String address2,
                                                 @Field("Country") String country,
                                                 @Field("State") String state,
                                                 @Field("City") String city,
                                                 @Field("Zip") String zip);

        @Multipart
        @POST("FileUpload")
        Call<UploadImageModel> updatePlanProfileImage(@Header("token") String token, @Part MultipartBody.Part image,
                                                      @Part("photo_path") RequestBody name);


        @FormUrlEncoded
        @Headers({"Accept: application/json"})
        @PUT("Ticket")
        Call<PostMessageSuccess> postMessage(@Header("token") String token, @Field("TicketId") int ticketId, @Field("Message") String message);

        @DELETE("Claim/{Id}")
        Call<APISuccessModel> cancelClaim(@Header("token") String token, @Path("Id") int claimId);
    }
}
