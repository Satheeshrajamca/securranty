
package com.android.securranty.view.activity.tickets.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class TicketList {

    @Expose
    private String status;
    @Expose
    private Long statusCode;
    @Expose
    private List<Ticket> tickets;

    public String getStatus() {
        return status;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public static class Builder {

        private String status;
        private Long statusCode;
        private List<Ticket> tickets;

        public TicketList.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public TicketList.Builder withStatusCode(Long statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public TicketList.Builder withTickets(List<Ticket> tickets) {
            this.tickets = tickets;
            return this;
        }

        public TicketList build() {
            TicketList ticketList = new TicketList();
            ticketList.status = status;
            ticketList.statusCode = statusCode;
            ticketList.tickets = tickets;
            return ticketList;
        }

    }

}
