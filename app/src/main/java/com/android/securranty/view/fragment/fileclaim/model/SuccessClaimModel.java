
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class SuccessClaimModel {

    @Expose
    private String claimId;
    @Expose
    private String message;
    @Expose
    private String status;
    @Expose
    private Long statusCode;

    public String getClaimId() {
        return claimId;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private String claimId;
        private String message;
        private String status;
        private Long statusCode;

        public SuccessClaimModel.Builder withClaimId(String claimId) {
            this.claimId = claimId;
            return this;
        }

        public SuccessClaimModel.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public SuccessClaimModel.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public SuccessClaimModel.Builder withStatusCode(Long statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public SuccessClaimModel build() {
            SuccessClaimModel successClaimModel = new SuccessClaimModel();
            successClaimModel.claimId = claimId;
            successClaimModel.message = message;
            successClaimModel.status = status;
            successClaimModel.statusCode = statusCode;
            return successClaimModel;
        }

    }

}
