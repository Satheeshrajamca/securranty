
package com.android.securranty.view.activity.plans.active_plan.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Deductable {

    @SerializedName("Name")
    private String name;
    @SerializedName("Type")
    private Long type;
    @SerializedName("Value")
    private Double value;

    public String getName() {
        return name;
    }

    public Long getType() {
        return type;
    }

    public Double getValue() {
        return value;
    }

    public static class Builder {

        private String name;
        private Long type;
        private Double value;

        public Deductable.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Deductable.Builder withType(Long type) {
            this.type = type;
            return this;
        }

        public Deductable.Builder withValue(Double value) {
            this.value = value;
            return this;
        }

        public Deductable build() {
            Deductable deductable = new Deductable();
            deductable.name = name;
            deductable.type = type;
            deductable.value = value;
            return deductable;
        }

    }

}
