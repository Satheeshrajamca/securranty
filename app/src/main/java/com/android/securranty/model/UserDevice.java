
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

public class UserDevice {

    @Expose
    private String Carrier;
    @Expose
    private Long DeviceId;
    @Expose
    private String Division;
    @Expose
    private String FirstName;
    @Expose
    private String IMEI;
    @Expose
    private Boolean IsVisibleDivision;
    @Expose
    private Boolean IsVisibleIMEI;
    @Expose
    private Boolean IsVisiblePhoneNumber;
    @Expose
    private String LastName;
    @Expose
    private String Model;
    @Expose
    private String PhoneNumber;
    @Expose
    private String SerialNumber;

    public String getCarrier() {
        return Carrier;
    }

    public Long getDeviceId() {
        return DeviceId;
    }

    public String getDivision() {
        return Division;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getIMEI() {
        return IMEI;
    }

    public Boolean getIsVisibleDivision() {
        return IsVisibleDivision;
    }

    public Boolean getIsVisibleIMEI() {
        return IsVisibleIMEI;
    }

    public Boolean getIsVisiblePhoneNumber() {
        return IsVisiblePhoneNumber;
    }

    public String getLastName() {
        return LastName;
    }

    public String getModel() {
        return Model;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public static class Builder {

        private String Carrier;
        private Long DeviceId;
        private String Division;
        private String FirstName;
        private String IMEI;
        private Boolean IsVisibleDivision;
        private Boolean IsVisibleIMEI;
        private Boolean IsVisiblePhoneNumber;
        private String LastName;
        private String Model;
        private String PhoneNumber;
        private String SerialNumber;

        public UserDevice.Builder withCarrier(String Carrier) {
            this.Carrier = Carrier;
            return this;
        }

        public UserDevice.Builder withDeviceId(Long DeviceId) {
            this.DeviceId = DeviceId;
            return this;
        }

        public UserDevice.Builder withDivision(String Division) {
            this.Division = Division;
            return this;
        }

        public UserDevice.Builder withFirstName(String FirstName) {
            this.FirstName = FirstName;
            return this;
        }

        public UserDevice.Builder withIMEI(String IMEI) {
            this.IMEI = IMEI;
            return this;
        }

        public UserDevice.Builder withIsVisibleDivision(Boolean IsVisibleDivision) {
            this.IsVisibleDivision = IsVisibleDivision;
            return this;
        }

        public UserDevice.Builder withIsVisibleIMEI(Boolean IsVisibleIMEI) {
            this.IsVisibleIMEI = IsVisibleIMEI;
            return this;
        }

        public UserDevice.Builder withIsVisiblePhoneNumber(Boolean IsVisiblePhoneNumber) {
            this.IsVisiblePhoneNumber = IsVisiblePhoneNumber;
            return this;
        }

        public UserDevice.Builder withLastName(String LastName) {
            this.LastName = LastName;
            return this;
        }

        public UserDevice.Builder withModel(String Model) {
            this.Model = Model;
            return this;
        }

        public UserDevice.Builder withPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
            return this;
        }

        public UserDevice.Builder withSerialNumber(String SerialNumber) {
            this.SerialNumber = SerialNumber;
            return this;
        }

        public UserDevice build() {
            UserDevice UserDevice = new UserDevice();
            UserDevice.Carrier = Carrier;
            UserDevice.DeviceId = DeviceId;
            UserDevice.Division = Division;
            UserDevice.FirstName = FirstName;
            UserDevice.IMEI = IMEI;
            UserDevice.IsVisibleDivision = IsVisibleDivision;
            UserDevice.IsVisibleIMEI = IsVisibleIMEI;
            UserDevice.IsVisiblePhoneNumber = IsVisiblePhoneNumber;
            UserDevice.LastName = LastName;
            UserDevice.Model = Model;
            UserDevice.PhoneNumber = PhoneNumber;
            UserDevice.SerialNumber = SerialNumber;
            return UserDevice;
        }

    }

}
