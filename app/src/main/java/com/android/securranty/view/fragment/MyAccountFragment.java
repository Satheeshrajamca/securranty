package com.android.securranty.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.billing.BillingProfileActivity;
import com.android.securranty.view.activity.creditcard.CreditCardListActivity;
import com.android.securranty.view.activity.shipping.ShippingActivity;
import com.android.securranty.view.activity.signs.SignInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.MainActivity.ACCOUNT_PROFILE_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.BILLING_PROFILE_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.CREDIT_CARD_ACTIVITY;

/**
 * Created by satheeshraja on 11/21/17.
 */

public class MyAccountFragment extends Fragment {
    @BindView(R.id.rootlayoutDashBoard)
    LinearLayout rootlayoutDashBoard;
    @BindView(R.id.textAccountProfile)
    TextView textAccountProfile;
    @BindView(R.id.textBillingProfile)
    TextView textBillingProfile;
    @BindView(R.id.textCreditCards)
    TextView textCreditCards;
    @BindView(R.id.textShippingAddress)
    TextView textShippingAddress;

    private MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    public interface ListenerLoginStatus {
        void changeLoginStatus(String status);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myaccount,
                container, false);
        ButterKnife.bind(this, view);
        setTypeFace();
        return view;
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/opensans_semibold.ttf");
        textAccountProfile.setTypeface(openSansSemiBold);
        textBillingProfile.setTypeface(openSansSemiBold);
        textCreditCards.setTypeface(openSansSemiBold);
        textShippingAddress.setTypeface(openSansSemiBold);
    }

    @OnClick({R.id.textAccountProfile, R.id.textBillingProfile, R.id.textCreditCards, R.id.textShippingAddress})
    public void onClickMyAccount(View view) {
        if (Utils.isNetworkAvailable(getActivity())) {
            switch (view.getId()) {
                case R.id.textAccountProfile:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivityForResult(new Intent(getActivity(), AccountProfileActivity.class),401);
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, ACCOUNT_PROFILE_ACTIVITY);
                    }
                    break;
                case R.id.textBillingProfile:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivityForResult(new Intent(getActivity(), BillingProfileActivity.class),401);
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, BILLING_PROFILE_ACTIVITY);
                    }
                    break;
                case R.id.textCreditCards:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivityForResult(new Intent(getActivity(), CreditCardListActivity.class),401);
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, CREDIT_CARD_ACTIVITY);
                    }
                    break;
                case R.id.textShippingAddress:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivityForResult(new Intent(getActivity(), ShippingActivity.class),401);
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, CREDIT_CARD_ACTIVITY);
                    }
                    break;

            }
        } else {
            showErrorMsg(200, getActivity().getResources().getString(R.string.msg_no_internet));
        }
    }

    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(rootlayoutDashBoard, errorMsg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ACCOUNT_PROFILE_ACTIVITY:
                    mainActivity.changeLoginStatus("Logout");
                    startActivity(new Intent(getActivity(), AccountProfileActivity.class));
                    break;
                case BILLING_PROFILE_ACTIVITY:
                    mainActivity.changeLoginStatus("Logout");
                    startActivity(new Intent(getActivity(), BillingProfileActivity.class));
                    break;
                case CREDIT_CARD_ACTIVITY:
                    mainActivity.changeLoginStatus("Logout");
                    startActivity(new Intent(getActivity(), CreditCardListActivity.class));
                    break;
                case 401:
                    mainActivity.changeLoginStatus("Login");
                    break;

            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
            mainActivity.changeLoginStatus("Login");
        } else {
            mainActivity.changeLoginStatus("Logout");
        }
    }
}
