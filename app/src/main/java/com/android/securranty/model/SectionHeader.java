package com.android.securranty.model;

import com.intrusoft.sectionedrecyclerview.Section;

import java.util.List;

/**
 * Created by satheeshraja on 4/1/18.
 */

public class SectionHeader implements Section<Child> {

    List<Child> childList;
    String sectionText;

    public SectionHeader(List<Child> childList, String sectionText) {
        this.childList = childList;
        this.sectionText = sectionText;
    }

    @Override
    public List<Child> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }
}
