
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConditionModel {

    @SerializedName("deviceConditions")
    private List<DeviceCondition> DeviceConditions;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<DeviceCondition> getDeviceConditions() {
        return DeviceConditions;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<DeviceCondition> DeviceConditions;
        private String Status;
        private Long StatusCode;

        public ConditionModel.Builder withDeviceConditions(List<DeviceCondition> deviceConditions) {
            DeviceConditions = deviceConditions;
            return this;
        }

        public ConditionModel.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public ConditionModel.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public ConditionModel build() {
            ConditionModel ConditionModel = new ConditionModel();
            ConditionModel.DeviceConditions = DeviceConditions;
            ConditionModel.Status = Status;
            ConditionModel.StatusCode = StatusCode;
            return ConditionModel;
        }

    }

}
