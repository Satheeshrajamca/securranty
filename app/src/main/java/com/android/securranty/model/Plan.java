
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class Plan {

    @SerializedName("Amount")
    private Double amount;
    @SerializedName("BasePlanId")
    private Long basePlanId;
    @SerializedName("MonthlyAmount")
    private Double monthlyAmount;
    @SerializedName("PlanName")
    private String planName;
    @SerializedName("PlanNameSubHeading")
    private String planNameSubHeading;

    public Double getAmount() {
        return amount;
    }

    public Long getBasePlanId() {
        return basePlanId;
    }

    public Double getMonthlyAmount() {
        return monthlyAmount;
    }

    public String getPlanName() {
        return planName;
    }

    public String getPlanNameSubHeading() {
        return planNameSubHeading;
    }

    public static class Builder {

        private Double amount;
        private Long basePlanId;
        private Double monthlyAmount;
        private String planName;
        private String planNameSubHeading;

        public Plan.Builder withAmount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Plan.Builder withBasePlanId(Long basePlanId) {
            this.basePlanId = basePlanId;
            return this;
        }

        public Plan.Builder withMonthlyAmount(Double monthlyAmount) {
            this.monthlyAmount = monthlyAmount;
            return this;
        }

        public Plan.Builder withPlanName(String planName) {
            this.planName = planName;
            return this;
        }

        public Plan.Builder withPlanNameSubHeading(String planNameSubHeading) {
            this.planNameSubHeading = planNameSubHeading;
            return this;
        }

        public Plan build() {
            Plan plan = new Plan();
            plan.amount = amount;
            plan.basePlanId = basePlanId;
            plan.monthlyAmount = monthlyAmount;
            plan.planName = planName;
            plan.planNameSubHeading = planNameSubHeading;
            return plan;
        }

    }

}
