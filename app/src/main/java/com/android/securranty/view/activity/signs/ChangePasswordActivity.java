package com.android.securranty.view.activity.signs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.presenter.implementor.ChangePassPresenterImp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshraja on 11/30/17.
 */

public class ChangePasswordActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.editNewPass)
    EditText editNewPass;
    @BindView(R.id.editRepeatPass)
    EditText editRepeatPass;
    @BindView(R.id.editCurrentPass)
    EditText editCurrentPass;

    @BindView(R.id.labelNewPass)
    TextView labelNewPass;
    @BindView(R.id.labelRepeatPass)
    TextView labelRepeatPass;
    @BindView(R.id.labelCurrentPass)
    TextView labelCurrentPass;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private ChangePassPresenterImp changePassPresenterImp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        changePassPresenterImp = new ChangePassPresenterImp(this);
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");

        labelNewPass.setTypeface(openSansRegular);
        labelRepeatPass.setTypeface(openSansRegular);
        labelCurrentPass.setTypeface(openSansRegular);
        editCurrentPass.setTypeface(openSansSemiBold);
        editNewPass.setTypeface(openSansSemiBold);
        editRepeatPass.setTypeface(openSansSemiBold);
    }

    private String newPassword;

    @OnClick({R.id.imageSave, R.id.imageBackButton})
    public void onclickChangePassword(View view) {
        switch (view.getId()) {
            case R.id.imageSave:
                newPassword=editNewPass.getText().toString().trim();
                changePassPresenterImp.validateCredential(editCurrentPass.getText().toString().trim(),
                        editNewPass.getText().toString().trim(), editRepeatPass.getText().toString().trim());
                break;
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    @Override
    public void success(Object obj) {
        Snackbar.make(coordinatorLayout, "Password has been changed", Snackbar.LENGTH_LONG).show();
        SharePref.getInstance(this).setPassword(newPassword);
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        changePassPresenterImp.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            finish();
        }
    }
}
