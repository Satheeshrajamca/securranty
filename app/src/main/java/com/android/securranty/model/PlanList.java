
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

import java.util.List;

@SuppressWarnings("unused")
public class PlanList {

    @Expose
    private String cancelAnytimeForRefundInfo;
    @Expose
    private String claimAndServiceOptionInfo;
    @Expose
    private List<DeductibleInfo> deductibleInfo;
    @Expose
    private List<Plan> plan;
    @Expose
    private String planInfo;
    @Expose
    private String status;
    @Expose
    private int statusCode;

    public String getCancelAnytimeForRefundInfo() {
        return cancelAnytimeForRefundInfo;
    }

    public String getClaimAndServiceOptionInfo() {
        return claimAndServiceOptionInfo;
    }

    public List<DeductibleInfo> getDeductibleInfo() {
        return deductibleInfo;
    }

    public List<Plan> getPlan() {
        return plan;
    }

    public String getPlanInfo() {
        return planInfo;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private String cancelAnytimeForRefundInfo;
        private String claimAndServiceOptionInfo;
        private List<DeductibleInfo> deductibleInfo;
        private List<Plan> plan;
        private String planInfo;
        private String status;
        private int statusCode;

        public PlanList.Builder withCancelAnytimeForRefundInfo(String cancelAnytimeForRefundInfo) {
            this.cancelAnytimeForRefundInfo = cancelAnytimeForRefundInfo;
            return this;
        }

        public PlanList.Builder withClaimAndServiceOptionInfo(String claimAndServiceOptionInfo) {
            this.claimAndServiceOptionInfo = claimAndServiceOptionInfo;
            return this;
        }

        public PlanList.Builder withDeductibleInfo(List<DeductibleInfo> deductibleInfo) {
            this.deductibleInfo = deductibleInfo;
            return this;
        }

        public PlanList.Builder withPlan(List<Plan> plan) {
            this.plan = plan;
            return this;
        }

        public PlanList.Builder withPlanInfo(String planInfo) {
            this.planInfo = planInfo;
            return this;
        }

        public PlanList.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public PlanList.Builder withStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public PlanList build() {
            PlanList planList = new PlanList();
            planList.cancelAnytimeForRefundInfo = cancelAnytimeForRefundInfo;
            planList.claimAndServiceOptionInfo = claimAndServiceOptionInfo;
            planList.deductibleInfo = deductibleInfo;
            planList.plan = plan;
            planList.planInfo = planInfo;
            planList.status = status;
            planList.statusCode = statusCode;
            return planList;
        }

    }

}
