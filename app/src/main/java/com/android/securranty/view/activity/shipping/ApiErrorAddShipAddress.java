
package com.android.securranty.view.activity.shipping;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ApiErrorAddShipAddress {

    @SerializedName("message")
    private List<String> Message;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private int StatusCode;

    public List<String> getMessage() {
        return Message;
    }

    public String getStatus() {
        return Status;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<String> Message;
        private String Status;
        private int StatusCode;

        public ApiErrorAddShipAddress.Builder withMessage(List<String> message) {
            Message = message;
            return this;
        }

        public ApiErrorAddShipAddress.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public ApiErrorAddShipAddress.Builder withStatusCode(int statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public ApiErrorAddShipAddress build() {
            ApiErrorAddShipAddress ApiErrorAddShipAddress = new ApiErrorAddShipAddress();
            ApiErrorAddShipAddress.Message = Message;
            ApiErrorAddShipAddress.Status = Status;
            ApiErrorAddShipAddress.StatusCode = StatusCode;
            return ApiErrorAddShipAddress;
        }

    }

}
