package com.android.securranty.view.activity.shipping;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.presenter.implementor.FileClaimPresenter;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.activity.tickets.CreateTicketActivity;
import com.android.securranty.view.activity.tickets.TicketListActivity;
import com.android.securranty.view.fragment.fileclaim.model.ShippingAddress;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshraja on 4/7/18.
 */

public class ShippingActivity extends Activity implements BaseView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.searchView)
    EditText searchView;
    @BindView(R.id.layoutClaims)
    RelativeLayout layoutClaims;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private ActionBar actionBar;
    private ShippingAddressAdapter shippingAddressAdapter;
    FileClaimPresenter shippingAddressPresenter;
    private MainActivity mainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        ButterKnife.bind(this);
        shippingAddressPresenter = new FileClaimPresenter(this);
        shippingAddressPresenter.getShippingAddress();
        search();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @OnClick({R.id.imageAdd, R.id.imageBackButton})
    public void onclickAdd(View view) {
        switch (view.getId()) {
            case R.id.imageAdd:
                startActivityForResult(new Intent(this, AddShippingAddressActivity.class), 100);
                break;
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    private void search() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                shippingAddressAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(layoutClaims, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    @Override
    public void success(Object obj) {
        if (ShippingAddress.class.cast(obj).getShippingAddresses().size() > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this);
            recyclerView.addItemDecoration(dividerItemDecoration);

            recyclerView.setLayoutManager(linearLayoutManager);
            shippingAddressAdapter = new ShippingAddressAdapter();
            shippingAddressAdapter.setData(ShippingAddress.class.cast(obj).getShippingAddresses());
            recyclerView.setAdapter(shippingAddressAdapter);
        } else {
            //Snackbar.make(layoutClaims, "Sorry, no shipping address!", Snackbar.LENGTH_LONG).show();
            new MaterialDialog.Builder(this).onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    //finish();
                    startActivityForResult(new Intent(ShippingActivity.this, AddShippingAddressActivity.class), 100);
                }
            }).onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    finish();
                }
            })
                    .canceledOnTouchOutside(false)
                    .title("Shipping Address")
                    .content("No Shipping Address Found.")
                    .positiveText("Add Shipping Address")
                    .negativeText("Cancel")
                    .show();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public interface ListenerClaims {
        public void backstackClaims();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            Snackbar.make(coordinatorLayout, data.getStringExtra("Key"), Snackbar.LENGTH_LONG).show();
            shippingAddressPresenter.getShippingAddress();
        }
        else if (requestCode == 401 && resultCode == RESULT_OK) {
            shippingAddressPresenter.getShippingAddress();
        } else {
            finish();
        }
    }

}
