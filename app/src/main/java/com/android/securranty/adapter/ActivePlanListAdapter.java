package com.android.securranty.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity;
import com.android.securranty.view.activity.plans.active_plan.model.Plan;

import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class ActivePlanListAdapter extends RecyclerView.Adapter<ActivePlanListAdapter.ViewHolder> {
    private List<Plan> activePlanList;
    private ActivePlanListActivity context;

    public interface FileClaim {
        void fileClaim(int position,String from);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_active_plans, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textPolicyNo.setText("" + activePlanList.get(position).getId());
        holder.textModel.setText(activePlanList.get(position).getModel());
        holder.textPolicyExp.setText(activePlanList.get(position).getPolicyExpirationDate().split("T")[0]);
    }

    @Override
    public int getItemCount() {
        return activePlanList.size();
    }

    public void setData(ActivePlanListActivity context, List<Plan> activePlanList) {
        this.activePlanList = activePlanList;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textPolicyNo, textModel, textPolicyExp;
        public ImageView imageFileClaim;

        public ViewHolder(View itemView) {
            super(itemView);
            textPolicyNo = (TextView) itemView.findViewById(R.id.textPolicyNo);
            textModel = (TextView) itemView.findViewById(R.id.textModel);
            textPolicyExp = (TextView) itemView.findViewById(R.id.textPolicyExp);
            Typeface openSansRegular = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_regular.ttf");
            textPolicyNo.setTypeface(openSansRegular);
            textModel.setTypeface(openSansRegular);
            textPolicyExp.setTypeface(openSansRegular);
            imageFileClaim=(ImageView)itemView.findViewById(R.id.imageFileClaim);
            imageFileClaim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("ClickablePosition", "" + (getAdapterPosition()));
                    context.fileClaim(getAdapterPosition(),"FileClaim");
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.fileClaim(getAdapterPosition(),"PolicyView");
                    //Toast.makeText(context, "ItemView"+getAdapterPosition(), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
