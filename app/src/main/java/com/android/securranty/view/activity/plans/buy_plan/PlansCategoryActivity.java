package com.android.securranty.view.activity.plans.buy_plan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.MainActivity.ACTIVE_PLAN;
import static com.android.securranty.view.fragment.PlanFragment.PURCHASE_PLAN_REQ_CODE;

/**
 * Created by satheeshraja on 11/21/17.
 */

public class PlansCategoryActivity extends Activity {
    @BindView(R.id.rootlayoutDashBoard)
    LinearLayout rootlayoutDashBoard;
    @BindView(R.id.textiPhones)
    TextView textiPhones;
    @BindView(R.id.textSmartPhones)
    TextView textSmartPhones;
    @BindView(R.id.textiPads)
    TextView textiPads;
    @BindView(R.id.textAllPlans)
    TextView textAllPlans;

    public static String PLANID = "PLANID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_types);
        ButterKnife.bind(this);
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        textiPhones.setTypeface(openSansSemiBold);
        textSmartPhones.setTypeface(openSansSemiBold);
        textiPads.setTypeface(openSansSemiBold);
        textAllPlans.setTypeface(openSansSemiBold);
    }

    @OnClick({R.id.textiPhones, R.id.textSmartPhones, R.id.textiPads, R.id.textAllPlans, R.id.imageBackButton})
    public void onClickMyAccount(View view) {
        if (Utils.isNetworkAvailable(this)) {
            switch (view.getId()) {
                case R.id.textiPhones:
                    startActivityForResult(new Intent(this, PlanDetailsActivity.class).putExtra(PLANID, "1"), PURCHASE_PLAN_REQ_CODE);
                    break;
                case R.id.textSmartPhones:
                    startActivityForResult(new Intent(this, PlanDetailsActivity.class).putExtra(PLANID, "2"), PURCHASE_PLAN_REQ_CODE);
                    break;
                case R.id.textiPads:
                    startActivityForResult(new Intent(this, PlanDetailsActivity.class).putExtra(PLANID, "3"), PURCHASE_PLAN_REQ_CODE);
                    break;
                case R.id.textAllPlans:
                    startActivityForResult(new Intent(this, AllPlansActivity.class).putExtra(PLANID, "0"), PURCHASE_PLAN_REQ_CODE);
                    break;
                case R.id.imageBackButton:
                    finish();
                    break;
            }
        } else {
            showErrorMsg(200, getResources().getString(R.string.msg_no_internet));
        }
    }

    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(rootlayoutDashBoard, errorMsg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PURCHASE_PLAN_REQ_CODE) {
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }

    }
}
