
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Carrier {

    @SerializedName("Active")
    private Boolean mActive;
    @SerializedName("carriers")
    private List<Carrier> mCarriers;
    @SerializedName("Id")
    private int mId;
    @SerializedName("Name")
    private String mName;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("statusCode")
    private Long mStatusCode;

    public Boolean getActive() {
        return mActive;
    }

    public List<Carrier> getCarriers() {
        return mCarriers;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getStatus() {
        return mStatus;
    }

    public Long getStatusCode() {
        return mStatusCode;
    }

    public static class Builder {

        private Boolean mActive;
        private List<Carrier> mCarriers;
        private int mId;
        private String mName;
        private String mStatus;
        private Long mStatusCode;

        public Carrier.Builder withActive(Boolean Active) {
            mActive = Active;
            return this;
        }

        public Carrier.Builder withCarriers(List<Carrier> carriers) {
            mCarriers = carriers;
            return this;
        }

        public Carrier.Builder withId(int Id) {
            mId = Id;
            return this;
        }

        public Carrier.Builder withName(String Name) {
            mName = Name;
            return this;
        }

        public Carrier.Builder withStatus(String status) {
            mStatus = status;
            return this;
        }

        public Carrier.Builder withStatusCode(Long statusCode) {
            mStatusCode = statusCode;
            return this;
        }

        public Carrier build() {
            Carrier Carrier = new Carrier();
            Carrier.mActive = mActive;
            Carrier.mCarriers = mCarriers;
            Carrier.mId = mId;
            Carrier.mName = mName;
            Carrier.mStatus = mStatus;
            Carrier.mStatusCode = mStatusCode;
            return Carrier;
        }

    }

}
