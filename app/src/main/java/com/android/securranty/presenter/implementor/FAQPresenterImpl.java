package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.FAQPresenterInter;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class FAQPresenterImpl implements BasePresenter {
    BaseView baseView;
    FAQPresenterInter basePresenter;

    public FAQPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        basePresenter = new FAQPresenterInter(this);
    }

    public void intFAQ() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        basePresenter.getFAQ();
    }

    @Override
    public void success(Object obj) {
        baseView.hideProgress();
        baseView.success(obj);
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.showErrorMsg(statusCode, msg);
        }
    }

    @Override
    public void onDestroy() {
        if (baseView != null) {
            baseView = null;
        }
    }
}
