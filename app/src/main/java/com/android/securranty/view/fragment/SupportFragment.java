package com.android.securranty.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.signs.ChangePasswordActivity;
import com.android.securranty.view.activity.faq.FAQActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.activity.tickets.TicketListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.MainActivity.ACCOUNT_PROFILE_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.CHANGE_PASSWORD;
import static com.android.securranty.view.activity.MainActivity.CREATE_TICKET;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class SupportFragment extends Fragment {
    @BindView(R.id.rootlayoutDashBoard)
    RelativeLayout rootlayoutDashBoard;
    @BindView(R.id.textTickets)
    TextView textTickets;
    @BindView(R.id.textFAQ)
    TextView textFAQ;
    @BindView(R.id.textChangePass)
    TextView textChangePass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support,
                container, false);
        ButterKnife.bind(this, view);
        setTypeFace();
        return view;
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/opensans_semibold.ttf");
        textTickets.setTypeface(openSansSemiBold);
        textFAQ.setTypeface(openSansSemiBold);
        textChangePass.setTypeface(openSansSemiBold);
    }

    @OnClick({R.id.textTickets, R.id.textFAQ, R.id.textChangePass})
    public void onClickMyAccount(View view) {
        if (Utils.isNetworkAvailable(getActivity())) {
            switch (view.getId()) {
                case R.id.textTickets:
                    if (Utils.isNetworkAvailable(getActivity())) {
                        if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                            startActivity(new Intent(getActivity(), TicketListActivity.class));
                        } else {
                            Intent i = new Intent(getActivity(), SignInActivity.class);
                            startActivityForResult(i, CREATE_TICKET);
                        }
                    } else {
                        showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
                    }
                    break;
                case R.id.textFAQ:
                    startActivity(new Intent(getActivity(), FAQActivity.class));
                    break;
                case R.id.textChangePass:
                    if (Utils.isNetworkAvailable(getActivity())) {
                        if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                            startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                        } else {
                            Intent i = new Intent(getActivity(), SignInActivity.class);
                            startActivityForResult(i, CHANGE_PASSWORD);
                        }
                    } else {
                        showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
                    }
                    break;
            }
        } else {
            Snackbar.make(rootlayoutDashBoard, getActivity().getResources().getString(R.string.msg_no_internet), Snackbar.LENGTH_LONG).show();
        }

    }

    private void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(rootlayoutDashBoard, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), SignInActivity.class));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHANGE_PASSWORD && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
            mainActivity.changeLoginStatus("Logout");
        } else if (requestCode == CREATE_TICKET && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), TicketListActivity.class));
            mainActivity.changeLoginStatus("Logout");
        }
    }

    private MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
            mainActivity.changeLoginStatus("Login");
        } else {
            mainActivity.changeLoginStatus("Logout");
        }
    }
}
