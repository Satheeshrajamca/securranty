
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class APISuccessModel {

    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private int StatusCode;

    public String getMessage() {
        return Message;
    }

    public String getStatus() {
        return Status;
    }

    public int getStatusCode() {
        return StatusCode;
    }
}
