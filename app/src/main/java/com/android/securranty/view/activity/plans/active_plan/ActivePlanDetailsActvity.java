package com.android.securranty.view.activity.plans.active_plan;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.AdapterSectionRecycler;
import com.android.securranty.model.Child;
import com.android.securranty.model.SectionHeader;
import com.android.securranty.presenter.implementor.ActivePlanDeatailsPresenter;
import com.android.securranty.presenter.implementor.ActivePlanPresenterImpl;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.plans.active_plan.model.ActivePlan;
import com.android.securranty.view.activity.plans.active_plan.model.ProofofPurchase;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.activePlans;
import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/1/18.
 */

public class ActivePlanDetailsActvity extends AppCompatActivity implements BaseView, AdapterSectionRecycler.ZoomImageView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageFileClaim)
    ImageView imageFileClaim;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.expanded_image)
    ImageView expanded_image;
   /* @BindView(R.id.containerFragment)
    FrameLayout containerFragment;*/

    private ActionBar actionBar;
    private ActivePlanDeatailsPresenter activePlanDeatailsPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_plan_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        positionActivePlans = getIntent().getIntExtra("PLAN_POSITION", 0);
        planId = activePlans.get(positionActivePlans).getId();
        Log.i("PlanId", "" + planId);
        Log.i("SelectedPlanPosition", "" + positionActivePlans);
        //daoFileClaim.setPolicyId(activePlans.get(positionActivePlans).getId());
        loadPlanDetails();
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_file_claim, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_edit_plan) {
            startActivityForResult(new Intent(this, ActivePlanUpdateActvity.class)
                    .putExtra("PLAN_POSITION", positionActivePlans), 400);
        } else if (item.getItemId() == R.id.menu_file_claim) {
            fileClaim();
            //startActivity(new Intent(this, FileClaimsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private Long planId;

    private void fileClaim() {
        if (activePlans.get(positionActivePlans).getIsOngoingClaim() == 0) {
            if (activePlans.get(positionActivePlans).getIsUpdatedMandatoryField() == 0) {
                daoFileClaim.setPolicyId(activePlans.get(positionActivePlans).getId());
                startActivityForResult(new Intent(this, FileClaimsActivity.class), 400);
            } else {
                new iOSDialogBuilder(this)
                        .setTitle("File a claim")
                        .setSubtitle("Please update your claim first")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else {

            new iOSDialogBuilder(this)
                    .setTitle("File a claim")
                    .setSubtitle("This policy already has a ongoing claim!")
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            dialog.dismiss();
                        }
                    }).build().show();

        }
    }

    @OnClick({R.id.imageBackButton, R.id.imageFileClaim})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                onBackPressed();
                break;
            case R.id.imageFileClaim:
                startActivity(new Intent(this, FileClaimsActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isPlanUpdated) {
            Intent intent = getIntent();
            setResult(RESULT_OK, intent);
            finish();
        }
        super.onBackPressed();

    }

    private int positionActivePlans;
    private AdapterSectionRecycler adapterSectionRecycler;
    public static List<Child> policyInformation;

    private void loadPlanDetails() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        List<Child> childList = new ArrayList<>();
        List<SectionHeader> headerList = new ArrayList<>();
        childList.add(new Child("Item Type", activePlans.get(positionActivePlans).getItemType(), true));
        childList.add(new Child("Manufacturer", activePlans.get(positionActivePlans).getManufacturer(), true));
        childList.add(new Child("Model", activePlans.get(positionActivePlans).getModel(), true));
        childList.add(new Child("Item Purchase Date", activePlans.get(positionActivePlans).getItemPurchaseDate().split("T")[0], false));
        childList.add(new Child("Item Purchase Price", String.format("%.2f", activePlans.get(positionActivePlans).getItemPurchasePrice()), false));
        childList.add(new Child("Item Condition", activePlans.get(positionActivePlans).getItemCondition(), true));
        if (activePlans.get(positionActivePlans).isCarrierVisible()) {
            childList.add(new Child("Carrier", activePlans.get(positionActivePlans).getCarrier(), true));
        }

        String phoneNumber = String.valueOf(activePlans.get(positionActivePlans).getPhoneNumber());
        if (phoneNumber != null) {
            if (phoneNumber != "null" && !phoneNumber.isEmpty() && !phoneNumber.contains("-")) {
                childList.add(new Child("Phone Number", String.format("%s-%s-%s", phoneNumber.substring(0, 3),
                        phoneNumber.substring(3, 6), phoneNumber.substring(6)), false));
            } else {
                childList.add(new Child("Phone Number", phoneNumber, false));
            }
        }
        childList.add(new Child("Serial", activePlans.get(positionActivePlans).getSerial(), false));
        childList.add(new Child("IMEI", activePlans.get(positionActivePlans).getIMEI(), false));
        //childList.add(new Child("finalChild", "finalChild", false));
        headerList.add(new SectionHeader(childList, "Item Covered"));

        childList = new ArrayList<>();
        childList.add(new Child("First Name", activePlans.get(positionActivePlans).getFirstName(), false));
        childList.add(new Child("Last Name", activePlans.get(positionActivePlans).getLastName(), false));
        childList.add(new Child("Division", String.valueOf(activePlans.get(positionActivePlans).getDivision()), true));
        childList.add(new Child("Cost Center", String.valueOf(activePlans.get(positionActivePlans).getCostCenter()), true));
        childList.add(new Child("Country", activePlans.get(positionActivePlans).getCountry(), true));
        //childList.add(new Child("finalChild", "finalChild", false));
        headerList.add(new SectionHeader(childList, "Item User Information"));

        childList = new ArrayList<>();
        policyInformation = new ArrayList<>();
        childList.add(new Child("Policy ID", String.valueOf(activePlans.get(positionActivePlans).getId()), false));
        childList.add(new Child("Policy Status", activePlans.get(positionActivePlans).getPolicyStatus(), false));
        childList.add(new Child("Coverage Limit", String.format("%.2f", activePlans.get(positionActivePlans).getCoverageLimit()), false));
        if (activePlans.get(positionActivePlans).getPolicyStartDate() != null && activePlans.get(positionActivePlans).getPolicyStartDate().contains("T")) {
            childList.add(new Child("Policy Start Date", activePlans.get(positionActivePlans).getPolicyStartDate().split("T")[0], false));
        } else {
            childList.add(new Child("Policy Start Date", activePlans.get(positionActivePlans).getPolicyStartDate(), false));
        }
        childList.add(new Child("Policy Expiration Date", activePlans.get(positionActivePlans).getPolicyExpirationDate().split("T")[0], false));
        //childList.add(new Child("Policy Expiration Date", activePlans.get(positionActivePlans).getPolicyExpirationDate(), false));
        childList.add(new Child("Warranty Summary", activePlans.get(positionActivePlans).getWarrantySummary(), false));
        childList.add(new Child("Plan Purchase Price", String.format("%.2f", activePlans.get(positionActivePlans).getPlanPurchasePrice()), false));
        childList.add(new Child("Service Level", activePlans.get(positionActivePlans).getServiceLevel(), false));
        childList.add(new Child("Coverage Term", activePlans.get(positionActivePlans).getCoverageTerm(), false));
        childList.add(new Child("Policy Type", activePlans.get(positionActivePlans).getPolicyType(), false));
        childList.add(new Child("Sold By", activePlans.get(positionActivePlans).getSoldBy(), false));
        //childList.add(new Child("finalChild", "finalChild", false));
        headerList.add(new SectionHeader(childList, "Policy Information"));
        policyInformation = childList;

        childList = new ArrayList<>();
        List<ProofofPurchase> proofofPurchases = activePlans.get(positionActivePlans).getProofofPurchase();
        int size = proofofPurchases.size();
        for (int i = 0; i < size; i++) {
            childList.add(new Child("Proof" + (i + 1), proofofPurchases.get(i).getFilePath(), false));
        }
        //childList.add(new Child("finalChild", "finalChild", false));
        headerList.add(new SectionHeader(childList, "Proof of Purchase"));

        adapterSectionRecycler = new AdapterSectionRecycler(this, headerList, positionActivePlans);
        recyclerView.setAdapter(adapterSectionRecycler);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    @Override
    public void success(Object obj) {
        activePlans.clear();
        Log.i("ActivePlanSize", "" + activePlans.size());
        activePlans = ActivePlan.class.cast(obj).getPlans();
        for (int i = 0; i < activePlans.size(); i++) {
            Log.i("ActivePlanId", "" + activePlans.get(i).getId());
            if (activePlans.get(i).getId() == planId) {
                positionActivePlans = i;
                Log.i("PlanIdnew", "" + planId);
            }
        }
        loadPlanDetails();
    }

    @Override
    public void hideKeyboard() {

    }

    private boolean isPlanUpdated;
    private ActivePlanPresenterImpl activePlanPresenter;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 400 && resultCode == RESULT_OK) {
            isPlanUpdated = true;
            activePlanPresenter = new ActivePlanPresenterImpl(this);
            activePlanPresenter.intActivePlans();
        }
    }

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    @Override
    public void zoomImageFromThumb(final ImageView thumbView, String imageUrl) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        /*final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);*/
        //expandedImageView.setImageResource(imageResId);
        Picasso.with(this).load(imageUrl).into(expanded_image);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        coordinatorLayout
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expanded_image.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expanded_image.setPivotX(0f);
        expanded_image.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expanded_image, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expanded_image, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expanded_image, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expanded_image,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expanded_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expanded_image, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expanded_image,
                                        View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expanded_image,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expanded_image,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expanded_image.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expanded_image.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }
}
