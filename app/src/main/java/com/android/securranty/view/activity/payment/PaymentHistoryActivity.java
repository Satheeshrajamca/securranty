package com.android.securranty.view.activity.payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.PaymentHistoryAdapter;
import com.android.securranty.model.PaymentHistoryModel;
import com.android.securranty.presenter.implementor.TicketListPresenterImpl;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.activity.tickets.CreateTicketActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.MainActivity.PAYMENT_HISTORY;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class PaymentHistoryActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerTicketHistory;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.imageAdd)
    ImageView imageAdd;

    private ActionBar actionBar;
    private PaymentHistoryAdapter paymentHistoryAdapter;
    TicketListPresenterImpl ticketListPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        ticketListPresenter = new TicketListPresenterImpl(this);
        ticketListPresenter.initPaymentList();
        toolbar_title.setText("Payment History");
        imageAdd.setVisibility(View.GONE);
        //search();
    }

    @OnClick({R.id.imageAdd, R.id.imageBackButton})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageAdd:
                startActivity(new Intent(this, CreateTicketActivity.class));
                break;
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    /*private void search() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paymentHistoryAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }*/

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    @Override
    public void success(Object obj) {
        if (PaymentHistoryModel.class.cast(obj).getPaymentHistory().size() > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this);
            recyclerTicketHistory.addItemDecoration(dividerItemDecoration);

            recyclerTicketHistory.setLayoutManager(linearLayoutManager);
            paymentHistoryAdapter = new PaymentHistoryAdapter();
            paymentHistoryAdapter.setData(PaymentHistoryModel.class.cast(obj).getPaymentHistory());
            recyclerTicketHistory.setAdapter(paymentHistoryAdapter);
        } else {
            showErrorMsg(400, "There's no payment history to display");
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            ticketListPresenter.initPaymentList();
        }
        else
        {
            finish();
        }
    }
}
