package com.android.securranty.view.activity.plans.buy_plan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.adapter.AllPlansAdapter;
import com.android.securranty.model.AdPage;
import com.android.securranty.model.MyPlans;
import com.android.securranty.presenter.interactor.PlanDetailsPresenter;
import com.android.securranty.utility.RecyclerItemClickListener;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.plans.buy_plan.PlansCategoryActivity.PLANID;
import static com.android.securranty.view.fragment.PlanFragment.PURCHASE_PLAN_REQ_CODE;

/**
 * Created by satheeshraja on 2/18/18.
 */

public class AllPlansActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerAllPlans;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    private ActionBar actionBar;
    PlanDetailsPresenter planDetailsPresenter;
    private String planId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_plan_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        toolbar_title.setText("All Plans");
        planId = getIntent().getStringExtra(PLANID);
        planDetailsPresenter = new PlanDetailsPresenter(this);
        planDetailsPresenter.initPlainDetails(planId);
    }

    @OnClick({R.id.imageBackButton})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            finish();
            startActivity(new Intent(this, SignInActivity.class));
        }
    }

    public static ArrayList<AdPage> listAdPage;

    @Override
    public void success(Object obj) {
        listAdPage = new ArrayList<>();
        MyPlans myPlans = MyPlans.class.cast(obj);
        for (AdPage adPage : myPlans.getAdPage()) {
            if (adPage.getVisible()) {
                listAdPage.add(adPage);
            }
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerAllPlans.setLayoutManager(linearLayoutManager);
        AllPlansAdapter allplansAdapter = new AllPlansAdapter(this, listAdPage);
        recyclerAllPlans.setAdapter(allplansAdapter);
        recyclerAllPlans.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intentAllPlans = new Intent(AllPlansActivity.this, PlanDetailsActivity.class);
                        intentAllPlans.putExtra(PLANID, String.valueOf(listAdPage.get(position).getId()));
                        intentAllPlans.putExtra("ADD_POSITION",position);
                        startActivityForResult(intentAllPlans,PURCHASE_PLAN_REQ_CODE);
                    }
                })
        );
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PURCHASE_PLAN_REQ_CODE) {
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
