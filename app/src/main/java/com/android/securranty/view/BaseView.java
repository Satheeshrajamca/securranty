package com.android.securranty.view;

/**
 * Created by satheeshraja on 11/16/17.
 */

public interface BaseView {
    void showProgress();

    void hideProgress();

    void showErrorMsg(int statusCode, String errorMsg);

    void success(Object obj);

    void hideKeyboard();

}
