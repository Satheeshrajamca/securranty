
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ShippingAddress {

    @Expose
    private String DisplayText;
    @Expose
    private int Id;
    @SerializedName("shippingAddresses")
    private List<ShippingAddress> ShippingAddresses;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public String getDisplayText() {
        return DisplayText;
    }

    public int getId() {
        return Id;
    }

    public List<ShippingAddress> getShippingAddresses() {
        return ShippingAddresses;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private String DisplayText;
        private int Id;
        private List<ShippingAddress> ShippingAddresses;
        private String Status;
        private Long StatusCode;

        public ShippingAddress.Builder withDisplayText(String DisplayText) {
            this.DisplayText = DisplayText;
            return this;
        }

        public ShippingAddress.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public ShippingAddress.Builder withShippingAddresses(List<ShippingAddress> shippingAddresses) {
            ShippingAddresses = shippingAddresses;
            return this;
        }

        public ShippingAddress.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public ShippingAddress.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public ShippingAddress build() {
            ShippingAddress ShippingAddress = new ShippingAddress();
            ShippingAddress.DisplayText = DisplayText;
            ShippingAddress.Id = Id;
            ShippingAddress.ShippingAddresses = ShippingAddresses;
            ShippingAddress.Status = Status;
            ShippingAddress.StatusCode = StatusCode;
            return ShippingAddress;
        }

    }

}
