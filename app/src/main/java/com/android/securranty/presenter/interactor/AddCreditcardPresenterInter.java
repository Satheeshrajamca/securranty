package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.presenter.implementor.AddCreditcardPresenterImpl;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import com.android.securranty.utility.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshr on 04-12-2017.
 */

public class AddCreditcardPresenterInter {
    AddCreditcardPresenterImpl addCreditcardPresenter;

    public AddCreditcardPresenterInter(AddCreditcardPresenterImpl addCreditcardPresenter) {
        this.addCreditcardPresenter = addCreditcardPresenter;
    }

    public void updateCreditCardInfo(String creditCardNo, String expireMonth, String expireYear, String cvv, String firstName, String lastName,
                                     String billingEmail, String phone, String billingAdd1, String billingAdd2,
                                     String city, String state, String country, String zip, boolean defaults, boolean active) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            apiCall(SharePref.getInstance(SecurrantyApp.context).getToken(), SharePref.getInstance(SecurrantyApp.context).getUserId(),
                    creditCardNo, expireMonth, expireYear, cvv, firstName, lastName,
                    billingEmail, phone, billingAdd1, billingAdd2, city,
                    state, country, zip, defaults, active);
        } else {
            addCreditcardPresenter.errorMsg(401, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void apiCall(String token, String userid, String creditCardNo, String expireMonth, String expireYear, String cvv, String firstName, String lastName, String billingEmail,
                         String phone, String billingAdd1, String billingAdd2, String city, String state, String country,
                         String zip, boolean defaults, boolean active) {
        ApiClient.getClient().updateCreditCard(token, userid, creditCardNo, expireMonth, expireYear, cvv, firstName, lastName,
                billingEmail, phone, billingAdd1, billingAdd2, city,
                state, country, zip, defaults, active).enqueue(new Callback<APISuccessModel>() {
            @Override
            public void onResponse(Call<APISuccessModel> call, Response<APISuccessModel> response) {
                if (response.isSuccessful()) {
                    addCreditcardPresenter.success(response.body());
                } else {
                    errorResponse(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<APISuccessModel> call, Throwable e) {
                if (e instanceof HttpException)
                    errorResponse(((HttpException) e).response().errorBody());
                else
                    addCreditcardPresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            addCreditcardPresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            addCreditcardPresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
