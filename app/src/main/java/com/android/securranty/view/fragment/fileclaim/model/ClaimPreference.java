
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ClaimPreference {

    @Expose
    private List<com.android.securranty.view.fragment.fileclaim.model.MesClaimServPreferencesage> MesClaimServPreferencesage;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<com.android.securranty.view.fragment.fileclaim.model.MesClaimServPreferencesage> getMesClaimServPreferencesage() {
        return MesClaimServPreferencesage;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<com.android.securranty.view.fragment.fileclaim.model.MesClaimServPreferencesage> MesClaimServPreferencesage;
        private String Status;
        private Long StatusCode;

        public ClaimPreference.Builder withMesClaimServPreferencesage(List<com.android.securranty.view.fragment.fileclaim.model.MesClaimServPreferencesage> MesClaimServPreferencesage) {
            this.MesClaimServPreferencesage = MesClaimServPreferencesage;
            return this;
        }

        public ClaimPreference.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public ClaimPreference.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public ClaimPreference build() {
            ClaimPreference ClaimPreference = new ClaimPreference();
            ClaimPreference.MesClaimServPreferencesage = MesClaimServPreferencesage;
            ClaimPreference.Status = Status;
            ClaimPreference.StatusCode = StatusCode;
            return ClaimPreference;
        }

    }

}
