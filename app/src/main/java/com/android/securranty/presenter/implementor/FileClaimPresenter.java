package com.android.securranty.presenter.implementor;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import android.util.Log;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.model.DeviceModel;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.fragment.fileclaim.model.ClaimPreference;
import com.android.securranty.view.fragment.fileclaim.model.ClaimType;
import com.android.securranty.view.fragment.fileclaim.model.PaymentReviewandCountryList;
import com.android.securranty.view.fragment.fileclaim.model.PaymentandReview;
import com.android.securranty.view.fragment.fileclaim.model.ShippingAddress;
import com.android.securranty.view.fragment.fileclaim.model.SuccessClaimModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/14/18.
 */

public class FileClaimPresenter {
    private BaseView baseView;

    public FileClaimPresenter(BaseView baseView) {
        this.baseView = baseView;
    }

    public void getDeviceInfo() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetDeviceInfo();
    }

    private void apiGetDeviceInfo() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getDeviceInfo(SharePref.getInstance(SecurrantyApp.context).getToken(),String.valueOf(daoFileClaim.getPolicyId())
            ).enqueue(new Callback<DeviceModel>() {
                @Override
                public void onResponse(Call<DeviceModel> call, Response<DeviceModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        try {
                            Gson gson = new Gson();
                            APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                            int statusCode = message.getStatusCode();
                            baseView.showErrorMsg(statusCode, message.getMessage());
                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                        }
                    }
                }

                @Override
                public void onFailure(Call<DeviceModel> call, Throwable t) {
                    baseView.hideProgress();
                    baseView.showErrorMsg(400, t.toString());
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getClaimType() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetClaimType();
    }

    private void apiGetClaimType() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getClaimType(SharePref.getInstance(SecurrantyApp.context).getToken(),String.valueOf(daoFileClaim.getPolicyId())
            ).enqueue(new Callback<ClaimType>() {
                @Override
                public void onResponse(Call<ClaimType> call, Response<ClaimType> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        Gson gson = new Gson();
                        APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                        baseView.showErrorMsg(message.getStatusCode(), message.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ClaimType> call, Throwable t) {
                    baseView.hideProgress();
                    baseView.showErrorMsg(400, t.toString());
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getSubClaimType(int selectedClaimTypeId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetSubClaimType(selectedClaimTypeId);
    }

    private void apiGetSubClaimType(int selectedClaimTypeId) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getSubClaimType(SharePref.getInstance(SecurrantyApp.context).getToken(),String.valueOf(daoFileClaim.getPolicyId()),
                    String.valueOf(selectedClaimTypeId)
            ).enqueue(new Callback<ClaimType>() {
                @Override
                public void onResponse(Call<ClaimType> call, Response<ClaimType> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        Gson gson = new Gson();
                        APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                        baseView.showErrorMsg(message.getStatusCode(), message.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ClaimType> call, Throwable t) {
                    baseView.hideProgress();
                    baseView.showErrorMsg(400, t.toString());
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getClaimPreference(int claimTypeId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetCalimPreference(claimTypeId);
    }

    private void apiGetCalimPreference(int claimTypeId) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getClaimPreference(SharePref.getInstance(SecurrantyApp.context).getToken(),claimTypeId
            ).enqueue(new Callback<ClaimPreference>() {
                @Override
                public void onResponse(Call<ClaimPreference> call, Response<ClaimPreference> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        Gson gson = new Gson();
                        APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                        baseView.showErrorMsg(message.getStatusCode(), message.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ClaimPreference> call, Throwable t) {
                    baseView.hideProgress();
                    baseView.showErrorMsg(400, t.toString());
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getShippingAddress() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetShippingAddress();
    }

    private void apiGetShippingAddress() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getShippingAddress(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<ShippingAddress>() {
                @Override
                public void onResponse(Call<ShippingAddress> call, Response<ShippingAddress> response) {
                    Log.i("Testing", "TEsting");
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        Gson gson = new Gson();
                        APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                        baseView.showErrorMsg(message.getStatusCode(), message.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ShippingAddress> call, Throwable t) {
                    baseView.hideProgress();
                    baseView.showErrorMsg(400, t.toString());
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getOtherAndCreditInfo() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetOtherAndCreditInfo();
    }

    private void apiGetOtherAndCreditInfo() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {

            Observable<PaymentandReview> paymentAndReview = ApiClient.getClient()
                    .getPaymentAndReview(SharePref.getInstance(SecurrantyApp.context).getToken(),String.valueOf(daoFileClaim.getPolicyId()))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());

            Observable<CreditCardList> creditCardList = ApiClient.getClient()
                    .getCreditcardInfo(SharePref.getInstance(SecurrantyApp.context).getToken(), SharePref.getInstance(SecurrantyApp.context).getUserId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());

            Observable<PaymentReviewandCountryList> combined = Observable.zip(paymentAndReview, creditCardList, new Func2<PaymentandReview, CreditCardList, PaymentReviewandCountryList>() {
                @Override
                public PaymentReviewandCountryList call(PaymentandReview jsonObject, CreditCardList jsonElements) {
                    return new PaymentReviewandCountryList(jsonObject, jsonElements);
                }
            });
            combined.subscribe(new Subscriber<PaymentReviewandCountryList>() {

                @Override
                public void onCompleted() {
                    Log.i("Status", "Completed");

                }

                @Override
                public void onError(Throwable e) {
                    Log.i("Status", "Error");
                    baseView.showErrorMsg(400, e.toString());
                    baseView.hideProgress();
                }

                @Override
                public void onNext(PaymentReviewandCountryList paymentReviewandCountryList) {
                    Log.i("PaymentReviewandCountry", "paymentandreview:" + paymentReviewandCountryList.getPaymentAndReview().getLostNotes());
                    Log.i("PaymentReviewandCountry", "country:" + paymentReviewandCountryList.getCreditCardList().getStatusCode());
                    baseView.success(paymentReviewandCountryList);
                    baseView.hideProgress();
                }
            });
        } else {
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
            baseView.hideProgress();
        }

    }

    public void submitClaim() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        //getRequestBody();
        apiSubmitClaim();
    }

    private void apiSubmitClaim() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().submitClaim(SharePref.getInstance(SecurrantyApp.context).getToken(), getRequestBody()
            ).enqueue(new Callback<SuccessClaimModel>() {
                @Override
                public void onResponse(Call<SuccessClaimModel> call, Response<SuccessClaimModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        Gson gson = new Gson();
                        APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                        baseView.showErrorMsg(message.getStatusCode(), message.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<SuccessClaimModel> call, Throwable t) {
                    baseView.hideProgress();
                    baseView.showErrorMsg(400, t.toString());
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private JsonObject getRequestBody() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("UserId", SharePref.getInstance(SecurrantyApp.context).getUserId());
        jsonObject.addProperty("PolicyId", daoFileClaim.getPolicyId());
        jsonObject.addProperty("DeviceId", daoFileClaim.getDeviceId());
        jsonObject.addProperty("ClaimTypeId", daoFileClaim.getClaimTypeId());
        jsonObject.addProperty("Deductable", daoFileClaim.getDeductible());
        jsonObject.add("SubClaimTypeId", getSubCalimIds(daoFileClaim.getSubClaimTypeIds()));
        jsonObject.add("ClaimFiles", new JsonArray());
        jsonObject.addProperty("IssueDate", daoFileClaim.getIssueDate());
        jsonObject.addProperty("ProblemDescription", daoFileClaim.getProblemDesc());
        jsonObject.addProperty("ClaimServicePereferenceId", daoFileClaim.getClaimServicePreferenceId());
        jsonObject.addProperty("ShippingAddressId", daoFileClaim.getShippingAddressId());
        jsonObject.addProperty("ConfirmDisabled", daoFileClaim.isConfirmDisabled());
        jsonObject.addProperty("ConfirmInfoTrue", daoFileClaim.isConfirmInfoTrue());
        jsonObject.addProperty("IsSLAFiveDay", daoFileClaim.isSLAFiveDay());
        jsonObject.addProperty("IsNotSLAFiveDay", daoFileClaim.isNotSLAFiveDay());
        jsonObject.addProperty("PayProfileId", String.valueOf(daoFileClaim.getPayProfileId()));
        JsonObject jsonFileClaim = new JsonObject();
        jsonFileClaim.add("FileClaim", jsonObject);
        Log.i("JsonObject", jsonFileClaim.toString());
        return jsonFileClaim;
    }

    private JsonArray getSubCalimIds(List<Long> subclaimsTypeId) {
        JsonArray jsonArray = new JsonArray();
        int subclaimsTypeIdSize = subclaimsTypeId.size();
        for (int i = 0; i < subclaimsTypeIdSize; i++) {
            jsonArray.add(subclaimsTypeId.get(i));
        }
        return jsonArray;
    }

}
