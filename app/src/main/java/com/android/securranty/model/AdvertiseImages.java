
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class AdvertiseImages {

    @SerializedName("adslist")
    private List<Adslist> Adslist;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Integer StatusCode;

    public List<Adslist> getAdslist() {
        return Adslist;
    }

    public void setAdslist(List<Adslist> adslist) {
        Adslist = adslist;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(Integer statusCode) {
        StatusCode = statusCode;
    }

    public class Adslist {

        @SerializedName("addimage")
        private String Addimage;
        @SerializedName("isRedirectUser")
        private String IsRedirectUser;
        @SerializedName("redirectUrl")
        private String RedirectUrl;

        public String getAddimage() {
            return Addimage;
        }

        public void setAddimage(String addimage) {
            Addimage = addimage;
        }

        public String getIsRedirectUser() {
            return IsRedirectUser;
        }

        public void setIsRedirectUser(String isRedirectUser) {
            IsRedirectUser = isRedirectUser;
        }

        public String getRedirectUrl() {
            return RedirectUrl;
        }

        public void setRedirectUrl(String redirectUrl) {
            RedirectUrl = redirectUrl;
        }

    }

}
