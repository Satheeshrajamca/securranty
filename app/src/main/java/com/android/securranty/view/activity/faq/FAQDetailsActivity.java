package com.android.securranty.view.activity.faq;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.android.securranty.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshr on 10-01-2018.
 */

public class FAQDetailsActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textQuestion)
    TextView textQuestion;
    @BindView(R.id.textAnswer)
    TextView textAnswer;
    @BindView(R.id.webViewAnswer)
    WebView webViewAnswer;
    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        displayQuestionAnser();
    }

    private void displayQuestionAnser() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        textQuestion.setTypeface(openSansSemiBold);
        textAnswer.setTypeface(openSansRegular);
        textQuestion.setText(getIntent().getStringExtra("QUESTION"));
        textAnswer.setText(getIntent().getStringExtra("ANSWER"));
        /*String textAnswer=getIntent().getStringExtra("ANSWER");
        String data = "<html><body>"+textAnswer+"</body></html>";
        webViewAnswer.loadData(data, "text/html", "UTF-8");*/
    }

    @OnClick({R.id.imageBackButton})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
        }
    }
}
