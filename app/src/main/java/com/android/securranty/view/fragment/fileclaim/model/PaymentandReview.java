
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class PaymentandReview {

    @Expose
    private List<ConfirmationNote> ConfirmationNotes;
    @Expose
    private String Deductibles;
    @Expose
    private String DeviceInfoNotes;
    @Expose
    private String FraudNotice;
    @Expose
    private String LostNotes;
    @Expose
    private String LostNotesLink;
    @Expose
    private String OtherInfo;
    @Expose
    private String ServicePreferenceNotes;
    @Expose
    private String ShppingNotes;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<ConfirmationNote> getConfirmationNotes() {
        return ConfirmationNotes;
    }

    public String getDeductibles() {
        return Deductibles;
    }

    public String getDeviceInfoNotes() {
        return DeviceInfoNotes;
    }

    public String getFraudNotice() {
        return FraudNotice;
    }

    public String getLostNotes() {
        return LostNotes;
    }

    public String getLostNotesLink() {
        return LostNotesLink;
    }

    public String getOtherInfo() {
        return OtherInfo;
    }

    public String getServicePreferenceNotes() {
        return ServicePreferenceNotes;
    }

    public String getShppingNotes() {
        return ShppingNotes;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<ConfirmationNote> ConfirmationNotes;
        private String Deductibles;
        private String DeviceInfoNotes;
        private String FraudNotice;
        private String LostNotes;
        private String LostNotesLink;
        private String OtherInfo;
        private String ServicePreferenceNotes;
        private String ShppingNotes;
        private String Status;
        private Long StatusCode;

        public PaymentandReview.Builder withConfirmationNotes(List<ConfirmationNote> ConfirmationNotes) {
            this.ConfirmationNotes = ConfirmationNotes;
            return this;
        }

        public PaymentandReview.Builder withDeductibles(String Deductibles) {
            this.Deductibles = Deductibles;
            return this;
        }

        public PaymentandReview.Builder withDeviceInfoNotes(String DeviceInfoNotes) {
            this.DeviceInfoNotes = DeviceInfoNotes;
            return this;
        }

        public PaymentandReview.Builder withFraudNotice(String FraudNotice) {
            this.FraudNotice = FraudNotice;
            return this;
        }

        public PaymentandReview.Builder withLostNotes(String LostNotes) {
            this.LostNotes = LostNotes;
            return this;
        }

        public PaymentandReview.Builder withLostNotesLink(String LostNotesLink) {
            this.LostNotesLink = LostNotesLink;
            return this;
        }

        public PaymentandReview.Builder withOtherInfo(String OtherInfo) {
            this.OtherInfo = OtherInfo;
            return this;
        }

        public PaymentandReview.Builder withServicePreferenceNotes(String ServicePreferenceNotes) {
            this.ServicePreferenceNotes = ServicePreferenceNotes;
            return this;
        }

        public PaymentandReview.Builder withShppingNotes(String ShppingNotes) {
            this.ShppingNotes = ShppingNotes;
            return this;
        }

        public PaymentandReview.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public PaymentandReview.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public PaymentandReview build() {
            PaymentandReview PaymentandReview = new PaymentandReview();
            PaymentandReview.ConfirmationNotes = ConfirmationNotes;
            PaymentandReview.Deductibles = Deductibles;
            PaymentandReview.DeviceInfoNotes = DeviceInfoNotes;
            PaymentandReview.FraudNotice = FraudNotice;
            PaymentandReview.LostNotes = LostNotes;
            PaymentandReview.LostNotesLink = LostNotesLink;
            PaymentandReview.OtherInfo = OtherInfo;
            PaymentandReview.ServicePreferenceNotes = ServicePreferenceNotes;
            PaymentandReview.ShppingNotes = ShppingNotes;
            PaymentandReview.Status = Status;
            PaymentandReview.StatusCode = StatusCode;
            return PaymentandReview;
        }

    }

}
