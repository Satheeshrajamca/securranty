
package com.android.securranty.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentHistoryModel {

    @SerializedName("paymentHistory")
    private List<PaymentHistory> PaymentHistory;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<PaymentHistory> getPaymentHistory() {
        return PaymentHistory;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public class PaymentHistory {

        @Expose
        private Object AmountDue;
        @Expose
        private String AmountPaid;
        @Expose
        private Object CheckNumber;
        @Expose
        private String CoverageTerm;
        @Expose
        private String CoverageType;
        @Expose
        private Object DateCreated;
        @Expose
        private String DeviceType;
        @Expose
        private Object DueDate;
        @Expose
        private Long Id;
        @Expose
        private Long InvoiceId;
        @Expose
        private String InvoiceType;
        @Expose
        private String PaymentDate;
        @Expose
        private Object PaymentDetails;
        @Expose
        private String PaymentMethod;
        @Expose
        private Object TransactionType;

        public Object getAmountDue() {
            return AmountDue;
        }

        public String getAmountPaid() {
            return AmountPaid;
        }

        public Object getCheckNumber() {
            return CheckNumber;
        }

        public String getCoverageTerm() {
            return CoverageTerm;
        }

        public String getCoverageType() {
            return CoverageType;
        }

        public Object getDateCreated() {
            return DateCreated;
        }

        public String getDeviceType() {
            return DeviceType;
        }

        public Object getDueDate() {
            return DueDate;
        }

        public Long getId() {
            return Id;
        }

        public Long getInvoiceId() {
            return InvoiceId;
        }

        public String getInvoiceType() {
            return InvoiceType;
        }

        public String getPaymentDate() {
            return PaymentDate;
        }

        public Object getPaymentDetails() {
            return PaymentDetails;
        }

        public String getPaymentMethod() {
            return PaymentMethod;
        }

        public Object getTransactionType() {
            return TransactionType;
        }
    }
}
