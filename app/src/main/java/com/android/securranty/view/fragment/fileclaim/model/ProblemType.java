
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class ProblemType {

    @Expose
    private int Id;
    @Expose
    private String Name;
    @Expose
    private Object ParentId;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public Object getParentId() {
        return ParentId;
    }

    public static class Builder {

        private int Id;
        private String Name;
        private Object ParentId;

        public ProblemType.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public ProblemType.Builder withName(String Name) {
            this.Name = Name;
            return this;
        }

        public ProblemType.Builder withParentId(Object ParentId) {
            this.ParentId = ParentId;
            return this;
        }

        public ProblemType build() {
            ProblemType ProblemType = new ProblemType();
            ProblemType.Id = Id;
            ProblemType.Name = Name;
            ProblemType.ParentId = ParentId;
            return ProblemType;
        }

    }

}
