package com.android.securranty.view.fragment.fileclaim;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.presenter.implementor.FileClaimPresenter;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.fileclaim.model.ClaimPreference;
import com.android.securranty.view.fragment.fileclaim.model.MesClaimServPreferencesage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;
import static com.android.securranty.view.fragment.fileclaim.ClaimTypeFragment.selectedClaimTypeId;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class ClaimPreferenceFragment extends Fragment implements BaseView {
    @BindView(R.id.radioClaimPreference)
    RadioGroup radiogroupClaimPreference;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.previous)
    TextView previous;
    @BindView(R.id.relativeClaimType)
    RelativeLayout relativeClaimType;

    private FileClaimsActivity context;
    private FileClaimPresenter fileclaimPresenter;
    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.layout_claim_preference,
                    container, false);
            ButterKnife.bind(this, view);
            previous.setVisibility(View.VISIBLE);
            fileclaimPresenter = new FileClaimPresenter(this);
            fileclaimPresenter.getClaimPreference(selectedClaimTypeId);
        }
        return view;
    }

    @OnClick({R.id.next, R.id.previous})
    public void onclickClaimType(View view) {
        switch (view.getId()) {
            case R.id.next:
                daoFileClaim.setClaimServicePreferenceId(Long.parseLong(claimServicePereferenceId));
                context.triggerClaim(new ShippingAddresFragment(), "Shipping Address");
                break;
            case R.id.previous:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeClaimType, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(getActivity(), SignInActivity.class), 401);
        }
    }

    private String claimServicePereferenceId;

    @Override
    public void success(Object obj) {
        List<MesClaimServPreferencesage> mesClaimServPreferencesageList = ClaimPreference.class.cast(obj).getMesClaimServPreferencesage();
        int sizeClaimServicePreference = mesClaimServPreferencesageList.size();
        final RadioButton[] rb = new RadioButton[sizeClaimServicePreference];
        radiogroupClaimPreference.setOrientation(RadioGroup.VERTICAL);
        for (int i = 0; i < sizeClaimServicePreference; i++) {
            rb[i] = new RadioButton(getActivity());
            rb[i].setText(mesClaimServPreferencesageList.get(i).getName());
            rb[i].setId(mesClaimServPreferencesageList.get(i).getId());
            radiogroupClaimPreference.addView(rb[i]);
        }
        radiogroupClaimPreference.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                Log.i("RadioGroupValue", "" + radioGroup.getCheckedRadioButtonId());
                claimServicePereferenceId = String.valueOf(radioGroup.getCheckedRadioButtonId());
                next.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            fileclaimPresenter.getClaimPreference(selectedClaimTypeId);
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
