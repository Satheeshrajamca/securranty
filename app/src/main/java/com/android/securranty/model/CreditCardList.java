package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreditCardList{

	@SerializedName("creditCardList")
	private List<CreditCardListItem> creditCardList;

	@SerializedName("statusCode")
	private int statusCode;

	@SerializedName("status")
	private String status;

	public List<CreditCardListItem> getCreditCardList(){
		return creditCardList;
	}

	public int getStatusCode(){
		return statusCode;
	}

	public String getStatus(){
		return status;
	}

    public class CreditCardListItem {

        @SerializedName("Zip")
        private String zip;

        @SerializedName("Email")
        private String email;

        @SerializedName("LastModBy")
        private String lastModBy;

        @SerializedName("CreatedById")
        private String createdById;

        @SerializedName("Phone")
        private String phone;

        @SerializedName("CardType")
        private String cardType;

        @SerializedName("LastModById")
        private String lastModById;

        @SerializedName("CreatedBy")
        private String createdBy;

        @SerializedName("FirstName")
        private String firstName;

        @SerializedName("Address2")
        private String address2;

        @SerializedName("ExMonth")
        private String exMonth;

        @SerializedName("Expired")
        private boolean expired;

        @SerializedName("Address1")
        private String address1;

        @SerializedName("City")
        private String city;

        @SerializedName("LastFour")
        private String lastFour;

        @SerializedName("Default")
        private boolean defaults;

        @SerializedName("AnPaymentProfileId")
        private String anPaymentProfileId;

        @SerializedName("LastModDate")
        private String lastModDate;

        @SerializedName("Active")
        private boolean active;

        @SerializedName("ExYear")
        private String exYear;

        @SerializedName("UserId")
        private String userId;

        @SerializedName("State")
        private String state;

        @SerializedName("CreatedDate")
        private String createdDate;

        @SerializedName("Country")
        private String country;

        @SerializedName("PrimaryCard")
        private String primaryCard;

        @SerializedName("Id")
        private int id;

        @SerializedName("LastName")
        private String lastName;

        public String getZip() {
            return zip;
        }

        public String getEmail() {
            return email;
        }

        public String getLastModBy() {
            return lastModBy;
        }

        public String getCreatedById() {
            return createdById;
        }

        public String getPhone() {
            return phone;
        }

        public String getCardType() {
            return cardType;
        }

        public String getLastModById() {
            return lastModById;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getAddress2() {
            return address2;
        }

        public String getExMonth() {
            return exMonth;
        }

        public boolean isExpired() {
            return expired;
        }

        public String getAddress1() {
            return address1;
        }

        public String getCity() {
            return city;
        }

        public String getLastFour() {
            return lastFour;
        }

        public boolean isDefault() {
            return defaults;
        }

        public String getAnPaymentProfileId() {
            return anPaymentProfileId;
        }

        public String getLastModDate() {
            return lastModDate;
        }

        public boolean isActive() {
            return active;
        }

        public String getExYear() {
            return exYear;
        }

        public String getUserId() {
            return userId;
        }

        public String getState() {
            return state;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public String getCountry() {
            return country;
        }

        public String getPrimaryCard() {
            return primaryCard;
        }

        public int getId() {
            return id;
        }

        public String getLastName() {
            return lastName;
        }
    }
}
