
package com.android.securranty.view.activity.plans.active_plan.model;

import com.google.gson.annotations.Expose;

import java.util.List;

@SuppressWarnings("unused")
public class ActivePlan {

    @Expose
    private List<Plan> plans;
    @Expose
    private String status;
    @Expose
    private Long statusCode;

    public List<Plan> getPlans() {
        return plans;
    }

    public String getStatus() {
        return status;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private List<Plan> plans;
        private String status;
        private Long statusCode;

        public ActivePlan.Builder withPlans(List<Plan> plans) {
            this.plans = plans;
            return this;
        }

        public ActivePlan.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public ActivePlan.Builder withStatusCode(Long statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public ActivePlan build() {
            ActivePlan activePlan = new ActivePlan();
            activePlan.plans = plans;
            activePlan.status = status;
            activePlan.statusCode = statusCode;
            return activePlan;
        }

    }

}
