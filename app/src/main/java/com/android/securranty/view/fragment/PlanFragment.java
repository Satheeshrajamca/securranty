package com.android.securranty.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity;
import com.android.securranty.view.activity.plans.buy_plan.AllPlansActivity;
import com.android.securranty.view.activity.plans.buy_plan.PlansCategoryActivity;
import com.android.securranty.view.activity.signs.SignInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.MainActivity.ACCOUNT_PROFILE_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.ACTIVE_PLAN;
import static com.android.securranty.view.activity.plans.buy_plan.PlansCategoryActivity.PLANID;

/**
 * Created by satheeshraja on 3/18/18.
 */

public class PlanFragment extends Fragment {
    @BindView(R.id.rootlayoutDashBoard)
    LinearLayout rootlayoutDashBoard;
    @BindView(R.id.textActivePlan)
    TextView textActivePlan;
    @BindView(R.id.textBuyPlan)
    TextView textBuyPlan;
    private MainActivity mainActivity;
    public static int PURCHASE_PLAN_REQ_CODE = 300;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plans,
                container, false);
        ButterKnife.bind(this, view);
        setTypeFace();
        return view;
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/opensans_semibold.ttf");
        textActivePlan.setTypeface(openSansSemiBold);
        textBuyPlan.setTypeface(openSansSemiBold);
    }

    @OnClick({R.id.textActivePlan, R.id.textBuyPlan})
    public void onClickMyAccount(View view) {
        if (Utils.isNetworkAvailable(getActivity())) {
            switch (view.getId()) {
                case R.id.textActivePlan:
                    if (Utils.isNetworkAvailable(getActivity())) {
                        if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                            startActivity(new Intent(getActivity(), ActivePlanListActivity.class));
                        } else {
                            Intent i = new Intent(getActivity(), SignInActivity.class);
                            startActivityForResult(i, ACTIVE_PLAN);
                        }
                    } else {
                        showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
                    }
                    break;
                case R.id.textBuyPlan:
                    startActivityForResult(new Intent(getActivity(), AllPlansActivity.class).putExtra(PLANID, "0"), PURCHASE_PLAN_REQ_CODE);
                    break;

            }
        } else {
            showErrorMsg(200, getActivity().getResources().getString(R.string.msg_no_internet));
        }
    }

    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(rootlayoutDashBoard, errorMsg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ACTIVE_PLAN || requestCode == PURCHASE_PLAN_REQ_CODE) {
                startActivity(new Intent(getActivity(), ActivePlanListActivity.class));
                mainActivity.changeLoginStatus("Logout");
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
            mainActivity.changeLoginStatus("Logout");
        } else {
            mainActivity.changeLoginStatus("Login");
        }
    }
}
