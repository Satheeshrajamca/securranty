package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.utility.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.AdvertiseImages;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshraja on 11/26/17.
 */

public class DashBoardPresenterInter {
    private BasePresenter basePresenter;

    public DashBoardPresenterInter(BasePresenter basePresenter) {
        this.basePresenter = basePresenter;
    }

    public void getAdvertiseImages() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getAdvertiseImages().enqueue(new Callback<AdvertiseImages>() {
                @Override
                public void onResponse(Call<AdvertiseImages> call, Response<AdvertiseImages> response) {
                    if (response.isSuccessful()) {
                        AdvertiseImages loginResponse = response.body();
                        if (loginResponse.getStatusCode() == 200) {
                            String json = new Gson().toJson(response);
                            basePresenter.success(loginResponse);
                        } else {
                            basePresenter.errorMsg(loginResponse.getStatusCode(), "Invalid User");
                        }

                    } else {
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<AdvertiseImages> call, Throwable t) {
                    if (t instanceof HttpException)
                        errorResponse(((HttpException) t).response().errorBody());
                    else
                        basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });

        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            basePresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
