package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.AddCreditcardPresenterInter;
import com.android.securranty.presenter.interactor.MyAccountPresenterInteractor;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshr on 04-12-2017.
 */

public class AddCreditcardPresenterImpl implements BasePresenter {
    AddCreditcardPresenterInter addCreditcardPresenterInter;
    private BaseView baseView;
    MyAccountPresenterInteractor myAccountPresenterInt;

    public AddCreditcardPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        addCreditcardPresenterInter = new AddCreditcardPresenterInter(this);
        this.myAccountPresenterInt = new MyAccountPresenterInteractor(this);
    }

    public void validateCredential(String creditCardNo, String expireMonth, String expireYear, String cvv, String firstName, String lastName,
                                   String billingEmail, String phone, String billingAdd1, String billingAdd2, String city,
                                   String state, String country, String zip, boolean defaults, boolean active) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        addCreditcardPresenterInter.updateCreditCardInfo(creditCardNo, expireMonth, expireYear, cvv, firstName, lastName,
                billingEmail, phone, billingAdd1, billingAdd2, city,
                state, country, zip, defaults, active);
    }

    @Override
    public void success(Object obj) {
        baseView.hideProgress();
        baseView.success(obj);
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.showErrorMsg(statusCode, msg);
        }
    }

    @Override
    public void onDestroy() {
        if (baseView != null) {
            baseView = null;
        }
    }

    public void initStatesList(String countryName, int selectedCountryId) {
        if (baseView != null) {
            baseView.showProgress();
        }
        myAccountPresenterInt.getStatesList(countryName, selectedCountryId);
    }

    public void initCountryList() {
        if (baseView != null) {
            baseView.showProgress();
        }
        myAccountPresenterInt.getCountryList();
    }
}
