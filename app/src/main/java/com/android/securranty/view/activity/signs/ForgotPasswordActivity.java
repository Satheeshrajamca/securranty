package com.android.securranty.view.activity.signs;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.presenter.implementor.ForgotPassPresenterImpl;
import com.android.securranty.view.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshr on 17-11-2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.editEmail)
    EditText editEmail;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.relativeForgotPass)
    RelativeLayout relativeForgotPass;
    @BindView(R.id.textForgotPass)
    TextView textForgotPass;

    private ForgotPassPresenterImpl presenterForgotPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpass);
        ButterKnife.bind(this);
        presenterForgotPass = new ForgotPassPresenterImpl(this);
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        textForgotPass.setTypeface(openSansSemiBold);
        editEmail.setTypeface(openSansSemiBold);
        btnSubmit.setTypeface(openSansSemiBold);
    }

    @OnClick(R.id.btnSubmit)
    public void onClickForgotPass(View view) {
        presenterForgotPass.validateCredential(editEmail.getText().toString().trim());
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeForgotPass, errorMsg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void success(Object obj) {
        finish();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenterForgotPass.onDestroy();
    }
}
