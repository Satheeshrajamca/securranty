package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import android.util.Log;

import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FileClaimModel {
    private Long policyId;
    private Long deviceId;
    private int deductible;
    private boolean isSLAFiveDay;
    private boolean isNotSLAFiveDay;
    private ArrayList<String> claimFile;

    @Override
    public String toString() {
        return "FileClaimModel{" +
                "policyId=" + policyId +
                ", deviceId='" + deviceId + '\'' +
                ", claimTypeId=" + claimTypeId +
                ", subClaimTypeIds=" + subClaimTypeIds +
                ", issueDate='" + issueDate + '\'' +
                ", problemDesc='" + problemDesc + '\'' +
                ", claimServicePreferenceId='" + claimServicePreferenceId + '\'' +
                ", shippingAddressId=" + shippingAddressId +
                ", confirmDisabled=" + confirmDisabled +
                ", confirmInfoTrue=" + confirmInfoTrue +
                ", payProfileId=" + payProfileId +
                '}';
    }

    private int claimTypeId;
    private List<Long> subClaimTypeIds;
    private String issueDate;
    private String problemDesc;
    private Long claimServicePreferenceId;
    private int shippingAddressId;
    private boolean confirmDisabled;
    private boolean confirmInfoTrue;
    private int payProfileId;

    public Long getPolicyId() {
        return policyId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public int getClaimTypeId() {
        return claimTypeId;
    }

    public List<Long> getSubClaimTypeIds() {
        return subClaimTypeIds;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public String getProblemDesc() {
        return problemDesc;
    }

    public Long getClaimServicePreferenceId() {
        return claimServicePreferenceId;
    }

    public int getShippingAddressId() {
        return shippingAddressId;
    }

    public boolean isConfirmDisabled() {
        return confirmDisabled;
    }

    public boolean isConfirmInfoTrue() {
        return confirmInfoTrue;
    }

    public int getPayProfileId() {
        return payProfileId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public void setClaimTypeId(int claimTypeId) {
        this.claimTypeId = claimTypeId;
    }

    public void setSubClaimTypeIds(List<Long> subClaimTypeIds) {
        this.subClaimTypeIds = subClaimTypeIds;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public void setProblemDesc(String problemDesc) {
        this.problemDesc = problemDesc;
    }

    public void setClaimServicePreferenceId(Long claimServicePreferenceId) {
        this.claimServicePreferenceId = claimServicePreferenceId;
    }

    public void setShippingAddressId(int shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public void setConfirmDisabled(boolean confirmDisabled) {
        this.confirmDisabled = confirmDisabled;
    }

    public void setConfirmInfoTrue(boolean confirmInfoTrue) {
        this.confirmInfoTrue = confirmInfoTrue;
    }

    public void setPayProfileId(int payProfileId) {
        this.payProfileId = payProfileId;
    }

    public void setDeductible(int deductible) {
        this.deductible = deductible;
    }

    public void setIsSLAFiveDay(boolean isSLAFiveDay) {
        this.isSLAFiveDay = isSLAFiveDay;
    }

    public int getDeductible() {
        return deductible;
    }

    public boolean isSLAFiveDay() {
        return isSLAFiveDay;
    }

    public boolean isNotSLAFiveDay() {
        return isNotSLAFiveDay;
    }

    public ArrayList<String> getClaimFile() {
        return claimFile;
    }

    public void setIsNotSLAFiveDay(boolean isNotSLAFiveDay) {
        this.isNotSLAFiveDay = isNotSLAFiveDay;
    }

    public void setClaimFile(ArrayList<String> claimFile) {
        this.claimFile = claimFile;
    }

    public String toJSON(){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("UserId", SharePref.getInstance(SecurrantyApp.context).getUserId());
            jsonObject.put("PolicyId", getPolicyId());
            jsonObject.put("DeviceId", getDeviceId());
            jsonObject.put("ClaimTypeId", getClaimTypeId());
            jsonObject.put("SubClaimTypeId", getSubClaimTypeIds());
            jsonObject.put("IssueDate", getIssueDate());
            jsonObject.put("ProblemDescription", getProblemDesc());
            jsonObject.put("Deductable",getDeductible());
            jsonObject.put("ClaimFiles", getClaimFile());
            jsonObject.put("ClaimServicePereferenceId", getClaimServicePreferenceId());
            jsonObject.put("ShippingAddressId", getShippingAddressId());
            jsonObject.put("ConfirmDisabled", isConfirmDisabled());
            jsonObject.put("ConfirmInfoTrue", isConfirmInfoTrue());
            jsonObject.put("IsSLAFiveDay", isSLAFiveDay());
            jsonObject.put("IsNotSLAFiveDay", isNotSLAFiveDay());
            /*JSONArray jsonArray=new JSONArray();
            jsonArray.put(jsonObject);*/
            jsonObject.put("PayProfileId", getPayProfileId());
            JSONObject jsonFileClaim = new JSONObject();
            jsonFileClaim.put("FileClaim", jsonObject);

            return jsonFileClaim.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }

    private JsonObject getRequestBody() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("UserId", SharePref.getInstance(SecurrantyApp.context).getUserId());
        jsonObject.addProperty("PolicyId", 30990);
        jsonObject.addProperty("DeviceId", 33640);
        jsonObject.addProperty("ClaimTypeId", 12);
        jsonObject.add("SubClaimTypeId", addValue());
        jsonObject.addProperty("IssueDate", "2018-03-20");
        jsonObject.addProperty("ProblemDescription", "sample string 6sample string 6sample string6sample string 6sample string 6sample string 6sample string 6sample string6sample string 6sample string 6sample string 6sample string 6sample string 6sample string 6");
        jsonObject.addProperty("ClaimServicePereferenceId", 3);
        jsonObject.addProperty("ShippingAddressId", 517);
        jsonObject.addProperty("ConfirmInfoTrue", true);
        jsonObject.addProperty("IsSLAFiveDay", true);
        jsonObject.addProperty("IsNotSLAFiveDay", true);
        jsonObject.addProperty("PayProfileId", "");
        JsonObject jsonFileClaim = new JsonObject();
        jsonFileClaim.add("FileClaim", jsonObject);
        Log.i("JsonObject", jsonFileClaim.toString());
        return jsonFileClaim;
    }

    private JsonArray addValue() {
        /*List<Integer> list = new ArrayList<>();
        list.add(13);
        list.add(53);
        String s = TextUtils.join(", ", list);*/
        JsonArray jsonArray=new JsonArray();
        jsonArray.add(13);
        jsonArray.add(53);
        return jsonArray;
    }
}
