package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.TicketListPresenterInter;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class TicketListPresenterImpl implements BasePresenter {
    BaseView baseView;
    TicketListPresenterInter ticketListPresenterInter;

    public TicketListPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        ticketListPresenterInter = new TicketListPresenterInter(this);
    }

    public void initPaymentList() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        ticketListPresenterInter.getPaymentList();
    }

    public void initTicketList() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        ticketListPresenterInter.getCreditCardList();
    }

    @Override
    public void success(Object obj) {
        baseView.hideProgress();
        baseView.success(obj);
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.showErrorMsg(statusCode, msg);
        }
    }

    @Override
    public void onDestroy() {
        if (baseView != null) {
            baseView = null;
        }
    }
}
