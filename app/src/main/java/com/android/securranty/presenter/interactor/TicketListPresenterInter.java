package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.PaymentHistoryModel;
import com.android.securranty.presenter.implementor.TicketListPresenterImpl;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.tickets.model.TicketList;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class TicketListPresenterInter {

    TicketListPresenterImpl ticketListPresenterInter;

    public TicketListPresenterInter(TicketListPresenterImpl ticketListPresenterInter) {
        this.ticketListPresenterInter = ticketListPresenterInter;
    }

    public void getCreditCardList() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            apiCall(SharePref.getInstance(SecurrantyApp.context).getToken(), SharePref.getInstance(SecurrantyApp.context).getUserId());
        } else {
            ticketListPresenterInter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void apiCall(String token, String userId) {
        ApiClient.getClient().getTicketList(token, userId).enqueue(new Callback<TicketList>() {
            @Override
            public void onResponse(Call<TicketList> call, Response<TicketList> response) {
                if (response.isSuccessful()) {
                    ticketListPresenterInter.success(response.body());
                } else {
                    errorResponse(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<TicketList> call, Throwable t) {
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    ticketListPresenterInter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    public void getPaymentList() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getPaymentList(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<PaymentHistoryModel>() {
                @Override
                public void onResponse(Call<PaymentHistoryModel> call, Response<PaymentHistoryModel> response) {
                    if (response.isSuccessful()) {
                        ticketListPresenterInter.success(response.body());
                    } else {
                        try {
                            Gson gson = new Gson();
                            APIError message = gson.fromJson(response.errorBody().charStream(), APIError.class);
                            int statusCode = message.getStatusCode();
                            ticketListPresenterInter.errorMsg(statusCode, message.getMessage());
                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            ticketListPresenterInter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                        }
                    }
                }

                @Override
                public void onFailure(Call<PaymentHistoryModel> call, Throwable t) {
                    ticketListPresenterInter.errorMsg(400, t.toString());
                }
            });
        } else {
            ticketListPresenterInter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            ticketListPresenterInter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            ticketListPresenterInter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
