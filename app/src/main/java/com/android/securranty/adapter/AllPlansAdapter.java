package com.android.securranty.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.AdPage;

import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class AllPlansAdapter extends RecyclerView.Adapter<AllPlansAdapter.ViewHolder> {
    private Context context;
    private List<AdPage> listAdPage;

    public AllPlansAdapter(Context context, List<AdPage> listAdPage) {
        this.context = context;
        this.listAdPage = listAdPage;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_all_plans, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (listAdPage.get(position).getVisible()) {
            holder.textPlanName.setText(listAdPage.get(position).getPageName());
        }
    }

    @Override
    public int getItemCount() {
        return listAdPage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textPlanName;

        public ViewHolder(View itemView) {
            super(itemView);
            textPlanName = (TextView) itemView.findViewById(R.id.textPlanName);
            Typeface openSansSemiBold = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_semibold.ttf");
            textPlanName.setTypeface(openSansSemiBold);

        }
    }
}
