
package com.android.securranty.view.activity.plans.active_plan.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProofofPurchase {

    @SerializedName("FileName")
    private String fileName;
    @SerializedName("FilePath")
    private String filePath;
    @SerializedName("Id")
    private Long id;

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public Long getId() {
        return id;
    }

    public static class Builder {

        private String fileName;
        private String filePath;
        private Long id;

        public ProofofPurchase.Builder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public ProofofPurchase.Builder withFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public ProofofPurchase.Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public ProofofPurchase build() {
            ProofofPurchase proofofPurchase = new ProofofPurchase();
            proofofPurchase.fileName = fileName;
            proofofPurchase.filePath = filePath;
            proofofPurchase.id = id;
            return proofofPurchase;
        }

    }

}
