package com.android.securranty.view.fragment.fileclaim;

import com.google.gson.JsonObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.presenter.implementor.FileClaimPresenter;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.fileclaim.model.PaymentReviewandCountryList;
import com.android.securranty.view.fragment.fileclaim.model.PaymentandReview;
import com.android.securranty.view.fragment.fileclaim.model.SuccessClaimModel;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;
import static com.android.securranty.view.fragment.fileclaim.ClaimTypeFragment.claimType;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class SubmitClaimFragment extends Fragment implements BaseView, CheckBox.OnCheckedChangeListener {
    @BindView(R.id.textDeductibleAmt)
    TextView textDeductibleAmt;
    @BindView(R.id.relativeClaimType)
    RelativeLayout relativeClaimType;
    @BindView(R.id.textCreditCard)
    TextView textCreditCard;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.previous)
    TextView previous;
    /*@BindView(R.id.firstCheckbox)
    CheckBox firstCheckbox;
    @BindView(R.id.secondCheckbox)
    CheckBox secondCheckbox;*/
    @BindView(R.id.checkboxLayout)
    LinearLayout checkboxLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private FileClaimsActivity context;
    private View view;
    private FileClaimPresenter fileClaimPresenter;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.payment_deductible,
                    container, false);
            ButterKnife.bind(this, view);
            fileClaimPresenter = new FileClaimPresenter(this);
            fileClaimPresenter.getOtherAndCreditInfo();
            previous.setVisibility(View.VISIBLE);
            next.setVisibility(View.VISIBLE);
            next.setText("Submit Claim");
        }
        return view;
    }

    private List<SearchListItem> listCreditCard;

    @OnClick({R.id.next, R.id.previous, R.id.btnEditCreditCard})
    public void onclickClaimType(View view) {
        switch (view.getId()) {
            case R.id.next:
                submitClaim();
                break;
            case R.id.previous:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
            case R.id.btnEditCreditCard:
                if (listCreditCard != null) {
                    displayCreditCard(listCreditCard);
                }
                break;
        }
    }

    private boolean isAllCheckboxSelected;

    private void submitClaim() {
        isAllCheckboxSelected = true;
        Iterator it = hashmapCheckBox.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (pair.getValue().equals(false)) {
                isAllCheckboxSelected = false;
            }

        }
        if (isAllCheckboxSelected) {
            if (!TextUtils.isEmpty(textCreditCard.getText().toString().trim())) {
                daoFileClaim.setConfirmDisabled(true);
                daoFileClaim.setConfirmInfoTrue(true);
                daoFileClaim.setPayProfileId(payProfileId);
                daoFileClaim.setDeductible(deductibleAmt);
                daoFileClaim.setIsSLAFiveDay(true);
                daoFileClaim.setIsNotSLAFiveDay(true);
                //daoFileClaim.withFileClaim(new ArrayList<String>());
                //daoFileClaim.withFileClaim(new FileClaim());
                //daoFileClaim.(SharePref.getInstance(SecurrantyApp.context).getUserId());
                Log.i("DAOClaim", daoFileClaim.toString());
                fileClaimPresenter.submitClaim();
            } else {
                Toast.makeText(context, "Please select billing method", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Please select terms and conditions", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean confirmDisabled, confirmInfoTrue;

    /*@OnCheckedChanged(R.id.firstCheckbox)
    public void onCheckedFirst(boolean isChecked) {
        confirmDisabled = isChecked;
    }

    @OnCheckedChanged(R.id.secondCheckbox)
    public void onCheckedSecond(boolean isChecked) {
        confirmInfoTrue = isChecked;
    }*/

    private int payProfileId;

    private void displayCreditCard(List<SearchListItem> listCreditCard) {
        final SearchableDialog searchableDialog = new SearchableDialog(getActivity(), listCreditCard, "Select Credit Card");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                textCreditCard.setText(getResources().getString(R.string.str_credit_card, Html.fromHtml(searchListItem.getTitle())));
                payProfileId = searchListItem.getId();
            }
        });
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        new iOSDialogBuilder(getContext())
                .setTitle("File Claim")
                .setSubtitle(errorMsg)
                .setBoldPositiveLabel(true)
                .setCancelable(false)
                .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        dialog.dismiss();
                    }
                }).build().show();

        //Snackbar.make(relativeClaimType, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(getActivity(), SignInActivity.class), 401);
        }
    }

    Map<String, Boolean> hashmapCheckBox;
    int deductibleAmt;

    @Override
    public void success(Object obj) {
        if (obj instanceof PaymentReviewandCountryList) {
            listCreditCard = new ArrayList<>();
            PaymentReviewandCountryList paymentReviewandCountryList = PaymentReviewandCountryList.class.cast(obj);
            //firstCheckbox.setText(paymentReviewandCountryList.getPaymentAndReview().getConfirmationNotes().get(0).getValue());
            //secondCheckbox.setText(paymentReviewandCountryList.getPaymentAndReview().getConfirmationNotes().get(1).getValue());
            CreditCardList creditCardList = paymentReviewandCountryList.getCreditCardList();
            PaymentandReview paymentandReview = paymentReviewandCountryList.getPaymentAndReview();
            String deductible = paymentandReview.getDeductibles();
            Log.i("CreditcardlistSize", "" + paymentReviewandCountryList.getCreditCardList().getCreditCardList().size());
            for (CreditCardList.CreditCardListItem creditCardListItem : paymentReviewandCountryList.getCreditCardList().getCreditCardList()) {
                listCreditCard.add(new SearchListItem(creditCardListItem.getId(), creditCardListItem.getCardType() + " XXXXXXXXXXXX" + creditCardListItem.getLastFour()));
                if (creditCardListItem.getPrimaryCard().equalsIgnoreCase("Yes") || creditCardListItem.isDefault()) {
                    String creditCard = "<b>" + creditCardListItem.getCardType() + " XXXXXXXXXXXX" + creditCardListItem.getLastFour() + "</b> ";
                    textCreditCard.setText(getResources().getString(R.string.str_credit_card, Html.fromHtml(creditCard)));
                    payProfileId = creditCardListItem.getId();
                }
            }
            if (TextUtils.isEmpty(textCreditCard.getText().toString().trim())) {
                CreditCardList.CreditCardListItem creditCardListItem = paymentReviewandCountryList.getCreditCardList().getCreditCardList().get(0);
                String creditCard = "<b>" + creditCardListItem.getCardType() + " XXXXXXXXXXXX" + creditCardListItem.getLastFour() + "</b> ";
                textCreditCard.setText(getResources().getString(R.string.str_credit_card, Html.fromHtml(creditCard)));
                payProfileId = creditCardListItem.getId();
            }
            //
            hashmapCheckBox = new HashMap<>();
            for (int i = 0; i < paymentReviewandCountryList.getPaymentAndReview().getConfirmationNotes().size(); i++) {
                CheckBox cb = new CheckBox(getActivity());
                cb.setText(paymentReviewandCountryList.getPaymentAndReview().getConfirmationNotes().get(i).getValue());
                cb.setTag(paymentReviewandCountryList.getPaymentAndReview().getConfirmationNotes().get(i).getBindFieldName());
                checkboxLayout.addView(cb);
                cb.setOnCheckedChangeListener(this);
                hashmapCheckBox.put(paymentReviewandCountryList.getPaymentAndReview().getConfirmationNotes().get(i).getBindFieldName(), false);
            }
            //
            //Get Deductibles
            String deductibles = paymentReviewandCountryList.getPaymentAndReview().getDeductibles();
            String[] result = deductibles.replaceAll("[^a-zA-Z0-9,]", "").split(",");
            HashMap<String, Integer> mapDeductibles = new HashMap<>();
            int length = (result.length) / 2;
            int j = 0;
            for (int i = 0; i < length; i++) {
                mapDeductibles.put(result[j], Integer.parseInt(result[j + 1]));
                j = j + 2;
            }
            Log.i("Dedcutionles", mapDeductibles.toString());
            if (claimType.equals("Mechanical Failure")) {
                deductibleAmt = mapDeductibles.get("ME");

            } else if (claimType.equals("Accidental Damage")) {
                deductibleAmt = mapDeductibles.get("ADH");
            }
            textDeductibleAmt.setText("$" + deductibleAmt);

        } else {
            SuccessClaimModel claimModel = SuccessClaimModel.class.cast(obj);
            new iOSDialogBuilder(getContext())
                    .setTitle("File Claim")
                    .setSubtitle("Your claim has been successfully submitted. Your claim ID is: " + claimModel.getClaimId() + ". Our team is now hard at work to provide you a simple, fast and hassle-free claims process. You can log into your account to check status of your claim including tracking & shipping information details.\n\n A confirmation email has been sent to you. If you do not see the email in your Inbox, please check your SPAM folder and add Securranty to your contacts so important email communications from our team members are received in your Inbox. \n\n If you have made an error in submitting the claim, please navigate to Claims to CANCEL the claim. For assistance, please open a Support Ticket.")
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            dialog.dismiss();
                            Intent intent = getActivity().getIntent();
                            getActivity().setResult(RESULT_OK, intent);
                            getActivity().finish();
                        }
                    }).build().show();
        }
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            submitClaim();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param buttonView The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String key = buttonView.getTag().toString();
        hashmapCheckBox.put(buttonView.getTag().toString(), isChecked);
        Log.i("Key", hashmapCheckBox.toString());
    }
}
