package com.android.securranty.view.fragment.fileclaim;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.DeviceInfoAdapter;
import com.android.securranty.model.DeviceModel;
import com.android.securranty.model.UserDevice;
import com.android.securranty.presenter.implementor.FileClaimPresenter;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.signs.SignInActivity;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class ConfirmPolicyFragment extends Fragment implements BaseView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.relativeConfirmPolicy)
    RelativeLayout relativeConfirmPolicy;
    @BindView(R.id.next)
    TextView next;

    private FileClaimPresenter fileclaimPresenter;
    FileClaimsActivity context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    public interface LaunchClaimType {
        void triggerClaim(Fragment fragment, String title);
    }

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.layout_confirm_policy,
                    container, false);
            ButterKnife.bind(this, view);
            fileclaimPresenter = new FileClaimPresenter(this);
            fileclaimPresenter.getDeviceInfo();
        }
        return view;
    }

    @OnClick({R.id.next})
    public void onclickChangePassword(View view) {
        switch (view.getId()) {
            case R.id.next:
                daoFileClaim.setDeviceId(Long.parseLong(deviceId));
                context.triggerClaim(new ClaimTypeFragment(), "Select Claim Type");
                break;
        }
    }

    @OnCheckedChanged(R.id.checkboxTermsandCondition)
    public void onCheckedChanged(boolean isChecked) {
        if (isChecked) {
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeConfirmPolicy, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(getActivity(), SignInActivity.class), 401);
        }
    }

    private DeviceInfoAdapter deviceInfoAdapter;
    private Map<String, String> hashDeviceInfo;
    private String deviceId;

    @Override
    public void success(Object obj) {
        UserDevice deviceInfo = DeviceModel.class.cast(obj).getUserDevice();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity());
        hashDeviceInfo = new LinkedHashMap<>();
        hashDeviceInfo.put("First Name", deviceInfo.getFirstName());
        hashDeviceInfo.put("Last Name", deviceInfo.getLastName());
        hashDeviceInfo.put("Model", deviceInfo.getModel());
        hashDeviceInfo.put("Phone", deviceInfo.getPhoneNumber());
        hashDeviceInfo.put("Carrier", deviceInfo.getCarrier());
        hashDeviceInfo.put("Serial", deviceInfo.getSerialNumber());
        hashDeviceInfo.put("IMEI", deviceInfo.getIMEI());
        hashDeviceInfo.put("Division", deviceInfo.getDivision());

        deviceId = String.valueOf(deviceInfo.getDeviceId());
        recyclerView.setLayoutManager(linearLayoutManager);
        deviceInfoAdapter = new DeviceInfoAdapter();
        deviceInfoAdapter.setData(hashDeviceInfo);
        recyclerView.setAdapter(deviceInfoAdapter);
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            fileclaimPresenter.getDeviceInfo();
        }
        else
        {
            getActivity().finish();
        }
    }
}
