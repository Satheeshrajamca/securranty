
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

import java.util.List;

@SuppressWarnings("unused")
public class GetCoverageType {

    @Expose
    private List<CoverageType> coverageType;
    @Expose
    private String coverageTypeInfo;
    @Expose
    private String status;
    @Expose
    private int statusCode;

    public List<CoverageType> getCoverageType() {
        return coverageType;
    }

    public String getCoverageTypeInfo() {
        return coverageTypeInfo;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private List<CoverageType> coverageType;
        private String coverageTypeInfo;
        private String status;
        private int statusCode;

        public GetCoverageType.Builder withCoverageType(List<CoverageType> coverageType) {
            this.coverageType = coverageType;
            return this;
        }

        public GetCoverageType.Builder withCoverageTypeInfo(String coverageTypeInfo) {
            this.coverageTypeInfo = coverageTypeInfo;
            return this;
        }

        public GetCoverageType.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public GetCoverageType.Builder withStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public GetCoverageType build() {
            GetCoverageType getCoverageType = new GetCoverageType();
            getCoverageType.coverageType = coverageType;
            getCoverageType.coverageTypeInfo = coverageTypeInfo;
            getCoverageType.status = status;
            getCoverageType.statusCode = statusCode;
            return getCoverageType;
        }

    }

}
