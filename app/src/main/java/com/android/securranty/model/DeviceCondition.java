
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class DeviceCondition {

    @Expose
    private Boolean Active;
    @Expose
    private List<Object> BasePlan;
    @Expose
    private int Id;
    @Expose
    private String Name;
    @Expose
    private List<Object> UserDevice;
    @Expose
    private List<Object> UserDeviceHistory;
    @Expose
    private List<Object> UserPlan;

    public Boolean getActive() {
        return Active;
    }

    public List<Object> getBasePlan() {
        return BasePlan;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public List<Object> getUserDevice() {
        return UserDevice;
    }

    public List<Object> getUserDeviceHistory() {
        return UserDeviceHistory;
    }

    public List<Object> getUserPlan() {
        return UserPlan;
    }

    public static class Builder {

        private Boolean Active;
        private List<Object> BasePlan;
        private int Id;
        private String Name;
        private List<Object> UserDevice;
        private List<Object> UserDeviceHistory;
        private List<Object> UserPlan;

        public DeviceCondition.Builder withActive(Boolean Active) {
            this.Active = Active;
            return this;
        }

        public DeviceCondition.Builder withBasePlan(List<Object> BasePlan) {
            this.BasePlan = BasePlan;
            return this;
        }

        public DeviceCondition.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public DeviceCondition.Builder withName(String Name) {
            this.Name = Name;
            return this;
        }

        public DeviceCondition.Builder withUserDevice(List<Object> UserDevice) {
            this.UserDevice = UserDevice;
            return this;
        }

        public DeviceCondition.Builder withUserDeviceHistory(List<Object> UserDeviceHistory) {
            this.UserDeviceHistory = UserDeviceHistory;
            return this;
        }

        public DeviceCondition.Builder withUserPlan(List<Object> UserPlan) {
            this.UserPlan = UserPlan;
            return this;
        }

        public DeviceCondition build() {
            DeviceCondition DeviceCondition = new DeviceCondition();
            DeviceCondition.Active = Active;
            DeviceCondition.BasePlan = BasePlan;
            DeviceCondition.Id = Id;
            DeviceCondition.Name = Name;
            DeviceCondition.UserDevice = UserDevice;
            DeviceCondition.UserDeviceHistory = UserDeviceHistory;
            DeviceCondition.UserPlan = UserPlan;
            return DeviceCondition;
        }

    }

}
