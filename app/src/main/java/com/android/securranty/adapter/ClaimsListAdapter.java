package com.android.securranty.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.Claim;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class ClaimsListAdapter extends RecyclerView.Adapter<ClaimsListAdapter.ViewHolder> implements Filterable {
    private List<Claim> claimList;
    private List<Claim> filteredClaimList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_claim_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textClaimId.setText(String.valueOf(filteredClaimList.get(position).getId()));
        holder.textClaimStatus.setText(filteredClaimList.get(position).getStatus());
        holder.textModel.setText(filteredClaimList.get(position).getModel());
        //String date = filteredClaimList.get(position).getCreatedDate().split("T")[0];
        //holder.textDatepriority.setText(date + " | " + filteredClaimList.get(position).getPolicyId());
        //holder.textDatepriority.setText(date);
        /*if (filteredClaimList.get(position).getActive()) {
            holder.textClaimStatus.setTextColor(SecurrantyApp.context.getResources().getColor(android.R.color.holo_green_dark));
        } else {
            holder.textClaimStatus.setTextColor(SecurrantyApp.context.getResources().getColor(R.color.red_color));
        }
        holder.textClaimStatus.setText(getStatus(filteredClaimList.get(position).getActive()));*/
    }

    @Override
    public int getItemCount() {
        return filteredClaimList.size();
    }

    public void setData(List<Claim> claimList) {
        this.claimList = claimList;
        filteredClaimList = claimList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textClaimId, textClaimStatus, textModel;

        public ViewHolder(View itemView) {
            super(itemView);
            textClaimId = (TextView) itemView.findViewById(R.id.textClaimNo);
            textClaimStatus = (TextView) itemView.findViewById(R.id.textClaimStatus);
            textModel = (TextView) itemView.findViewById(R.id.textModel);
            textModel.setVisibility(View.VISIBLE);
            textClaimStatus.setVisibility(View.VISIBLE);
            Typeface openSansRegular = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_regular.ttf");
            Typeface openSansSemiBold = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_semibold.ttf");
            textClaimId.setTypeface(openSansSemiBold);
            textClaimStatus.setTypeface(openSansSemiBold);
            textModel.setTypeface(openSansSemiBold);
        }
    }

    String getStatus(Boolean status) {
        if (status) {
            return "Active";
        } else {
            return "Closed";
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredClaimList = claimList;
                } else {
                    List<Claim> filteredList = new ArrayList<>();
                    for (Claim copyClaimList : claimList) {
                        if (String.valueOf(copyClaimList.getId()).toLowerCase().contains(charString.toLowerCase()) ||
                                String.valueOf(copyClaimList.getPolicyId()).toLowerCase().contains(charString.toLowerCase()) ||
                                copyClaimList.getStatus().toLowerCase().contains(charString.toLowerCase())||
                                copyClaimList.getModel().toLowerCase().contains(charString.toLowerCase())
                                ) {
                            filteredList.add(copyClaimList);
                        }
                    }
                    filteredClaimList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredClaimList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredClaimList = (ArrayList<Claim>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
