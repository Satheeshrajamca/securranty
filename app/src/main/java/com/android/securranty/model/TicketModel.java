package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TicketModel{

	@SerializedName("supportTypes")
	private List<SupportTypesItem> supportTypes;

	@SerializedName("priorities")
	private List<String> priorities;

	@SerializedName("statusCode")
	private int statusCode;

	@SerializedName("status")
	private String status;

	public void setSupportTypes(List<SupportTypesItem> supportTypes){
		this.supportTypes = supportTypes;
	}

	public List<SupportTypesItem> getSupportTypes(){
		return supportTypes;
	}

	public void setPriorities(List<String> priorities){
		this.priorities = priorities;
	}

	public List<String> getPriorities(){
		return priorities;
	}

	public void setStatusCode(int statusCode){
		this.statusCode = statusCode;
	}

	public int getStatusCode(){
		return statusCode;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
	public class SupportTypesItem{

		@SerializedName("SupportTicket")
		private List<Object> supportTicket;

		@SerializedName("Id")
		private int id;

		@SerializedName("Name")
		private String name;

		public void setSupportTicket(List<Object> supportTicket){
			this.supportTicket = supportTicket;
		}

		public List<Object> getSupportTicket(){
			return supportTicket;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setName(String name){
			this.name = name;
		}

		public String getName(){
			return name;
		}
	}
}
