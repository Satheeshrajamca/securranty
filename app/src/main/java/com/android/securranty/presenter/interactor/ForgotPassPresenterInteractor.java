package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import android.text.TextUtils;

import com.android.securranty.model.LoginResponse;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshraja on 11/16/17.
 */

public class ForgotPassPresenterInteractor {
    private BasePresenter signinPresenter;

    public ForgotPassPresenterInteractor(BasePresenter signinPresenter) {
        this.signinPresenter = signinPresenter;
    }

    public void validateEmail(String email) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (!TextUtils.isEmpty(email)) {
                if (Utils.isEmailValid(email)) {
                    doForgotPassword(email);
                } else {
                    signinPresenter.errorMsg(20, SecurrantyApp.context.getResources().getString(R.string.err_invalid_email));
                }
            } else {
                signinPresenter.errorMsg(400, "Email is empty");
            }
        } else {
            signinPresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void doForgotPassword(String email) {
        ApiClient.getClient().getForgotPassResponse(email).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    if (loginResponse.getStatusCode() == 200) {
                        String json = new Gson().toJson(response);
                        signinPresenter.success(loginResponse);
                    } else {
                        signinPresenter.errorMsg(loginResponse.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    signinPresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            signinPresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            signinPresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
