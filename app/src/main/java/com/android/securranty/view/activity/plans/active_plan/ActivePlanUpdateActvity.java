package com.android.securranty.view.activity.plans.active_plan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.AdapterEditSectionRecycler;
import com.android.securranty.adapter.PolicyInformationAdapter;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.Carrier;
import com.android.securranty.model.ConditionModel;
import com.android.securranty.model.CostCenter;
import com.android.securranty.model.CostCenterModel;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.DeviceCondition;
import com.android.securranty.model.DeviceModelsItem;
import com.android.securranty.model.DeviceTypeList;
import com.android.securranty.model.DeviceTypesModel;
import com.android.securranty.model.Division;
import com.android.securranty.model.DivisionModel;
import com.android.securranty.model.Manufacturer;
import com.android.securranty.model.PolicyModel;
import com.android.securranty.presenter.implementor.EditPlanPresenterImpl;
import com.android.securranty.utility.PhoneNumberTextWatcher;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.plans.active_plan.model.Plan;
import com.android.securranty.view.activity.plans.active_plan.model.ProofofPurchase;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.fileclaim.model.SuccessClaimModel;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.activePlans;

/**
 * Created by satheeshraja on 4/1/18.
 */

public class ActivePlanUpdateActvity extends AppCompatActivity implements BaseView, DatePickerDialog.OnDateSetListener, View.OnTouchListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.editItemType)
    EditText editItemType;
    @BindView(R.id.editManufacturer)
    EditText editManufacturer;
    @BindView(R.id.editModel)
    EditText editModel;
    @BindView(R.id.editItemPurchaseDate)
    EditText editItemPurchaseDate;
    @BindView(R.id.editItemPurchasePrice)
    EditText editItemPurchasePrice;
    @BindView(R.id.editItemCondition)
    EditText editItemCondition;
    @BindView(R.id.editCarrier)
    EditText editCarrier;
    @BindView(R.id.editPhoneNo)
    EditText editPhoneNo;
    @BindView(R.id.editSerial)
    EditText editSerial;
    @BindView(R.id.editIMEI)
    EditText editIMEI;
    @BindView(R.id.editFirstName)
    EditText editFirstName;
    @BindView(R.id.editLastName)
    EditText editLastName;
    @BindView(R.id.editDivision)
    EditText editDivision;
    @BindView(R.id.editCostCenter)
    EditText editCostCenter;
    @BindView(R.id.editCountry)
    EditText editCountry;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.imageUpload)
    ImageView imageUpload;
    @BindView(R.id.textCarrier)
    TextView textCarrier;

    private Long policyId;
    private ActionBar actionBar;
    private EditPlanPresenterImpl editPlanPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.row_update_active_plan_child);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        editPhoneNo.addTextChangedListener(new PhoneNumberTextWatcher(editPhoneNo));
        editPlanPresenter = new EditPlanPresenterImpl(this);
        positionActivePlans = getIntent().getIntExtra("PLAN_POSITION", 0);
        Log.i("SelectedPlanPosition", "" + positionActivePlans);
        loadPlanDetails();
    }

    private int positionActivePlans;
    private AdapterEditSectionRecycler adapterSectionRecycler;
    Plan plan;

    private void loadPlanDetails() {
        plan = ActivePlanListActivity.activePlans.get(positionActivePlans);
        Log.i("PhoneNumber", "" + plan.getPhoneNumber());
        manufacturer = plan.getManufacturer() == "null" ? "" : plan.getManufacturer();
        policyId = plan.getId();
        editItemType.setText(plan.getItemType() == "null" ? "" : plan.getItemType());
        editManufacturer.setText(manufacturer);
        manufacturerId = String.valueOf(plan.getManufacturerId()) == "null" ? "" : String.valueOf(plan.getManufacturerId());
        editModel.setText(plan.getModel() == "null" ? "" : plan.getModel());
        modelId = String.valueOf(plan.getModelId()) == "null" ? "" : String.valueOf(plan.getModelId());
        editItemPurchaseDate.setText(plan.getItemPurchaseDate().split("T")[0] == "null" ? "" : plan.getItemPurchaseDate().split("T")[0]);
        editItemPurchasePrice.setText(String.valueOf(plan.getItemPurchasePrice()) == "null" ? "" : String.valueOf(plan.getItemPurchasePrice()));
        editItemCondition.setText(plan.getItemCondition() == "null" ? "" : plan.getItemCondition());
        conditionId = String.valueOf(plan.getItemConditionId());
        if (activePlans.get(positionActivePlans).isCarrierVisible()) {
            editCarrier.setText(plan.getCarrier() == "null" ? "" : plan.getCarrier());
        } else {
            textCarrier.setVisibility(View.GONE);
            editCarrier.setVisibility(View.GONE);
        }
        carrierId = String.valueOf(plan.getCarrierId()) == "null" ? "" : String.valueOf(plan.getCarrierId());
        //editPhoneNo.setText(String.valueOf(plan.getPhoneNumber()) == "null" ? "" : "" + plan.getPhoneNumber());
        editSerial.setText(plan.getSerial() == "null" ? "" : plan.getSerial());
        editIMEI.setText(plan.getIMEI() == "null" ? "" : plan.getIMEI());
        editFirstName.setText(plan.getFirstName() == "null" ? "" : plan.getFirstName());
        editLastName.setText(plan.getLastName() == "null" ? "" : plan.getLastName());
        editDivision.setText(String.valueOf(plan.getDivision()) == "null" ? "" : "" + plan.getDivision());
        divisionId = String.valueOf(plan.getDivisionId()) == "null" ? "" : String.valueOf(plan.getDivisionId());
        editCostCenter.setText(String.valueOf(plan.getCostCenter()) == "null" ? "" : "" + plan.getCostCenter());
        costcenterId = String.valueOf(plan.getCostCenterId()) == "null" ? "" : String.valueOf(plan.getCostCenterId());
        editCountry.setText(plan.getCountry() == "null" ? "" : plan.getCountry());
        countryId = String.valueOf(plan.getCountryId()) == "null" ? "" : String.valueOf(plan.getCountryId());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        PolicyInformationAdapter planDetailsAdapter = new PolicyInformationAdapter(this, ActivePlanDetailsActvity.policyInformation);
        recyclerView.setAdapter(planDetailsAdapter);

        String phoneNumber = String.valueOf(plan.getPhoneNumber());
        if (phoneNumber != null) {
            if (phoneNumber != "null" && !phoneNumber.isEmpty() && !phoneNumber.contains("-")) {
                editPhoneNo.setText(String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6)));
            } else {
                editPhoneNo.setText(String.valueOf(plan.getPhoneNumber()) == "null" ? "" : "" + plan.getPhoneNumber());
            }
        }
        if (manufacturer != null && !manufacturer.isEmpty()) {
            editItemType.setOnTouchListener(this);
            editManufacturer.setOnTouchListener(this);
            editModel.setOnTouchListener(this);
            editItemPurchaseDate.setOnTouchListener(this);
            editItemPurchasePrice.setOnTouchListener(this);
            editItemCondition.setOnTouchListener(this);
            editCarrier.setOnTouchListener(this);
            editPhoneNo.setOnTouchListener(this);
            editSerial.setOnTouchListener(this);
            editIMEI.setOnTouchListener(this);
            editFirstName.setOnTouchListener(this);
            editLastName.setOnTouchListener(this);
            editDivision.setOnTouchListener(this);
            editCostCenter.setOnTouchListener(this);
            editCountry.setOnTouchListener(this);
        }
    }

    private String manufacturer;

    @OnClick({R.id.editItemType, R.id.editManufacturer, R.id.editModel, R.id.editItemPurchaseDate,
            R.id.editItemCondition, R.id.editCarrier, R.id.editPhoneNo,
            R.id.editIMEI, R.id.editFirstName, R.id.editLastName, R.id.editDivision, R.id.editCostCenter,
            R.id.editCountry, R.id.imageSave, R.id.imageBackButton, R.id.imageUpload})
    public void activePlans(View view) {

        switch (view.getId()) {
            case R.id.editItemType:
                /*if (manufacturer.isEmpty())
                    editPlanPresenter.getDeviceModel();*/
                break;
            case R.id.editManufacturer:
                //if (!manufacturer.isEmpty())
                editPlanPresenter.getManufacturer(ActivePlanListActivity.activePlans.get(positionActivePlans).getItemTypeId());
                break;
            case R.id.editModel:
                if (!manufacturerId.isEmpty()) {
                    Log.i("ItemtypeId", "TypeId" + ActivePlanListActivity.activePlans.get(positionActivePlans).getItemTypeId()
                            + "ManufactureId" + Long.parseLong(manufacturerId));
                    editPlanPresenter.getModel(ActivePlanListActivity.activePlans.get(positionActivePlans).getItemTypeId(), Long.parseLong(manufacturerId));
                } else {
                    Toast.makeText(this, "Please Select Manufacturer", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.editItemPurchaseDate:
                //if (!manufacturer.isEmpty()) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ActivePlanUpdateActvity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
                dpd.setMaxDate(now);
                //}
                break;
            case R.id.editItemCondition:
                //if (!manufacturer.isEmpty())
                editPlanPresenter.getConditions();
                break;
            case R.id.editCarrier:
                editPlanPresenter.getCarrier();
                break;
            case R.id.editDivision:
                editPlanPresenter.getDivision();
                break;
            case R.id.editCostCenter:
                editPlanPresenter.getCostCenter();
                break;
            case R.id.editCountry:
                editPlanPresenter.getCountry();
                break;
            case R.id.imageBackButton:
                onBackPressed();
                break;
            case R.id.imageSave:
                //uploadImagePlan();
                updatePlan();
                break;
            case R.id.imageUpload:
                uploadImage();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        if (isUpdatedPlan) {
            Intent intent = getIntent();
            setResult(RESULT_OK, intent);
            finish();
        }
        super.onBackPressed();
    }

    private File fileAccountImage = null;

    private void uploadImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Profile Pic");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    RxImagePicker.with(ActivePlanUpdateActvity.this).requestImage(Sources.CAMERA).subscribe(new Consumer<Uri>() {
                        @Override
                        public void accept(@NonNull Uri uri) throws Exception {
                            //uploadImage(convertFilePath(uri));
                            fileAccountImage = convertFilePath(uri);
                            Picasso.with(ActivePlanUpdateActvity.this).load(fileAccountImage).into(imageUpload);

                        }
                    });
                } else if (items[item].equals("Choose from Gallery")) {
                    RxImagePicker.with(ActivePlanUpdateActvity.this).requestImage(Sources.GALLERY)
                            .flatMap(new Function<Uri, ObservableSource<File>>() {
                                @Override
                                public ObservableSource<File> apply(@NonNull Uri uri) throws Exception {
                                    return RxImageConverters.uriToFile(ActivePlanUpdateActvity.this,
                                            uri, new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                                                    "securranty.jpeg"));
                                }
                            }).subscribe(new Consumer<File>() {
                        @Override
                        public void accept(@NonNull File file) throws Exception {
                            fileAccountImage = new Compressor(ActivePlanUpdateActvity.this).compressToFile(file);
                            Picasso.with(ActivePlanUpdateActvity.this).load(fileAccountImage).skipMemoryCache().into(imageUpload);
                            //uploadImage(file);
                        }
                    });

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private File convertFilePath(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            File file = new File(cursor.getString(column_index));
            Log.i("FileSize", "Before" + file.length());
            File compressedImageFile = null;
            try {
                compressedImageFile = new Compressor(this).compressToFile(file);
                Log.i("FileSize", "After" + compressedImageFile.length());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return compressedImageFile;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }


    }

    private void uploadImagePlan() {
        editPlanPresenter.uploadImagePlan(fileAccountImage);

    }

    private void updatePlan() {
        if (!manufacturerId.isEmpty()) {
            if (!modelId.isEmpty()) {
                if (!editSerial.getText().toString().trim().isEmpty()) {
                    if (editPhoneNo.getText().length() == 12) {
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        editPlanPresenter.updatePlan(carrierId, costcenterId, countryId, divisionId, editFirstName.getText().toString().trim(),
                                editIMEI.getText().toString().trim(), editItemCondition.getText().toString().trim(),
                                editItemPurchaseDate.getText().toString().trim(), editItemPurchasePrice.getText().toString().trim()
                                , editLastName.getText().toString().trim(), manufacturerId, modelId, editPhoneNo.getText().toString().trim().replaceAll("[-]", ""),
                                policyId, editSerial.getText().toString().trim(), fileAccountImage);
                    } else {
                        Toast.makeText(this, "Invalid Item phone", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Serial Number is required", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Model is required", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Manufacturer is required", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    private List<SearchListItem> listManufacturer;
    private List<SearchListItem> listModel;
    private List<SearchListItem> listCarrier;
    private String manufacturerId, countryId, deviceModelId, modelId, carrierId, conditionId, divisionId, costcenterId;
    private boolean isUpdatedPlan;

    @Override
    public void success(Object obj) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        if (obj instanceof DeviceTypesModel) {
            listManufacturer = new ArrayList<>();
            for (DeviceTypeList deviceModel : DeviceTypesModel.class.cast(obj).getDeviceTypeList()) {
                listManufacturer.add(new SearchListItem(deviceModel.getId(), deviceModel.getName()));
            }
            displayPopup(listManufacturer, "DeviceModel", editItemType);
        }
        if (obj instanceof Manufacturer) {
            listManufacturer = new ArrayList<>();
            for (Manufacturer.ManufacturersItem manufacturersItem : Manufacturer.class.cast(obj).getManufacturers()) {
                listManufacturer.add(new SearchListItem(manufacturersItem.getId(), manufacturersItem.getName()));
            }
            displayPopup(listManufacturer, "Manufacturer", editManufacturer);
        } else if (obj instanceof PolicyModel) {
            listModel = new ArrayList<>();
            for (DeviceModelsItem deviceModelsItem : PolicyModel.class.cast(obj).getDeviceModels()) {
                listModel.add(new SearchListItem(deviceModelsItem.getId(), deviceModelsItem.getModelName()));
            }
            displayPopup(listModel, "Model", editModel);
        } else if (obj instanceof Carrier) {
            listCarrier = new ArrayList<>();
            for (Carrier carrierModel : Carrier.class.cast(obj).getCarriers()) {
                listCarrier.add(new SearchListItem(carrierModel.getId(), carrierModel.getName()));
            }
            displayPopup(listCarrier, "Carrier", editCarrier);
        } else if (obj instanceof ConditionModel) {
            listCarrier = new ArrayList<>();
            for (DeviceCondition deviceCondition : ConditionModel.class.cast(obj).getDeviceConditions()) {
                listCarrier.add(new SearchListItem(deviceCondition.getId(), deviceCondition.getName()));
            }
            displayPopup(listCarrier, "Conditions", editItemCondition);
        } else if (obj instanceof DivisionModel) {
            listCarrier = new ArrayList<>();
            for (Division division : DivisionModel.class.cast(obj).getDivisions()) {
                listCarrier.add(new SearchListItem(division.getId(), division.getName()));
            }
            displayPopup(listCarrier, "Division", editDivision);
        } else if (obj instanceof CostCenterModel) {
            listCarrier = new ArrayList<>();
            for (CostCenter costCenter : CostCenterModel.class.cast(obj).getCostCenters()) {
                listCarrier.add(new SearchListItem(costCenter.getId(), costCenter.getName()));
            }
            displayPopup(listCarrier, "CostCenter", editDivision);
        } else if (obj instanceof CountryList) {
            listCarrier = new ArrayList<>();
            for (CountryList.CountriesItem countries : CountryList.class.cast(obj).getCountries()) {
                listCarrier.add(new SearchListItem(countries.getId(), countries.getName()));
            }
            displayPopup(listCarrier, "Country", editCountry);
        } else if (obj instanceof APISuccessModel) {
            isUpdatedPlan = true;
            fileAccountImage = null;
            new iOSDialogBuilder(this)
                    .setTitle("Plan")
                    .setSubtitle(APISuccessModel.class.cast(obj).getMessage())
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            dialog.dismiss();
                            Intent intent = getIntent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }).build().show();
            /*Plan.Builder plans = new Plan.Builder();
            plans.withId(policyId)
            .withItemTypeId(plan.getItemTypeId()).withItemType(editItemType.getText().toString().trim())
                    .withManufacturerId(Long.parseLong(manufacturerId)).withManufacturer(manufacturer).
                    withModelId(Long.parseLong(modelId)).withModel(editModel.getText().toString().trim()).
                    withItemPurchaseDate(editItemPurchaseDate.getText().toString().trim());
            if (!editItemPurchasePrice.getText().toString().trim().isEmpty()) {
                plans.withItemPurchasePrice(Double.parseDouble(editItemPurchasePrice.getText().toString().trim()));
            }
            plans.withItemConditionId(Long.parseLong(conditionId)).withItemCondition(editItemCondition.getText().toString().trim());
            if (!carrierId.isEmpty()) {
                plans.withCarrierId(Long.parseLong(carrierId)).withCarrier(editCarrier.getText().toString().trim());
            }
            if (!editPhoneNo.getText().toString().trim().isEmpty()) {
                plans.withPhoneNumber(Long.parseLong(editPhoneNo.getText().toString().trim())).withSerial(editSerial.getText().toString().trim());
            }
            plans.withIMEI(editIMEI.getText().toString().trim())
                    .withFirstName(editFirstName.getText().toString().trim())
                    .withLastName(editLastName.getText().toString().trim())
                    .withDivisionId(divisionId)
                    .withDivision(editDivision.getText().toString().trim())
                    .withCostCenterId(costcenterId);
            if (!editCostCenter.getText().toString().trim().isEmpty()) {
                plans.withCostCenter(editCostCenter.getText().toString().trim());
            }
            if (!countryId.isEmpty()) {
                plans.withCountryId(Long.parseLong(countryId))
                        .withCountry(editCountry.getText().toString().trim());
            }
            plans.withPolicyStatusId(policyId)
                    .withPolicyStatus(plan.getPolicyStatus())
                    .withCoverageLimit(plan.getCoverageLimit())
                    .withPolicyExpirationDate(plan.getPolicyExpirationDate())
                    .withWarrantySummary(plan.getWarrantySummary())
                    .withPolicyStartDate(plan.getPolicyStartDate())
                    .withServiceLevel(plan.getServiceLevel())
                    .withCoverageTerm(plan.getCoverageTerm())
                    .withPolicyType(plan.getPolicyType())
                    .withSoldBy(plan.getSoldBy())
                    .withProofofPurchase(new ArrayList<ProofofPurchase>());


            activePlans.set(positionActivePlans, plans.build());*/
        }
    }

    private void displayPopup(List<SearchListItem> listSearchItem, final String key, final EditText editText) {
        final SearchableDialog searchableDialog = new SearchableDialog(this, listSearchItem, "Select\t" + key);
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editText.setText(searchListItem.getTitle());
                switch (key) {
                    case "DeviceModel":
                        deviceModelId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        break;
                    case "Manufacturer":
                        manufacturerId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        editModel.setText("");
                        /*editItemCondition.setText("");
                        editCarrier.setText("");*/
                        break;
                    case "Model":
                        modelId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        /*editItemCondition.setText("");
                        editCarrier.setText("");*/
                        break;
                    case "Carrier":
                        carrierId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        break;
                    case "Conditions":
                        conditionId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        break;
                    case "Division":
                        divisionId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        break;
                    case "CostCenter":
                        costcenterId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        break;
                    case "Country":
                        countryId = String.valueOf(searchListItem.getId()) == "null" ? "" : String.valueOf(searchListItem.getId());
                        break;
                }
            }
        });
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        editItemPurchaseDate.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        v.setFocusable(false);
        v.setFocusableInTouchMode(false);
        return true;
    }
}
