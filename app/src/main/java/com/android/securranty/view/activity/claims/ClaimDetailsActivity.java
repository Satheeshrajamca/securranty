package com.android.securranty.view.activity.claims;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.BuyPlan;
import com.android.securranty.model.Claim;
import com.android.securranty.model.CountryList;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.tickets.CreateTicketActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.android.securranty.view.fragment.ClaimFragment.claims;

public class ClaimDetailsActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textClaimId)
    TextView textClaimId;
    @BindView(R.id.textPolicyId)
    TextView textPolicyId;
    @BindView(R.id.textDamagedDate)
    TextView textDamagedDate;
    @BindView(R.id.textCreatedDate)
    TextView textCreatedDate;
    @BindView(R.id.textCreatedBy)
    TextView textCreatedBy;
    @BindView(R.id.textLastModifyDate)
    TextView textLastModifyDate;
    @BindView(R.id.textLastModifiedBy)
    TextView textLastModifiedBy;
    @BindView(R.id.textClaimDesc)
    TextView textClaimDesc;
    @BindView(R.id.textModel)
    TextView textModel;
    @BindView(R.id.textCoveredLosses)
    TextView textCoveredLosses;
    @BindView(R.id.textClaimProblem)
    TextView textClaimProblem;
    @BindView(R.id.btnCancelClaim)
    Button btnCancelClaim;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        int claimPosition = getIntent().getIntExtra("CLAIM_POSITION", 0);
        claim = claims.get(claimPosition);
        textClaimId.setText(String.valueOf(claim.getId()));
        textPolicyId.setText(String.valueOf(claim.getPolicyId()));
        textModel.setText(claim.getModel() != null ? claim.getModel() : "");
        textCoveredLosses.setText(claim.getCoveredLosses() != null ? claim.getCoveredLosses() : "");
        textClaimProblem.setText(claim.getClaimProblem() != null ? claim.getClaimProblem() : "");
        textDamagedDate.setText(claim.getDamageDate() != null ? claim.getDamageDate().split("T")[0] : "");
        textCreatedDate.setText(claim.getCreatedDate() != null ? claim.getCreatedDate().split("T")[0] : "");
        textCreatedBy.setText(claim.getCreatedBy() != null ? claim.getCreatedBy().toString() : "");
        textLastModifyDate.setText(claim.getLastModDate() != null ? claim.getLastModDate().split("T")[0] : "");
        textLastModifiedBy.setText(claim.getLastModBy() != null ? claim.getLastModBy() : "");
        textClaimDesc.setText(claim.getProblemDescription() != null ? claim.getProblemDescription() : "");
        if (claim.getStatus().equals("Claim Cancelled by User")) {
            btnCancelClaim.setVisibility(View.GONE);
        }
    }

    private Claim claim;

    @OnClick({R.id.imageBackButton, R.id.btnCancelClaim})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
            case R.id.btnCancelClaim:
                cancelClaim();
                break;
        }
    }

    private void cancelClaim() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            progressBar.setVisibility(View.VISIBLE);
            ApiClient.getClient().cancelClaim(SharePref.getInstance(SecurrantyApp.context).getToken(), claim.getId()).enqueue(new Callback<APISuccessModel>() {
                @Override
                public void onResponse(Call<APISuccessModel> call, Response<APISuccessModel> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        APISuccessModel myaccountResponse = response.body();
                        if (myaccountResponse.getStatusCode() == 200) {
                            new iOSDialogBuilder(ClaimDetailsActivity.this)
                                    .setTitle("Cancel Claim")
                                    .setSubtitle(myaccountResponse.getMessage())
                                    .setBoldPositiveLabel(true)
                                    .setCancelable(false)
                                    .setPositiveListener("Ok", new iOSDialogClickListener() {
                                        @Override
                                        public void onClick(iOSDialog dialog) {
                                            Intent returnIntent = new Intent();
                                            setResult(Activity.RESULT_OK, returnIntent);
                                            dialog.dismiss();
                                            finish();
                                        }
                                    }).build().show();
                        } else {
                        }

                    } else {
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<APISuccessModel> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    if (t instanceof HttpException)
                        errorResponse(((HttpException) t).response().errorBody());
                    else
                        Toast.makeText(ClaimDetailsActivity.this, SecurrantyApp.context.getString(R.string.err_back_end), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            Toast.makeText(this, message.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IllegalStateException | JsonSyntaxException exception) {
            Toast.makeText(this, SecurrantyApp.context.getString(R.string.err_back_end), Toast.LENGTH_SHORT).show();
        }
    }


}
