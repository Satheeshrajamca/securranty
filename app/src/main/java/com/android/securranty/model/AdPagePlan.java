
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AdPagePlan {

    @SerializedName("Active")
    private Boolean active;
    @SerializedName("BillingType")
    private Long billingType;
    @SerializedName("Category")
    private Long category;
    @SerializedName("CategoryName")
    private String categoryName;
    @SerializedName("Condition")
    private Long condition;
    @SerializedName("ConditionName")
    private String conditionName;
    @SerializedName("CoverageLimit")
    private Double coverageLimit;
    @SerializedName("CoverageTerm")
    private Long coverageTerm;
    @SerializedName("CoverageTermName")
    private String coverageTermName;
    @SerializedName("DeductableADH")
    private Double deductableADH;
    @SerializedName("DeductableLost")
    private Double deductableLost;
    @SerializedName("DeductableME")
    private Double deductableME;
    @SerializedName("DeductableTheft")
    private Double deductableTheft;
    @SerializedName("FriendlyName")
    private String friendlyName;
    @SerializedName("Id")
    private Long id;
    @SerializedName("IsForAllUsers")
    private Boolean isForAllUsers;
    @SerializedName("PerMonthPrice")
    private Double perMonthPrice;
    @SerializedName("Postage")
    private String postage;
    @SerializedName("Reseller")
    private Double reseller;
    @SerializedName("Retail")
    private Double retail;
    @SerializedName("RetailStrikPrice")
    private Double retailStrikPrice;
    @SerializedName("SKUName")
    private String sKUName;
    @SerializedName("SKUNotes")
    private String sKUNotes;
    @SerializedName("SKUStatus")
    private String sKUStatus;
    @SerializedName("SLA")
    private Long sLA;
    @SerializedName("Section")
    private Long section;
    @SerializedName("SectionName")
    private String sectionName;
    @SerializedName("TermsUrl")
    private String termsUrl;
    @SerializedName("UnderwriterFee")
    private Double underwriterFee;
    @SerializedName("UnderwriterSKU")
    private String underwriterSKU;
    @SerializedName("WarrantySummary")
    private Long warrantySummary;
    @SerializedName("WarrantySummaryName")
    private String warrantySummaryName;

    public Boolean getActive() {
        return active;
    }

    public Long getBillingType() {
        return billingType;
    }

    public Long getCategory() {
        return category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Long getCondition() {
        return condition;
    }

    public String getConditionName() {
        return conditionName;
    }

    public Double getCoverageLimit() {
        return coverageLimit;
    }

    public Long getCoverageTerm() {
        return coverageTerm;
    }

    public String getCoverageTermName() {
        return coverageTermName;
    }

    public Double getDeductableADH() {
        return deductableADH;
    }

    public Double getDeductableLost() {
        return deductableLost;
    }

    public Double getDeductableME() {
        return deductableME;
    }

    public Double getDeductableTheft() {
        return deductableTheft;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public Long getId() {
        return id;
    }

    public Boolean getIsForAllUsers() {
        return isForAllUsers;
    }

    public Double getPerMonthPrice() {
        return perMonthPrice;
    }

    public String getPostage() {
        return postage;
    }

    public Double getReseller() {
        return reseller;
    }

    public Double getRetail() {
        return retail;
    }

    public Double getRetailStrikPrice() {
        return retailStrikPrice;
    }

    public String getSKUName() {
        return sKUName;
    }

    public String getSKUNotes() {
        return sKUNotes;
    }

    public String getSKUStatus() {
        return sKUStatus;
    }

    public Long getSLA() {
        return sLA;
    }

    public Long getSection() {
        return section;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getTermsUrl() {
        return termsUrl;
    }

    public Double getUnderwriterFee() {
        return underwriterFee;
    }

    public String getUnderwriterSKU() {
        return underwriterSKU;
    }

    public Long getWarrantySummary() {
        return warrantySummary;
    }

    public String getWarrantySummaryName() {
        return warrantySummaryName;
    }

    public static class Builder {

        private Boolean active;
        private Long billingType;
        private Long category;
        private String categoryName;
        private Long condition;
        private String conditionName;
        private Double coverageLimit;
        private Long coverageTerm;
        private String coverageTermName;
        private Double deductableADH;
        private Double deductableLost;
        private Double deductableME;
        private Double deductableTheft;
        private String friendlyName;
        private Long id;
        private Boolean isForAllUsers;
        private Double perMonthPrice;
        private String postage;
        private Double reseller;
        private Double retail;
        private Double retailStrikPrice;
        private String sKUName;
        private String sKUNotes;
        private String sKUStatus;
        private Long sLA;
        private Long section;
        private String sectionName;
        private String termsUrl;
        private Double underwriterFee;
        private String underwriterSKU;
        private Long warrantySummary;
        private String warrantySummaryName;

        public AdPagePlan.Builder withActive(Boolean active) {
            this.active = active;
            return this;
        }

        public AdPagePlan.Builder withBillingType(Long billingType) {
            this.billingType = billingType;
            return this;
        }

        public AdPagePlan.Builder withCategory(Long category) {
            this.category = category;
            return this;
        }

        public AdPagePlan.Builder withCategoryName(String categoryName) {
            this.categoryName = categoryName;
            return this;
        }

        public AdPagePlan.Builder withCondition(Long condition) {
            this.condition = condition;
            return this;
        }

        public AdPagePlan.Builder withConditionName(String conditionName) {
            this.conditionName = conditionName;
            return this;
        }

        public AdPagePlan.Builder withCoverageLimit(Double coverageLimit) {
            this.coverageLimit = coverageLimit;
            return this;
        }

        public AdPagePlan.Builder withCoverageTerm(Long coverageTerm) {
            this.coverageTerm = coverageTerm;
            return this;
        }

        public AdPagePlan.Builder withCoverageTermName(String coverageTermName) {
            this.coverageTermName = coverageTermName;
            return this;
        }

        public AdPagePlan.Builder withDeductableADH(Double deductableADH) {
            this.deductableADH = deductableADH;
            return this;
        }

        public AdPagePlan.Builder withDeductableLost(Double deductableLost) {
            this.deductableLost = deductableLost;
            return this;
        }

        public AdPagePlan.Builder withDeductableME(Double deductableME) {
            this.deductableME = deductableME;
            return this;
        }

        public AdPagePlan.Builder withDeductableTheft(Double deductableTheft) {
            this.deductableTheft = deductableTheft;
            return this;
        }

        public AdPagePlan.Builder withFriendlyName(String friendlyName) {
            this.friendlyName = friendlyName;
            return this;
        }

        public AdPagePlan.Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public AdPagePlan.Builder withIsForAllUsers(Boolean isForAllUsers) {
            this.isForAllUsers = isForAllUsers;
            return this;
        }

        public AdPagePlan.Builder withPerMonthPrice(Double perMonthPrice) {
            this.perMonthPrice = perMonthPrice;
            return this;
        }

        public AdPagePlan.Builder withPostage(String postage) {
            this.postage = postage;
            return this;
        }

        public AdPagePlan.Builder withReseller(Double reseller) {
            this.reseller = reseller;
            return this;
        }

        public AdPagePlan.Builder withRetail(Double retail) {
            this.retail = retail;
            return this;
        }

        public AdPagePlan.Builder withRetailStrikPrice(Double retailStrikPrice) {
            this.retailStrikPrice = retailStrikPrice;
            return this;
        }

        public AdPagePlan.Builder withSKUName(String sKUName) {
            this.sKUName = sKUName;
            return this;
        }

        public AdPagePlan.Builder withSKUNotes(String sKUNotes) {
            this.sKUNotes = sKUNotes;
            return this;
        }

        public AdPagePlan.Builder withSKUStatus(String sKUStatus) {
            this.sKUStatus = sKUStatus;
            return this;
        }

        public AdPagePlan.Builder withSLA(Long sLA) {
            this.sLA = sLA;
            return this;
        }

        public AdPagePlan.Builder withSection(Long section) {
            this.section = section;
            return this;
        }

        public AdPagePlan.Builder withSectionName(String sectionName) {
            this.sectionName = sectionName;
            return this;
        }

        public AdPagePlan.Builder withTermsUrl(String termsUrl) {
            this.termsUrl = termsUrl;
            return this;
        }

        public AdPagePlan.Builder withUnderwriterFee(Double underwriterFee) {
            this.underwriterFee = underwriterFee;
            return this;
        }

        public AdPagePlan.Builder withUnderwriterSKU(String underwriterSKU) {
            this.underwriterSKU = underwriterSKU;
            return this;
        }

        public AdPagePlan.Builder withWarrantySummary(Long warrantySummary) {
            this.warrantySummary = warrantySummary;
            return this;
        }

        public AdPagePlan.Builder withWarrantySummaryName(String warrantySummaryName) {
            this.warrantySummaryName = warrantySummaryName;
            return this;
        }

        public AdPagePlan build() {
            AdPagePlan adPagePlan = new AdPagePlan();
            adPagePlan.active = active;
            adPagePlan.billingType = billingType;
            adPagePlan.category = category;
            adPagePlan.categoryName = categoryName;
            adPagePlan.condition = condition;
            adPagePlan.conditionName = conditionName;
            adPagePlan.coverageLimit = coverageLimit;
            adPagePlan.coverageTerm = coverageTerm;
            adPagePlan.coverageTermName = coverageTermName;
            adPagePlan.deductableADH = deductableADH;
            adPagePlan.deductableLost = deductableLost;
            adPagePlan.deductableME = deductableME;
            adPagePlan.deductableTheft = deductableTheft;
            adPagePlan.friendlyName = friendlyName;
            adPagePlan.id = id;
            adPagePlan.isForAllUsers = isForAllUsers;
            adPagePlan.perMonthPrice = perMonthPrice;
            adPagePlan.postage = postage;
            adPagePlan.reseller = reseller;
            adPagePlan.retail = retail;
            adPagePlan.retailStrikPrice = retailStrikPrice;
            adPagePlan.sKUName = sKUName;
            adPagePlan.sKUNotes = sKUNotes;
            adPagePlan.sKUStatus = sKUStatus;
            adPagePlan.sLA = sLA;
            adPagePlan.section = section;
            adPagePlan.sectionName = sectionName;
            adPagePlan.termsUrl = termsUrl;
            adPagePlan.underwriterFee = underwriterFee;
            adPagePlan.underwriterSKU = underwriterSKU;
            adPagePlan.warrantySummary = warrantySummary;
            adPagePlan.warrantySummaryName = warrantySummaryName;
            return adPagePlan;
        }

    }

}
