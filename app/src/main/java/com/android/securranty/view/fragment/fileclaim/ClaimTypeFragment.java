package com.android.securranty.view.fragment.fileclaim;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.presenter.implementor.FileClaimPresenter;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.fileclaim.model.ClaimType;
import com.android.securranty.view.fragment.fileclaim.model.ProblemType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class ClaimTypeFragment extends Fragment implements BaseView {

    @BindView(R.id.editItemType)
    EditText editItemType;
    @BindView(R.id.recyclerView)
    ListView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.relativeClaimType)
    RelativeLayout relativeClaimType;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.previous)
    TextView previous;
    @BindView(R.id.textAddMoreDamages)
    TextView textAddMoreDamages;

    private FileClaimPresenter fileclaimPresenter;
    private boolean isClaimType;
    private View view;
    FileClaimsActivity context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.layout_claim_type,
                    container, false);
            ButterKnife.bind(this, view);
            fileclaimPresenter = new FileClaimPresenter(this);
            previous.setVisibility(View.VISIBLE);
            selectedClaimTypeId=-1;
        }
        return view;
    }

    @OnClick({R.id.editItemType, R.id.textAddMoreDamages, R.id.next, R.id.previous})
    public void onclickClaimType(View view) {
        switch (view.getId()) {
            case R.id.editItemType:
                fileclaimPresenter.getClaimType();
                isClaimType = true;
                break;
            case R.id.textAddMoreDamages:
                showMaterialDialog();
                break;
            case R.id.next:
                daoFileClaim.setClaimTypeId(selectedClaimTypeId);
                daoFileClaim.setSubClaimTypeIds(listselectedSubClaimsId);
                context.triggerClaim(new IssueDateFragment(), "Date of Issue/Loss");
                break;
            case R.id.previous:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeClaimType, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(getActivity(), SignInActivity.class), 401);
        }
    }

    private List<SearchListItem> listClaimType;
    private HashMap<String, String> hashSubClaimType;
    private List<String> listSubClaimType;

    @Override
    public void success(Object obj) {
        List<ProblemType> claimTypes = ClaimType.class.cast(obj).getProblemTypes();
        if (isClaimType) {
            listClaimType = new ArrayList<>();
            for (ProblemType claimType : claimTypes) {
                listClaimType.add(new SearchListItem(claimType.getId(), claimType.getName()));
            }
            displayTypes();
        } else {
            hashSubClaimType = new HashMap<>();
            listSubClaimType = new ArrayList<>();
            for (ProblemType problemType : claimTypes) {
                hashSubClaimType.put(problemType.getName(), String.valueOf(problemType.getId()));
                listSubClaimType.add(problemType.getName());
            }
            showMaterialDialog();
        }
    }

    ArrayAdapter<String> itemsAdapter;
    List<String> listselectedSubItems = new ArrayList<>();
    List<Long> listselectedSubClaimsId = new ArrayList<>();
    MaterialDialog.Builder materialDialog;

    private void showMaterialDialog() {
        materialDialog = new MaterialDialog.Builder(getActivity());
        materialDialog.title("Select one or more " + claimTypeName);
        materialDialog.items(listSubClaimType);
        materialDialog.itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMultiChoice() {
            @Override
            public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                if (text.length != 0 || listselectedSubItems.size() != 0) {
                    int i = 0;
                    for (CharSequence ch : text) {
                        listselectedSubItems.add(ch.toString());
                        listselectedSubClaimsId.add(Long.parseLong(hashSubClaimType.get(ch.toString())));
                        listSubClaimType.remove(ch.toString());
                    }
                    if (itemsAdapter == null) {
                        itemsAdapter =
                                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listselectedSubItems);
                        recyclerView.setAdapter(itemsAdapter);
                    } else {
                        itemsAdapter.notifyDataSetChanged();
                    }
                    if (listSubClaimType.size() > 0) {
                        textAddMoreDamages.setVisibility(View.VISIBLE);
                    } else {
                        textAddMoreDamages.setVisibility(View.GONE);
                    }
                    next.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                    return true;
                } else {
                    Toast.makeText(getActivity(), "You are need to check at least one problem type to complete this step.", Toast.LENGTH_LONG).show();
                    return false;
                }

            }
        });
        materialDialog.canceledOnTouchOutside(false);
        materialDialog.autoDismiss(false);
        materialDialog.positiveText("Ok");
        materialDialog.show();
    }

    private int claimTypeId;
    private String claimTypeName;
    public static int selectedClaimTypeId;
    public static String claimType;


    private void displayTypes() {
        final SearchableDialog searchableDialog = new SearchableDialog(getActivity(), listClaimType, "Select Claim Type");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                claimTypeName = searchListItem.getTitle();
                claimTypeId = searchListItem.getId();
                if (claimTypeId != selectedClaimTypeId) {
                    if (itemsAdapter != null) {
                        listselectedSubItems.clear();
                        itemsAdapter.notifyDataSetChanged();
                    }
                    selectedClaimTypeId = claimTypeId;
                    editItemType.setText(searchListItem.getTitle());
                    fileclaimPresenter.getSubClaimType(selectedClaimTypeId);
                    isClaimType = false;
                    claimType = searchListItem.getTitle().trim();
                }
            }
        });
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            if (isClaimType) {
                fileclaimPresenter.getClaimType();
            } else {
                fileclaimPresenter.getSubClaimType(selectedClaimTypeId);
            }
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
