package com.android.securranty.presenter.interactor;

import android.text.TextUtils;
import android.util.Log;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.CreateTicketModel;
import com.android.securranty.model.TicketModel;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class CreateTicketPresenterInter {
    BasePresenter basePresenter;

    public CreateTicketPresenterInter(BasePresenter createTicketPresenter) {
        basePresenter = createTicketPresenter;
    }

    public void getTicketDetils() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getTicketDetails(SharePref.getInstance(SecurrantyApp.context).getToken()).enqueue(new Callback<TicketModel>() {
                @Override
                public void onResponse(Call<TicketModel> call, Response<TicketModel> response) {
                    if (response.isSuccessful()) {
                        TicketModel ticketResponse = response.body();
                        if (ticketResponse.getStatusCode() == 200) {
                            basePresenter.success(ticketResponse);
                        } else {
                            basePresenter.errorMsg(ticketResponse.getStatusCode(), "Invalid User");
                        }

                    } else {
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<TicketModel> call, Throwable t) {
                    if (t instanceof HttpException)
                        errorResponse(((HttpException) t).response().errorBody());
                    else
                        basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void createTicket(String subject, String typeId, String priority, String message) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (!TextUtils.isEmpty(subject)) {
                if (!TextUtils.isEmpty(message)) {
                    ApiClient.getClient().createTicket(SharePref.getInstance(SecurrantyApp.context).getToken(), SharePref.getInstance(SecurrantyApp.context).getUserId(),
                            subject, typeId, priority, message).enqueue(new Callback<CreateTicketModel>() {
                        @Override
                        public void onResponse(Call<CreateTicketModel> call, Response<CreateTicketModel> response) {
                            if (response.isSuccessful()) {
                                CreateTicketModel ticketResponse = response.body();
                                if (ticketResponse.getStatusCode() == 200) {
                                    basePresenter.success(ticketResponse);
                                } else {
                                    basePresenter.errorMsg(ticketResponse.getStatusCode(), "Invalid User");
                                }

                            } else {
                                errorResponse(response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<CreateTicketModel> call, Throwable t) {
                            if (t instanceof HttpException)
                                errorResponse(((HttpException) t).response().errorBody());
                            else
                                basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                        }
                    });
                } else {
                    basePresenter.errorMsg(400, "Message is required");
                }
            } else {
                basePresenter.errorMsg(400, "Subject is required");
            }
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            basePresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
