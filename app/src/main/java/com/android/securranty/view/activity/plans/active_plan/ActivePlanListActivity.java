package com.android.securranty.view.activity.plans.active_plan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.ActivePlanListAdapter;
import com.android.securranty.presenter.implementor.ActivePlanPresenterImpl;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.plans.active_plan.model.ActivePlan;
import com.android.securranty.view.activity.plans.active_plan.model.Plan;
import com.android.securranty.view.activity.plans.buy_plan.AllPlansActivity;
import com.android.securranty.view.activity.plans.buy_plan.PlansCategoryActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.fileclaim.model.FileClaimModel;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshraja on 3/23/18.
 */

public class ActivePlanListActivity extends AppCompatActivity implements BaseView, ActivePlanListAdapter.FileClaim {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.textPolicyNo)
    TextView textPolicyNo;
    @BindView(R.id.textModel)
    TextView textModel;
    @BindView(R.id.textPolicyExp)
    TextView textPolicyExp;

    private ActionBar actionBar;
    private ActivePlanPresenterImpl activePlanPresenter;
    private ActivePlanListAdapter activePlanListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activeplans);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        activePlanPresenter = new ActivePlanPresenterImpl(this);
        activePlanPresenter.intActivePlans();
        recyclerviewItemClick();
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        textPolicyNo.setTypeface(openSansSemiBold);
        textModel.setTypeface(openSansSemiBold);
        textPolicyExp.setTypeface(openSansSemiBold);
    }

    private void recyclerviewItemClick() {
        /*recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        startActivity(new Intent(ActivePlanListActivity.this,
                                ActivePlanDetailsActvity.class).putExtra("PLAN_POSITION", position));
                    }
                })
        );*/

    }

    @OnClick({R.id.imageBackButton})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    public static List<Plan> activePlans;

    @Override
    public void success(Object obj) {
        activePlans = ActivePlan.class.cast(obj).getPlans();
        if (activePlans.size() > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(linearLayoutManager);
            activePlanListAdapter = new ActivePlanListAdapter();
            activePlanListAdapter.setData(ActivePlanListActivity.this, activePlans);
            recyclerView.setAdapter(activePlanListAdapter);
        } else {
            new MaterialDialog.Builder(this).onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    finish();
                    startActivity(new Intent(ActivePlanListActivity.this, AllPlansActivity.class));
                }
            }).onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    finish();
                }
            })
                    .canceledOnTouchOutside(false)
                    .title("Active Plans")
                    .content("No Active Claims Currently.")
                    .positiveText("Purchase a Policy")
                    .negativeText("Cancel")
                    .show();
        }

    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public static FileClaimModel daoFileClaim = new FileClaimModel();

    @Override
    public void fileClaim(int position, String from) {
        if (from.equals("FileClaim")) {
            if (activePlans.get(position).getIsOngoingClaim() == 0) {
                if (activePlans.get(position).getIsUpdatedMandatoryField() == 0) {
                    daoFileClaim.setPolicyId(ActivePlanListActivity.activePlans.get(position).getId());
                    startActivityForResult(new Intent(this, FileClaimsActivity.class),400);
                } else {
                    new iOSDialogBuilder(this)
                            .setTitle("File a claim")
                            .setSubtitle("Please update the policy information first")
                            .setBoldPositiveLabel(true)
                            .setCancelable(false)
                            .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).build().show();
                }
            } else {

                new iOSDialogBuilder(this)
                        .setTitle("File a claim")
                        .setSubtitle("This policy already has a ongoing claim!")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();

            }
        } else {
            startActivityForResult(new Intent(ActivePlanListActivity.this,
                    ActivePlanDetailsActvity.class).putExtra("PLAN_POSITION", position), 400);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            activePlanPresenter.intActivePlans();
        } else if (requestCode == 400) {
            if (resultCode == RESULT_OK) {
                activePlanPresenter.intActivePlans();
            }
        } else {
            finish();
        }
    }
}
