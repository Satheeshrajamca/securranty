package com.android.securranty.presenter.interactor;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.presenter.implementor.CreditcardListPresenterImpl;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class CreditcardListPresenterInter {

    CreditcardListPresenterImpl creditcardListPresenter;

    public CreditcardListPresenterInter(CreditcardListPresenterImpl creditcardListPresenter) {
        this.creditcardListPresenter = creditcardListPresenter;
    }

    public void getCreditCardList() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            apiCall(SharePref.getInstance(SecurrantyApp.context).getToken(), SharePref.getInstance(SecurrantyApp.context).getUserId());
        } else {
            creditcardListPresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void apiCall(String token, String userId) {
        ApiClient.getClient().getCreditcardList(token, userId).enqueue(new Callback<CreditCardList>() {
            @Override
            public void onResponse(Call<CreditCardList> call, Response<CreditCardList> response) {
                if (response.isSuccessful()) {
                    creditcardListPresenter.success(response.body());
                } else {
                    errorResponse(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<CreditCardList> call, Throwable t) {
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    creditcardListPresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            creditcardListPresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            creditcardListPresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
