package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClaimsModel{

	@SerializedName("claims")
	private List<Claim> claims;

	@SerializedName("statusCode")
	private int statusCode;

	@SerializedName("status")
	private String status;

	public void setClaims(List<Claim> claims){
		this.claims = claims;
	}

	public List<Claim> getClaims(){
		return claims;
	}

	public void setStatusCode(int statusCode){
		this.statusCode = statusCode;
	}

	public int getStatusCode(){
		return statusCode;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ClaimsModel{" + 
			"claims = '" + claims + '\'' + 
			",statusCode = '" + statusCode + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}