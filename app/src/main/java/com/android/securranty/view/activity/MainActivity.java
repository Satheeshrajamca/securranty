package com.android.securranty.view.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.BillingFragment;
import com.android.securranty.view.fragment.ClaimFragment;
import com.android.securranty.view.fragment.DashboardFragment;
import com.android.securranty.view.fragment.MyAccountFragment;
import com.android.securranty.view.fragment.PlanFragment;
import com.android.securranty.view.fragment.SupportFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ClaimFragment.ListenerClaims, MyAccountFragment.ListenerLoginStatus, DashboardFragment.ListenerRecentClaims {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    private TextView toolbarTitle;
    private int selectedId;
    private TextView textUserName;
    private ImageView imageProfile;

    public static final int ACCOUNT_PROFILE_ACTIVITY = 1;
    public static final int BILLING_PROFILE_ACTIVITY = 2;
    public static final int CREDIT_CARD_ACTIVITY = 3;
    public static final int PAYMENT_HISTORY = 4;
    public static final int PURCHASE_PLAN = 5;
    public static final int ACTIVE_PLAN = 6;
    public static final int CLAIMS = 7;
    public static final int CHANGE_PASSWORD = 8;
    public static final int CREATE_TICKET = 9;
    public static final int LOGIN = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Dash Board");
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);
        imageProfile = (ImageView) headerLayout.findViewById(R.id.imageProfile);
        textUserName = (TextView) headerLayout.findViewById(R.id.textUserName);
        launchFragment(new DashboardFragment());
        selectedId = R.id.nav_dash_board;
        setFontType();
        setLoginStatus();
    }

    private void setLoginStatus() {
        if (SharePref.getInstance(this).isLoggedinCurrently()) {
            navigationView.getMenu().getItem(6).setTitle("Logout");
        }
    }

    private void setFontType() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        textUserName.setTypeface(openSansSemiBold);
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            SpannableString s = new SpannableString(mi.getTitle());
            s.setSpan(new TypefaceSpan("fonts/opensans_regular.ttf"), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mi.setTitle(s);
        }
    }

    private void launchFragment(final Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (fragment instanceof DashboardFragment) {
            fragmentTransaction.replace(R.id.containerFragment, fragment);
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            fragmentTransaction.replace(R.id.containerFragment, fragment);
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                toolbarTitle.setText("Dash Board");
                navigationView.getMenu().getItem(0).setChecked(true);
                navigationSeletedPosition = 0;
                selectedId = R.id.nav_dash_board;
            } else {
                super.onBackPressed();
            }
        }
    }

    private int navigationSeletedPosition, navigationSeletedId;

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (selectedId != id) {
            selectedId = id;
            if (id == R.id.nav_dash_board) {
                navigationSeletedId = R.id.nav_dash_board;
                navigationSeletedPosition = 0;
                toolbarTitle.setText("Dash Board");
                launchFragment(new DashboardFragment());
            } else if (id == R.id.nav_my_account) {
                navigationSeletedId = R.id.nav_my_account;
                navigationSeletedPosition = 1;
                toolbarTitle.setText("My Account");
                launchFragment(new MyAccountFragment());
            } else if (id == R.id.nav_plans) {
                navigationSeletedPosition = 2;
                navigationSeletedId = R.id.nav_plans;
                toolbarTitle.setText("Plans");
                launchFragment(new PlanFragment());
            } else if (id == R.id.nav_claims) {
                navigateClaims();
            } else if (id == R.id.nav_billing) {
                navigationSeletedId = R.id.nav_billing;
                navigationSeletedPosition = 4;
                toolbarTitle.setText("Billing");
                launchFragment(new BillingFragment());
            } else if (id == R.id.nav_support) {
                navigationSeletedId = R.id.nav_support;
                navigationSeletedPosition = 5;
                toolbarTitle.setText("Support");
                launchFragment(new SupportFragment());
            } else if (id == R.id.nav_logout) {
                if (SharePref.getInstance(this).isLoggedinCurrently()) {
                    SharePref.getInstance(MainActivity.this).setProfileImage("");
                    SharePref.getInstance(MainActivity.this).clearCredential();
                    SharePref.getInstance(MainActivity.this).resetUserDetails();
                    item.setTitle("Login");
                    item.setCheckable(false);

                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        toolbarTitle.setText("Dash Board");
                        navigationView.getMenu().getItem(0).setChecked(true);
                        navigationSeletedPosition = 0;
                        navigationSeletedId = R.id.nav_dash_board;
                        selectedId = R.id.nav_dash_board;
                    } else {
                        navigationSeletedPosition = 0;
                        navigationSeletedId = R.id.nav_dash_board;
                        selectedId = R.id.nav_dash_board;
                    }
                    setProfileImage();

                } else {
                    Intent i = new Intent(this, SignInActivity.class);
                    startActivityForResult(i, LOGIN);
                }
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void navigateClaims() {
        if (Utils.isNetworkAvailable(this)) {
            navigationSeletedId = R.id.nav_claims;
            navigationSeletedPosition = 3;
            toolbarTitle.setText("Claims");
            navigationView.getMenu().getItem(navigationSeletedPosition).setChecked(true);
            launchFragment(new ClaimFragment());
        } else {
            showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(drawer, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            this.finish();
            startActivity(new Intent(this, SignInActivity.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProfileImage();

    }

    private void setProfileImage() {
        if (!SharePref.getInstance(this).getProfileImage().isEmpty()) {
            Picasso.with(this).load(SharePref.getInstance(this).getProfileImage()).placeholder(R.drawable.dashboard_default_avatar)
                    .into(imageProfile);
        } else {
            Picasso.with(this).load(R.drawable.dashboard_default_avatar)
                    .into(imageProfile);
        }
        textUserName.setText(SharePref.getInstance(SecurrantyApp.context).getFirstName() + " "
                + SharePref.getInstance(SecurrantyApp.context).getLastName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!SharePref.getInstance(this).isLoggedIn())
            SharePref.getInstance(this).resetUserDetails();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CLAIMS && resultCode == RESULT_OK) {
            navigationSeletedPosition = 3;
            navigationView.getMenu().getItem(navigationSeletedPosition).setChecked(true);
            toolbarTitle.setText("Claims");
            launchFragment(new ClaimFragment());
            changeLoginStatus("Logout");
        }
        if (requestCode == LOGIN && resultCode == RESULT_OK) {
            selectedId = navigationSeletedId;
            navigationView.getMenu().getItem(navigationSeletedPosition).setChecked(true);
            navigationView.getMenu().getItem(6).setTitle("Logout");
        } else if (requestCode == CLAIMS) {
            navigationView.getMenu().getItem(navigationSeletedPosition).setChecked(true);
            selectedId = navigationSeletedId;
        } else if (requestCode == LOGIN) {
            navigationView.getMenu().getItem(navigationSeletedPosition).setChecked(true);
            selectedId = navigationSeletedId;
        }
    }

    @Override
    public void backstackClaims() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            toolbarTitle.setText("Dash Board");
            navigationView.getMenu().getItem(0).setChecked(true);
            navigationSeletedPosition = 0;
            selectedId = R.id.nav_dash_board;
        }
    }

    @Override
    public void changeLoginStatus(String status) {
        navigationView.getMenu().getItem(6).setTitle(status);
    }

    @Override
    public void launchRecentClaims() {
        navigateClaims();
    }
}
