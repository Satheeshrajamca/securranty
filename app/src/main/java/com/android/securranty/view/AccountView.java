package com.android.securranty.view;

import android.support.annotation.DrawableRes;

/**
 * Created by satheeshr on 21-11-2017.
 */

public interface AccountView extends BaseView {
    void showEditView(int visible, int gone);

    void setTag(String account_state);

    void changeIcon(@DrawableRes int icon);
}
