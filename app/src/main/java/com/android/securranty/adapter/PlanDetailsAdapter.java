package com.android.securranty.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.model.AdPagePlan;
import com.android.securranty.view.activity.plans.buy_plan.PlanDetailsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by satheeshr on 19-02-2018.
 */


public class PlanDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<AdPagePlan> listAdPagePlans, listUpdatedAdPagePlans, listCopyAdPagePlans;
    private String planSummary, imageUrl;
    private Context context;
    private Typeface openSansRegular, openSansSemiBold;
    PlanDetailsActivity activity;
    private String coverageAmt = "", coverageCondition = "", coverageType = "";
    private String title, renewalText;


    public interface ListenerBuyPlan {
        void buyPlan(Long purchasePlanId, Double warrantyPrice, String coverageName);

        void scrollTo();

        void onClickDeductible();
    }

    public PlanDetailsAdapter(PlanDetailsActivity context, String planSummary, String imageUrl, List<AdPagePlan> listAdPagePlans, String title, String renewalText) {
        this.listAdPagePlans = listAdPagePlans;
        this.listUpdatedAdPagePlans = new ArrayList<>();
        listCopyAdPagePlans = new ArrayList<>();
        listCopyAdPagePlans.add(new AdPagePlan());
        this.planSummary = planSummary;
        this.imageUrl = imageUrl;
        this.context = context;
        this.title = title;
        this.renewalText = renewalText;
        activity = context;
        openSansRegular = Typeface.createFromAsset(context.getAssets(),
                "fonts/opensans_regular.ttf");
        openSansSemiBold = Typeface.createFromAsset(context.getAssets(),
                "fonts/opensans_semibold.ttf");
    }

    @Override
    public int getItemCount() {
        return listCopyAdPagePlans.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_price_plans, parent, false);
            return new VHItem(viewItem);
        } else if (viewType == TYPE_HEADER) {
            //inflate your layout and pass it to view holder
            View viewHeader = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_plan_header, parent, false);
            return new VHHeader(viewHeader);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    private List<SearchListItem> listCoverageAmt, listCoverageType, listCoverageItemCond;

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            ((VHHeader) holder).planTitle.setText(title);
            ((VHHeader) holder).textRenewalText.setText(renewalText);
            //cast holder to VHHeader and set data for header.
            /*if (planSummary.isEmpty()) {
                ((VHHeader) holder).textRenewalText.setVisibility(View.GONE);
            } else {
                ((VHHeader) holder).textRenewalText.setText(planSummary);
            }*/
            Picasso.with(context).load(imageUrl).error(R.drawable.place_holder).into(((VHHeader) holder).imagePlan, new Callback() {
                @Override
                public void onSuccess() {
                    ((VHHeader) holder).progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    ((VHHeader) holder).progressBar.setVisibility(View.GONE);
                }
            });

            ((VHHeader) holder).btnSelectCoverageAmt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listCoverageAmt = new ArrayList<>();
                    for (AdPagePlan adPagePlan : listAdPagePlans) {
                        listCoverageAmt.add(new SearchListItem(0, adPagePlan.getFriendlyName()));
                    }
                    Log.i("ListInfo", listCoverageAmt.toString());
                    HashSet<SearchListItem> hashSet = new HashSet(listCoverageAmt);
                    listCoverageAmt.clear();
                    listCoverageAmt.addAll(hashSet);
                    Collections.sort(listCoverageAmt, new SearchListItem());
                    showCoverageAmtList(((VHHeader) holder).btnSelectCoverageAmt, ((VHHeader) holder).btnSelectItemCond,
                            ((VHHeader) holder).btnSelectCoverageType, listCoverageAmt, "Select Coverage Amount");
                }
            });
            ((VHHeader) holder).btnSelectItemCond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!coverageAmt.isEmpty()) {
                        listCoverageItemCond = new ArrayList<>();
                        for (AdPagePlan adPagePlan : listAdPagePlans) {
                            if (adPagePlan.getFriendlyName().equals(coverageAmt)) {
                                listCoverageItemCond.add(new SearchListItem(1, adPagePlan.getConditionName()));
                            }
                        }
                        HashSet<SearchListItem> hashSet = new HashSet(listCoverageItemCond);
                        listCoverageItemCond.clear();
                        listCoverageItemCond.addAll(hashSet);
                        Collections.sort(listCoverageItemCond, new SearchListItem());
                        showCoverageAmtList(((VHHeader) holder).btnSelectCoverageAmt, ((VHHeader) holder).btnSelectItemCond,
                                ((VHHeader) holder).btnSelectCoverageType, listCoverageItemCond, "Select Item Condition");
                    } else {
                        Toast.makeText(context, "Please select coverage amount", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            ((VHHeader) holder).btnSelectCoverageType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!coverageAmt.isEmpty()) {
                        if (!coverageCondition.isEmpty()) {
                            listCoverageType = new ArrayList<>();
                            listUpdatedAdPagePlans.clear();
                            for (AdPagePlan adPagePlan : listAdPagePlans) {
                                if (adPagePlan.getFriendlyName().equals(coverageAmt)) {
                                    if (adPagePlan.getConditionName().equals(coverageCondition)) {
                                        listCoverageType.add(new SearchListItem(2, adPagePlan.getWarrantySummaryName()));
                                        listUpdatedAdPagePlans.add(adPagePlan);
                                    }
                                }
                            }
                            HashSet<SearchListItem> hashSet = new HashSet(listCoverageType);
                            listCoverageType.clear();
                            listCoverageType.addAll(hashSet);
                            Collections.sort(listCoverageType, new SearchListItem());
                            showCoverageAmtList(((VHHeader) holder).btnSelectCoverageAmt, ((VHHeader) holder).btnSelectItemCond,
                                    ((VHHeader) holder).btnSelectCoverageType, listCoverageType, "Select Coverage Type");
                        } else {
                            Toast.makeText(context, "Please select item condition", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Please select coverage amount", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            ((VHHeader) holder).textDeductible.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onRefundHelp();
                }
            });

        } else if (holder instanceof VHItem) {
            final AdPagePlan dataItem = listCopyAdPagePlans.get(position);
            ((VHItem) holder).title.setText(dataItem.getCoverageTermName() + "-Plan");
            ((VHItem) holder).perMonthPrice.setText(context.getResources().getString(R.string.str_per_month_price, dataItem.getPerMonthPrice()));
            ((VHItem) holder).retailPrice.setText("$" + String.valueOf(dataItem.getRetail()));
            if ((listAdPagePlans.size() - 1) == position)
                ((VHItem) holder).buttonBuyPlan.setVisibility(View.VISIBLE);
            else
                ((VHItem) holder).buttonBuyPlan.setVisibility(View.GONE);
            ((VHItem) holder).buttonBuyPlan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //activity.buyPlan(dataItem.getId(), dataItem.getRetail(), dataItem.getCoverageTermName() + "-Plan");
                }
            });
            activity.scrollTo();
        }
    }

    private AdPagePlan getItem(int position) {
        return listCopyAdPagePlans.get(position - 1);
    }

    private void showCoverageAmtList(final Button btnCoverageAmt, final Button btnCoverageItemCond, final Button btnCoverageType,
                                     final List<SearchListItem> listCoverage, String title) {

        final SearchableDialog searchableDialog = new SearchableDialog(activity, listCoverage, title);
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                switch (searchListItem.getId()) {
                    case 0:
                        btnCoverageAmt.setText(searchListItem.getTitle());
                        btnCoverageItemCond.setText("");
                        btnCoverageType.setText("");
                        coverageAmt = searchListItem.getTitle();
                        coverageCondition = "";
                        coverageType = "";
                        break;
                    case 1:
                        btnCoverageItemCond.setText(searchListItem.getTitle());
                        btnCoverageType.setText("");
                        coverageCondition = searchListItem.getTitle();
                        coverageType = "";
                        break;
                    case 2:
                        //listCopyAdPagePlans.clear();
                        btnCoverageType.setText(searchListItem.getTitle());
                        coverageType = searchListItem.getTitle();
                        for (AdPagePlan adPagePlan : listUpdatedAdPagePlans) {
                            if (adPagePlan.getWarrantySummaryName().equals(coverageType)) {
                                listCopyAdPagePlans.add(adPagePlan);
                            }
                        }
                        activity.scrollTo();
                        notifyDataSetChanged();
                        break;
                }
            }
        });
    }

    /*public class VHItem extends RecyclerView.ViewHolder {
        public TextView title, perMonthPrice, retailPrice;
        public Button buttonBuyPlan;

        public VHItem(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.planName);
            perMonthPrice = (TextView) itemView.findViewById(R.id.perMonthPrice);
            buttonBuyPlan = (Button) itemView.findViewById(R.id.buttonBuyPlan);
            //perMonthPrice.setPaintFlags(perMonthPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            retailPrice = (TextView) itemView.findViewById(R.id.retailPrice);
            //Set font
            title.setTypeface(openSansSemiBold);
            perMonthPrice.setTypeface(openSansRegular);
            buttonBuyPlan.setTypeface(openSansSemiBold);
        }
    }*/
    public class VHItem extends RecyclerView.ViewHolder {
        public TextView title, perMonthPrice, retailPrice, totalAmtDesc;
        public Button buttonBuyPlan;

        public VHItem(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.planName);
            perMonthPrice = (TextView) itemView.findViewById(R.id.monthlyAmt);
            retailPrice = (TextView) itemView.findViewById(R.id.totalAmt);
            totalAmtDesc = (TextView) itemView.findViewById(R.id.planNameSubhead);

            buttonBuyPlan = (Button) itemView.findViewById(R.id.buyNow);
            //perMonthPrice.setPaintFlags(perMonthPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            //Set font
            title.setTypeface(openSansSemiBold);
            perMonthPrice.setTypeface(openSansRegular);
            retailPrice.setTypeface(openSansRegular);
            totalAmtDesc.setTypeface(openSansRegular);
            buttonBuyPlan.setTypeface(openSansSemiBold);

        }
    }

    public class VHHeader extends RecyclerView.ViewHolder {
        TextView textRenewalText, textDeductible, planTitle;
        ImageView imagePlan;
        ProgressBar progressBar;
        public Button btnSelectCoverageAmt, btnSelectItemCond, btnSelectCoverageType;

        public VHHeader(View itemView) {
            super(itemView);
            planTitle = (TextView) itemView.findViewById(R.id.planTitle);
            textRenewalText = (TextView) itemView.findViewById(R.id.textRenewalText);
            imagePlan = (ImageView) itemView.findViewById(R.id.planImage);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            btnSelectCoverageAmt = (Button) itemView.findViewById(R.id.btnSelectCoverageAmt);
            btnSelectItemCond = (Button) itemView.findViewById(R.id.btnSelectItemCond);
            btnSelectCoverageType = (Button) itemView.findViewById(R.id.btnSelectCoverageType);
            textRenewalText.setTypeface(openSansRegular);
            planTitle.setTypeface(openSansRegular);
            textDeductible = (TextView) itemView.findViewById(R.id.textDeductible);
            /*btnSelectCoverageAmt.setOnClickListener(PlanDetailsAdapter.this);
            btnSelectItemCond.setOnClickListener(PlanDetailsAdapter.this);
            btnSelectCoverageType.setOnClickListener(PlanDetailsAdapter.this);*/
        }
    }

}
