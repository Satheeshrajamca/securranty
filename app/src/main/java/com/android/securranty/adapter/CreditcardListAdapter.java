package com.android.securranty.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.CreditCardList;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class CreditcardListAdapter extends RecyclerView.Adapter<CreditcardListAdapter.ViewHolder> {
    private List<CreditCardList.CreditCardListItem> creditCardList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_creditcard_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textCardName.setText(creditCardList.get(position).getCardType());
        holder.textAccountNo4Digit.setText("..." + creditCardList.get(position).getLastFour());
        if (!creditCardList.get(position).getPrimaryCard().equals("No")) {
            holder.textPrimaryCard.setVisibility(View.VISIBLE);
        }
        Picasso.with(SecurrantyApp.context).load(getCardType(creditCardList.get(position).getCardType()))
                .into(holder.imageCreditCard);
    }

    private int getCardType(String cardType) {
        switch (cardType) {
            case "MasterCard":
                return R.drawable.master;
            case "AmericanExpress":
                return R.drawable.amex;
            default:
                return R.drawable.visa;
        }
    }

    @Override
    public int getItemCount() {
        return creditCardList.size();
    }

    public void setData(List<CreditCardList.CreditCardListItem> creditCardList) {
        this.creditCardList = creditCardList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textCardName, textAccountNo4Digit, textPrimaryCard;
        public ImageView imageCreditCard;

        public ViewHolder(View itemView) {
            super(itemView);
            textCardName = (TextView) itemView.findViewById(R.id.textCardName);
            textAccountNo4Digit = (TextView) itemView.findViewById(R.id.textAccountNo4Digit);
            textPrimaryCard = (TextView) itemView.findViewById(R.id.textPrimaryCard);
            imageCreditCard=(ImageView)itemView.findViewById(R.id.imageCreditCard);
            Typeface openSansRegular = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_regular.ttf");
            Typeface openSansSemiBold = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_semibold.ttf");
            textCardName.setTypeface(openSansSemiBold);
            textAccountNo4Digit.setTypeface(openSansRegular);
            textPrimaryCard.setTypeface(openSansRegular);
        }
    }
}
