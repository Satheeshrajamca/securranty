package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PolicyModel{

	@SerializedName("deviceModels")
	private List<DeviceModelsItem> deviceModels;

	@SerializedName("statusCode")
	private int statusCode;

	@SerializedName("status")
	private String status;

	public List<DeviceModelsItem> getDeviceModels(){
		return deviceModels;
	}

	public int getStatusCode(){
		return statusCode;
	}

	public String getStatus(){
		return status;
	}
}
