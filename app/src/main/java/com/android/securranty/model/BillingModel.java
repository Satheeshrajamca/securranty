package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class BillingModel {

    @SerializedName("billingProfile")
    private BillingProfile billingProfile;

    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("status")
    private String status;

    public void setBillingProfile(BillingProfile billingProfile) {
        this.billingProfile = billingProfile;
    }

    public BillingProfile getBillingProfile() {
        return billingProfile;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public class BillingProfile {

        @SerializedName("BillingAddress1")
        private String billingAddress1;

        @SerializedName("BillingCity")
        private String billingCity;

        @SerializedName("BillingAddress2")
        private String billingAddress2;

        @SerializedName("BillingMethod")
        private String billingMethod;

        @SerializedName("Billingzip")
        private String billingzip;

        @SerializedName("BillingContact")
        private String billingContact;

        @SerializedName("BillingCountry")
        private String billingCountry;

        @SerializedName("Id")
        private String id;

        @SerializedName("BillingContactEmails")
        private String billingContactEmails;

        @SerializedName("BillingPO")
        private String billingPO;

        @SerializedName("BillingContactPhone")
        private String billingContactPhone;

        @SerializedName("BillingEmail")
        private String billingEmail;

        @SerializedName("BillingState")
        private String billingState;

        public void setBillingAddress1(String billingAddress1) {
            this.billingAddress1 = billingAddress1;
        }

        public String getBillingAddress1() {
            return billingAddress1;
        }

        public void setBillingCity(String billingCity) {
            this.billingCity = billingCity;
        }

        public String getBillingCity() {
            return billingCity;
        }

        public void setBillingAddress2(String billingAddress2) {
            this.billingAddress2 = billingAddress2;
        }

        public String getBillingAddress2() {
            return billingAddress2;
        }

        public void setBillingMethod(String billingMethod) {
            this.billingMethod = billingMethod;
        }

        public String getBillingMethod() {
            return billingMethod;
        }

        public void setBillingzip(String billingzip) {
            this.billingzip = billingzip;
        }

        public String getBillingzip() {
            return billingzip;
        }

        public void setBillingContact(String billingContact) {
            this.billingContact = billingContact;
        }

        public String getBillingContact() {
            return billingContact;
        }

        public void setBillingCountry(String billingCountry) {
            this.billingCountry = billingCountry;
        }

        public String getBillingCountry() {
            return billingCountry;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setBillingContactEmails(String billingContactEmails) {
            this.billingContactEmails = billingContactEmails;
        }

        public String getBillingContactEmails() {
            return billingContactEmails;
        }

        public void setBillingPO(String billingPO) {
            this.billingPO = billingPO;
        }

        public String getBillingPO() {
            return billingPO;
        }

        public void setBillingContactPhone(String billingContactPhone) {
            this.billingContactPhone = billingContactPhone;
        }

        public String getBillingContactPhone() {
            return billingContactPhone;
        }

        public void setBillingEmail(String billingEmail) {
            this.billingEmail = billingEmail;
        }

        public String getBillingEmail() {
            return billingEmail;
        }

        public void setBillingState(String billingState) {
            this.billingState = billingState;
        }

        public String getBillingState() {
            return billingState;
        }
    }
}
