
package com.android.securranty.view.activity.account;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class AccountProfile {

    @Expose
    private String Address1;
    @Expose
    private Object Address2;
    @Expose
    private String City;
    @Expose
    private String CompanyName;
    @Expose
    private String ContactEmail;
    @Expose
    private Object ContactName;
    @Expose
    private String Country;
    @Expose
    private String Email;
    @Expose
    private String FirstName;
    @Expose
    private String Id;
    @Expose
    private String LastName;
    @Expose
    private String MobilePhone;
    @Expose
    private String OfficePhone;
    @Expose
    private String ProfileImage;
    @Expose
    private Boolean SalesTax;
    @Expose
    private Object SalesTaxCountry;
    @Expose
    private Object SalesTaxPercent;
    @Expose
    private String State;
    @Expose
    private String Zip;

    public String getAddress1() {
        return Address1;
    }

    public Object getAddress2() {
        return Address2;
    }

    public String getCity() {
        return City;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public String getContactEmail() {
        return ContactEmail;
    }

    public Object getContactName() {
        return ContactName;
    }

    public String getCountry() {
        return Country;
    }

    public String getEmail() {
        return Email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getId() {
        return Id;
    }

    public String getLastName() {
        return LastName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public String getOfficePhone() {
        return OfficePhone;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public Boolean getSalesTax() {
        return SalesTax;
    }

    public Object getSalesTaxCountry() {
        return SalesTaxCountry;
    }

    public Object getSalesTaxPercent() {
        return SalesTaxPercent;
    }

    public String getState() {
        return State;
    }

    public String getZip() {
        return Zip;
    }

    public static class Builder {

        private String Address1;
        private Object Address2;
        private String City;
        private String CompanyName;
        private String ContactEmail;
        private Object ContactName;
        private String Country;
        private String Email;
        private String FirstName;
        private String Id;
        private String LastName;
        private String MobilePhone;
        private String OfficePhone;
        private String ProfileImage;
        private Boolean SalesTax;
        private Object SalesTaxCountry;
        private Object SalesTaxPercent;
        private String State;
        private String Zip;

        public AccountProfile.Builder withAddress1(String Address1) {
            this.Address1 = Address1;
            return this;
        }

        public AccountProfile.Builder withAddress2(Object Address2) {
            this.Address2 = Address2;
            return this;
        }

        public AccountProfile.Builder withCity(String City) {
            this.City = City;
            return this;
        }

        public AccountProfile.Builder withCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
            return this;
        }

        public AccountProfile.Builder withContactEmail(String ContactEmail) {
            this.ContactEmail = ContactEmail;
            return this;
        }

        public AccountProfile.Builder withContactName(Object ContactName) {
            this.ContactName = ContactName;
            return this;
        }

        public AccountProfile.Builder withCountry(String Country) {
            this.Country = Country;
            return this;
        }

        public AccountProfile.Builder withEmail(String Email) {
            this.Email = Email;
            return this;
        }

        public AccountProfile.Builder withFirstName(String FirstName) {
            this.FirstName = FirstName;
            return this;
        }

        public AccountProfile.Builder withId(String Id) {
            this.Id = Id;
            return this;
        }

        public AccountProfile.Builder withLastName(String LastName) {
            this.LastName = LastName;
            return this;
        }

        public AccountProfile.Builder withMobilePhone(String MobilePhone) {
            this.MobilePhone = MobilePhone;
            return this;
        }

        public AccountProfile.Builder withOfficePhone(String OfficePhone) {
            this.OfficePhone = OfficePhone;
            return this;
        }

        public AccountProfile.Builder withProfileImage(String ProfileImage) {
            this.ProfileImage = ProfileImage;
            return this;
        }

        public AccountProfile.Builder withSalesTax(Boolean SalesTax) {
            this.SalesTax = SalesTax;
            return this;
        }

        public AccountProfile.Builder withSalesTaxCountry(Object SalesTaxCountry) {
            this.SalesTaxCountry = SalesTaxCountry;
            return this;
        }

        public AccountProfile.Builder withSalesTaxPercent(Object SalesTaxPercent) {
            this.SalesTaxPercent = SalesTaxPercent;
            return this;
        }

        public AccountProfile.Builder withState(String State) {
            this.State = State;
            return this;
        }

        public AccountProfile.Builder withZip(String Zip) {
            this.Zip = Zip;
            return this;
        }

        public AccountProfile build() {
            AccountProfile AccountProfile = new AccountProfile();
            AccountProfile.Address1 = Address1;
            AccountProfile.Address2 = Address2;
            AccountProfile.City = City;
            AccountProfile.CompanyName = CompanyName;
            AccountProfile.ContactEmail = ContactEmail;
            AccountProfile.ContactName = ContactName;
            AccountProfile.Country = Country;
            AccountProfile.Email = Email;
            AccountProfile.FirstName = FirstName;
            AccountProfile.Id = Id;
            AccountProfile.LastName = LastName;
            AccountProfile.MobilePhone = MobilePhone;
            AccountProfile.OfficePhone = OfficePhone;
            AccountProfile.ProfileImage = ProfileImage;
            AccountProfile.SalesTax = SalesTax;
            AccountProfile.SalesTaxCountry = SalesTaxCountry;
            AccountProfile.SalesTaxPercent = SalesTaxPercent;
            AccountProfile.State = State;
            AccountProfile.Zip = Zip;
            return AccountProfile;
        }

    }

}
