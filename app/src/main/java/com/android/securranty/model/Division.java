
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

public class Division {

    @Expose
    private Boolean Active;
    @Expose
    private int Id;
    @Expose
    private String Name;

    public Boolean getActive() {
        return Active;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public static class Builder {

        private Boolean Active;
        private int Id;
        private String Name;

        public Division.Builder withActive(Boolean Active) {
            this.Active = Active;
            return this;
        }

        public Division.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public Division.Builder withName(String Name) {
            this.Name = Name;
            return this;
        }

        public Division build() {
            Division Division = new Division();
            Division.Active = Active;
            Division.Id = Id;
            Division.Name = Name;
            return Division;
        }

    }

}
