
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CostCenterModel {

    @SerializedName("costCenters")
    private List<CostCenter> CostCenters;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<CostCenter> getCostCenters() {
        return CostCenters;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<CostCenter> CostCenters;
        private String Status;
        private Long StatusCode;

        public CostCenterModel.Builder withCostCenters(List<CostCenter> costCenters) {
            CostCenters = costCenters;
            return this;
        }

        public CostCenterModel.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public CostCenterModel.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public CostCenterModel build() {
            CostCenterModel CostCenterModel = new CostCenterModel();
            CostCenterModel.CostCenters = CostCenters;
            CostCenterModel.Status = Status;
            CostCenterModel.StatusCode = StatusCode;
            return CostCenterModel;
        }

    }

}
