package com.android.securranty.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.securranty.view.fragment.BillingFragment;

import java.util.List;

/**
 * Created by satheeshraja on 4/8/18.
 */

public class AdapterFileClaim extends FragmentPagerAdapter {
    private List<String> listHeadings;

    public AdapterFileClaim(FragmentManager fm, List<String> listHeadings) {
        super(fm);
        this.listHeadings = listHeadings;
    }

    /**
     * Return the Fragment associated with a specified position.
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return new BillingFragment();
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return new BillingFragment();
            case 2: // Fragment # 1 - This will show SecondFragment
                return new BillingFragment();
            case 3: // Fragment # 1 - This will show SecondFragment
                return new BillingFragment();
            case 4: // Fragment # 1 - This will show SecondFragment
                return new BillingFragment();
            case 5: // Fragment # 1 - This will show SecondFragment
                return new BillingFragment();
            default:
                return null;
        }
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return listHeadings.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listHeadings.get(position);
    }
}
