package com.android.securranty.view.activity.account;

import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.MyAccount;

public class UpdateAccountProfile {


    public UpdateAccountProfile(APISuccessModel updateAccount, AccountProfileModel updateProfileImage) {
        this.updateAccount = updateAccount;
        this.updateProfileImage = updateProfileImage;
    }

    public APISuccessModel updateAccount;
    public AccountProfileModel updateProfileImage;

    public APISuccessModel getUpdateAccount() {
        return updateAccount;
    }

    public AccountProfileModel getUpdateProfileImage() {
        return updateProfileImage;
    }
}
