
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class FileClaim {

    @Expose
    private Long ClaimServicePereferenceId;
    @Expose
    private int ClaimTypeId;
    @Expose
    private Boolean ConfirmInfoTrue;
    @Expose
    private Long DeviceId;
    @Expose
    private FileClaim FileClaim;
    @Expose
    private Boolean IsNotSLAFiveDay;
    @Expose
    private Boolean IsSLAFiveDay;
    @Expose
    private String IssueDate;
    @Expose
    private String PayProfileId;
    @Expose
    private Long PolicyId;
    @Expose
    private String ProblemDescription;
    @Expose
    private int ShippingAddressId;
    @Expose
    private List<Long> SubClaimTypeId;
    @Expose
    private String UserId;

    public Long getClaimServicePereferenceId() {
        return ClaimServicePereferenceId;
    }

    public int getClaimTypeId() {
        return ClaimTypeId;
    }

    public Boolean getConfirmInfoTrue() {
        return ConfirmInfoTrue;
    }

    public Long getDeviceId() {
        return DeviceId;
    }

    public FileClaim getFileClaim() {
        return FileClaim;
    }


    @Override
    public String toString() {
        return "FileClaim{" +
                "ClaimServicePereferenceId=" + getClaimServicePereferenceId() +
                ", ClaimTypeId=" + getClaimTypeId() +
                ", ConfirmInfoTrue=" + getConfirmInfoTrue() +
                ", DeviceId=" + getDeviceId() +
                ", IsNotSLAFiveDay=" + getIsNotSLAFiveDay() +
                ", IsSLAFiveDay=" + getIsSLAFiveDay() +
                ", IssueDate='" + getIssueDate() + '\'' +
                ", PayProfileId='" + getPayProfileId() + '\'' +
                ", PolicyId=" + getPolicyId() +
                ", ProblemDescription='" + getProblemDescription() + '\'' +
                ", ShippingAddressId=" + getShippingAddressId() +
                ", SubClaimTypeId=" + getSubClaimTypeId() +
                ", UserId='" + getUserId() + '\'' +
                '}';
    }

    public Boolean getIsNotSLAFiveDay() {
        return IsNotSLAFiveDay;
    }

    public Boolean getIsSLAFiveDay() {
        return IsSLAFiveDay;
    }

    public String getIssueDate() {
        return IssueDate;
    }

    public String getPayProfileId() {
        return PayProfileId;
    }

    public Long getPolicyId() {
        return PolicyId;
    }

    public String getProblemDescription() {
        return ProblemDescription;
    }

    public int getShippingAddressId() {
        return ShippingAddressId;
    }

    public List<Long> getSubClaimTypeId() {
        return SubClaimTypeId;
    }

    public String getUserId() {
        return UserId;
    }

    public static class Builder {

        private Long ClaimServicePereferenceId;
        private int ClaimTypeId;
        private Boolean ConfirmInfoTrue;
        private Long DeviceId;
        private FileClaim FileClaim;
        private Boolean IsNotSLAFiveDay;
        private Boolean IsSLAFiveDay;
        private String IssueDate;
        private String PayProfileId;
        private Long PolicyId;
        private String ProblemDescription;
        private int ShippingAddressId;
        private List<Long> SubClaimTypeId;
        private String UserId;

        public FileClaim.Builder withClaimServicePereferenceId(Long ClaimServicePereferenceId) {
            this.ClaimServicePereferenceId = ClaimServicePereferenceId;
            return this;
        }

        public FileClaim.Builder withClaimTypeId(int ClaimTypeId) {
            this.ClaimTypeId = ClaimTypeId;
            return this;
        }

        public FileClaim.Builder withConfirmInfoTrue(Boolean ConfirmInfoTrue) {
            this.ConfirmInfoTrue = ConfirmInfoTrue;
            return this;
        }

        public FileClaim.Builder withDeviceId(Long DeviceId) {
            this.DeviceId = DeviceId;
            return this;
        }

        public FileClaim.Builder withFileClaim(FileClaim FileClaim) {
            this.FileClaim = FileClaim;
            return this;
        }

        public FileClaim.Builder withIsNotSLAFiveDay(Boolean IsNotSLAFiveDay) {
            this.IsNotSLAFiveDay = IsNotSLAFiveDay;
            return this;
        }

        public FileClaim.Builder withIsSLAFiveDay(Boolean IsSLAFiveDay) {
            this.IsSLAFiveDay = IsSLAFiveDay;
            return this;
        }

        public FileClaim.Builder withIssueDate(String IssueDate) {
            this.IssueDate = IssueDate;
            return this;
        }

        public FileClaim.Builder withPayProfileId(String PayProfileId) {
            this.PayProfileId = PayProfileId;
            return this;
        }

        public FileClaim.Builder withPolicyId(Long PolicyId) {
            this.PolicyId = PolicyId;
            return this;
        }

        public FileClaim.Builder withProblemDescription(String ProblemDescription) {
            this.ProblemDescription = ProblemDescription;
            return this;
        }

        public FileClaim.Builder withShippingAddressId(int ShippingAddressId) {
            this.ShippingAddressId = ShippingAddressId;
            return this;
        }

        public FileClaim.Builder withSubClaimTypeId(List<Long> SubClaimTypeId) {
            this.SubClaimTypeId = SubClaimTypeId;
            return this;
        }

        public FileClaim.Builder withUserId(String UserId) {
            this.UserId = UserId;
            return this;
        }

        public FileClaim build() {
            FileClaim FileClaim = new FileClaim();
            FileClaim.ClaimServicePereferenceId = ClaimServicePereferenceId;
            FileClaim.ClaimTypeId = ClaimTypeId;
            FileClaim.ConfirmInfoTrue = ConfirmInfoTrue;
            FileClaim.DeviceId = DeviceId;
            FileClaim.FileClaim = FileClaim;
            FileClaim.IsNotSLAFiveDay = IsNotSLAFiveDay;
            FileClaim.IsSLAFiveDay = IsSLAFiveDay;
            FileClaim.IssueDate = IssueDate;
            FileClaim.PayProfileId = PayProfileId;
            FileClaim.PolicyId = PolicyId;
            FileClaim.ProblemDescription = ProblemDescription;
            FileClaim.ShippingAddressId = ShippingAddressId;
            FileClaim.SubClaimTypeId = SubClaimTypeId;
            FileClaim.UserId = UserId;
            return FileClaim;
        }

    }

}
