package com.android.securranty.view.activity.shipping;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.BillingModel;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.StatesList;
import com.android.securranty.presenter.implementor.MyAccountPresenterImpl;
import com.android.securranty.utility.PhoneNumberTextWatcher;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.AccountView;
import com.android.securranty.view.activity.signs.SignInActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.account.AccountProfileActivity.ACCOUNT_EDIT;
import static com.android.securranty.view.activity.account.AccountProfileActivity.ACCOUNT_IDLE;

/**
 * Created by satheeshr on 27-11-2017.
 */

public class AddShippingAddressActivity extends AppCompatActivity implements AccountView {

    @BindView(R.id.editBillingContactName)
    EditText editBillingContactName;
    @BindView(R.id.editBillingCompany)
    EditText editBillingCompany;
    @BindView(R.id.editContactPhone)
    EditText editContactPhone;
    @BindView(R.id.editBillingEmail)
    EditText editBillingEmail;
    @BindView(R.id.editAddress1)
    EditText editAddress1;
    @BindView(R.id.editAddress2)
    EditText editAddress2;
    @BindView(R.id.editBillingCity)
    EditText editBillingCity;
    @BindView(R.id.editState)
    EditText editBillingState;
    @BindView(R.id.editZip)
    EditText editZip;
    @BindView(R.id.editCountry)
    EditText editCountry;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageEdit)
    ImageView imageEdit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private MyAccountPresenterImpl myAccountPresenter;
    private String tagBillingStatus = ACCOUNT_EDIT;
    private int selectedCountryId;
    private ActionBar actionBar;
    private int countryId;
    private List<SearchListItem> listCountry, stateList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shipping_address);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        editContactPhone.addTextChangedListener(new PhoneNumberTextWatcher(editContactPhone));
        myAccountPresenter = new MyAccountPresenterImpl(this);
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        editBillingCompany.setTypeface(openSansSemiBold);
        editBillingContactName.setTypeface(openSansSemiBold);
        editContactPhone.setTypeface(openSansSemiBold);
        editBillingEmail.setTypeface(openSansSemiBold);
        editAddress1.setTypeface(openSansSemiBold);
        editAddress2.setTypeface(openSansSemiBold);
        editBillingCity.setTypeface(openSansSemiBold);
        editBillingState.setTypeface(openSansSemiBold);
        editZip.setTypeface(openSansSemiBold);
        editCountry.setTypeface(openSansSemiBold);
    }

    @OnClick({R.id.imageEdit, R.id.imageBackButton, R.id.editCountry, R.id.editState,R.id.btnSave})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                saveShippingAddress();
                break;
            case R.id.imageBackButton:
                onBackPressed();
                break;
            case R.id.editCountry:
                if (listCountry == null) {
                    myAccountPresenter.initCountryList();
                } else {
                    displayCountry();
                }
                break;
            case R.id.editState:
                if (selectedCountryId != countryId || stateList == null) {
                    myAccountPresenter.initStatesList(editCountry.getText().toString().trim(), countryId);
                } else
                    displayState();
                break;
        }
    }

    private void saveShippingAddress() {
        myAccountPresenter.addShippingAddress(
                editBillingContactName.getText().toString().trim(),
                editBillingCompany.getText().toString().trim(),
                editBillingEmail.getText().toString().trim(),
                editContactPhone.getText().toString().trim(),
                editAddress1.getText().toString().trim(),
                editAddress2.getText().toString().trim(),
                editCountry.getText().toString().trim(),
                editBillingState.getText().toString().trim(),
                editBillingCity.getText().toString().trim(),
                editZip.getText().toString().trim()
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void displayState() {
        final SearchableDialog searchableDialog = new SearchableDialog(AddShippingAddressActivity.this, stateList, "Select State");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editBillingState.setText(searchListItem.getTitle());
            }
        });
    }

    @Override
    public void showEditView(int editorVisibility, int textVisibility) {
        editBillingContactName.setVisibility(editorVisibility);
        editContactPhone.setVisibility(editorVisibility);
        editBillingEmail.setVisibility(editorVisibility);
        editAddress1.setVisibility(editorVisibility);
        editAddress2.setVisibility(editorVisibility);
        editBillingCity.setVisibility(editorVisibility);
        editBillingState.setVisibility(editorVisibility);
        editZip.setVisibility(editorVisibility);
        editCountry.setVisibility(editorVisibility);

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    private Object billingModel;

    @Override
    public void success(final Object obj) {
        if (obj instanceof APISuccessModel) {
            Intent intent = getIntent();
            intent.putExtra("Key", APISuccessModel.class.cast(obj).getMessage());
            setResult(RESULT_OK, intent);
            finish();
        } else if (obj instanceof CountryList) {
            listCountry = new ArrayList<>();
            for (CountryList.CountriesItem countryList : CountryList.class.cast(obj).getCountries()) {
                listCountry.add(new SearchListItem(countryList.getId(), countryList.getName()));
            }
            displayCountry();
        } else if (obj instanceof StatesList) {
            selectedCountryId = countryId;
            stateList = new ArrayList<>();
            for (StatesList.State states : StatesList.class.cast(obj).getStates()) {
                stateList.add(new SearchListItem(0, states.getStateName()));
            }
            displayState();
        }
    }

    private void displayCountry() {
        final SearchableDialog searchableDialog = new SearchableDialog(AddShippingAddressActivity.this, listCountry, "Select Country");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editCountry.setText(searchListItem.getTitle());
                countryId = searchListItem.getId();
                if (selectedCountryId != countryId) {
                    editBillingState.setText("");
                }
            }
        });
    }

    private void setCountryId(String billingCountry) {
        if (billingCountry != null && !billingCountry.isEmpty()) {
            switch (billingCountry) {
                case "United States":
                    countryId = 2;
                    selectedCountryId = 2;
                    break;
                case "Canada":
                    selectedCountryId = 3;
                    countryId = 3;
                    break;
                default:
                    countryId = 0;
            }
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void setTag(String account_state) {
        tagBillingStatus = account_state;
    }

    @Override
    public void changeIcon(@DrawableRes int save_icon) {
        imageEdit.setImageResource(save_icon);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myAccountPresenter.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            saveShippingAddress();
        } else {
            finish();
        }
    }
}
