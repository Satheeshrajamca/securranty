package com.android.securranty.view.fragment.fileclaim.model;

import com.android.securranty.model.CountryList;
import com.android.securranty.model.CreditCardList;

/**
 * Created by satheeshr on 28-12-2017.
 */

public class PaymentReviewandCountryList {

    public PaymentReviewandCountryList(PaymentandReview paymentAndReview, CreditCardList creditCardList) {
        this.creditCardList = creditCardList;
        this.paymentAndReview = paymentAndReview;
    }

    private CreditCardList creditCardList;
    private PaymentandReview paymentAndReview;

    public CreditCardList getCreditCardList() {
        return creditCardList;
    }

    public PaymentandReview getPaymentAndReview() {
        return paymentAndReview;
    }
}

