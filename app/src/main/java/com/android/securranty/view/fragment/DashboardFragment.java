package com.android.securranty.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.AdvertiseImages;
import com.android.securranty.presenter.implementor.DashBoardPresenterImp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.creditcard.AddEditCreditcardActivity;
import com.android.securranty.view.activity.faq.FAQActivity;
import com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity;
import com.android.securranty.view.activity.plans.buy_plan.AllPlansActivity;
import com.android.securranty.view.activity.plans.buy_plan.PlansCategoryActivity;
import com.android.securranty.view.activity.signs.ChangePasswordActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.activity.tickets.CreateTicketActivity;
import com.android.securranty.view.activity.tickets.TicketListActivity;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.MainActivity.ACCOUNT_PROFILE_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.ACTIVE_PLAN;
import static com.android.securranty.view.activity.MainActivity.CHANGE_PASSWORD;
import static com.android.securranty.view.activity.MainActivity.CLAIMS;
import static com.android.securranty.view.activity.MainActivity.CREATE_TICKET;
import static com.android.securranty.view.activity.MainActivity.CREDIT_CARD_ACTIVITY;
import static com.android.securranty.view.activity.plans.buy_plan.PlansCategoryActivity.PLANID;
import static com.android.securranty.view.fragment.PlanFragment.PURCHASE_PLAN_REQ_CODE;

/**
 * Created by satheeshr on 20-11-2017.
 */

public class DashboardFragment extends Fragment implements BaseView, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    @BindView(R.id.rootlayoutDashBoard)
    LinearLayout rootlayoutDashBoard;
    @BindView(R.id.textWelcome)
    TextView textWelcome;
    @BindView(R.id.textUserName)
    TextView textUserName;
    @BindView(R.id.textUserType)
    TextView textUserType;
    @BindView(R.id.textFileClaim)
    TextView textFileClaim;
    @BindView(R.id.textPurchasePlan)
    TextView textPurchasePlan;
    @BindView(R.id.textReferFrnd)
    TextView textReferFrnd;
    @BindView(R.id.textFAQ)
    TextView textFAQ;
    @BindView(R.id.textAddDevice)
    TextView textAddDevice;
    @BindView(R.id.textSettings)
    TextView textSettings;
    @BindView(R.id.textRecentClaims)
    TextView textRecentClaims;
    @BindView(R.id.relativeFileClaim)
    RelativeLayout relativeFileClaim;
    @BindView(R.id.relativePurchasePlan)
    RelativeLayout relativePurchasePlan;
    @BindView(R.id.relativeAddCards)
    RelativeLayout relativeAddCards;
    @BindView(R.id.relativeTickets)
    RelativeLayout relativeTickets;
    @BindView(R.id.relativeChangePass)
    RelativeLayout relativeChangePass;
    @BindView(R.id.relativeFAQ)
    RelativeLayout relativeFAQ;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.relativeRecentClaims)
    RelativeLayout relativeRecentClaims;


    @BindView(R.id.slider)
    BannerSlider slider;
    private View view;
    private MainActivity mainActivity;

    public interface ListenerRecentClaims {
        void launchRecentClaims();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_dashboard,
                    container, false);
            setHasOptionsMenu(true);
            ButterKnife.bind(this, view);
            textUserName.setText(SharePref.getInstance(SecurrantyApp.context).getFirstName() + " "
                    + SharePref.getInstance(SecurrantyApp.context).getLastName());
            textUserType.setText("User : " + SharePref.getInstance(SecurrantyApp.context).getAccountCategory());
            DashBoardPresenterImp dashBoardPresenter = new DashBoardPresenterImp(this);
            dashBoardPresenter.initAdvertiseImages();
            setTypeFace();
        }
        return view;
    }

    @OnClick({R.id.relativeFAQ, R.id.relativeChangePass, R.id.relativeTickets, R.id.relativeAddCards,
            R.id.relativePurchasePlan, R.id.relativeFileClaim, R.id.relativeRecentClaims})
    public void onClick(View view) {
        if (Utils.isNetworkAvailable(getActivity())) {
            switch (view.getId()) {
                case R.id.relativeFileClaim:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivity(new Intent(getActivity(), ActivePlanListActivity.class));
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, ACTIVE_PLAN);
                    }
                    break;
                case R.id.relativePurchasePlan:
                    startActivityForResult(new Intent(getActivity(), AllPlansActivity.class).putExtra(PLANID, "0"), PURCHASE_PLAN_REQ_CODE);
                    break;
                case R.id.relativeAddCards:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivity(new Intent(getActivity(), AddEditCreditcardActivity.class));
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, CREDIT_CARD_ACTIVITY);
                    }
                    break;
                case R.id.relativeTickets:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivity(new Intent(getActivity(), TicketListActivity.class));
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, CREATE_TICKET);
                    }
                    break;
                case R.id.relativeChangePass:
                    if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                        startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                    } else {
                        Intent i = new Intent(getActivity(), SignInActivity.class);
                        startActivityForResult(i, CHANGE_PASSWORD);
                    }
                    break;
                case R.id.relativeFAQ:
                    startActivity(new Intent(getActivity(), FAQActivity.class));
                    break;
                case R.id.relativeRecentClaims:
                    mainActivity.launchRecentClaims();
                    break;
            }
        } else {
            showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/opensans_semibold.ttf");
        textWelcome.setTypeface(openSansSemiBold);
        textUserName.setTypeface(openSansSemiBold);
        textFileClaim.setTypeface(openSansSemiBold);
        textPurchasePlan.setTypeface(openSansSemiBold);
        textReferFrnd.setTypeface(openSansSemiBold);
        textFAQ.setTypeface(openSansSemiBold);
        textUserType.setTypeface(openSansSemiBold);
        textAddDevice.setTypeface(openSansSemiBold);
        textSettings.setTypeface(openSansSemiBold);
        textRecentClaims.setTypeface(openSansSemiBold);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Toast.makeText(mainActivity, errorMsg, Toast.LENGTH_LONG).show();
        //Snackbar.make(view.findViewById(R.id.relativeRootLayout), errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), SignInActivity.class));
        }
    }

    @Override
    public void success(Object obj) {
        AdvertiseImages advertiseImages = AdvertiseImages.class.cast(obj);
        Log.i("AdvertiseImages", advertiseImages.getAdslist().get(0).getAddimage());
        bannerSlide(advertiseImages);
        //sliderLayout();
    }

    private void bannerSlide(final AdvertiseImages advertiseImages) {
        List<Banner> remoteBanners = new ArrayList<>();
        for (AdvertiseImages.Adslist adslist : advertiseImages.getAdslist()) {
            remoteBanners.add(new RemoteBanner(adslist.getAddimage()
            ));
        }
        slider.setBanners(remoteBanners);
        slider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                AdvertiseImages.Adslist adslist = advertiseImages.getAdslist().get(position);
                if (adslist.getIsRedirectUser().equalsIgnoreCase("Yes")) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(adslist.getRedirectUrl()));
                    startActivity(browserIntent);
                }
            }
        });
    }

    private void sliderLayout() {
        /*HashMap<String, String> url_maps = new HashMap<String, String>();
        for (AdvertiseImages.Adslist advertiseImages : AdvertiseImages.class.cast(obj).getAdslist()) {
            url_maps.put(advertiseImages.getAddimage(), advertiseImages.getAddimage());
        }

        for (String name : url_maps.keySet()) {
            DefaultSliderView sliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            sliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderView.bundle(new Bundle());
            sliderView.getBundle()
                    .putString("extra", name);
            slider.addSlider(sliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(10000);
        slider.addOnPageChangeListener(this);*/
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_myaccount, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.nav_my_account) {
            if (Utils.isNetworkAvailable(getActivity())) {
                if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                    startActivity(new Intent(getActivity(), AccountProfileActivity.class));
                } else {
                    Intent i = new Intent(getActivity(), SignInActivity.class);
                    startActivityForResult(i, ACCOUNT_PROFILE_ACTIVITY);
                }
            } else {
                showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCOUNT_PROFILE_ACTIVITY && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), AccountProfileActivity.class));
        } else if (requestCode == CHANGE_PASSWORD && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
        } else if (requestCode == CREDIT_CARD_ACTIVITY && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), AddEditCreditcardActivity.class));
        } else if (requestCode == CREATE_TICKET && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), TicketListActivity.class));
        } else if (requestCode == ACTIVE_PLAN && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), ActivePlanListActivity.class));
        } else if (requestCode == CLAIMS && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), ActivePlanListActivity.class));
        }


    }

}
