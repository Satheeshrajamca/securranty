package com.android.securranty.view.activity.shipping;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.view.fragment.fileclaim.model.ShippingAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.ViewHolder> implements Filterable {
    private List<ShippingAddress> listShippingAddress;
    private List<ShippingAddress> filteredShippingAddress;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shipping_address, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textShippingAddress.setText(String.valueOf(filteredShippingAddress.get(position).getDisplayText()));
    }

    @Override
    public int getItemCount() {
        return filteredShippingAddress.size();
    }

    public void setData(List<ShippingAddress> listShippingAddress) {
        this.listShippingAddress = listShippingAddress;
        filteredShippingAddress = listShippingAddress;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textShippingAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            textShippingAddress = (TextView) itemView.findViewById(R.id.textShippingAddress);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredShippingAddress = listShippingAddress;
                } else {
                    List<ShippingAddress> filteredList = new ArrayList<>();
                    for (ShippingAddress copyShippingAddress : listShippingAddress) {
                        if (String.valueOf(copyShippingAddress.getDisplayText()).toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(copyShippingAddress);
                        }
                    }
                    filteredShippingAddress = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredShippingAddress;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredShippingAddress = (ArrayList<ShippingAddress>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
