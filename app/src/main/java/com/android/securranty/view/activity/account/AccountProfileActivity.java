package com.android.securranty.view.activity.account;

import com.google.gson.Gson;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.AccountandCreditcardList;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.MyAccount;
import com.android.securranty.model.StatesList;
import com.android.securranty.presenter.implementor.MyAccountPresenterImpl;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.PhoneNumberTextWatcher;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.AccountView;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by satheeshraja on 11/21/17.
 */

public class AccountProfileActivity extends AppCompatActivity implements AccountView {
    @BindView(R.id.labelFirstName)
    TextView labelFirstName;
    @BindView(R.id.lableLastName)
    TextView lableLastName;
    @BindView(R.id.labelEmail)
    TextView labelEmail;
    @BindView(R.id.labelMobile)
    TextView labelMobile;
    @BindView(R.id.labelAddress1)
    TextView labelAddress1;
    @BindView(R.id.labelCompany)
    TextView labelCompany;
    @BindView(R.id.labelContactEmail)
    TextView labelContactEmail;
    @BindView(R.id.labelOfficePhone)
    TextView labelOfficePhone;
    @BindView(R.id.labelAddress2)
    TextView labelAddress2;
    @BindView(R.id.labelCity)
    TextView labelCity;
    @BindView(R.id.labelState)
    TextView labelState;
    @BindView(R.id.labelCountry)
    TextView labelCountry;
    @BindView(R.id.labelZip)
    TextView labelZip;
    /*@BindView(R.id.labelSalesTax)
    TextView labelSalesTax;
    @BindView(R.id.labelSalesTaxCountry)
    TextView labelSalesTaxCountry;
    @BindView(R.id.labelSalesTaxPercent)
    TextView labelSalesTaxPercent;*/

    @BindView(R.id.textFirstName)
    TextView textFirstName;
    @BindView(R.id.textLastName)
    TextView textLastName;
    @BindView(R.id.textEmail)
    TextView textEmail;
    @BindView(R.id.textMobile)
    TextView textMobile;
    @BindView(R.id.textAddress1)
    TextView textAddress1;
    @BindView(R.id.textCompany)
    TextView textCompany;
    @BindView(R.id.textContactEmail)
    TextView textContactEmail;
    @BindView(R.id.textOfficePhone)
    TextView textOfficePhone;
    @BindView(R.id.textAddress2)
    TextView textAddress2;
    @BindView(R.id.textCity)
    TextView textCity;
    @BindView(R.id.textState)
    TextView textState;
    @BindView(R.id.textCountry)
    TextView textCountry;
    @BindView(R.id.textZip)
    TextView textZip;
    /*@BindView(R.id.textSalesTax)
    TextView textSalesTax;
    @BindView(R.id.textSalesTaxCountry)
    TextView textSalesTaxCountry;
    @BindView(R.id.textSalesTaxPercent)
    TextView textSalesTaxPercent;*/

    @BindView(R.id.editFirstName)
    EditText editFirstName;
    @BindView(R.id.editLastName)
    EditText editLastName;
    @BindView(R.id.editEmail)
    EditText editEmail;
    @BindView(R.id.editMobile)
    EditText editMobile;
    @BindView(R.id.editAddress1)
    EditText editAddress1;
    @BindView(R.id.editState)
    EditText editState;
    @BindView(R.id.editCountry)
    EditText editCountry;
    @BindView(R.id.editZip)
    EditText editZip;
    /*@BindView(R.id.editSalesTax)
    EditText editSalesTax;
    @BindView(R.id.editSalesTaxCountry)
    EditText editSalesTaxCountry;
    @BindView(R.id.editSalesTaxPercent)
    EditText editSalesTaxPercent;*/
    @BindView(R.id.editComapny)
    EditText editComapny;
    @BindView(R.id.editContactEmail)
    EditText editContactEmail;
    @BindView(R.id.editOfficePhone)
    EditText editOfficePhone;
    @BindView(R.id.editAddress2)
    EditText editAddress2;
    @BindView(R.id.editCity)
    EditText editCity;
    @BindView(R.id.progressBarImage)
    ProgressBar progressBarImage;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageEdit)
    ImageView imageEdit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.profile_image)
    ImageView profile_image;

    private ActionBar actionBar;
    private MyAccountPresenterImpl myAccountPresenter;
    private int countryId;
    private int selectedCountryId;
    private List<SearchListItem> stateList;
    private String tagAccountStatus = ACCOUNT_EDIT;

    public static final String ACCOUNT_EDIT = "ACCOUNT_EDIT";
    public static final String ACCOUNT_IDLE = "ACCOUNT_IDLE";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        editMobile.addTextChangedListener(new PhoneNumberTextWatcher(editMobile));
        editOfficePhone.addTextChangedListener(new PhoneNumberTextWatcher(editOfficePhone));
        myAccountPresenter = new MyAccountPresenterImpl(this);
        myAccountPresenter.getMyAccountDetails();
        updateProfileImage();
    }

    private void updateProfileImage() {
        if (!SharePref.getInstance(this).getProfileImage().isEmpty()) {
            progressBarImage.setVisibility(View.VISIBLE);
            Picasso.with(this).load(SharePref.getInstance(this).getProfileImage())
                    .into(profile_image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBarImage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBarImage.setVisibility(View.GONE);
                        }
                    });
        }
    }

    @OnClick({R.id.imageEdit, R.id.imageBackButton, R.id.editCountry, R.id.editState, R.id.profile_image})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageEdit:
                myAccountPresenter.editProfile(tagAccountStatus, editFirstName.getText().toString().trim(), editLastName.getText().toString().trim(),
                        editEmail.getText().toString().trim(), editMobile.getText().toString().trim(), editAddress1.getText().toString().trim(),
                        editState.getText().toString().trim(), editCountry.getText().toString().trim(), editZip.getText().toString().trim(),
                        "", "", "", editComapny.getText().toString().trim(),
                        editContactEmail.getText().toString().trim(), editOfficePhone.getText().toString().trim(),
                        editAddress2.getText().toString().trim(), editCity.getText().toString().trim(), fileAccountImage);
                break;
            case R.id.imageBackButton:
                onBackPressed();
                break;
            case R.id.editCountry:
                final SearchableDialog searchableDialog = new SearchableDialog(AccountProfileActivity.this, listCountry, "Select Country");
                searchableDialog.show();
                searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
                    @Override
                    public void onClick(int position, SearchListItem searchListItem) {
                        editCountry.setText(searchListItem.getTitle());
                        countryId = searchListItem.getId();
                        if (selectedCountryId != countryId) {
                            editState.setText("");
                        }
                    }
                });
                break;
            case R.id.editState:
                if (selectedCountryId != countryId || stateList == null)
                    myAccountPresenter.initStatesList(editCountry.getText().toString().trim(), countryId);
                else
                    loadCity();
                break;

            case R.id.profile_image:
                if (tagAccountStatus.equals(ACCOUNT_IDLE)) {
                    selectImage();
                }
                break;
        }
    }

    private File fileAccountImage = null;

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Profile Pic");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    RxImagePicker.with(AccountProfileActivity.this).requestImage(Sources.CAMERA).subscribe(new Consumer<Uri>() {
                        @Override
                        public void accept(@NonNull Uri uri) throws Exception {
                            //uploadImage(convertFilePath(uri));
                            fileAccountImage = convertFilePath(uri);
                            Picasso.with(AccountProfileActivity.this).load(fileAccountImage).into(profile_image);

                        }
                    });
                } else if (items[item].equals("Choose from Gallery")) {
                    RxImagePicker.with(AccountProfileActivity.this).requestImage(Sources.GALLERY)
                            .flatMap(new Function<Uri, ObservableSource<File>>() {
                                @Override
                                public ObservableSource<File> apply(@NonNull Uri uri) throws Exception {
                                    return RxImageConverters.uriToFile(AccountProfileActivity.this,
                                            uri, new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                                                    "securranty.jpeg"));
                                }
                            }).subscribe(new Consumer<File>() {
                        @Override
                        public void accept(@NonNull File file) throws Exception {
                            fileAccountImage = new Compressor(AccountProfileActivity.this).compressToFile(file);
                            Picasso.with(AccountProfileActivity.this).load(fileAccountImage).skipMemoryCache().into(profile_image);
                            //uploadImage(file);
                        }
                    });

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private File convertFilePath(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            File file = new File(cursor.getString(column_index));
            Log.i("FileSize","Before"+file.length());
            File compressedImageFile = null;
            try {
                compressedImageFile = new Compressor(this).compressToFile(file);
                Log.i("FileSize","After"+compressedImageFile.length());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return compressedImageFile;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }


    }

    /*private void uploadByteImage(byte[] imageBytes) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", "photo_path", requestFile);
        ApiClient.getClient().uploadImage(SharePref.getInstance(SecurrantyApp.context).getUserId(),
                SharePref.getInstance(SecurrantyApp.context).getToken()
                , body).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                if (response.isSuccessful()) {

                } else {
                    ResponseBody errorBody = response.errorBody();
                    Gson gson = new Gson();
                    try {
                        Response errorResponse = gson.fromJson(errorBody.string(), Response.class);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.i("Errorthrows", t.toString());
            }
        });
    }*/

    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }


    private void uploadImage(File file) {
        Log.i("FilePath", file.getAbsolutePath());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo_path", file.getName(), requestFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "photo_path");
        ApiClient.getClient().updateProfileImage(SharePref.getInstance(SecurrantyApp.context).getUserId(),
                SharePref.getInstance(SecurrantyApp.context).getToken()
                , body, name).enqueue(new Callback<AccountProfileModel>() {
            @Override
            public void onResponse(Call<AccountProfileModel> call, Response<AccountProfileModel> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(AccountProfileActivity.this, "Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AccountProfileActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AccountProfileModel> call, Throwable t) {
                Toast.makeText(AccountProfileActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }

        });
    }

    void uploadImageProfile() {
       /* RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), fileAccountImage);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo_path", fileAccountImage.getName(), requestFile);

        // add another part within the multipart request
        RequestBody fullName =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), "photo_path");

        service.updateProfile(id, fullName, body, other)*/
    }

    Bitmap bitmap;

    void uploadImageVolley() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        final String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        //sending image to server
        StringRequest request = new StringRequest(Request.Method.PUT, "http://35.164.107.239/api/AccountProfile/UploadImage?UserId=" + SharePref.getInstance(SecurrantyApp.context).getUserId(), new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s.equals("true")) {
                    Toast.makeText(AccountProfileActivity.this, "Uploaded Successful", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(AccountProfileActivity.this, "Some error occurred!", Toast.LENGTH_LONG).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(AccountProfileActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();
            }
        }) {
            //adding parameters to send
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("file_path", imageString);
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", SharePref.getInstance(SecurrantyApp.context).getToken());
                return headers;
            }
        };

        RequestQueue rQueue = Volley.newRequestQueue(AccountProfileActivity.this);
        rQueue.add(request);
    }


    @Override
    public void onBackPressed() {
        if (tagAccountStatus.equals(ACCOUNT_IDLE)) {
            myAccountPresenter.backPressed(accountandCountryList);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    private List<SearchListItem> listCountry;
    private Object accountandCountryList;

    @Override
    public void success(final Object obj) {
        fileAccountImage = null;
        if (obj instanceof AccountandCreditcardList) {
            accountandCountryList = obj;
            MyAccount.AccountProfile myAccountprofile = AccountandCreditcardList.class.cast(obj).getMyAccount().getAccountProfile();
            listCountry = new ArrayList<>();
            for (CountryList.CountriesItem countryList : AccountandCreditcardList.class.cast(obj).getCountryList().getCountries()) {
                listCountry.add(new SearchListItem(countryList.getId(), countryList.getName()));
            }
            if (myAccountprofile != null) {
                SharePref.getInstance(this).setFirstName(myAccountprofile.getFirstName());
                SharePref.getInstance(this).setLastName(myAccountprofile.getLastName());
                updateAccountLabel(myAccountprofile.getFirstName(), myAccountprofile.getLastName(), myAccountprofile.getEmail(),
                        myAccountprofile.getMobilePhone(), myAccountprofile.getAddress1(), myAccountprofile.getCompanyName(),
                        myAccountprofile.getContactEmail(), myAccountprofile.getOfficePhone(), myAccountprofile.getAddress2(),
                        myAccountprofile.getCity(), myAccountprofile.getState(), myAccountprofile.getCountry(),
                        myAccountprofile.getZip());
                /*editSalesTax.setText(String.valueOf(myAccountprofile.getSalesTax()));
                textSalesTaxCountry.setText(myAccountprofile.getSalesTaxCountry());
                textSalesTaxPercent.setText(myAccountprofile.getSalesTaxPercent());*/
                SharePref.getInstance(this).setProfileImage(myAccountprofile.getProfileImage());
                progressBarImage.setVisibility(View.VISIBLE);
                Picasso.with(AccountProfileActivity.this).load(myAccountprofile.getProfileImage()).error(R.drawable.dashboard_default_avatar)
                        .into(profile_image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressBarImage.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progressBarImage.setVisibility(View.GONE);
                            }
                        });
                editFirstName.setText(myAccountprofile.getFirstName());
                editLastName.setText(myAccountprofile.getLastName());
                editEmail.setText(myAccountprofile.getEmail());

                String mobilePhone = myAccountprofile.getMobilePhone();
                if (mobilePhone != null) {
                    if (!mobilePhone.isEmpty() && !mobilePhone.contains("-")) {
                        editMobile.setText(String.format("%s-%s-%s", mobilePhone.substring(0, 3), mobilePhone.substring(3, 6), mobilePhone.substring(6)));
                    } else {
                        editMobile.setText(myAccountprofile.getMobilePhone());
                    }
                }
                editAddress1.setText(myAccountprofile.getAddress1());
                editComapny.setText(myAccountprofile.getCompanyName());
                editContactEmail.setText(myAccountprofile.getContactEmail());
                String officePhone = myAccountprofile.getOfficePhone();
                if (officePhone != null) {
                    if (!officePhone.isEmpty() && !officePhone.contains("-")) {
                        editOfficePhone.setText(String.format("%s-%s-%s", officePhone.substring(0, 3), officePhone.substring(3, 6), officePhone.substring(6)));
                    } else {
                        editOfficePhone.setText(myAccountprofile.getOfficePhone());
                    }
                }
                editAddress2.setText(myAccountprofile.getAddress2());
                editCity.setText(myAccountprofile.getCity());
                editState.setText(myAccountprofile.getState());
                editCountry.setText(myAccountprofile.getCountry());
                editZip.setText(myAccountprofile.getZip());
                /*editSalesTax.setText(String.valueOf(myAccountprofile.getSalesTax()));
                editSalesTaxCountry.setText(myAccountprofile.getSalesTaxCountry());
                editSalesTaxPercent.setText(myAccountprofile.getSalesTaxPercent());*/
                if (myAccountprofile.getCountry() != null) {
                    setCountryId(myAccountprofile.getCountry());
                }
            } else {
                Snackbar.make(coordinatorLayout, "Account Profile Updated Successfully", Snackbar.LENGTH_LONG).show();
            }
        } else if (obj instanceof APISuccessModel) {
            Toast.makeText(this, ((APISuccessModel) obj).getMessage(), Toast.LENGTH_SHORT).show();
            SharePref.getInstance(this).setFirstName(editFirstName.getText().toString().trim());
            SharePref.getInstance(this).setLastName(editLastName.getText().toString().trim());
            updateAccountLabel(editFirstName.getText().toString().trim(), editLastName.getText().toString().trim(),
                    editEmail.getText().toString().trim(), editMobile.getText().toString().trim(), editAddress1.getText().toString().trim(),
                    editComapny.getText().toString().trim(), editContactEmail.getText().toString().trim(), editOfficePhone.getText().toString().trim(),
                    editAddress2.getText().toString().trim(), editCity.getText().toString().trim(),
                    editState.getText().toString().trim(), editCountry.getText().toString().trim(), editZip.getText().toString().trim()
            );
            myAccountPresenter.getMyAccountDetails();
        } else if (obj instanceof UpdateAccountProfile) {
            AccountProfile accountProfile = UpdateAccountProfile.class.cast(obj).updateProfileImage.getAccountProfile();
            SharePref.getInstance(this).setProfileImage(accountProfile.getProfileImage());
            SharePref.getInstance(this).setFirstName(accountProfile.getFirstName());
            SharePref.getInstance(this).setLastName(accountProfile.getLastName());
            updateAccountLabel(editFirstName.getText().toString().trim(), editLastName.getText().toString().trim(),
                    editEmail.getText().toString().trim(), editMobile.getText().toString().trim(), editAddress1.getText().toString().trim(),
                    editComapny.getText().toString().trim(), editContactEmail.getText().toString().trim(), editOfficePhone.getText().toString().trim(),
                    editAddress2.getText().toString().trim(), editCity.getText().toString().trim(),
                    editState.getText().toString().trim(), editCountry.getText().toString().trim(), editZip.getText().toString().trim());
            Snackbar.make(coordinatorLayout, "Account Profile Updated Successfully", Snackbar.LENGTH_LONG).show();
            myAccountPresenter.getMyAccountDetails();
        } else {
            selectedCountryId = countryId;
            stateList = new ArrayList<>();
            for (StatesList.State states : StatesList.class.cast(obj).getStates()) {
                stateList.add(new SearchListItem(0, states.getStateName()));
            }
            loadCity();
        }
    }

    private void updateAccountLabel(String firstName, String lastName, String email, String mobilePhone,
                                    String address1, String companyName, String contactEmail,
                                    String officePhone, String address2, String city,
                                    String state, String country, String zip) {
        textFirstName.setText(firstName);
        textLastName.setText(lastName);
        textEmail.setText(email);
        if (mobilePhone != null) {
            if (!TextUtils.isEmpty(mobilePhone) && !mobilePhone.contains("-")) {
                textMobile.setText(String.format("%s-%s-%s", mobilePhone.substring(0, 3), mobilePhone.substring(3, 6), mobilePhone.substring(6)));
            } else {
                textMobile.setText(mobilePhone);
            }
        }
        textAddress1.setText(address1);
        textCompany.setText(companyName);
        textContactEmail.setText(contactEmail);
        if (officePhone != null) {
            if (!officePhone.isEmpty() && !officePhone.contains("-")) {
                textOfficePhone.setText(String.format("%s-%s-%s", officePhone.substring(0, 3), officePhone.substring(3, 6), officePhone.substring(6)));
            } else {
                textOfficePhone.setText(officePhone);
            }
        }
        textAddress2.setText(address2);
        textCity.setText(city);
        textState.setText(state);
        textCountry.setText(country);
        textZip.setText(zip);
    }

    private void loadCity() {
        final SearchableDialog searchableDialog = new SearchableDialog(AccountProfileActivity.this, stateList, "Select State");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editState.setText(searchListItem.getTitle());
            }
        });
    }

    private void setCountryId(String country) {
        if (!country.isEmpty()) {
            switch (country) {
                case "United States":
                    countryId = 2;
                    selectedCountryId = 2;
                    break;
                case "Canada":
                    selectedCountryId = 3;
                    countryId = 3;
                    break;
                default:
                    countryId = 0;
            }
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void setTag(String account_state) {
        tagAccountStatus = account_state;
    }

    @Override
    public void changeIcon(@DrawableRes int save_icon) {
        imageEdit.setImageResource(save_icon);
    }

    @Override
    public void showEditView(int editorVisibility, int textVisibility) {
        editFirstName.setVisibility(editorVisibility);
        editLastName.setVisibility(editorVisibility);
        editMobile.setVisibility(editorVisibility);
        editAddress1.setVisibility(editorVisibility);
        editComapny.setVisibility(editorVisibility);
        editContactEmail.setVisibility(editorVisibility);
        editOfficePhone.setVisibility(editorVisibility);
        editAddress2.setVisibility(editorVisibility);
        editCity.setVisibility(editorVisibility);
        editState.setVisibility(editorVisibility);
        editCountry.setVisibility(editorVisibility);
        editZip.setVisibility(editorVisibility);
        /*editSalesTax.setVisibility(editorVisibility);
        editSalesTaxCountry.setVisibility(editorVisibility);
        editSalesTaxPercent.setVisibility(editorVisibility);*/

        textFirstName.setVisibility(textVisibility);
        textLastName.setVisibility(textVisibility);
        textMobile.setVisibility(textVisibility);
        textAddress1.setVisibility(textVisibility);
        textCompany.setVisibility(textVisibility);
        textContactEmail.setVisibility(textVisibility);
        textOfficePhone.setVisibility(textVisibility);
        textAddress2.setVisibility(textVisibility);
        textCity.setVisibility(textVisibility);
        textState.setVisibility(textVisibility);
        textCountry.setVisibility(textVisibility);
        textZip.setVisibility(textVisibility);
       /* textSalesTax.setVisibility(textVisibility);
        textSalesTaxCountry.setVisibility(textVisibility);
        textSalesTaxPercent.setVisibility(textVisibility);*/
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");

        labelFirstName.setTypeface(openSansRegular);
        lableLastName.setTypeface(openSansRegular);
        labelMobile.setTypeface(openSansRegular);
        labelEmail.setTypeface(openSansRegular);
        labelAddress1.setTypeface(openSansRegular);
        labelCompany.setTypeface(openSansRegular);
        labelContactEmail.setTypeface(openSansRegular);
        labelOfficePhone.setTypeface(openSansRegular);
        labelAddress2.setTypeface(openSansRegular);
        labelCity.setTypeface(openSansRegular);
        labelState.setTypeface(openSansRegular);
        labelCountry.setTypeface(openSansRegular);
        labelZip.setTypeface(openSansRegular);
        /*labelSalesTax.setTypeface(openSansRegular);
        labelSalesTaxCountry.setTypeface(openSansRegular);
        labelSalesTaxPercent.setTypeface(openSansRegular);*/

        textFirstName.setTypeface(openSansSemiBold);
        textLastName.setTypeface(openSansSemiBold);
        textMobile.setTypeface(openSansSemiBold);
        textAddress1.setTypeface(openSansSemiBold);
        textCompany.setTypeface(openSansSemiBold);
        textContactEmail.setTypeface(openSansSemiBold);
        textOfficePhone.setTypeface(openSansSemiBold);
        textAddress2.setTypeface(openSansSemiBold);
        textCity.setTypeface(openSansSemiBold);
        textState.setTypeface(openSansSemiBold);
        textCountry.setTypeface(openSansSemiBold);
        textZip.setTypeface(openSansSemiBold);
        /*textSalesTax.setTypeface(openSansSemiBold);
        textSalesTaxCountry.setTypeface(openSansSemiBold);
        textSalesTaxPercent.setTypeface(openSansSemiBold);*/
        textEmail.setTypeface(openSansSemiBold);

        editFirstName.setTypeface(openSansSemiBold);
        editLastName.setTypeface(openSansSemiBold);
        editMobile.setTypeface(openSansSemiBold);
        editAddress1.setTypeface(openSansSemiBold);
        editComapny.setTypeface(openSansSemiBold);
        editContactEmail.setTypeface(openSansSemiBold);
        editOfficePhone.setTypeface(openSansSemiBold);
        editAddress2.setTypeface(openSansSemiBold);
        editState.setTypeface(openSansSemiBold);
        textState.setTypeface(openSansSemiBold);
        editCountry.setTypeface(openSansSemiBold);
        editZip.setTypeface(openSansSemiBold);
        /*editSalesTax.setTypeface(openSansSemiBold);
        editSalesTaxCountry.setTypeface(openSansSemiBold);
        editSalesTaxPercent.setTypeface(openSansSemiBold);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myAccountPresenter.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            myAccountPresenter.getMyAccountDetails();
        } else {
            finish();
        }
    }

}
