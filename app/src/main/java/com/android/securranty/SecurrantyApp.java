package com.android.securranty;

import android.app.Application;
import android.content.Context;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by satheeshr on 24-11-2017.
 */
@ReportsCrashes(
        mode = ReportingInteractionMode.DIALOG,
        mailTo = "satheeshrmca@gmail.com",
        // optional, displayed as soon as the crash occurs, before collecting data
        // which can take a few seconds
        resToastText = R.string.crash_toast_text,
        resDialogText = R.string.crash_dialog_text,
        //optional. default is a warning sign
        resDialogIcon = android.R.drawable.ic_dialog_info,
        // optional. default is your application name
        resDialogTitle = R.string.crash_dialog_title,
        // optional. When defined, adds a user text field input with this text
        // resource as a label
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt,
        // optional. displays a Toast message when the user accepts to send a report.
        resDialogOkToast = R.string.crash_dialog_ok_toast)
public class SecurrantyApp extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        context = getApplicationContext();
    }
}
