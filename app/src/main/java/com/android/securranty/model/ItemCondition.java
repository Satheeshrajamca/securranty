
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ItemCondition {

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static class Builder {

        private int id;
        private String name;

        public ItemCondition.Builder withId(int id) {
            this.id = id;
            return this;
        }

        public ItemCondition.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public ItemCondition build() {
            ItemCondition itemCondition = new ItemCondition();
            itemCondition.id = id;
            itemCondition.name = name;
            return itemCondition;
        }

    }

}
