package com.android.securranty.view.activity.tickets;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.view.activity.tickets.model.SupportTicketMessage;

import java.util.List;

public class AdapterTicketDetails extends RecyclerView.Adapter<AdapterTicketDetails.ViewHolder> {
    private List<SupportTicketMessage> ticketMessage;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_reply, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textPostReply.setText(ticketMessage.get(position).getMessage());
        if (ticketMessage.get(position).getIsMessageFromSecurranty()) {
            holder.textEmail.setText("Securranty Support");
        } else {
            holder.textEmail.setText(ticketMessage.get(position).getCreatedBy());
        }
        holder.textDate.setText(ticketMessage.get(position).getCreatedDate().split("T")[0]);
    }

    @Override
    public int getItemCount() {
        return ticketMessage.size();
    }


    public void setData(List<SupportTicketMessage> ticketMessage) {
        this.ticketMessage = ticketMessage;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textPostReply, textEmail, textDate;

        public ViewHolder(View itemView) {
            super(itemView);
            textPostReply = (TextView) itemView.findViewById(R.id.textPostReply);
            textEmail = (TextView) itemView.findViewById(R.id.textEmail);
            textDate = (TextView) itemView.findViewById(R.id.textDate);

        }
    }
}
