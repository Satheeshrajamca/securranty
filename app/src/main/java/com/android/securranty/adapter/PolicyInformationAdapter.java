package com.android.securranty.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.Child;

import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class PolicyInformationAdapter extends RecyclerView.Adapter<PolicyInformationAdapter.ViewHolder> {
    private Context context;
    private List<Child> policyInformation;

    public PolicyInformationAdapter(Context context, List<Child> policyInformation) {
        this.context = context;
        this.policyInformation = policyInformation;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_policy_information, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Child child = policyInformation.get(position);
        holder.textPolicyHeader.setText(child.getKey());
        holder.textPolicyBorder.setText(child.getValue());
    }

    @Override
    public int getItemCount() {
        return policyInformation.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textPolicyHeader, textPolicyBorder;

        public ViewHolder(View itemView) {
            super(itemView);
            textPolicyHeader = (TextView) itemView.findViewById(R.id.textPolicyHeader);
            textPolicyBorder = (TextView) itemView.findViewById(R.id.textPolicyBorder);
            Typeface openSansSemiBold = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_semibold.ttf");
            Typeface openSansRegular = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_regular.ttf");
            textPolicyHeader.setTypeface(openSansSemiBold);
            textPolicyBorder.setTypeface(openSansRegular);

        }
    }
}
