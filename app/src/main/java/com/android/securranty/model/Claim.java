package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class Claim {

	@SerializedName("ConfirmCharge")
	private boolean confirmCharge;

	@SerializedName("ResolvedDate")
	private Object resolvedDate;

	@SerializedName("ProblemDescription")
	private String problemDescription;

	@SerializedName("ConfirmReturnClaim")
	private boolean confirmReturnClaim;

	@SerializedName("Deductable")
	private double deductable;

	@SerializedName("LastModBy")
	private String lastModBy;

	@SerializedName("ShipToState")
	private String shipToState;

	@SerializedName("CreatedById")
	private String createdById;

	@SerializedName("LastModById")
	private String lastModById;

	@SerializedName("PayProfileId")
	private int payProfileId;

	@SerializedName("Status")
	private String status;

	@SerializedName("ShipToZip")
	private String shipToZip;

	@SerializedName("TypeId")
	private Object typeId;

	@SerializedName("DamageDate")
	private String damageDate;

	@SerializedName("CreatedBy")
	private Object createdBy;

	@SerializedName("PreAuthTransactionId")
	private Object preAuthTransactionId;

	@SerializedName("ShipToPhone")
	private String shipToPhone;

	@SerializedName("PayType")
	private String payType;

	@SerializedName("OutOfWarranty")
	private boolean outOfWarranty;

	@SerializedName("IsReimburse")
	private boolean isReimburse;

	@SerializedName("LostStolenConfirm")
	private boolean lostStolenConfirm;

	@SerializedName("Active")
	private boolean active;

	@SerializedName("ReceivedDate")
	private Object receivedDate;

	@SerializedName("RowVersion")
	private Object rowVersion;

	@SerializedName("IsConfirmSLAFiveDay")
	private boolean isConfirmSLAFiveDay;

	@SerializedName("CreatedDate")
	private String createdDate;

	@SerializedName("StatusId")
	private int statusId;

	@SerializedName("Id")
	private int id;

	@SerializedName("Resolution")
	private Object resolution;

	@SerializedName("RecDevLossesCovered")
	private Object recDevLossesCovered;

	@SerializedName("DeviceId")
	private int deviceId;

	@SerializedName("ConfirmDamageAfter")
	private boolean confirmDamageAfter;

	@SerializedName("ShipToCity")
	private String shipToCity;

	@SerializedName("RecDeviceMatches")
	private Object recDeviceMatches;

	@SerializedName("CoveredLosses")
	private String coveredLosses;

	@SerializedName("RecPasswordLocked")
	private Object recPasswordLocked;

	@SerializedName("ConfirmInfoTrue")
	private boolean confirmInfoTrue;

	@SerializedName("IRIUsed")
	private Object iRIUsed;

	@SerializedName("ShipToAddress2")
	private String shipToAddress2;

	@SerializedName("ShipToAddress1")
	private String shipToAddress1;

	@SerializedName("ModelId")
	private int modelId;

	@SerializedName("ReplacementDeviceId")
	private Object replacementDeviceId;

	@SerializedName("ShipToEmail")
	private String shipToEmail;

	@SerializedName("NotReceivedDate")
	private Object notReceivedDate;

	@SerializedName("ShipToCountry")
	private String shipToCountry;

	@SerializedName("IsConfirmNotSLAFiveDay")
	private boolean isConfirmNotSLAFiveDay;

	@SerializedName("IRIAvailable")
	private boolean iRIAvailable;

	@SerializedName("ClaimProblem")
	private String claimProblem;

	@SerializedName("ShipToName")
	private String shipToName;

	@SerializedName("IsConfirmSLACellairis")
	private Object isConfirmSLACellairis;

	@SerializedName("ConfirmDisabled")
	private Object confirmDisabled;

	@SerializedName("LastModDate")
	private String lastModDate;

	@SerializedName("ReceivedBy")
	private Object receivedBy;

	@SerializedName("UserId")
	private String userId;

	@SerializedName("ResolutionId")
	private Object resolutionId;

	@SerializedName("Model")
	private String model;

	@SerializedName("RecIncompleteDev")
	private Object recIncompleteDev;

	@SerializedName("UserDeviceHistoryId")
	private Object userDeviceHistoryId;

	@SerializedName("PolicyId")
	private int policyId;

	public void setConfirmCharge(boolean confirmCharge){
		this.confirmCharge = confirmCharge;
	}

	public boolean isConfirmCharge(){
		return confirmCharge;
	}

	public void setResolvedDate(Object resolvedDate){
		this.resolvedDate = resolvedDate;
	}

	public Object getResolvedDate(){
		return resolvedDate;
	}

	public void setProblemDescription(String problemDescription){
		this.problemDescription = problemDescription;
	}

	public String getProblemDescription(){
		return problemDescription;
	}

	public void setConfirmReturnClaim(boolean confirmReturnClaim){
		this.confirmReturnClaim = confirmReturnClaim;
	}

	public boolean isConfirmReturnClaim(){
		return confirmReturnClaim;
	}

	public void setDeductable(double deductable){
		this.deductable = deductable;
	}

	public double getDeductable(){
		return deductable;
	}

	public void setLastModBy(String lastModBy){
		this.lastModBy = lastModBy;
	}

	public String getLastModBy(){
		return lastModBy;
	}

	public void setShipToState(String shipToState){
		this.shipToState = shipToState;
	}

	public String getShipToState(){
		return shipToState;
	}

	public void setCreatedById(String createdById){
		this.createdById = createdById;
	}

	public String getCreatedById(){
		return createdById;
	}

	public void setLastModById(String lastModById){
		this.lastModById = lastModById;
	}

	public String getLastModById(){
		return lastModById;
	}

	public void setPayProfileId(int payProfileId){
		this.payProfileId = payProfileId;
	}

	public int getPayProfileId(){
		return payProfileId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setShipToZip(String shipToZip){
		this.shipToZip = shipToZip;
	}

	public String getShipToZip(){
		return shipToZip;
	}

	public void setTypeId(Object typeId){
		this.typeId = typeId;
	}

	public Object getTypeId(){
		return typeId;
	}

	public void setDamageDate(String damageDate){
		this.damageDate = damageDate;
	}

	public String getDamageDate(){
		return damageDate;
	}

	public void setCreatedBy(Object createdBy){
		this.createdBy = createdBy;
	}

	public Object getCreatedBy(){
		return createdBy;
	}

	public void setPreAuthTransactionId(Object preAuthTransactionId){
		this.preAuthTransactionId = preAuthTransactionId;
	}

	public Object getPreAuthTransactionId(){
		return preAuthTransactionId;
	}

	public void setShipToPhone(String shipToPhone){
		this.shipToPhone = shipToPhone;
	}

	public String getShipToPhone(){
		return shipToPhone;
	}

	public void setPayType(String payType){
		this.payType = payType;
	}

	public String getPayType(){
		return payType;
	}

	public void setOutOfWarranty(boolean outOfWarranty){
		this.outOfWarranty = outOfWarranty;
	}

	public boolean isOutOfWarranty(){
		return outOfWarranty;
	}

	public void setIsReimburse(boolean isReimburse){
		this.isReimburse = isReimburse;
	}

	public boolean isIsReimburse(){
		return isReimburse;
	}

	public void setLostStolenConfirm(boolean lostStolenConfirm){
		this.lostStolenConfirm = lostStolenConfirm;
	}

	public boolean isLostStolenConfirm(){
		return lostStolenConfirm;
	}

	public void setActive(boolean active){
		this.active = active;
	}

	public boolean isActive(){
		return active;
	}

	public void setReceivedDate(Object receivedDate){
		this.receivedDate = receivedDate;
	}

	public Object getReceivedDate(){
		return receivedDate;
	}

	public void setRowVersion(Object rowVersion){
		this.rowVersion = rowVersion;
	}

	public Object getRowVersion(){
		return rowVersion;
	}

	public void setIsConfirmSLAFiveDay(boolean isConfirmSLAFiveDay){
		this.isConfirmSLAFiveDay = isConfirmSLAFiveDay;
	}

	public boolean isIsConfirmSLAFiveDay(){
		return isConfirmSLAFiveDay;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setStatusId(int statusId){
		this.statusId = statusId;
	}

	public int getStatusId(){
		return statusId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setResolution(Object resolution){
		this.resolution = resolution;
	}

	public Object getResolution(){
		return resolution;
	}

	public void setRecDevLossesCovered(Object recDevLossesCovered){
		this.recDevLossesCovered = recDevLossesCovered;
	}

	public Object getRecDevLossesCovered(){
		return recDevLossesCovered;
	}

	public void setDeviceId(int deviceId){
		this.deviceId = deviceId;
	}

	public int getDeviceId(){
		return deviceId;
	}

	public void setConfirmDamageAfter(boolean confirmDamageAfter){
		this.confirmDamageAfter = confirmDamageAfter;
	}

	public boolean isConfirmDamageAfter(){
		return confirmDamageAfter;
	}

	public void setShipToCity(String shipToCity){
		this.shipToCity = shipToCity;
	}

	public String getShipToCity(){
		return shipToCity;
	}

	public void setRecDeviceMatches(Object recDeviceMatches){
		this.recDeviceMatches = recDeviceMatches;
	}

	public Object getRecDeviceMatches(){
		return recDeviceMatches;
	}

	public void setCoveredLosses(String coveredLosses){
		this.coveredLosses = coveredLosses;
	}

	public String getCoveredLosses(){
		return coveredLosses;
	}

	public void setRecPasswordLocked(Object recPasswordLocked){
		this.recPasswordLocked = recPasswordLocked;
	}

	public Object getRecPasswordLocked(){
		return recPasswordLocked;
	}

	public void setConfirmInfoTrue(boolean confirmInfoTrue){
		this.confirmInfoTrue = confirmInfoTrue;
	}

	public boolean isConfirmInfoTrue(){
		return confirmInfoTrue;
	}

	public void setIRIUsed(Object iRIUsed){
		this.iRIUsed = iRIUsed;
	}

	public Object getIRIUsed(){
		return iRIUsed;
	}

	public void setShipToAddress2(String shipToAddress2){
		this.shipToAddress2 = shipToAddress2;
	}

	public String getShipToAddress2(){
		return shipToAddress2;
	}

	public void setShipToAddress1(String shipToAddress1){
		this.shipToAddress1 = shipToAddress1;
	}

	public String getShipToAddress1(){
		return shipToAddress1;
	}

	public void setModelId(int modelId){
		this.modelId = modelId;
	}

	public int getModelId(){
		return modelId;
	}

	public void setReplacementDeviceId(Object replacementDeviceId){
		this.replacementDeviceId = replacementDeviceId;
	}

	public Object getReplacementDeviceId(){
		return replacementDeviceId;
	}

	public void setShipToEmail(String shipToEmail){
		this.shipToEmail = shipToEmail;
	}

	public String getShipToEmail(){
		return shipToEmail;
	}

	public void setNotReceivedDate(Object notReceivedDate){
		this.notReceivedDate = notReceivedDate;
	}

	public Object getNotReceivedDate(){
		return notReceivedDate;
	}

	public void setShipToCountry(String shipToCountry){
		this.shipToCountry = shipToCountry;
	}

	public String getShipToCountry(){
		return shipToCountry;
	}

	public void setIsConfirmNotSLAFiveDay(boolean isConfirmNotSLAFiveDay){
		this.isConfirmNotSLAFiveDay = isConfirmNotSLAFiveDay;
	}

	public boolean isIsConfirmNotSLAFiveDay(){
		return isConfirmNotSLAFiveDay;
	}

	public void setIRIAvailable(boolean iRIAvailable){
		this.iRIAvailable = iRIAvailable;
	}

	public boolean isIRIAvailable(){
		return iRIAvailable;
	}

	public void setClaimProblem(String claimProblem){
		this.claimProblem = claimProblem;
	}

	public String getClaimProblem(){
		return claimProblem;
	}

	public void setShipToName(String shipToName){
		this.shipToName = shipToName;
	}

	public String getShipToName(){
		return shipToName;
	}

	public void setIsConfirmSLACellairis(Object isConfirmSLACellairis){
		this.isConfirmSLACellairis = isConfirmSLACellairis;
	}

	public Object getIsConfirmSLACellairis(){
		return isConfirmSLACellairis;
	}

	public void setConfirmDisabled(Object confirmDisabled){
		this.confirmDisabled = confirmDisabled;
	}

	public Object getConfirmDisabled(){
		return confirmDisabled;
	}

	public void setLastModDate(String lastModDate){
		this.lastModDate = lastModDate;
	}

	public String getLastModDate(){
		return lastModDate;
	}

	public void setReceivedBy(Object receivedBy){
		this.receivedBy = receivedBy;
	}

	public Object getReceivedBy(){
		return receivedBy;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setResolutionId(Object resolutionId){
		this.resolutionId = resolutionId;
	}

	public Object getResolutionId(){
		return resolutionId;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setRecIncompleteDev(Object recIncompleteDev){
		this.recIncompleteDev = recIncompleteDev;
	}

	public Object getRecIncompleteDev(){
		return recIncompleteDev;
	}

	public void setUserDeviceHistoryId(Object userDeviceHistoryId){
		this.userDeviceHistoryId = userDeviceHistoryId;
	}

	public Object getUserDeviceHistoryId(){
		return userDeviceHistoryId;
	}

	public void setPolicyId(int policyId){
		this.policyId = policyId;
	}

	public int getPolicyId(){
		return policyId;
	}

	@Override
 	public String toString(){
		return 
			"Claim{" +
			"confirmCharge = '" + confirmCharge + '\'' + 
			",resolvedDate = '" + resolvedDate + '\'' + 
			",problemDescription = '" + problemDescription + '\'' + 
			",confirmReturnClaim = '" + confirmReturnClaim + '\'' + 
			",deductable = '" + deductable + '\'' + 
			",lastModBy = '" + lastModBy + '\'' + 
			",shipToState = '" + shipToState + '\'' + 
			",createdById = '" + createdById + '\'' + 
			",lastModById = '" + lastModById + '\'' + 
			",payProfileId = '" + payProfileId + '\'' + 
			",status = '" + status + '\'' + 
			",shipToZip = '" + shipToZip + '\'' + 
			",typeId = '" + typeId + '\'' + 
			",damageDate = '" + damageDate + '\'' + 
			",createdBy = '" + createdBy + '\'' + 
			",preAuthTransactionId = '" + preAuthTransactionId + '\'' + 
			",shipToPhone = '" + shipToPhone + '\'' + 
			",payType = '" + payType + '\'' + 
			",outOfWarranty = '" + outOfWarranty + '\'' + 
			",isReimburse = '" + isReimburse + '\'' + 
			",lostStolenConfirm = '" + lostStolenConfirm + '\'' + 
			",active = '" + active + '\'' + 
			",receivedDate = '" + receivedDate + '\'' + 
			",rowVersion = '" + rowVersion + '\'' + 
			",isConfirmSLAFiveDay = '" + isConfirmSLAFiveDay + '\'' + 
			",createdDate = '" + createdDate + '\'' + 
			",statusId = '" + statusId + '\'' + 
			",id = '" + id + '\'' + 
			",resolution = '" + resolution + '\'' + 
			",recDevLossesCovered = '" + recDevLossesCovered + '\'' + 
			",deviceId = '" + deviceId + '\'' + 
			",confirmDamageAfter = '" + confirmDamageAfter + '\'' + 
			",shipToCity = '" + shipToCity + '\'' + 
			",recDeviceMatches = '" + recDeviceMatches + '\'' + 
			",coveredLosses = '" + coveredLosses + '\'' + 
			",recPasswordLocked = '" + recPasswordLocked + '\'' + 
			",confirmInfoTrue = '" + confirmInfoTrue + '\'' + 
			",iRIUsed = '" + iRIUsed + '\'' + 
			",shipToAddress2 = '" + shipToAddress2 + '\'' + 
			",shipToAddress1 = '" + shipToAddress1 + '\'' + 
			",modelId = '" + modelId + '\'' + 
			",replacementDeviceId = '" + replacementDeviceId + '\'' + 
			",shipToEmail = '" + shipToEmail + '\'' + 
			",notReceivedDate = '" + notReceivedDate + '\'' + 
			",shipToCountry = '" + shipToCountry + '\'' + 
			",isConfirmNotSLAFiveDay = '" + isConfirmNotSLAFiveDay + '\'' + 
			",iRIAvailable = '" + iRIAvailable + '\'' + 
			",claimProblem = '" + claimProblem + '\'' + 
			",shipToName = '" + shipToName + '\'' + 
			",isConfirmSLACellairis = '" + isConfirmSLACellairis + '\'' + 
			",confirmDisabled = '" + confirmDisabled + '\'' + 
			",lastModDate = '" + lastModDate + '\'' + 
			",receivedBy = '" + receivedBy + '\'' + 
			",userId = '" + userId + '\'' + 
			",resolutionId = '" + resolutionId + '\'' + 
			",model = '" + model + '\'' + 
			",recIncompleteDev = '" + recIncompleteDev + '\'' + 
			",userDeviceHistoryId = '" + userDeviceHistoryId + '\'' + 
			",policyId = '" + policyId + '\'' + 
			"}";
		}
}