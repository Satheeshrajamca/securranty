package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Manufacturer{

	@SerializedName("manufacturers")
	private List<ManufacturersItem> manufacturers;

	@SerializedName("statusCode")
	private int statusCode;

	@SerializedName("status")
	private String status;

	public List<ManufacturersItem> getManufacturers() {
		return manufacturers;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getStatus() {
		return status;
	}

	public class ManufacturersItem{

		@SerializedName("AdPage")
		private List<Object> adPage;

		@SerializedName("DeviceModels")
		private List<Object> deviceModels;

		@SerializedName("Active")
		private boolean active;

		@SerializedName("UserDeviceHistory")
		private List<Object> userDeviceHistory;

		@SerializedName("Id")
		private int id;

		@SerializedName("UserDevice")
		private List<Object> userDevice;

		@SerializedName("Name")
		private String name;

		public List<Object> getAdPage() {
			return adPage;
		}

		public List<Object> getDeviceModels() {
			return deviceModels;
		}

		public boolean isActive() {
			return active;
		}

		public List<Object> getUserDeviceHistory() {
			return userDeviceHistory;
		}

		public int getId() {
			return id;
		}

		public List<Object> getUserDevice() {
			return userDevice;
		}

		public String getName() {
			return name;
		}
	}
}
