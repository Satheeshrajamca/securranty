
package com.android.securranty.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceModel {

    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;
    @Expose
    private com.android.securranty.model.UserDevice UserDevice;

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public com.android.securranty.model.UserDevice getUserDevice() {
        return UserDevice;
    }

    public static class Builder {

        private String Status;
        private Long StatusCode;
        private com.android.securranty.model.UserDevice UserDevice;

        public DeviceModel.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public DeviceModel.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public DeviceModel.Builder withUserDevice(com.android.securranty.model.UserDevice UserDevice) {
            this.UserDevice = UserDevice;
            return this;
        }

        public DeviceModel build() {
            DeviceModel DeviceModel = new DeviceModel();
            DeviceModel.Status = Status;
            DeviceModel.StatusCode = StatusCode;
            DeviceModel.UserDevice = UserDevice;
            return DeviceModel;
        }

    }

}
