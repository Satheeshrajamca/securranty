
package com.android.securranty.view.activity.shipping;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ApiErrorShipping {

    @SerializedName("message")
    private List<String> Message;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<String> getMessage() {
        return Message;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<String> Message;
        private String Status;
        private Long StatusCode;

        public ApiErrorShipping.Builder withMessage(List<String> message) {
            Message = message;
            return this;
        }

        public ApiErrorShipping.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public ApiErrorShipping.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public ApiErrorShipping build() {
            ApiErrorShipping ApiErrorShipping = new ApiErrorShipping();
            ApiErrorShipping.Message = Message;
            ApiErrorShipping.Status = Status;
            ApiErrorShipping.StatusCode = StatusCode;
            return ApiErrorShipping;
        }

    }

}
