package com.android.securranty.presenter;

/**
 * Created by satheeshraja on 11/16/17.
 */

public interface BasePresenter {
    void success(Object obj);

    void errorMsg(int statusCode, String msg);

    void onDestroy();

}
