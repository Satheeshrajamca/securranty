
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class DeviceTypeList {

    @Expose
    private Boolean Active;
    @Expose
    private List<Object> AdPage;
    @Expose
    private List<Object> BasePlan;
    @Expose
    private String Description;
    @Expose
    private List<Object> DeviceModels;
    @Expose
    private int Id;
    @Expose
    private String Name;
    @Expose
    private Object SHeight;
    @Expose
    private Object SLength;
    @Expose
    private Object SWidth;
    @Expose
    private List<Object> UserDevice;
    @Expose
    private List<Object> UserPlan;
    @Expose
    private Long Weight;

    public Boolean getActive() {
        return Active;
    }

    public List<Object> getAdPage() {
        return AdPage;
    }

    public List<Object> getBasePlan() {
        return BasePlan;
    }

    public String getDescription() {
        return Description;
    }

    public List<Object> getDeviceModels() {
        return DeviceModels;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public Object getSHeight() {
        return SHeight;
    }

    public Object getSLength() {
        return SLength;
    }

    public Object getSWidth() {
        return SWidth;
    }

    public List<Object> getUserDevice() {
        return UserDevice;
    }

    public List<Object> getUserPlan() {
        return UserPlan;
    }

    public Long getWeight() {
        return Weight;
    }

    public static class Builder {

        private Boolean Active;
        private List<Object> AdPage;
        private List<Object> BasePlan;
        private String Description;
        private List<Object> DeviceModels;
        private int Id;
        private String Name;
        private Object SHeight;
        private Object SLength;
        private Object SWidth;
        private List<Object> UserDevice;
        private List<Object> UserPlan;
        private Long Weight;

        public DeviceTypeList.Builder withActive(Boolean Active) {
            this.Active = Active;
            return this;
        }

        public DeviceTypeList.Builder withAdPage(List<Object> AdPage) {
            this.AdPage = AdPage;
            return this;
        }

        public DeviceTypeList.Builder withBasePlan(List<Object> BasePlan) {
            this.BasePlan = BasePlan;
            return this;
        }

        public DeviceTypeList.Builder withDescription(String Description) {
            this.Description = Description;
            return this;
        }

        public DeviceTypeList.Builder withDeviceModels(List<Object> DeviceModels) {
            this.DeviceModels = DeviceModels;
            return this;
        }

        public DeviceTypeList.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public DeviceTypeList.Builder withName(String Name) {
            this.Name = Name;
            return this;
        }

        public DeviceTypeList.Builder withSHeight(Object SHeight) {
            this.SHeight = SHeight;
            return this;
        }

        public DeviceTypeList.Builder withSLength(Object SLength) {
            this.SLength = SLength;
            return this;
        }

        public DeviceTypeList.Builder withSWidth(Object SWidth) {
            this.SWidth = SWidth;
            return this;
        }

        public DeviceTypeList.Builder withUserDevice(List<Object> UserDevice) {
            this.UserDevice = UserDevice;
            return this;
        }

        public DeviceTypeList.Builder withUserPlan(List<Object> UserPlan) {
            this.UserPlan = UserPlan;
            return this;
        }

        public DeviceTypeList.Builder withWeight(Long Weight) {
            this.Weight = Weight;
            return this;
        }

        public DeviceTypeList build() {
            DeviceTypeList DeviceTypeList = new DeviceTypeList();
            DeviceTypeList.Active = Active;
            DeviceTypeList.AdPage = AdPage;
            DeviceTypeList.BasePlan = BasePlan;
            DeviceTypeList.Description = Description;
            DeviceTypeList.DeviceModels = DeviceModels;
            DeviceTypeList.Id = Id;
            DeviceTypeList.Name = Name;
            DeviceTypeList.SHeight = SHeight;
            DeviceTypeList.SLength = SLength;
            DeviceTypeList.SWidth = SWidth;
            DeviceTypeList.UserDevice = UserDevice;
            DeviceTypeList.UserPlan = UserPlan;
            DeviceTypeList.Weight = Weight;
            return DeviceTypeList;
        }

    }

}
