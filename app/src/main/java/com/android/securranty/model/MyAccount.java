package com.android.securranty.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by satheeshr on 22-11-2017.
 */


public class MyAccount {

    @SerializedName("accountProfile")
    @Expose
    private AccountProfile accountProfile;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("status")
    @Expose
    private String status;

    public AccountProfile getAccountProfile() {
        return accountProfile;
    }

    public void setAccountProfile(AccountProfile accountProfile) {
        this.accountProfile = accountProfile;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class AccountProfile {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("FirstName")
        @Expose
        private String firstName;
        @SerializedName("LastName")
        @Expose
        private String lastName;
        @SerializedName("Email")
        @Expose
        private String email;
        @SerializedName("ContactName")
        @Expose
        private String contactName;
        @SerializedName("ContactEmail")
        @Expose
        private String contactEmail;
        @SerializedName("OfficePhone")
        @Expose
        private String officePhone;
        @SerializedName("MobilePhone")
        @Expose
        private String mobilePhone;
        @SerializedName("Address1")
        @Expose
        private String address1;
        @SerializedName("Address2")
        @Expose
        private String address2;
        @SerializedName("City")
        @Expose
        private String city;
        @SerializedName("State")
        @Expose
        private String state;
        @SerializedName("Country")
        @Expose
        private String country;
        @SerializedName("Zip")
        @Expose
        private String zip;
        @SerializedName("SalesTax")
        @Expose
        private Boolean salesTax;
        @SerializedName("SalesTaxCountry")
        @Expose
        private String salesTaxCountry;
        @SerializedName("SalesTaxPercent")
        @Expose
        private String salesTaxPercent;
        @SerializedName("ProfileImage")
        @Expose
        private String profileImage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactEmail() {
            return contactEmail;
        }

        public void setContactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
        }

        public String getOfficePhone() {
            return officePhone;
        }

        public void setOfficePhone(String officePhone) {
            this.officePhone = officePhone;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public Boolean getSalesTax() {
            return salesTax;
        }

        public void setSalesTax(Boolean salesTax) {
            this.salesTax = salesTax;
        }

        public String getSalesTaxCountry() {
            return salesTaxCountry;
        }

        public void setSalesTaxCountry(String salesTaxCountry) {
            this.salesTaxCountry = salesTaxCountry;
        }

        public String getSalesTaxPercent() {
            return salesTaxPercent;
        }

        public void setSalesTaxPercent(String salesTaxPercent) {
            this.salesTaxPercent = salesTaxPercent;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

    }

}
