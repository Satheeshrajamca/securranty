
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

public class CostCenter {

    @Expose
    private Boolean Active;
    @Expose
    private int Id;
    @Expose
    private String Name;

    public Boolean getActive() {
        return Active;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public static class Builder {

        private Boolean Active;
        private int Id;
        private String Name;

        public CostCenter.Builder withActive(Boolean Active) {
            this.Active = Active;
            return this;
        }

        public CostCenter.Builder withId(int Id) {
            this.Id = Id;
            return this;
        }

        public CostCenter.Builder withName(String Name) {
            this.Name = Name;
            return this;
        }

        public CostCenter build() {
            CostCenter CostCenter = new CostCenter();
            CostCenter.Active = Active;
            CostCenter.Id = Id;
            CostCenter.Name = Name;
            return CostCenter;
        }

    }

}
