package com.android.securranty.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.account.AccountProfileActivity;
import com.android.securranty.view.activity.payment.PaymentHistoryActivity;
import com.android.securranty.view.activity.signs.SignInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.MainActivity.ACCOUNT_PROFILE_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.CREDIT_CARD_ACTIVITY;
import static com.android.securranty.view.activity.MainActivity.PAYMENT_HISTORY;

/**
 * Created by satheeshr on 27-11-2017.
 */

public class BillingFragment extends Fragment {
    @BindView(R.id.rootlayoutDashBoard)
    LinearLayout rootlayoutDashBoard;
    @BindView(R.id.textAccountProfile)
    TextView textPaymentHistory;
    @BindView(R.id.textBillingProfile)
    TextView textBillingProfile;
    @BindView(R.id.textCreditCards)
    TextView textCreditCards;
    @BindView(R.id.textShippingAddress)
    TextView textShippingAddress;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.view4)
    View view4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myaccount,
                container, false);
        ButterKnife.bind(this, view);
        setTypeFace();
        return view;
    }

    private void setTypeFace() {
        textPaymentHistory.setText("PAYMENT HISTORY");
        textBillingProfile.setVisibility(View.GONE);
        textCreditCards.setVisibility(View.GONE);
        textShippingAddress.setVisibility(View.GONE);
        view2.setVisibility(View.GONE);
        view3.setVisibility(View.GONE);
        view4.setVisibility(View.GONE);
        Typeface openSansSemiBold = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/opensans_semibold.ttf");
        textPaymentHistory.setTypeface(openSansSemiBold);
    }

    @OnClick({R.id.textAccountProfile})
    public void onClickMyAccount(View view) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                startActivity(new Intent(getActivity(), PaymentHistoryActivity.class));
            } else {
                Intent i = new Intent(getActivity(), SignInActivity.class);
                startActivityForResult(i, PAYMENT_HISTORY);
            }
        } else {
            Snackbar.make(rootlayoutDashBoard, getActivity().getResources().getString(R.string.msg_no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_HISTORY && resultCode == RESULT_OK) {
            startActivity(new Intent(getActivity(), PaymentHistoryActivity.class));
            mainActivity.changeLoginStatus("Logout");
        }
    }

    private MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
            mainActivity.changeLoginStatus("Login");
        } else {
            mainActivity.changeLoginStatus("Logout");
        }
    }
}
