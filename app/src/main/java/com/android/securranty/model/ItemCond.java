
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

import java.util.List;

@SuppressWarnings("unused")
public class ItemCond {

    @Expose
    private List<ItemCondition> itemCondition;
    @Expose
    private String status;
    @Expose
    private int statusCode;

    public List<ItemCondition> getItemCondition() {
        return itemCondition;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private List<ItemCondition> itemCondition;
        private String status;
        private int statusCode;

        public ItemCond.Builder withItemCondition(List<ItemCondition> itemCondition) {
            this.itemCondition = itemCondition;
            return this;
        }

        public ItemCond.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public ItemCond.Builder withStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public ItemCond build() {
            ItemCond itemCond = new ItemCond();
            itemCond.itemCondition = itemCondition;
            itemCond.status = status;
            itemCond.statusCode = statusCode;
            return itemCond;
        }

    }

}
