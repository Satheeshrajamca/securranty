package com.android.securranty.view.fragment.fileclaim;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class IssueDateFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.editDateIssue)
    EditText editDateIssue;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.previous)
    TextView previous;

    private FileClaimsActivity context;
    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.layout_issue_date,
                    container, false);
            ButterKnife.bind(this, view);
            previous.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @OnClick({R.id.editDateIssue, R.id.next,R.id.previous})
    public void onclickClaimType(View view) {
        switch (view.getId()) {
            case R.id.editDateIssue:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                dpd.setMaxDate(now);
                break;
            case R.id.next:
                daoFileClaim.setIssueDate(issueDate);
                context.triggerClaim(new DamageDescriptionFragment(), "Provide Details of Failure");
                break;
            case R.id.previous:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
        }
    }

    /**
     * @param view        The view associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    private String issueDate;
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        issueDate=String.valueOf(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
        editDateIssue.setText(monthOfYear+1 + "/" + dayOfMonth + "/" + year);
        next.setVisibility(View.VISIBLE);
    }
}
