
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class DeductibleInfo {

    @SerializedName("Amount")
    private String amount;
    @SerializedName("Name")
    private String name;

    public String getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public static class Builder {

        private String amount;
        private String name;

        public DeductibleInfo.Builder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public DeductibleInfo.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public DeductibleInfo build() {
            DeductibleInfo deductibleInfo = new DeductibleInfo();
            deductibleInfo.amount = amount;
            deductibleInfo.name = name;
            return deductibleInfo;
        }

    }

}
