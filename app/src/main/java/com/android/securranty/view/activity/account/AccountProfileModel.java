
package com.android.securranty.view.activity.account;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AccountProfileModel {

    @SerializedName("accountProfile")
    private com.android.securranty.view.activity.account.AccountProfile AccountProfile;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public com.android.securranty.view.activity.account.AccountProfile getAccountProfile() {
        return AccountProfile;
    }

    public String getMessage() {
        return Message;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private com.android.securranty.view.activity.account.AccountProfile AccountProfile;
        private String Message;
        private String Status;
        private Long StatusCode;

        public AccountProfileModel.Builder withAccountProfile(com.android.securranty.view.activity.account.AccountProfile accountProfile) {
            AccountProfile = accountProfile;
            return this;
        }

        public AccountProfileModel.Builder withMessage(String message) {
            Message = message;
            return this;
        }

        public AccountProfileModel.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public AccountProfileModel.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public AccountProfileModel build() {
            AccountProfileModel AccountProfileModel = new AccountProfileModel();
            AccountProfileModel.AccountProfile = AccountProfile;
            AccountProfileModel.Message = Message;
            AccountProfileModel.Status = Status;
            AccountProfileModel.StatusCode = StatusCode;
            return AccountProfileModel;
        }

    }

}
