package com.android.securranty.view.activity.tickets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.TicketModel;
import com.android.securranty.presenter.implementor.CreateTicketPresenterImpl;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.billing.BillingProfileActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class CreateTicketActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.textSubject)
    TextView textSubject;
    @BindView(R.id.textSupportType)
    TextView textSupportType;
    @BindView(R.id.textPriority)
    TextView textPriority;

    @BindView(R.id.spinnerSupportType)
    MaterialBetterSpinner spinnerSupportType;
    @BindView(R.id.spinnerPriority)
    MaterialBetterSpinner spinnerPriority;
    @BindView(R.id.editSubject)
    EditText editSubject;
    @BindView(R.id.editMessage)
    EditText editMessage;

    private ActionBar actionBar;
    private CreateTicketPresenterImpl createTicketPresenter;
    private List<String> listSupportTypeName, listSupportTypeId, listPriorities;
    private String supportTypeId, priority;
    private ArrayAdapter<String> adapterSupportType;
    private ArrayAdapter<String> adapterPriority;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ticket);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        createTicketPresenter = new CreateTicketPresenterImpl(this);
        createTicketPresenter.getTicketDetails();
        editMessage.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editMessage.setRawInputType(InputType.TYPE_CLASS_TEXT);
        setTypeFace();

    }

    @OnClick({R.id.imageBackButton, R.id.imageCreteTicket})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageCreteTicket:
                createTicketPresenter.validateTicketInput(editSubject.getText().toString().trim(),
                        supportTypeId, priority, editMessage.getText().toString().trim());
                break;
            case R.id.imageBackButton:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");

        textSubject.setTypeface(openSansRegular);
        textSupportType.setTypeface(openSansRegular);
        textSupportType.setTypeface(openSansRegular);
        spinnerPriority.setTypeface(openSansSemiBold);
        spinnerSupportType.setTypeface(openSansSemiBold);
        editSubject.setTypeface(openSansSemiBold);
        editMessage.setTypeface(openSansSemiBold);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    @Override
    public void success(Object obj) {
        if (obj instanceof TicketModel) {
            listSupportTypeId = new ArrayList<String>();
            listSupportTypeName = new ArrayList<String>();
            List<TicketModel.SupportTypesItem> listSupportTypes = TicketModel.class.cast(obj).getSupportTypes();
            for (TicketModel.SupportTypesItem supportTypesItem : listSupportTypes) {
                listSupportTypeId.add(String.valueOf(supportTypesItem.getId()));
                listSupportTypeName.add(supportTypesItem.getName());
            }
            listPriorities = TicketModel.class.cast(obj).getPriorities();
            adapterSupportType = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, listSupportTypeName);
            adapterPriority = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, listPriorities);
            spinnerPriority.setBaseColor(R.color.black_color);
            spinnerSupportType.setBaseColor(R.color.black_color);
            spinnerSupportType.setAdapter(adapterSupportType);
            spinnerPriority.setAdapter(adapterPriority);
            spinnerSupportType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    supportTypeId = listSupportTypeId.get(position);
                }
            });
            spinnerPriority.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    priority = listPriorities.get(position);
                }
            });
        } else {
            //Snackbar.make(coordinatorLayout, "Ticket Created Successfully", Snackbar.LENGTH_LONG).show();
            new iOSDialogBuilder(CreateTicketActivity.this)
                    .setTitle("Create Ticket")
                    .setSubtitle("Ticket Created Successfully")
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Ok", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            dialog.dismiss();
                            finish();
                        }
                    }).build().show();
            listSupportTypeId.clear();
            listSupportTypeName.clear();
            listPriorities.clear();
            editSubject.setText("");
            editMessage.setText("");
            spinnerSupportType.setAdapter(adapterSupportType);
            spinnerPriority.setAdapter(adapterPriority);
            spinnerSupportType.setHint(R.string.support_type_hint);
            spinnerPriority.setHint(R.string.priority_hint);
            editSubject.requestFocus();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            createTicketPresenter.getTicketDetails();
        } else {
            finish();
        }
    }

}
