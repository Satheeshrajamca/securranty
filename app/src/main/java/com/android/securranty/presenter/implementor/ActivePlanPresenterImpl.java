package com.android.securranty.presenter.implementor;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.plans.active_plan.model.ActivePlan;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by satheeshraja on 3/23/18.
 */

public class ActivePlanPresenterImpl {
    private BaseView baseView;

    public ActivePlanPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
    }

    public void intActivePlans() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiCall();
    }

    private void apiCall() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getActivePlans(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<ActivePlan>() {
                @Override
                public void onResponse(Call<ActivePlan> call, Response<ActivePlan> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<ActivePlan> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            baseView.showErrorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
