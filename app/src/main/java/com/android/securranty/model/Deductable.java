
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

public class Deductable {

    @Expose
    private String Name;
    @Expose
    private Long Type;
    @Expose
    private Long Value;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Long getType() {
        return Type;
    }

    public void setType(Long Type) {
        this.Type = Type;
    }

    public Long getValue() {
        return Value;
    }

    public void setValue(Long Value) {
        this.Value = Value;
    }

}
