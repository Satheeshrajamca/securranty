package com.android.securranty.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.view.activity.tickets.model.Ticket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.ViewHolder> implements Filterable {
    private List<Ticket> ticketList;
    private List<Ticket> filteredTicketList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ticket_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textCreditcardTitle.setText(filteredTicketList.get(position).getTitle());
        String date = filteredTicketList.get(position).getCreatedDate().split("T")[0];
        holder.textDatepriority.setText(date + " | " + filteredTicketList.get(position).getPriority() + " | " +
                getSupportType(filteredTicketList.get(position).getTypeId()));
    }

    public String getSupportType(int supportTypeId) {
        String supportType;
        switch (supportTypeId) {
            case 1:
                return "Claims";
            case 2:
                return "Billing and Payment";
            case 3:
                return "Account Management";
            default:
                return "Other";
        }
    }

    @Override
    public int getItemCount() {
        return filteredTicketList.size();
    }

    public void setData(List<Ticket> creditCardList) {
        this.ticketList = creditCardList;
        filteredTicketList = creditCardList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textCreditcardTitle, textDatepriority;

        public ViewHolder(View itemView) {
            super(itemView);
            textCreditcardTitle = (TextView) itemView.findViewById(R.id.textCreditcardTitle);
            textDatepriority = (TextView) itemView.findViewById(R.id.datepriority);
            Typeface openSansRegular = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_regular.ttf");
            Typeface openSansSemiBold = Typeface.createFromAsset(SecurrantyApp.context.getAssets(),
                    "fonts/opensans_semibold.ttf");
            textCreditcardTitle.setTypeface(openSansSemiBold);
            textDatepriority.setTypeface(openSansRegular);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredTicketList = ticketList;
                } else {
                    List<Ticket> filteredList = new ArrayList<>();
                    for (Ticket copyTicketList : ticketList) {
                        if (copyTicketList.getTitle().toLowerCase().contains(charString.toLowerCase()) ||
                                copyTicketList.getCreatedDate().split("T")[0].toLowerCase().contains(charString.toLowerCase()) ||
                                copyTicketList.getPriority().toLowerCase().contains(charString.toLowerCase()) ||
                                getSupportType(copyTicketList.getTypeId()).toLowerCase().contains(charString.toLowerCase())
                                ) {
                            filteredList.add(copyTicketList);
                        }
                    }
                    filteredTicketList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredTicketList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredTicketList = (List<Ticket>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
