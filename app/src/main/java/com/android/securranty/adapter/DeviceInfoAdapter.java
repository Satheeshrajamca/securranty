package com.android.securranty.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.securranty.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by satheeshr on 13-12-2017.
 */

public class DeviceInfoAdapter extends RecyclerView.Adapter<DeviceInfoAdapter.ViewHolder> {
    Map<String, String> deviceInfo;

    public void setData(Map<String, String> deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_device_info, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Object key = deviceInfo.keySet().toArray()[position];
        Object value = deviceInfo.get(key);

        holder.deviceKey.setText(key!= null ? key.toString():"");
        holder.deviceValue.setText(value!=null ?value.toString():"");
    }

    @Override
    public int getItemCount() {
        return deviceInfo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView deviceKey, deviceValue;

        public ViewHolder(View itemView) {
            super(itemView);
            deviceKey = (TextView) itemView.findViewById(R.id.deviceKey);
            deviceValue = (TextView) itemView.findViewById(R.id.deviceValue);
        }
    }
}
