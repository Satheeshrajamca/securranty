package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.DashBoardPresenterInter;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshraja on 11/30/17.
 */

public class DashBoardPresenterImp implements BasePresenter {
    BaseView baseView;
    DashBoardPresenterInter dashBoardPresenter;

    public DashBoardPresenterImp(BaseView baseView) {
        this.baseView = baseView;
        dashBoardPresenter = new DashBoardPresenterInter(this);
    }

    public void initAdvertiseImages() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        dashBoardPresenter.getAdvertiseImages();
    }

    @Override
    public void success(Object obj) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.success(obj);
        }
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.showErrorMsg(statusCode, msg);
        }

    }

    @Override
    public void onDestroy() {
        if (baseView != null) {
            baseView = null;
        }
    }
}
