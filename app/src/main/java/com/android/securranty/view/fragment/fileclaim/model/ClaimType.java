
package com.android.securranty.view.fragment.fileclaim.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ClaimType {

    @SerializedName("problemTypes")
    private List<ProblemType> ProblemTypes;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<ProblemType> getProblemTypes() {
        return ProblemTypes;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<ProblemType> ProblemTypes;
        private String Status;
        private Long StatusCode;

        public ClaimType.Builder withProblemTypes(List<ProblemType> problemTypes) {
            ProblemTypes = problemTypes;
            return this;
        }

        public ClaimType.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public ClaimType.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public ClaimType build() {
            ClaimType ClaimType = new ClaimType();
            ClaimType.ProblemTypes = ProblemTypes;
            ClaimType.Status = Status;
            ClaimType.StatusCode = StatusCode;
            return ClaimType;
        }

    }

}
