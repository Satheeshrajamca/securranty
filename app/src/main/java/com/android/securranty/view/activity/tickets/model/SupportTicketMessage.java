
package com.android.securranty.view.activity.tickets.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SupportTicketMessage {

    @SerializedName("CreatedBy")
    private String createdBy;
    @SerializedName("CreatedDate")
    private String createdDate;
    @SerializedName("Id")
    private Long id;
    @SerializedName("IsMessageFromSecurranty")
    private Boolean isMessageFromSecurranty;
    @SerializedName("Message")
    private String message;

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public Long getId() {
        return id;
    }

    public Boolean getIsMessageFromSecurranty() {
        return isMessageFromSecurranty;
    }

    public String getMessage() {
        return message;
    }

    public static class Builder {

        private String createdBy;
        private String createdDate;
        private Long id;
        private Boolean isMessageFromSecurranty;
        private String message;

        public SupportTicketMessage.Builder withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public SupportTicketMessage.Builder withCreatedDate(String createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public SupportTicketMessage.Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public SupportTicketMessage.Builder withIsMessageFromSecurranty(Boolean isMessageFromSecurranty) {
            this.isMessageFromSecurranty = isMessageFromSecurranty;
            return this;
        }

        public SupportTicketMessage.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public SupportTicketMessage build() {
            SupportTicketMessage supportTicketMessage = new SupportTicketMessage();
            supportTicketMessage.createdBy = createdBy;
            supportTicketMessage.createdDate = createdDate;
            supportTicketMessage.id = id;
            supportTicketMessage.isMessageFromSecurranty = isMessageFromSecurranty;
            supportTicketMessage.message = message;
            return supportTicketMessage;
        }

    }

}
