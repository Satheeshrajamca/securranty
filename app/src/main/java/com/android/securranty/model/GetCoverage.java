
package com.android.securranty.model;

import com.google.gson.annotations.Expose;

import java.util.List;

@SuppressWarnings("unused")
public class GetCoverage {

    @Expose
    private List<Coverage> coverage;
    @Expose
    private String devicetype;
    @Expose
    private String devicetypeInfo;
    @Expose
    private String status;
    @Expose
    private int statusCode;

    public List<Coverage> getCoverage() {
        return coverage;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public String getDevicetypeInfo() {
        return devicetypeInfo;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private List<Coverage> coverage;
        private String devicetype;
        private String devicetypeInfo;
        private String status;
        private int statusCode;

        public GetCoverage.Builder withCoverage(List<Coverage> coverage) {
            this.coverage = coverage;
            return this;
        }

        public GetCoverage.Builder withDevicetype(String devicetype) {
            this.devicetype = devicetype;
            return this;
        }

        public GetCoverage.Builder withDevicetypeInfo(String devicetypeInfo) {
            this.devicetypeInfo = devicetypeInfo;
            return this;
        }

        public GetCoverage.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public GetCoverage.Builder withStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public GetCoverage build() {
            GetCoverage getCoverage = new GetCoverage();
            getCoverage.coverage = coverage;
            getCoverage.devicetype = devicetype;
            getCoverage.devicetypeInfo = devicetypeInfo;
            getCoverage.status = status;
            getCoverage.statusCode = statusCode;
            return getCoverage;
        }

    }

}
