package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.CreateTicketPresenterInter;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class CreateTicketPresenterImpl implements BasePresenter {
    private BaseView baseView;
    private CreateTicketPresenterInter createTicketPresenterInter;

    public CreateTicketPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        createTicketPresenterInter = new CreateTicketPresenterInter(this);
    }

    public void getTicketDetails() {
        if (baseView != null) {
            baseView.showProgress();
        }
        createTicketPresenterInter.getTicketDetils();
    }

    @Override
    public void success(Object obj) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.success(obj);
        }
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.showErrorMsg(statusCode, msg);
            baseView.hideProgress();
        }
    }

    @Override
    public void onDestroy() {
        baseView = null;
    }

    public void validateTicketInput(String subject, String typeId, String priority, String message) {
        if(baseView != null)
        {
            baseView.hideKeyboard();
            baseView.showProgress();
        }
        createTicketPresenterInter.createTicket(subject,typeId,priority,message);
    }
}
