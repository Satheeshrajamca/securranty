package com.android.securranty.model;

/**
 * Created by satheeshraja on 4/1/18.
 */

public class Child {
    private String key, value;
    private boolean isAutoComplete;

    public Child(String key, String value, boolean isAutoComplete) {
        this.key = key;
        this.value = value;
        this.isAutoComplete = isAutoComplete;
    }

    public String getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }

    public boolean isAutoComplete() {
        return isAutoComplete;
    }

}
