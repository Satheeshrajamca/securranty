package com.android.securranty.view.activity.tickets.model;


public class TicketDetailsModel {

    public TicketDetailsModel(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    private String title, description;


}
