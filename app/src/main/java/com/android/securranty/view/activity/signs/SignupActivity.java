package com.android.securranty.view.activity.signs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.model.LoginResponse;
import com.android.securranty.presenter.implementor.SignInUpPresenterImpl;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.*;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshr on 17-11-2017.
 */

public class SignupActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.editUserName)
    EditText editUserName;
    @BindView(R.id.editPassword)
    EditText editPassword;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.textForgotPass)
    TextView textForgotPass;
    @BindView(R.id.textCreateAcc)
    TextView textCreateAcc;
    @BindView(R.id.textPrivacyPolicy)
    TextView textPrivacyPolicy;
    @BindView(R.id.launchSignin)
    TextView launchSignin;
    @BindView(R.id.relativeSignin)
    RelativeLayout relativeSignin;
    @BindView(R.id.textSignIn)
    TextView textSignIn;

    private SignInUpPresenterImpl presenterSignUp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        presenterSignUp = new SignInUpPresenterImpl(this);
        textPrivacyPolicy.setVisibility(View.VISIBLE);
        textSignIn.setText("Create Account");
        launchSignin.setVisibility(View.VISIBLE);
        btnSignIn.setText("Sign Up");
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        textSignIn.setTypeface(openSansSemiBold);
        editUserName.setTypeface(openSansSemiBold);
        editPassword.setTypeface(openSansSemiBold);
        btnSignIn.setTypeface(openSansSemiBold);
        textPrivacyPolicy.setTypeface(openSansRegular);
        launchSignin.setTypeface(openSansRegular);
    }

    @OnClick({R.id.btnSignIn, R.id.launchSignin})
    public void onClickSignup(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:
                presenterSignUp.validateCredential(editUserName.getText().toString().trim(),
                        editPassword.getText().toString().trim(), SignInActivity.SIGNING.SIGNOUT, false);
                break;
            case R.id.launchSignin:
                finish();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeSignin, errorMsg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void success(Object obj) {
        LoginResponse loginResponse = LoginResponse.class.cast(obj);
        SharePref.getInstance(getApplicationContext()).storeUserDetails(loginResponse, "");

        Intent intent = getIntent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenterSignUp.onDestroy();
    }
}
