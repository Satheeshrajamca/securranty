package com.android.securranty.view.activity.plans.buy_plan;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.adapter.PlanDetailsAdapter;
import com.android.securranty.adapter.PlanListAdapter;
import com.android.securranty.model.AdPagePlan;
import com.android.securranty.model.Coverage;
import com.android.securranty.model.CoverageType;
import com.android.securranty.model.DeductibleInfo;
import com.android.securranty.model.GetCoverage;
import com.android.securranty.model.GetCoverageType;
import com.android.securranty.model.ItemCond;
import com.android.securranty.model.ItemCondition;
import com.android.securranty.model.Plan;
import com.android.securranty.model.PlanList;
import com.android.securranty.presenter.interactor.PlanDetailsPresenter;
import com.android.securranty.utility.RecyclerItemClickListener;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.MainActivity.PURCHASE_PLAN;
import static com.android.securranty.view.fragment.PlanFragment.PURCHASE_PLAN_REQ_CODE;

/**
 * Created by satheeshraja on 2/18/18.
 */

public class PlanDetailsActivity extends AppCompatActivity implements BaseView, PlanListAdapter.ListenerBuyPlan {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerYearPlan;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.planTitle)
    TextView planTitle;
    @BindView(R.id.planImage)
    ImageView planImage;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    @BindView(R.id.textRenewalText)
    TextView textRenewalText;
    @BindView(R.id.btnSelectCoverageAmt)
    Button btnSelectCoverageAmt;
    @BindView(R.id.radioGroupItemCondition)
    RadioGroup radioGroupItemCondition;
    @BindView(R.id.radioGroupCoverageType)
    RadioGroup radioGroupCoverageType;
    @BindView(R.id.textSelectDevice)
    TextView textSelectDevice;
    @BindView(R.id.textCoverageType)
    TextView textCoverageType;
    @BindView(R.id.textDeductiblehelp)
    TextView textDeductiblehelp;
    @BindView(R.id.textSelectPlan)
    TextView textSelectPlan;
    @BindView(R.id.buyNow)
    Button buyNow;
    @BindView(R.id.textCancelRefund)
    TextView textCancelRefund;

    private ActionBar actionBar;
    PlanDetailsPresenter planDetailsPresenter;
    private String planId;
    private int positionAdPage;
    private List<AdPagePlan> listAdPagePlans;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getIntent().hasExtra(PlansCategoryActivity.PLANID)) {
            planId = getIntent().getStringExtra(PlansCategoryActivity.PLANID);
            planDetailsPresenter = new PlanDetailsPresenter(this);
            planDetailsPresenter.initCoverage(planId);
            positionAdPage = getIntent().getIntExtra("ADD_POSITION", 0);
            loadData(AllPlansActivity.listAdPage.get(positionAdPage).getSummary(), AllPlansActivity.listAdPage.get(positionAdPage).getImageUrl(),
                    AllPlansActivity.listAdPage.get(positionAdPage).getAdPagePlans(), AllPlansActivity.listAdPage.get(positionAdPage).getTitle(),
                    AllPlansActivity.listAdPage.get(positionAdPage).getRenewalText());
            toolbar_title.setText(AllPlansActivity.listAdPage.get(positionAdPage).getPageName());
        }
        helpContent();
        recyclerviewItemClick();

    }

    private void recyclerviewItemClick() {
        recyclerYearPlan.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        selectedPlan = position;
                    }
                })
        );
    }

    private void helpContent() {
        textSelectDevice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (textSelectDevice.getRight() - textSelectDevice.getCompoundDrawables()[2].getBounds().width())) {
                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PlanDetailsActivity.this);
                        LayoutInflater inflater = PlanDetailsActivity.this.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.layout_custom_alert, null);
                        dialogBuilder.setView(dialogView);
                        final AlertDialog dialog = dialogBuilder.show();
                        TextView textTitle = (TextView) dialogView.findViewById(R.id.planName);
                        TextView description = (TextView) dialogView.findViewById(R.id.monthlyAmt);
                        ImageView imageClose = (ImageView) dialogView.findViewById(R.id.imageClose);
                        textTitle.setText("Select Device");
                        String a = coverage.getDevicetypeInfo().replaceAll("< div >","<div>")
                                .replaceAll("< strong >","<strong>")
                                .replaceAll("< br />","<br/>");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            description.setText(Html.fromHtml(a, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            description.setText(Html.fromHtml(a));
                        }
                        imageClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        return true;
                    }
                }
                return false;
            }
        });

        textCoverageType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (textSelectDevice.getRight() - textSelectDevice.getCompoundDrawables()[2].getBounds().width())) {
                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PlanDetailsActivity.this);
                        LayoutInflater inflater = PlanDetailsActivity.this.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.layout_custom_alert, null);
                        dialogBuilder.setView(dialogView);
                        final AlertDialog dialog = dialogBuilder.show();
                        TextView textTitle = (TextView) dialogView.findViewById(R.id.planName);
                        TextView description = (TextView) dialogView.findViewById(R.id.monthlyAmt);
                        ImageView imageClose = (ImageView) dialogView.findViewById(R.id.imageClose);
                       /* String a=itemCoverageType.getCoverageTypeInfo().
                                replaceAll("< ", "");
                        String b=a.replaceAll("</ ","");
                        String c= b.replaceAll(" >","");*/
                        textTitle.setText("Select Coverage Type");
                        String a = itemCoverageType.getCoverageTypeInfo().replaceAll("< div >","<div>")
                                .replaceAll("< strong >","<strong>")
                                .replaceAll("< br />","<br/>");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            //String strRegEx = "<[^>]*>";
                            //description.setText(android.text.Html.escapeHtml(itemCoverageType.getCoverageTypeInfo()).toString());
                            //String b = a.replaceAll("< strong >","<strong>");
                            //String c = b.replaceAll("< br />","<br/>");
                            String test="<div>Select the plan from options below.For mobile phones, coverage may be transferred to another device, if you upgrade and report the upgrade to us within 30 days of device upgrade.<br/><br/><strong> Billed Monthly: </strong> You will be Billed Monthly for this plan and typically it has a 2 - Year commitment.<br/> <br/><strong> One Payment:  </strong> You will make One Payment now and you will be covered for the entire term of the plan.If you purchase a 3 Year Plan, the One Payment covers your policy for 3 Years.</div>";
                            description.setText(Html.fromHtml(a, Html.FROM_HTML_MODE_COMPACT));
                            //description.setText(Html.fromHtml(c));
                        } else {
                            description.setText(Html.fromHtml(a));
                        }
                        //description.getSettings().setJavaScriptEnabled(true);
                        /*description.loadDataWithBaseURL(null, itemCoverageType.getCoverageTypeInfo().replaceAll(" ",""),
                                "text/html", "utf-8", null);*/
                        //description.loadDataWithBaseURL(null, itemCoverageType.getCoverageTypeInfo(), "text/html", "utf-8", null);


                        imageClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        return true;
                    }
                }


                return false;
            }
        });

        textSelectPlan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (textSelectPlan.getRight() - textSelectPlan.getCompoundDrawables()[2].getBounds().width())) {
                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PlanDetailsActivity.this);
                        LayoutInflater inflater = PlanDetailsActivity.this.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.layout_custom_alert, null);
                        dialogBuilder.setView(dialogView);
                        final AlertDialog dialog = dialogBuilder.show();
                        TextView textTitle = (TextView) dialogView.findViewById(R.id.planName);
                        TextView description = (TextView) dialogView.findViewById(R.id.monthlyAmt);
                        ImageView imageClose = (ImageView) dialogView.findViewById(R.id.imageClose);
                        textTitle.setText("Select Plan");
                        String a = planList.getPlanInfo().replaceAll("< div >","<div>")
                                .replaceAll("< strong >","<strong>")
                                .replaceAll("< br />","<br/>");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            description.setText(Html.fromHtml(a, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            description.setText(Html.fromHtml(planList.getPlanInfo().
                                    replaceAll(" ", "")));
                        }
                        imageClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        return true;
                    }
                }
                return false;
            }
        });

        textDeductiblehelp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (event.getRawX() >= (textDeductiblehelp.getRight() - textDeductiblehelp.getCompoundDrawables()[2].getBounds().width())) {
                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PlanDetailsActivity.this);
                        LayoutInflater inflater = PlanDetailsActivity.this.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.layout_custom_alert, null);
                        dialogBuilder.setView(dialogView);
                        final AlertDialog dialog = dialogBuilder.show();
                        TextView textTitle = (TextView) dialogView.findViewById(R.id.planName);
                        TextView description = (TextView) dialogView.findViewById(R.id.monthlyAmt);
                        ImageView imageClose = (ImageView) dialogView.findViewById(R.id.imageClose);
                        textTitle.setText("Deductible");

                        List<String> listdeductions = new ArrayList<>();
                        int sizeDeductibles = listDeductibleInfo.size();
                        for (int i = 0; i < sizeDeductibles; i++) {
                            description.setText(listDeductibleInfo.get(i).getName() + " : " + listDeductibleInfo.get(i).getAmount() + "\n");
                        }

                        imageClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        return true;
                    }
                }
                return false;
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        radioGroupItemCondition.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            private boolean isChecked(RadioGroup group, int viewId) {
                if (viewId != -1) {
                    View v = group.findViewById(viewId);
                    if (v instanceof RadioButton) {
                        return ((RadioButton) v).isChecked();
                    }
                }
                return false;
            }

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (!isChecked(group, checkedId)) {
                    return;
                }
                Log.i("radioGroupItemCondition", "radioGroupItemCondition");
                if (itemCondId != checkedId) {
                    itemCondId = checkedId;
                    getCoverageType();
                }
            }
        });

        radioGroupCoverageType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            private boolean isChecked(RadioGroup group, int viewId) {
                if (viewId != -1) {
                    View v = group.findViewById(viewId);
                    if (v instanceof RadioButton) {
                        return ((RadioButton) v).isChecked();
                    }
                }
                return false;
            }

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (!isChecked(group, checkedId)) {
                    return;
                }
                Log.i("radioGroupItemCondition", "radioGroupItemCondition");
                if (itemCondId != checkedId) {
                    coverageTypeId = checkedId;
                    getPlanList();
                }
            }
        });
    }

    @OnClick({R.id.imageBackButton, R.id.btnSelectCoverageAmt, R.id.textCancelRefund, R.id.buyNow})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
            case R.id.btnSelectCoverageAmt:
                AlertDialog.Builder builder = new AlertDialog.Builder(PlanDetailsActivity.this);
                builder.setItems(listDevice.toArray(new CharSequence[listDevice.size()]), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!listDevice.get(which).equals(btnSelectCoverageAmt.getText().toString().trim())) {
                            btnSelectCoverageAmt.setText(listDevice.get(which));
                            deviceId = listCoverage.get(which).getId();
                            getItemCondition();
                        }

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.textCancelRefund:
                onRefundHelp();
                break;
            case R.id.buyNow:
                buyPlan();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar2.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar2.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            finish();
            startActivity(new Intent(this, SignInActivity.class));
        }
    }

    /*@Override
    public void success(Object obj) {
        MyPlans myPlans = MyPlans.class.cast(obj);
        String planSummary = myPlans.getAdPage().get(0).getSummary();
        String imageUrl = myPlans.getAdPage().get(0).getImageUrl();
        List<AdPagePlan> listAdPagePlans = myPlans.getAdPage().get(0).getAdPagePlans();
        toolbar_title.setText(myPlans.getAdPage().get(0).getPageName());
        loadData(planSummary, imageUrl, listAdPagePlans, myPlans.getAdPage().get(0).getTitle(), myPlans.getAdPage().get(0).getRenewalText());
    }*/
    String deviceId;
    int itemCondId;
    int coverageTypeId;
    List<Coverage> listCoverage;
    List<ItemCondition> listItemCond;
    List<CoverageType> listCoverageType;
    GetCoverage coverage;
    GetCoverageType itemCoverageType;

    @Override
    public void success(Object obj) {
        if (obj instanceof GetCoverage) {
            coverage = GetCoverage.class.cast(obj);
            listCoverage = new ArrayList<>();
            listCoverage = coverage.getCoverage();
            btnSelectCoverageAmt.setText(listCoverage.get(0).getName());
            deviceId = listCoverage.get(0).getId();
            loadDevice();
            getItemCondition();
        } else if (obj instanceof ItemCond) {
            ItemCond itemCondition = ItemCond.class.cast(obj);
            listItemCond = new ArrayList<>();
            listItemCond = itemCondition.getItemCondition();
            itemCondId = listItemCond.get(0).getId();
            addItemCondition(listItemCond);
            getCoverageType();
        } else if (obj instanceof GetCoverageType) {
            itemCoverageType = GetCoverageType.class.cast(obj);
            listCoverageType = new ArrayList<>();
            listCoverageType = itemCoverageType.getCoverageType();
            coverageTypeId = listCoverageType.get(0).getId();
            addCoverageType(listCoverageType);
            getPlanList();
        } else if (obj instanceof PlanList) {
            selectedPlan = 0;
            planList = PlanList.class.cast(obj);
            listPlan = planList.getPlan();
            planInfo = planList.getPlanInfo();
            refundInfo = planList.getCancelAnytimeForRefundInfo();
            serviceOptionInfo = planList.getClaimAndServiceOptionInfo();

            listDeductibleInfo = planList.getDeductibleInfo();
            for (int i = 0; i < listDeductibleInfo.size(); i++) {

            }
            PlanListAdapter planListAdapter = new PlanListAdapter(PlanDetailsActivity.this, listPlan);
            recyclerYearPlan.setAdapter(planListAdapter);
        }
    }

    List<Plan> listPlan;
    private String planInfo, refundInfo, serviceOptionInfo;
    private PlanList planList;
    List<DeductibleInfo> listDeductibleInfo;

    private void addItemCondition(List<ItemCondition> listItemCondition) {
        radioGroupItemCondition.clearCheck();
        radioGroupItemCondition.removeAllViews();
        int sizeItemCond = listItemCondition.size();
        RadioButton[] rb = new RadioButton[sizeItemCond];
        for (int i = 0; i < sizeItemCond; i++) {
            rb[i] = new RadioButton(this);
            //rb[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_icon, 0);
            rb[i].setText(listItemCondition.get(i).getName());
            rb[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
            rb[i].setId(listItemCondition.get(i).getId());
            radioGroupItemCondition.addView(rb[i]);
        }
        radioGroupItemCondition.check(radioGroupItemCondition.getChildAt(0).getId());
    }

    private void addCoverageType(List<CoverageType> listCoverageType) {
        radioGroupCoverageType.clearCheck();
        radioGroupCoverageType.removeAllViews();
        int sizeItemCond = listCoverageType.size();
        RadioButton[] rb = new RadioButton[sizeItemCond];
        for (int i = 0; i < sizeItemCond; i++) {
            rb[i] = new RadioButton(this);
            //rb[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_icon, 0);
            rb[i].setText(listCoverageType.get(i).getName());
            rb[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
            rb[i].setId(listCoverageType.get(i).getId());
            radioGroupCoverageType.addView(rb[i]);
        }
        radioGroupCoverageType.check(radioGroupCoverageType.getChildAt(0).getId());
        radioGroupCoverageType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
    }

    private void getPlanList() {
        planDetailsPresenter.getPlanList(planId, deviceId, itemCondId, coverageTypeId);
    }

    private void getCoverageType() {
        planDetailsPresenter.getCoverageType(planId, deviceId, itemCondId);
    }

    private void getItemCondition() {
        planDetailsPresenter.getItemCondition(planId, deviceId);
    }

    PlanDetailsAdapter planDetailsAdapter;

    private void loadData(String planSummary, String imageUrl, List<AdPagePlan> listAdPagePlans, String title, String renewalText) {
        this.listAdPagePlans = listAdPagePlans;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerYearPlan.setLayoutManager(linearLayoutManager);
        recyclerYearPlan.setNestedScrollingEnabled(false);
        planTitle.setText(title);
        textRenewalText.setText(renewalText);
        Picasso.with(this).load(imageUrl).error(R.drawable.place_holder).into(planImage, new Callback() {
            @Override
            public void onSuccess() {
                progressBar1.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progressBar1.setVisibility(View.GONE);
            }
        });
        //loadDevice();
        /*planDetailsAdapter = new PlanDetailsAdapter(this, planSummary, imageUrl, listAdPagePlans,title,renewalText);
        recyclerYearPlan.setAdapter(planDetailsAdapter);*/

    }

    private List<String> listDevice;

    private void loadDevice() {
        listDevice = new ArrayList<>();
        for (Coverage coverage : listCoverage) {
            listDevice.add(coverage.getName());
        }
        /*Log.i("ListInfo", listDevice.toString());
        HashSet<SearchListItem> hashSet = new HashSet(listDevice);
        listDevice.clear();
        listDevice.addAll(hashSet);
        btnSelectCoverageAmt.setText(listDevice.get(0).getTitle());
        Collections.sort(listDevice, new SearchListItem());*/
        /*btnSelectCoverageAmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SearchableDialog searchableDialog = new SearchableDialog(PlanDetailsActivity.this,
                        listDevice, "Select Coverage Amount");
                searchableDialog.show();
                searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
                    @Override
                    public void onClick(int position, SearchListItem searchListItem) {
                        btnSelectCoverageAmt.setText(searchListItem.getTitle());

                *//*btnCoverageItemCond.setText("");
                btnCoverageType.setText("");
                coverageAmt = searchListItem.getTitle();
                coverageCondition = "";
                coverageType = "";*//*
                    }
                });
            }
        });*/

    }

    private int selectedPlan;

    @Override
    public void hideKeyboard() {

    }

    private Long purchasePlanId;
    private Double warrantyPrice;

    @Override
    public void buyPlan() {
        //Log.i("AdPlanId", "" + listAdPagePlans.get(position).getId());
        Plan plan = listPlan.get(selectedPlan);
        this.purchasePlanId = plan.getBasePlanId();
        this.warrantyPrice = plan.getAmount();
        if (SharePref.getInstance(this).isLoggedinCurrently()) {
            startActivityForResult(new Intent(PlanDetailsActivity.this, PurchasePlanActivity.class)
                    .putExtra("PLAN_ID", purchasePlanId).putExtra("WARANTY_PRICE", warrantyPrice).
                            putExtra("COVERAGE_NAME", plan.getPlanName()), PURCHASE_PLAN_REQ_CODE);
        } else {
            Intent i = new Intent(this, SignInActivity.class);
            startActivityForResult(i, PURCHASE_PLAN);
        }
    }

    @Override
    public void scrollTo() {
        recyclerYearPlan.scrollToPosition(1);
        //recyclerYearPlan.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onRefundHelp() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PlanDetailsActivity.this);
        LayoutInflater inflater = PlanDetailsActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_custom_alert, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog dialog = dialogBuilder.show();
        TextView textTitle = (TextView) dialogView.findViewById(R.id.planName);
        TextView description = (TextView) dialogView.findViewById(R.id.monthlyAmt);
        ImageView imageClose = (ImageView) dialogView.findViewById(R.id.imageClose);
        textTitle.setText("Cancel Anytime For a Refund!");
        String a = refundInfo.replaceAll("< div >","<div>")
                .replaceAll("< strong >","<strong>")
                .replaceAll("< br />","<br/>");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            description.setText(Html.fromHtml(a, Html.FROM_HTML_MODE_COMPACT));
        } else {
            description.setText(Html.fromHtml(a));
        }
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PURCHASE_PLAN) {
                startActivityForResult(new Intent(PlanDetailsActivity.this, PurchasePlanActivity.class)
                        .putExtra("PLAN_ID", purchasePlanId).putExtra("WARANTY_PRICE", warrantyPrice), PURCHASE_PLAN_REQ_CODE);
            } else if (requestCode == PURCHASE_PLAN_REQ_CODE) {
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
