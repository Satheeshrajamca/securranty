package com.ajithvgiri.searchdialog;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.Objects;

/**
 * Created by ajithvgiri on 06/11/17.
 */

public class SearchListItem implements Comparator<SearchListItem> {
    int id;
    String title;

    public SearchListItem() {
    }

    public SearchListItem(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchListItem that = (SearchListItem) o;

        return title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }


    @Override
    public int compare(SearchListItem a, SearchListItem b) {
        return a.getTitle().compareTo(b.getTitle());
    }
}
