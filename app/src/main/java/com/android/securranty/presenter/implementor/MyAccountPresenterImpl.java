package com.android.securranty.presenter.implementor;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import android.view.View;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.BillingPresenterInteractor;
import com.android.securranty.presenter.interactor.MyAccountPresenterInteractor;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.AccountView;
import com.android.securranty.view.activity.account.UpdateAccountProfile;
import com.android.securranty.view.activity.creditcard.AddEditCreditcardActivity;
import com.android.securranty.view.activity.shipping.ApiErrorAddShipAddress;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.android.securranty.view.activity.account.AccountProfileActivity.ACCOUNT_EDIT;
import static com.android.securranty.view.activity.account.AccountProfileActivity.ACCOUNT_IDLE;

/**
 * Created by satheeshr on 21-11-2017.
 */

public class MyAccountPresenterImpl implements BasePresenter {
    private AccountView accountView;
    private MyAccountPresenterInteractor myAccountPresenterInt;
    private BillingPresenterInteractor billingPresenterInteractor;
    private AddEditCreditcardActivity addEditCreditcardActivity;

    public MyAccountPresenterImpl(AccountView accountView) {
        this.accountView = accountView;
        this.myAccountPresenterInt = new MyAccountPresenterInteractor(this);
        this.billingPresenterInteractor = new BillingPresenterInteractor(this);
    }

    public void editProfile(String tag, String firstName, String lastName, String email, String mobile, String address1,
                            String state, String country, String zip, String salesTax, String salesTaxCountry, String salesTaxPercent,
                            String companyName, String contactEmail, String officePhone,
                            String address2, String city,File profileImage) {
        if (tag.equals(ACCOUNT_EDIT)) {
            if (accountView != null) {
                accountView.showEditView(View.VISIBLE, View.GONE);
                accountView.setTag(ACCOUNT_IDLE);
                accountView.changeIcon(R.drawable.save_button);
            }
        } else {
            accountView.showProgress();
            accountView.hideKeyboard();

            if (!firstName.isEmpty()) {
                if (!lastName.isEmpty()) {
                        if (!email.isEmpty()) {
                            if (!contactEmail.isEmpty()) {
                                if (!mobile.isEmpty()) {
                                    if (!officePhone.isEmpty()) {
                                        if (!address1.isEmpty()) {
                                            if (!country.isEmpty()) {
                                                if (!state.isEmpty()) {
                                                    if (!city.isEmpty()) {
                                                        if (!zip.isEmpty()) {
                                                            myAccountPresenterInt.updateMyAccount(firstName, lastName, email, mobile, address1, state, country, zip, salesTax, salesTaxCountry, salesTaxPercent,
                                                                    companyName, contactEmail, officePhone, address2, city,profileImage);
                                                        } else {
                                                            accountView.hideProgress();
                                                            accountView.showErrorMsg(201, "Zip do not empty");
                                                        }
                                                    } else {
                                                        accountView.hideProgress();
                                                        accountView.showErrorMsg(201, "City do not empty");
                                                    }
                                                } else {
                                                    accountView.hideProgress();
                                                    accountView.showErrorMsg(201, "State do not empty");
                                                }
                                            } else {
                                                accountView.hideProgress();
                                                accountView.showErrorMsg(201, "Country do not empty");
                                            }
                                        } else {
                                            accountView.hideProgress();
                                            accountView.showErrorMsg(201, "Address1 do not empty");
                                        }
                                    } else {
                                        accountView.hideProgress();
                                        accountView.showErrorMsg(201, "Office Phone No do not empty");
                                    }
                                } else {
                                    accountView.hideProgress();
                                    accountView.showErrorMsg(201, "Mobile Phone No do not empty");
                                }
                            } else {
                                accountView.hideProgress();
                                accountView.showErrorMsg(201, "Contact Email do not empty");
                            }
                        } else {
                            accountView.hideProgress();
                            accountView.showErrorMsg(201, "Account Email do not empty");
                        }

                } else {
                    accountView.hideProgress();
                    accountView.showErrorMsg(201, "Last Name do not empty");
                }
            } else {
                accountView.hideProgress();
                accountView.showErrorMsg(201, "First Name do not empty");
            }
        }
    }

    public void editBillingProfile(String tag, String billinngContactName, String billingContactPhone, String billingContactEmail, String billingEmail, String billingOrderNo,
                                   String billingAddress1, String billingAddress2, String billingCity, String billingState, String billingZip, String billingCountry, String billingMethod) {
        if (tag.equals(ACCOUNT_EDIT)) {
            if (accountView != null) {
                accountView.showEditView(View.VISIBLE, View.GONE);
                accountView.setTag(ACCOUNT_IDLE);
                accountView.changeIcon(0);
            }
        } else {
            accountView.showProgress();
            accountView.hideKeyboard();
            billingPresenterInteractor.updateBillingProfile(billinngContactName, billingContactPhone, billingContactEmail, billingEmail, billingOrderNo,
                    billingAddress1, billingAddress2, billingCity, billingState, billingZip, billingCountry,
                    billingMethod);
        }
    }

    public void getMyAccountDetails() {
        if (accountView != null) {
            accountView.showProgress();
        }
        myAccountPresenterInt.getMyAccountandCountryList();
    }

    public void getBillingDetails() {
        if (accountView != null) {
            accountView.showProgress();
        }
        billingPresenterInteractor.getBillingAccount();
    }

    @Override
    public void success(Object obj) {
        if (accountView != null) {
            if (obj instanceof APISuccessModel || obj instanceof UpdateAccountProfile) {
                accountView.setTag(ACCOUNT_EDIT);
                accountView.changeIcon(R.drawable.edit_icon);
                accountView.showEditView(View.GONE, View.VISIBLE);
            }
            accountView.hideProgress();
            accountView.success(obj);

        }
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (accountView != null) {
            accountView.showErrorMsg(statusCode, msg);
            accountView.hideProgress();
        }
    }

    @Override
    public void onDestroy() {
        accountView = null;
    }


    public void initStatesList(String countryName, int selectedCountryId) {
        if (accountView != null) {
            accountView.showProgress();
        }
        myAccountPresenterInt.getStatesList(countryName, selectedCountryId);
    }

    public void initCountryList() {
        if (accountView != null) {
            accountView.showProgress();
        }
        myAccountPresenterInt.getCountryList();
    }


    public void initBuyPlan(String email, String address1, String address2, String country, String state, String city,
                            String zip, String creditcardno, String expMonth, String expYear, String cvv, String planId,String paymentProfileId) {
        if (accountView != null) {
            accountView.showProgress();
            accountView.hideKeyboard();
        }
        myAccountPresenterInt.buyPlan(email, address1, address2, country, state, city,
                zip, creditcardno, expMonth, expYear, cvv, planId,paymentProfileId);
    }

    public void backPressed(Object obj) {
        if (accountView != null) {
            accountView.hideKeyboard();
            accountView.setTag(ACCOUNT_EDIT);
            accountView.changeIcon(R.drawable.edit_icon);
            accountView.success(obj);
            accountView.showEditView(View.GONE, View.VISIBLE);
        }
    }

    public void addShippingAddress(String shippingName, String companyName, String email, String phone,
                                   String address1, String address2, String country, String state, String city,
                                   String zip) {
        if (accountView != null) {
            accountView.showProgress();
            accountView.hideKeyboard();
        }
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (!shippingName.isEmpty()) {
                /*if (!companyName.isEmpty()) {*/
                    if (Utils.isEmailValid(email)) {
                        if (!phone.isEmpty()) {
                            if (!address1.isEmpty()) {
                                if (!country.isEmpty()) {
                                    if (!state.isEmpty()) {
                                        if (!city.isEmpty()) {
                                            if (!zip.isEmpty()) {
                                                ApiClient.getClient().addShippingAddress(SharePref.getInstance(SecurrantyApp.context).getToken(),
                                                        SharePref.getInstance(SecurrantyApp.context).getUserId(),
                                                        shippingName, companyName, email, phone, address1, address2,
                                                        country, state, city, zip).enqueue(new Callback<APISuccessModel>() {
                                                    @Override
                                                    public void onResponse(Call<APISuccessModel> call, Response<APISuccessModel> response) {
                                                        accountView.hideProgress();
                                                        if (response.isSuccessful()) {
                                                            APISuccessModel apiSuccessModel = response.body();
                                                            if (apiSuccessModel.getStatusCode() == 200) {
                                                                accountView.success(apiSuccessModel);
                                                            } else {
                                                                errorResponse(response.errorBody());
                                                            }

                                                        } else {
                                                            errorResponse(response.errorBody());
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<APISuccessModel> call, Throwable t) {
                                                        accountView.hideProgress();
                                                        if (t instanceof HttpException)
                                                            errorResponse(((HttpException) t).response().errorBody());
                                                        else
                                                            accountView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                                                    }
                                                });
                                            } else {
                                                accountView.hideProgress();
                                                accountView.showErrorMsg(201, "Zip do not empty");
                                            }
                                        } else {
                                            accountView.hideProgress();
                                            accountView.showErrorMsg(201, "City do not empty");
                                        }
                                    } else {
                                        accountView.hideProgress();
                                        accountView.showErrorMsg(201, "State do not empty");
                                    }
                                } else {
                                    accountView.hideProgress();
                                    accountView.showErrorMsg(201, "Country do not empty");
                                }
                            } else {
                                accountView.hideProgress();
                                accountView.showErrorMsg(201, "Address1 do not empty");
                            }
                        } else {
                            accountView.hideProgress();
                            accountView.showErrorMsg(201, "Phone number do not empty");
                        }
                    } else {
                        accountView.hideProgress();
                        accountView.showErrorMsg(401, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
                    }
                /*} else {
                    accountView.hideProgress();
                    accountView.showErrorMsg(201, "Company name do not empty");
                }*/
            } else {
                accountView.hideProgress();
                accountView.showErrorMsg(201, "Shipping name do not empty");
            }
        } else {
            accountView.hideProgress();
            accountView.showErrorMsg(20, SecurrantyApp.context.getResources().getString(R.string.err_invalid_email));
        }
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            ApiErrorAddShipAddress message = gson.fromJson(responseBody.charStream(), ApiErrorAddShipAddress.class);
            int statusCode = message.getStatusCode();
            accountView.showErrorMsg(statusCode, message.getMessage().get(0));
        } catch (IllegalStateException | JsonSyntaxException exception) {
            accountView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
