package com.android.securranty.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.model.AdapterMenu;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by satheeshr on 20-11-2017.
 */

public class MenuAdapter extends ArrayAdapter<AdapterMenu> {
    private Context context;

    public MenuAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<AdapterMenu> objects) {
        super(context, resource, objects);
        this.context = context;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        AdapterMenu menu = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_girdmenu, parent, false);
        }
        // Lookup view for data population
        ImageView imageMenu = (ImageView) convertView.findViewById(R.id.menu);
        TextView textMenuName = (TextView) convertView.findViewById(R.id.textMenuName);
        Picasso.with(context).load(menu.getMenuIcon()).into(imageMenu);
        textMenuName.setText(menu.getMenuName());
        return convertView;
    }
}
