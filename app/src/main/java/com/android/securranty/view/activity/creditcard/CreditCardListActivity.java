package com.android.securranty.view.activity.creditcard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.CreditcardListAdapter;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.presenter.implementor.CreditcardListPresenterImpl;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.RecyclerItemClickListener;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.shipping.AddShippingAddressActivity;
import com.android.securranty.view.activity.shipping.ShippingActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshraja on 12/3/17.
 */

public class CreditCardListActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.recyclerCreditcardList)
    RecyclerView recyclerCreditcardList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private CreditcardListAdapter creditcardListAdapter;
    private ActionBar actionBar;
    private CreditcardListPresenterImpl creditcardListPresenterImpl;
    public static String ISEditCreditCard = "EDIT_CREDITCARD";
    private String from;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creditcard_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        creditcardListPresenterImpl = new CreditcardListPresenterImpl(this);
        creditcardListPresenterImpl.initCreditcardList();
        recyclerviewItemClick();
        from=getIntent().getStringExtra("FROM");
    }

    private void recyclerviewItemClick() {
        recyclerCreditcardList.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if(from == null) {
                            startActivityForResult(new Intent(CreditCardListActivity.this,
                                    AddEditCreditcardActivity.class).putExtra(ISEditCreditCard, true)
                                    .putExtra("SelectedPosition", String.valueOf(position)), 401);
                        }
                        else
                        {
                            Intent intent = getIntent();
                            intent.putExtra("PAYMENT_PROFILE_ID",String.valueOf(listCreditCardList.get(position).getId()));
                            intent.putExtra("CARD_NO",listCreditCardList.get(position).getLastFour());
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                })
        );
    }

    @OnClick({R.id.imageAdd, R.id.imageBackButton})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageAdd:
                startActivityForResult(new Intent(this, AddEditCreditcardActivity.class)
                        .putExtra(ISEditCreditCard, false), 401);
                break;
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    public static List<CreditCardList.CreditCardListItem> listCreditCardList;

    @Override
    public void success(Object obj) {
        if (CreditCardList.class.cast(obj).getCreditCardList().size() > 0) {
            listCreditCardList = new ArrayList<>();
            listCreditCardList = CreditCardList.class.cast(obj).getCreditCardList();
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this);
            recyclerCreditcardList.addItemDecoration(dividerItemDecoration);

            recyclerCreditcardList.setLayoutManager(linearLayoutManager);
            creditcardListAdapter = new CreditcardListAdapter();
            creditcardListAdapter.setData(listCreditCardList);
            recyclerCreditcardList.setAdapter(creditcardListAdapter);
        } else {
            //showErrorMsg(400, "There's no credit card to display");
           showErrorDialog();
        }
    }

    private void showErrorDialog() {
        new MaterialDialog.Builder(this).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                //finish();
                startActivityForResult(new Intent(CreditCardListActivity.this, AddEditCreditcardActivity.class), 402);
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                finish();
            }
        })
                .canceledOnTouchOutside(false)
                .title("Credit Cards")
                .content("No Credit Card Found.")
                .positiveText("Add Credit Card")
                .negativeText("Cancel")
                .show();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        creditcardListPresenterImpl.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            creditcardListPresenterImpl.initCreditcardList();
        }
        else {
            if(listCreditCardList==null)
            {
                showErrorDialog();
            }
        }
    }
}
