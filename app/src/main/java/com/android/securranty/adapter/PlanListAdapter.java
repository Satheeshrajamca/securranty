package com.android.securranty.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.model.Plan;
import com.android.securranty.view.activity.plans.buy_plan.PlanDetailsActivity;

import java.util.List;

public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.PlanHolder> {
    Context context;
    List<Plan> listPlan;
    PlanDetailsActivity planDetailsActivity;

    public PlanListAdapter(PlanDetailsActivity planDetailsActivity, List<Plan> listPlan) {
        this.planDetailsActivity=planDetailsActivity;
        this.context = planDetailsActivity;
        this.listPlan = listPlan;
    }

    @Override
    public PlanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_price_plans, parent, false);
        return new PlanHolder(itemView);
    }
    public interface ListenerBuyPlan {
        void buyPlan();

        void scrollTo();

        void onRefundHelp();
    }
    int row_index=0;

    @Override
    public void onBindViewHolder(PlanHolder holder, final int position) {
        final Plan plan = listPlan.get(position);
        holder.planName.setText(plan.getPlanName());
        holder.totalAmt.setText(String.valueOf(plan.getAmount()));
        holder.monthlyAmt.setText(String.valueOf(plan.getMonthlyAmount()));
        holder.planNameSubhead.setText(plan.getPlanNameSubHeading());
        /*if ((listPlan.size() - 1) == position)
            holder.linearPlan.setVisibility(View.VISIBLE);
        else
            holder.linearPlan.setVisibility(View.GONE);*/
        holder.relativePlans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index = position;
                notifyDataSetChanged();
            }
        });

        if(row_index == position)
        {
            holder.radioButton.setChecked(true);
            holder.relativePlans.setBackground(ContextCompat.getDrawable(context,R.drawable.reltaive_selected_corner));
        }
        else
        {
            holder.relativePlans.setBackground(ContextCompat.getDrawable(context,R.drawable.relative_noraml_corner));
            holder.radioButton.setChecked(false);
        }

        holder.buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                planDetailsActivity.buyPlan();
            }
        });
        holder.planNameSubhead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                planDetailsActivity.onRefundHelp();
            }
        });
    }

    public class PlanHolder extends RecyclerView.ViewHolder {
        public TextView planName, totalAmt, monthlyAmt, planNameSubhead;
        public LinearLayout linearPlan;
        public RelativeLayout relativePlans;
        public RadioButton radioButton;
        public Button buyNow;

        public PlanHolder(View view) {
            super(view);
            planName = (TextView) view.findViewById(R.id.planName);
            totalAmt = (TextView) view.findViewById(R.id.totalAmt);
            monthlyAmt = (TextView) view.findViewById(R.id.monthlyAmt);
            planNameSubhead = (TextView) view.findViewById(R.id.planNameSubhead);
            linearPlan = (LinearLayout) view.findViewById(R.id.linearPlan);
            relativePlans = (RelativeLayout) view.findViewById(R.id.relativePlans);
            radioButton=(RadioButton)view.findViewById(R.id.radioBtn);
            buyNow=(Button)view.findViewById(R.id.buyNow);
        }
    }

    @Override
    public int getItemCount() {
        return listPlan.size();
    }
}
