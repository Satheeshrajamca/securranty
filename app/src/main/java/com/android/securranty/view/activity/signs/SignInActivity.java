package com.android.securranty.view.activity.signs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.LoginResponse;
import com.android.securranty.presenter.implementor.SignInUpPresenterImpl;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by satheeshr on 16-11-2017.
 */

public class SignInActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.editUserName)
    EditText editUserName;
    @BindView(R.id.editPassword)
    EditText editPassword;
    @BindView(R.id.checkbox_rememberme)
    CheckBox checkboxRememberme;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.textForgotPass)
    TextView textForgotPass;
    @BindView(R.id.textCreateAcc)
    TextView textCreateAcc;
    @BindView(R.id.relativeForgotPassNewAc)
    RelativeLayout relativeForgotPassNewAc;
    @BindView(R.id.relativeSignin)
    RelativeLayout relativeSignin;
    @BindView(R.id.textSignIn)
    TextView textSignIn;

    private SignInUpPresenterImpl presenterSignIn;
    private boolean isCheckedRememberMe;
    private String userName, Password;

    public enum SIGNING {
        SIGNIN, SIGNOUT;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        SharePref.getInstance(this).setProfileImage("");
        presenterSignIn = new SignInUpPresenterImpl(this);
        checkboxRememberme.setVisibility(View.VISIBLE);
        relativeForgotPassNewAc.setVisibility(View.VISIBLE);
        ifAlreadyLoggedIn();
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        textSignIn.setTypeface(openSansSemiBold);
        editUserName.setTypeface(openSansSemiBold);
        editPassword.setTypeface(openSansSemiBold);
        btnSignIn.setTypeface(openSansSemiBold);
        checkboxRememberme.setTypeface(openSansRegular);
        textCreateAcc.setTypeface(openSansRegular);
        textForgotPass.setTypeface(openSansRegular);
    }

    private void ifAlreadyLoggedIn() {
        if (SharePref.getInstance(this).isLoggedIn()) {
           /* editUserName.setText(SharePref.getInstance(this).getUserEmail());
        } else {*/
            checkboxRememberme.setChecked(true);
            editUserName.setText(SharePref.getInstance(this).getUserEmail());
            editPassword.setText(SharePref.getInstance(this).getPassword());
        }
    }

    @OnClick({R.id.btnSignIn, R.id.textCreateAcc, R.id.textForgotPass})
    public void onClickSignIn(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:
                userName = editUserName.getText().toString().trim();
                Password = editPassword.getText().toString().trim();
                presenterSignIn.validateCredential(userName, Password, SIGNING.SIGNIN, isCheckedRememberMe);
                break;
            case R.id.textCreateAcc:
                startActivityForResult(new Intent(this, SignupActivity.class), 401);
                break;
            case R.id.textForgotPass:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
        }

    }

    @OnCheckedChanged(R.id.checkbox_rememberme)
    public void onCheckedChanged(boolean isChecked) {
        isCheckedRememberMe = isChecked;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeSignin, errorMsg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void success(Object obj) {
        LoginResponse loginResponse = LoginResponse.class.cast(obj);
        SharePref.getInstance(getApplicationContext()).storeUserDetails(loginResponse, isCheckedRememberMe ? Password : "");

        Intent intent = getIntent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenterSignIn.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            Intent intent = getIntent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
