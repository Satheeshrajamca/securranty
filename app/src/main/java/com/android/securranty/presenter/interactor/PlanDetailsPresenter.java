package com.android.securranty.presenter.interactor;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.GetCoverage;
import com.android.securranty.model.GetCoverageType;
import com.android.securranty.model.ItemCond;
import com.android.securranty.model.MyPlans;
import com.android.securranty.model.PlanList;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;


/**
 * Created by satheeshraja on 2/18/18.
 */

public class PlanDetailsPresenter {
    BaseView baseView;

    public PlanDetailsPresenter(BaseView baseView) {
        this.baseView = baseView;
    }

    public void initPlainDetails(String planId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        apiCall(planId);
    }

    public void initCoverage(String planId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        getCoverage(planId);
    }

    public void apiCall(String planId) {
        ApiClient.getClient().getPlanDetails(planId).enqueue(new Callback<MyPlans>() {
            @Override
            public void onResponse(Call<MyPlans> call, Response<MyPlans> response) {
                if (response.isSuccessful()) {
                    MyPlans myplansResponse = response.body();
                    if (myplansResponse.getStatusCode() == 200) {
                        String json = new Gson().toJson(response);
                        baseView.success(myplansResponse);
                    } else {
                        baseView.showErrorMsg(myplansResponse.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
                baseView.hideProgress();
            }

            @Override
            public void onFailure(Call<MyPlans> call, Throwable t) {
                baseView.hideProgress();
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void getCoverage(String planId) {
        ApiClient.getClient().getCoverage(SharePref.getInstance(SecurrantyApp.context).getToken(), planId).enqueue(new Callback<GetCoverage>() {
            @Override
            public void onResponse(Call<GetCoverage> call, Response<GetCoverage> response) {
                if (response.isSuccessful()) {
                    GetCoverage myplansResponse = response.body();
                    if (myplansResponse.getStatusCode() == 200) {
                        String json = new Gson().toJson(response);
                        baseView.success(myplansResponse);
                    } else {
                        baseView.showErrorMsg(myplansResponse.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
                baseView.hideProgress();
            }

            @Override
            public void onFailure(Call<GetCoverage> call, Throwable t) {
                baseView.hideProgress();
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            baseView.showErrorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }

    public void getItemCondition(String planId, String coverageId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        ApiClient.getClient().getItemCondtion(SharePref.getInstance(SecurrantyApp.context).getToken(), planId, coverageId).enqueue(new Callback<ItemCond>() {
            @Override
            public void onResponse(Call<ItemCond> call, Response<ItemCond> response) {
                if (response.isSuccessful()) {
                    ItemCond itemCondition = response.body();
                    if (itemCondition.getStatusCode() == 200) {
                        String json = new Gson().toJson(response);
                        baseView.success(itemCondition);
                    } else {
                        baseView.showErrorMsg(itemCondition.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
                baseView.hideProgress();
            }

            @Override
            public void onFailure(Call<ItemCond> call, Throwable t) {
                baseView.hideProgress();
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });

    }


    public void getCoverageType(String planId, String coverageId, int itemCondId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        ApiClient.getClient().getCoverageType(SharePref.getInstance(SecurrantyApp.context).getToken(), planId, coverageId, itemCondId).enqueue(new Callback<GetCoverageType>() {
            @Override
            public void onResponse(Call<GetCoverageType> call, Response<GetCoverageType> response) {
                if (response.isSuccessful()) {
                    GetCoverageType coverageType = response.body();
                    if (coverageType.getStatusCode() == 200) {
                        String json = new Gson().toJson(response);
                        baseView.success(coverageType);
                    } else {
                        baseView.showErrorMsg(coverageType.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
                baseView.hideProgress();
            }

            @Override
            public void onFailure(Call<GetCoverageType> call, Throwable t) {
                baseView.hideProgress();
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    public void getPlanList(String planId, String coverageId, int itemCondId, int coverageTypeId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        ApiClient.getClient().getPlanList(SharePref.getInstance(SecurrantyApp.context).getToken(), planId, coverageId, itemCondId,coverageTypeId)
                .enqueue(new Callback<PlanList>() {
            @Override
            public void onResponse(Call<PlanList> call, Response<PlanList> response) {
                if (response.isSuccessful()) {
                    PlanList coverageType = response.body();
                    if (coverageType.getStatusCode() == 200) {
                        String json = new Gson().toJson(response);
                        baseView.success(coverageType);
                    } else {
                        baseView.showErrorMsg(coverageType.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
                baseView.hideProgress();
            }

            @Override
            public void onFailure(Call<PlanList> call, Throwable t) {
                baseView.hideProgress();
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }
}
