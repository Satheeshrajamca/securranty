package com.android.securranty.presenter.implementor;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import android.util.Log;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.Carrier;
import com.android.securranty.model.ConditionModel;
import com.android.securranty.model.CostCenterModel;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.DeviceTypesModel;
import com.android.securranty.model.DivisionModel;
import com.android.securranty.model.Manufacturer;
import com.android.securranty.model.PolicyModel;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;
import com.android.securranty.utility.SharePref;
import com.android.securranty.utility.Utils;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.account.AccountProfileModel;
import com.android.securranty.view.activity.plans.active_plan.model.ActivePlan;
import com.android.securranty.view.activity.plans.active_plan.model.UploadImageModel;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 3/23/18.
 */

public class EditPlanPresenterImpl {
    private BaseView baseView;

    public EditPlanPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
    }

    public void intActivePlans() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiCall();
    }

    private void apiCall() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getActivePlans(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<ActivePlan>() {
                @Override
                public void onResponse(Call<ActivePlan> call, Response<ActivePlan> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<ActivePlan> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getManufacturer(Long itemTypeId) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiManufacturer(itemTypeId);


    }

    private void apiManufacturer(Long itemTypeId) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getManufacturer(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    itemTypeId).enqueue(new Callback<Manufacturer>() {
                @Override
                public void onResponse(Call<Manufacturer> call, Response<Manufacturer> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Manufacturer> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getModel(Long itemTypeId, Long manufacturerId) {
        if (manufacturerId > 0) {
            if (baseView != null) {
                baseView.showProgress();
                baseView.hideKeyboard();
            }
            apiModel(itemTypeId, manufacturerId);
        } else {
            baseView.showErrorMsg(400, "Please choose manufacturer");
        }

    }

    private void apiModel(Long itemTypeId, Long manufacturerId) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getModel(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    itemTypeId, manufacturerId).enqueue(new Callback<PolicyModel>() {
                @Override
                public void onResponse(Call<PolicyModel> call, Response<PolicyModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<PolicyModel> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getCarrier() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiCarrier();
    }

    private void apiCarrier() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getCarrier(SharePref.getInstance(SecurrantyApp.context).getToken()).enqueue(new Callback<Carrier>() {
                @Override
                public void onResponse(Call<Carrier> call, Response<Carrier> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Carrier> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getDeviceModel() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiDeviceModel();
    }

    private void apiDeviceModel() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getDeviceModel(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<DeviceTypesModel>() {
                @Override
                public void onResponse(Call<DeviceTypesModel> call, Response<DeviceTypesModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<DeviceTypesModel> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getConditions() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiConditions();
    }

    private void apiConditions() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getConditionModel(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<ConditionModel>() {
                @Override
                public void onResponse(Call<ConditionModel> call, Response<ConditionModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<ConditionModel> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }


    public void getDivision() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiDivision();
    }

    private void apiDivision() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getDivisionModel(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<DivisionModel>() {
                @Override
                public void onResponse(Call<DivisionModel> call, Response<DivisionModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<DivisionModel> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getCostCenter() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiCostCenter();
    }

    private void apiCostCenter() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getCostCenterModel(SharePref.getInstance(SecurrantyApp.context).getToken(),
                    SharePref.getInstance(SecurrantyApp.context).getUserId()).enqueue(new Callback<CostCenterModel>() {
                @Override
                public void onResponse(Call<CostCenterModel> call, Response<CostCenterModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<CostCenterModel> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getCountry() {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }
        apiGetCountry();
    }

    private void apiGetCountry() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getCountryLists(SharePref.getInstance(SecurrantyApp.context).getToken()
            ).enqueue(new Callback<CountryList>() {
                @Override
                public void onResponse(Call<CountryList> call, Response<CountryList> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<CountryList> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void updatePlan(final String carrierId, final String costcenterId, final String countryId, final String divisionId,
                           final String FirstName, final String IMEI, final String ItemConditionId, final String ItemPurchaseDate, final String ItemPurchasePrice,
                           final String LastName, final
                           String manufacturerId, final String modelId, final String PhoneNumber, final Long policyId, final String Serial, final File fileUploadImage) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();
        }

        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (fileUploadImage != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), fileUploadImage);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("photo_path", fileUploadImage.getName(), requestFile);
                RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "photo_path");

                ApiClient.getClient()
                        .updatePlanProfileImage(
                                SharePref.getInstance(SecurrantyApp.context).getToken()
                                , body, name).enqueue(new Callback<UploadImageModel>() {
                    @Override
                    public void onResponse(Call<UploadImageModel> call, Response<UploadImageModel> response) {

                        Log.i("Status", "Success");
                        if (response.isSuccessful()) {
                            JsonArray jsonArray = new JsonArray();
                            jsonArray.add(response.body().getFiles().get(0));
                            apiUpdatePlan(getRequestBody(carrierId, costcenterId, countryId, divisionId,
                                    FirstName, IMEI, ItemConditionId, ItemPurchaseDate, ItemPurchasePrice, LastName,
                                    manufacturerId, modelId, PhoneNumber, policyId, Serial, jsonArray));
                        } else {
                            baseView.hideProgress();
                            errorResponse(response.errorBody());
                        }

                    }

                    @Override
                    public void onFailure(Call<UploadImageModel> call, Throwable e) {
                        baseView.hideProgress();
                        if (e instanceof HttpException)
                            errorResponse(((HttpException) e).response().errorBody());
                        else
                            baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                    }
                });
            } else {
                apiUpdatePlan(getRequestBody(carrierId, costcenterId, countryId, divisionId,
                        FirstName, IMEI, ItemConditionId, ItemPurchaseDate, ItemPurchasePrice, LastName,
                        manufacturerId, modelId, PhoneNumber, policyId, Serial, new JsonArray()));
            }
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }


    }

    private void apiUpdatePlan(JsonObject jsonObject) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().updatePlan(SharePref.getInstance(SecurrantyApp.context).getToken(), jsonObject
            ).enqueue(new Callback<APISuccessModel>() {
                @Override
                public void onResponse(Call<APISuccessModel> call, Response<APISuccessModel> response) {
                    if (response.isSuccessful()) {
                        baseView.hideProgress();
                        baseView.success(response.body());
                    } else {
                        baseView.hideProgress();
                        errorResponse(response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<APISuccessModel> call, Throwable e) {
                    baseView.hideProgress();
                    if (e instanceof HttpException)
                        errorResponse(((HttpException) e).response().errorBody());
                    else
                        baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }
            });
        } else {
            baseView.hideProgress();
            baseView.showErrorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    private JsonObject getRequestBody(String carrierId, String costcenterId, String countryId,
                                      String divisionId, String firstName, String IMEI,
                                      String itemConditionId, String itemPurchaseDate,
                                      String itemPurchasePrice, String lastName, String manufacturerId,
                                      String modelId, String phoneNumber, Long policyId, String serial
            , JsonArray fileId) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("CarrierId", carrierId);
        jsonObject.addProperty("CostCenterId", costcenterId);
        jsonObject.addProperty("CountryId", countryId);
        jsonObject.addProperty("DivisionId", divisionId);
        jsonObject.addProperty("FirstName", firstName);
        jsonObject.addProperty("IMEI", IMEI);
        jsonObject.addProperty("ItemConditionId", itemConditionId);
        jsonObject.addProperty("ItemPurchaseDate", itemPurchaseDate);
        jsonObject.addProperty("ItemPurchasePrice", itemPurchasePrice);
        jsonObject.addProperty("LastName", lastName);
        jsonObject.addProperty("ManufactureId", manufacturerId);
        jsonObject.addProperty("ModelId", modelId);
        jsonObject.addProperty("PhoneNumber", phoneNumber);
        jsonObject.addProperty("PolicyId", policyId);
        jsonObject.addProperty("Serial", serial);
        jsonObject.add("File", fileId);

        /*JsonObject jsonFileClaim = new JsonObject();
        jsonFileClaim.add("FileClaim", jsonObject);*/
        Log.i("JsonObject", jsonObject.toString());
        return jsonObject;
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            baseView.showErrorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            baseView.showErrorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }

    public void uploadImagePlan(File profileImage) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), profileImage);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo_path", profileImage.getName(), requestFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "photo_path");

        ApiClient.getClient()
                .updatePlanProfileImage(
                        SharePref.getInstance(SecurrantyApp.context).getToken()
                        , body, name).enqueue(new Callback<UploadImageModel>() {
            @Override
            public void onResponse(Call<UploadImageModel> call, Response<UploadImageModel> response) {

                Log.i("Status", "Success");
            }

            @Override
            public void onFailure(Call<UploadImageModel> call, Throwable t) {
                Log.i("Status", "Failure");
            }
        });

    }
}
