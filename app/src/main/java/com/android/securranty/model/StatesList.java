
package com.android.securranty.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatesList {

    @SerializedName("states")
    private List<State> States;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private int StatusCode;

    public List<State> getStates() {
        return States;
    }

    public String getStatus() {
        return Status;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public class State {
        @Expose
        private String CountryCode;

        @Expose
        private String StateCode;

        @Expose
        private String StateName;

        public String getCountryCode() {
            return CountryCode;
        }

        public String getStateCode() {
            return StateCode;
        }

        public String getStateName() {
            return StateName;
        }


    }

}
