package com.android.securranty.presenter.interactor;

import android.util.Log;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.utility.SharePref;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.AccountandCreditcardList;
import com.android.securranty.model.BuyPlan;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.MyAccount;
import com.android.securranty.model.StatesList;
import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.service.APIError;
import com.android.securranty.service.ApiClient;

import com.android.securranty.utility.Utils;
import com.android.securranty.view.activity.account.AccountProfileModel;
import com.android.securranty.view.activity.account.UpdateAccountProfile;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by satheeshr on 21-11-2017.
 */

public class MyAccountPresenterInteractor {
    private BasePresenter basePresenter;

    public MyAccountPresenterInteractor(BasePresenter myAccountPresenter) {
        this.basePresenter = myAccountPresenter;
    }

    public void getStatesList(String countryName, int selectedCountryId) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (!countryName.isEmpty()) {
                ApiClient.getClient().getStates(SharePref.getInstance(SecurrantyApp.context).getToken(),
                        selectedCountryId).enqueue(new Callback<StatesList>() {
                    @Override
                    public void onResponse(Call<StatesList> call, Response<StatesList> response) {
                        if (response.isSuccessful()) {
                            StatesList statesResponse = response.body();
                            if (statesResponse.getStatusCode() == 200) {
                                basePresenter.success(statesResponse);
                            } else {
                                basePresenter.errorMsg(statesResponse.getStatusCode(), "Invalid User");
                            }

                        } else {
                            errorResponse(response.errorBody());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatesList> call, Throwable t) {
                        if (t instanceof HttpException)
                            errorResponse(((HttpException) t).response().errorBody());
                        else
                            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                    }
                });
            } else {
                basePresenter.errorMsg(400, "Please select country");
            }
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void getCountryList() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            ApiClient.getClient().getCountryList(SharePref.getInstance(SecurrantyApp.context).getToken())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<CountryList>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable t) {
                            if (t instanceof HttpException)
                                errorResponse(((HttpException) t).response().errorBody());
                            else
                                basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                        }

                        @Override
                        public void onNext(CountryList countryList) {
                            basePresenter.success(countryList);
                        }
                    });
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void updateMyAccount(String firstName, String lastName, String email,
                                String mobile, String address1, String state,
                                String country, String zip, String salesTax, String salesTaxCountry, String salesTaxPercent,
                                String companyName, String contactEmail, String officePhone, String address2, String city, File profileImage) {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {
            if (profileImage != null) {
                Log.i("ProfileImage","NotNull");
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), profileImage);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("photo_path", profileImage.getName(), requestFile);
                RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "photo_path");

                Observable<APISuccessModel> myaccountObservable = ApiClient.getClient().updateAccountInfo(SharePref.getInstance(SecurrantyApp.context).getToken(),
                        SharePref.getInstance(SecurrantyApp.context).getUserId(), firstName, lastName, email, mobile, address1, state,
                        country, zip, salesTax, salesTaxCountry, salesTaxPercent, companyName, contactEmail, officePhone, address2, city,
                        "Satheesh", "").subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

                Observable<AccountProfileModel> uploadImageObservable = ApiClient.getClient()
                        .updateProfile(SharePref.getInstance(SecurrantyApp.context).getUserId(),
                                SharePref.getInstance(SecurrantyApp.context).getToken()
                                , body, name)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

                Observable<UpdateAccountProfile> combined = Observable.zip(myaccountObservable, uploadImageObservable,
                        new Func2<APISuccessModel, AccountProfileModel, UpdateAccountProfile>() {
                            @Override
                            public UpdateAccountProfile call(APISuccessModel jsonObject, AccountProfileModel jsonElements) {
                                return new UpdateAccountProfile(jsonObject, jsonElements);
                            }
                        });

                combined.subscribe(new Subscriber<UpdateAccountProfile>() {

                    @Override
                    public void onCompleted() {
                        Log.i("Status", "Completed");

                    }

                    @Override
                    public void onError(Throwable t) {
                        if (t instanceof HttpException)
                            errorResponse(((HttpException) t).response().errorBody());
                        else
                            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                    }

                    @Override
                    public void onNext(UpdateAccountProfile updateAccountProfile) {
                        //Log.i("MyAccount", "account:" + accountAndCountryList.getMyAccount().getAccountProfile().getFirstName());
                        //Log.i("MyAccount", "country:" + accountAndCountryList.getCountryList().getCountries().get(0).getName());
                        basePresenter.success(updateAccountProfile);
                    }
                });
            } else {
                ApiClient.getClient().updateAccountInfo(SharePref.getInstance(SecurrantyApp.context).getToken(),
                        SharePref.getInstance(SecurrantyApp.context).getUserId(), firstName, lastName, email, mobile, address1, state,
                        country, zip, salesTax, salesTaxCountry, salesTaxPercent, companyName, contactEmail, officePhone, address2, city, "Satheesh", "").subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<APISuccessModel>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable t) {
                                if (t instanceof HttpException)
                                    errorResponse(((HttpException) t).response().errorBody());
                                else
                                    basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                            }

                            @Override
                            public void onNext(APISuccessModel responseAccountUpdate) {
                                if (responseAccountUpdate.getStatusCode() == 200) {
                                    basePresenter.success(responseAccountUpdate);
                                } else {
                                    basePresenter.errorMsg(responseAccountUpdate.getStatusCode(), "Invalid User");
                                }
                                basePresenter.success(responseAccountUpdate);
                            }
                        });
            }
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }


    public void getMyAccountandCountryList() {
        if (Utils.isNetworkAvailable(SecurrantyApp.context)) {

            Observable<MyAccount> myaccountObservable = ApiClient.getClient()
                    .getAccountInfo(SharePref.getInstance(SecurrantyApp.context).getToken(), SharePref.getInstance(SecurrantyApp.context).getUserId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());

            Observable<CountryList> countrylistObservable = ApiClient.getClient()
                    .getCountryList(SharePref.getInstance(SecurrantyApp.context).getToken())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());

            Observable<AccountandCreditcardList> combined = Observable.zip(myaccountObservable, countrylistObservable,
                    new Func2<MyAccount, CountryList, AccountandCreditcardList>() {
                @Override
                public AccountandCreditcardList call(MyAccount jsonObject, CountryList jsonElements) {
                    return new AccountandCreditcardList(jsonObject, jsonElements);
                }
            });
            combined.subscribe(new Subscriber<AccountandCreditcardList>() {

                @Override
                public void onCompleted() {
                    Log.i("Status", "Completed");

                }

                @Override
                public void onError(Throwable t) {
                    if (t instanceof HttpException)
                        errorResponse(((HttpException) t).response().errorBody());
                    else
                        basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
                }

                @Override
                public void onNext(AccountandCreditcardList accountAndCountryList) {
                    Log.i("MyAccount", "account:" + accountAndCountryList.getMyAccount().getAccountProfile().getFirstName());
                    Log.i("MyAccount", "country:" + accountAndCountryList.getCountryList().getCountries().get(0).getName());
                    basePresenter.success(accountAndCountryList);
                }
            });
        } else {
            basePresenter.errorMsg(200, SecurrantyApp.context.getResources().getString(R.string.msg_no_internet));
        }
    }

    public void buyPlan(String email, String address1, String address2, String country, String state,
                        String city, String zip, String creditcardno, String expMonth, String expYear,
                        String cvv, String planId,String paymentProfileId) {
        ApiClient.getClient().buyPlan(email, address1, address2, country, state, city,
                zip, creditcardno, expMonth, expYear, cvv, planId,paymentProfileId).enqueue(new Callback<BuyPlan>() {
            @Override
            public void onResponse(Call<BuyPlan> call, Response<BuyPlan> response) {
                if (response.isSuccessful()) {
                    BuyPlan myaccountResponse = response.body();
                    if (myaccountResponse.getStatusCode() == 200) {
                        basePresenter.success(myaccountResponse);
                    } else {
                        basePresenter.errorMsg(myaccountResponse.getStatusCode(), "Invalid User");
                    }

                } else {
                    errorResponse(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<BuyPlan> call, Throwable t) {
                if (t instanceof HttpException)
                    errorResponse(((HttpException) t).response().errorBody());
                else
                    basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
            }
        });
    }

    private void errorResponse(ResponseBody responseBody) {
        try {
            Gson gson = new Gson();
            APIError message = gson.fromJson(responseBody.charStream(), APIError.class);
            int statusCode = message.getStatusCode();
            basePresenter.errorMsg(statusCode, message.getMessage());
        } catch (IllegalStateException | JsonSyntaxException exception) {
            basePresenter.errorMsg(201, SecurrantyApp.context.getString(R.string.err_back_end));
        }
    }
}
