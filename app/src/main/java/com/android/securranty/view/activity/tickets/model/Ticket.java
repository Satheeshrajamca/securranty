
package com.android.securranty.view.activity.tickets.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class Ticket {

    @SerializedName("Active")
    private Boolean active;
    @SerializedName("CreatedBy")
    private String createdBy;
    @SerializedName("CreatedById")
    private String createdById;
    @SerializedName("CreatedDate")
    private String createdDate;
    @SerializedName("DateClosed")
    private Object dateClosed;
    @SerializedName("Id")
    private int id;
    @SerializedName("LastModBy")
    private Object lastModBy;
    @SerializedName("LastModById")
    private String lastModById;
    @SerializedName("LastModDate")
    private String lastModDate;
    @SerializedName("Priority")
    private String priority;
    @SerializedName("Section")
    private String section;
    @SerializedName("SectionId")
    private Long sectionId;
    @SerializedName("Status")
    private String status;
    @SerializedName("StatusId")
    private Long statusId;
    @SerializedName("SupportTicketMessage")
    private List<SupportTicketMessage> supportTicketMessage;
    @SerializedName("Title")
    private String title;
    @SerializedName("Type")
    private String type;
    @SerializedName("TypeId")
    private int typeId;
    @SerializedName("UserId")
    private String userId;

    public Boolean getActive() {
        return active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreatedById() {
        return createdById;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public Object getDateClosed() {
        return dateClosed;
    }

    public int getId() {
        return id;
    }

    public Object getLastModBy() {
        return lastModBy;
    }

    public String getLastModById() {
        return lastModById;
    }

    public String getLastModDate() {
        return lastModDate;
    }

    public String getPriority() {
        return priority;
    }

    public String getSection() {
        return section;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public String getStatus() {
        return status;
    }

    public Long getStatusId() {
        return statusId;
    }

    public List<SupportTicketMessage> getSupportTicketMessage() {
        return supportTicketMessage;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public int getTypeId() {
        return typeId;
    }

    public String getUserId() {
        return userId;
    }

    public static class Builder {

        private Boolean active;
        private String createdBy;
        private String createdById;
        private String createdDate;
        private Object dateClosed;
        private int id;
        private Object lastModBy;
        private String lastModById;
        private String lastModDate;
        private String priority;
        private String section;
        private Long sectionId;
        private String status;
        private Long statusId;
        private List<SupportTicketMessage> supportTicketMessage;
        private String title;
        private String type;
        private int typeId;
        private String userId;

        public Ticket.Builder withActive(Boolean active) {
            this.active = active;
            return this;
        }

        public Ticket.Builder withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Ticket.Builder withCreatedById(String createdById) {
            this.createdById = createdById;
            return this;
        }

        public Ticket.Builder withCreatedDate(String createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Ticket.Builder withDateClosed(Object dateClosed) {
            this.dateClosed = dateClosed;
            return this;
        }

        public Ticket.Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Ticket.Builder withLastModBy(Object lastModBy) {
            this.lastModBy = lastModBy;
            return this;
        }

        public Ticket.Builder withLastModById(String lastModById) {
            this.lastModById = lastModById;
            return this;
        }

        public Ticket.Builder withLastModDate(String lastModDate) {
            this.lastModDate = lastModDate;
            return this;
        }

        public Ticket.Builder withPriority(String priority) {
            this.priority = priority;
            return this;
        }

        public Ticket.Builder withSection(String section) {
            this.section = section;
            return this;
        }

        public Ticket.Builder withSectionId(Long sectionId) {
            this.sectionId = sectionId;
            return this;
        }

        public Ticket.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public Ticket.Builder withStatusId(Long statusId) {
            this.statusId = statusId;
            return this;
        }

        public Ticket.Builder withSupportTicketMessage(List<SupportTicketMessage> supportTicketMessage) {
            this.supportTicketMessage = supportTicketMessage;
            return this;
        }

        public Ticket.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Ticket.Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Ticket.Builder withTypeId(int typeId) {
            this.typeId = typeId;
            return this;
        }

        public Ticket.Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Ticket build() {
            Ticket ticket = new Ticket();
            ticket.active = active;
            ticket.createdBy = createdBy;
            ticket.createdById = createdById;
            ticket.createdDate = createdDate;
            ticket.dateClosed = dateClosed;
            ticket.id = id;
            ticket.lastModBy = lastModBy;
            ticket.lastModById = lastModById;
            ticket.lastModDate = lastModDate;
            ticket.priority = priority;
            ticket.section = section;
            ticket.sectionId = sectionId;
            ticket.status = status;
            ticket.statusId = statusId;
            ticket.supportTicketMessage = supportTicketMessage;
            ticket.title = title;
            ticket.type = type;
            ticket.typeId = typeId;
            ticket.userId = userId;
            return ticket;
        }

    }

}
