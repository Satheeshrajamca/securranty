package com.android.securranty.model;

/**
 * Created by satheeshr on 20-11-2017.
 */

public class AdapterMenu {
    int menuIcon;
    String menuName;

    public AdapterMenu(int menuIcon, String menuName) {
        this.menuIcon = menuIcon;
        this.menuName = menuName;
    }

    public int getMenuIcon() {
        return menuIcon;
    }

    public String getMenuName() {
        return menuName;
    }
}
