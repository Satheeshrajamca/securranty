package com.android.securranty.view.activity.claims;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.adapter.AdapterFileClaim;
import com.android.securranty.utility.CustomViewPager;
import com.android.securranty.view.fragment.DashboardFragment;
import com.android.securranty.view.fragment.fileclaim.ClaimTypeFragment;
import com.android.securranty.view.fragment.fileclaim.ConfirmPolicyFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshraja on 4/8/18.
 */

public class FileClaimsActivity extends AppCompatActivity implements ConfirmPolicyFragment.LaunchClaimType {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.imageBackButton)
    ImageView imageBackButton;

    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_claim);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        launchFragment(new ConfirmPolicyFragment(), "Confirm Policy For Claim");
    }

    @OnClick(R.id.imageBackButton)
    public void onClick(View view)
    {
        finish();
    }


    private void launchFragment(final Fragment fragment, final String title) {
        toolbar_title.setText("File a Claim");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerFileClaim, fragment);
        if (!(fragment instanceof ConfirmPolicyFragment)) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void triggerClaim(Fragment fragment, String title) {
        launchFragment(fragment, title);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
