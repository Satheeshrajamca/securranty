package com.android.securranty.adapter;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.securranty.R;
import com.android.securranty.model.Child;
import com.android.securranty.model.SectionHeader;
import com.android.securranty.view.activity.plans.active_plan.ActivePlanDetailsActvity;
import com.android.securranty.view.activity.plans.active_plan.ActivePlanUpdateActvity;
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by satheeshraja on 4/1/18.
 */

public class AdapterSectionRecycler extends SectionRecyclerViewAdapter<SectionHeader, Child, AdapterSectionRecycler.SectionViewHolder,
        AdapterSectionRecycler.ChildViewHolder> {

    private ActivePlanDetailsActvity context;
    private int selectedActivePlanPosition;
    List<SectionHeader> sectionItemList;

    public interface ZoomImageView
    {
        void zoomImageFromThumb(ImageView thumbView,String imageUrl);
    }


    public AdapterSectionRecycler(ActivePlanDetailsActvity context, List<SectionHeader> sectionItemList, int selectedActivePlanPosition) {
        super(context, sectionItemList);
        this.context = context;
        this.selectedActivePlanPosition = selectedActivePlanPosition;
        this.sectionItemList = sectionItemList;
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_active_plan_header, sectionViewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_active_plan_child, childViewGroup, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, SectionHeader section) {
        /*if (sectionPosition == 0) {
            sectionViewHolder.textEditPlan.setVisibility(View.VISIBLE);
        } else {
            sectionViewHolder.textEditPlan.setVisibility(View.GONE);
        }*/
        sectionViewHolder.sectionName.setText(section.getSectionText());
    }

    @Override
    public void onBindChildViewHolder(final ChildViewHolder childViewHolder, int sectionPosition, int childPosition, final Child child) {
        if (child.getKey().equals("finalChild")) {
            childViewHolder.viewChild.setVisibility(View.GONE);
        } else {
            Log.i("Proof of Purchase", child.getKey());
            if (sectionItemList.get(sectionPosition).getSectionText().equals("Proof of Purchase")) {
                childViewHolder.activeplanValue.setVisibility(View.GONE);
                childViewHolder.imageProofOfImage.setVisibility(View.VISIBLE);
                childViewHolder.viewChild.setVisibility(View.GONE);
                childViewHolder.activeplanKey.setText(child.getKey());
                Picasso.with(context).load(child.getValue()).into(childViewHolder.imageProofOfImage);

                childViewHolder.imageProofOfImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.zoomImageFromThumb(childViewHolder.imageProofOfImage, child.getValue());

                    }
                });
            } else {
                childViewHolder.activeplanValue.setVisibility(View.VISIBLE);
                childViewHolder.imageProofOfImage.setVisibility(View.GONE);
                childViewHolder.viewChild.setVisibility(View.VISIBLE);
                childViewHolder.activeplanKey.setText(child.getKey());
                if (child.getValue() != "null") {
                    childViewHolder.activeplanValue.setText(child.getValue());
                }
            }
        }
    }


    public class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView sectionName, textEditPlan;

        public SectionViewHolder(View itemView) {
            super(itemView);
            sectionName = (TextView) itemView.findViewById(R.id.section);
            textEditPlan = (TextView) itemView.findViewById(R.id.textEditPlan);
            /*textEditPlan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ActivePlanUpdateActvity.class)
                            .putExtra("PLAN_POSITION", selectedActivePlanPosition));
                }
            });*/
        }
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {

        TextView activeplanValue, activeplanKey;
        View viewChild;
        ImageView imageProofOfImage;

        public ChildViewHolder(View itemView) {
            super(itemView);
            activeplanKey = (TextView) itemView.findViewById(R.id.activeplanKey);
            activeplanValue = (TextView) itemView.findViewById(R.id.activeplanValue);
            viewChild = (View) itemView.findViewById(R.id.viewChild);
            imageProofOfImage = (ImageView) itemView.findViewById(R.id.imageProofOfImage);
        }
    }

}
