
package com.android.securranty.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FAQModel {

    @SerializedName("faqs")
    private List<Faq> Faqs;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<Faq> getFaqs() {
        return Faqs;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public class Faq {

        @Expose
        private String CategoryName;
        @Expose
        private List<QuestionList> QuestionList;

        public String getCategoryName() {
            return CategoryName;
        }

        public List<QuestionList> getQuestionList() {
            return QuestionList;
        }
    }

    public class QuestionList {

        @Expose
        private String Answer;
        @Expose
        private String Question;

        public String getAnswer() {
            return Answer;
        }

        public String getQuestion() {
            return Question;
        }

    }

}
