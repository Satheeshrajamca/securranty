package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeviceModelsItem{

	@SerializedName("TypeId")
	private int typeId;

	@SerializedName("ModelName")
	private String modelName;

	@SerializedName("RetailPrice")
	private Object retailPrice;

	@SerializedName("UserDeviceHistory")
	private List<Object> userDeviceHistory;

	@SerializedName("PurchaseInvoiceItems")
	private List<Object> purchaseInvoiceItems;

	@SerializedName("MPN")
	private Object mPN;

	@SerializedName("MfgSupportLink")
	private Object mfgSupportLink;

	@SerializedName("GazellePrice")
	private Object gazellePrice;

	@SerializedName("EbayPrice")
	private Object ebayPrice;

	@SerializedName("UserDevice")
	private List<Object> userDevice;

	@SerializedName("SupplierPrice")
	private Object supplierPrice;

	@SerializedName("Active")
	private boolean active;

	@SerializedName("ManufactureId")
	private int manufactureId;

	@SerializedName("DeviceTypes")
	private Object deviceTypes;

	@SerializedName("RepairParts")
	private List<Object> repairParts;

	@SerializedName("Id")
	private int id;

	@SerializedName("AmazonPrice")
	private Object amazonPrice;

	@SerializedName("DeviceManufacturers")
	private Object deviceManufacturers;

	@SerializedName("PurchaseRequest")
	private List<Object> purchaseRequest;

	@SerializedName("RefurbishedPrice")
	private Object refurbishedPrice;

	public int getTypeId(){
		return typeId;
	}

	public String getModelName(){
		return modelName;
	}

	public Object getRetailPrice(){
		return retailPrice;
	}

	public List<Object> getUserDeviceHistory(){
		return userDeviceHistory;
	}

	public List<Object> getPurchaseInvoiceItems(){
		return purchaseInvoiceItems;
	}

	public Object getMPN(){
		return mPN;
	}

	public Object getMfgSupportLink(){
		return mfgSupportLink;
	}

	public Object getGazellePrice(){
		return gazellePrice;
	}

	public Object getEbayPrice(){
		return ebayPrice;
	}

	public List<Object> getUserDevice(){
		return userDevice;
	}

	public Object getSupplierPrice(){
		return supplierPrice;
	}

	public boolean isActive(){
		return active;
	}

	public int getManufactureId(){
		return manufactureId;
	}

	public Object getDeviceTypes(){
		return deviceTypes;
	}

	public List<Object> getRepairParts(){
		return repairParts;
	}

	public int getId(){
		return id;
	}

	public Object getAmazonPrice(){
		return amazonPrice;
	}

	public Object getDeviceManufacturers(){
		return deviceManufacturers;
	}

	public List<Object> getPurchaseRequest(){
		return purchaseRequest;
	}

	public Object getRefurbishedPrice(){
		return refurbishedPrice;
	}
}
