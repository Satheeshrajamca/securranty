
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CoverageType {

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static class Builder {

        private int id;
        private String name;

        public CoverageType.Builder withId(int id) {
            this.id = id;
            return this;
        }

        public CoverageType.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public CoverageType build() {
            CoverageType coverageType = new CoverageType();
            coverageType.id = id;
            coverageType.name = name;
            return coverageType;
        }

    }

}
