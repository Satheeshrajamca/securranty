package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

public class BuyPlan {

    @SerializedName("policyId")
    private int policyId;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("status")
    private String status;

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
