package com.android.securranty.presenter.implementor;

import com.android.securranty.presenter.BasePresenter;
import com.android.securranty.presenter.interactor.ChangePassPresenterInter;
import com.android.securranty.view.BaseView;

/**
 * Created by satheeshraja on 11/30/17.
 */

public class ChangePassPresenterImp implements BasePresenter {
    BaseView baseView;
    ChangePassPresenterInter changePassPresenterInter;

    public ChangePassPresenterImp(BaseView baseView) {
        this.baseView = baseView;
        changePassPresenterInter = new ChangePassPresenterInter(this);
    }

    public void validateCredential(String currentPassword, String newPassword, String repeatPassword) {
        if (baseView != null) {
            baseView.showProgress();
            baseView.hideKeyboard();

        }
        changePassPresenterInter.changePassword(currentPassword, newPassword, repeatPassword);
    }

    @Override
    public void success(Object obj) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.success(obj);
        }
    }

    @Override
    public void errorMsg(int statusCode, String msg) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.showErrorMsg(statusCode, msg);
        }

    }

    @Override
    public void onDestroy() {
        if (baseView != null) {
            baseView = null;
        }
    }
}
