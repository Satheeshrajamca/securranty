package com.android.securranty.model;

/**
 * Created by satheeshr on 28-12-2017.
 */

public class AccountandCreditcardList {

    public AccountandCreditcardList(MyAccount myAccount, CountryList countryList) {
        this.countryList = countryList;
        this.myAccount = myAccount;
    }

    public CountryList countryList;
    public MyAccount myAccount;

    public CountryList getCountryList() {
        return countryList;
    }

    public MyAccount getMyAccount() {
        return myAccount;
    }
}

