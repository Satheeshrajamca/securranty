package com.android.securranty.view.activity.tickets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.TicketListAdapter;
import com.android.securranty.presenter.implementor.TicketListPresenterImpl;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.RecyclerItemClickListener;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.activity.tickets.model.Ticket;
import com.android.securranty.view.activity.tickets.model.TicketList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class TicketListActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerTicketHistory;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.searchView)
    EditText searchView;

    private ActionBar actionBar;
    private TicketListAdapter ticketListAdapter;
    TicketListPresenterImpl ticketListPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        ticketListPresenter = new TicketListPresenterImpl(this);
        ticketListPresenter.initTicketList();
        search();
        recyclerviewItemClick();
    }

    private void recyclerviewItemClick() {
        recyclerTicketHistory.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        startActivityForResult(new Intent(TicketListActivity.this,
                                        TicketDetailsActivity.class).putExtra("TICKET_POSITION",position)
                                , 401);
                    }
                })
        );
    }

    @OnClick({R.id.imageAdd, R.id.imageBackButton})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageAdd:
                startActivityForResult(new Intent(this, CreateTicketActivity.class), 100);
                break;
            case R.id.imageBackButton:
                finish();
                break;
        }
    }

    private void search() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ticketListAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    public static List<Ticket> ticketsItem;

    @Override
    public void success(Object obj) {
        ticketsItem = TicketList.class.cast(obj).getTickets();
        if (ticketsItem.size() > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this);
            recyclerTicketHistory.addItemDecoration(dividerItemDecoration);

            recyclerTicketHistory.setLayoutManager(linearLayoutManager);
            ticketListAdapter = new TicketListAdapter();
            ticketListAdapter.setData(ticketsItem);
            recyclerTicketHistory.setAdapter(ticketListAdapter);
        } else {
            new MaterialDialog.Builder(this).onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    //finish();
                    startActivityForResult(new Intent(TicketListActivity.this, CreateTicketActivity.class), 100);
                }
            }).onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    finish();
                }
            })
                    .canceledOnTouchOutside(false)
                    .title("Tickets")
                    .content("No Active Tickets Currently.")
                    .positiveText("Create Ticket")
                    .negativeText("Cancel")
                    .show();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                ticketListPresenter.initTicketList();
            } else if (requestCode == 401) {
                ticketListPresenter.initTicketList();
            }
        } else {
            //finish();
        }
    }
}
