package com.android.securranty.view.activity.plans.buy_plan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.BillingModel;
import com.android.securranty.model.BuyPlan;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.model.StatesList;
import com.android.securranty.presenter.implementor.CreditcardListPresenterImpl;
import com.android.securranty.presenter.implementor.MyAccountPresenterImpl;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.AccountView;
import com.android.securranty.view.activity.creditcard.CreditCardListActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.plans.buy_plan.AllPlansActivity.listAdPage;

/**
 * Created by satheeshr on 27-11-2017.
 */

public class PurchasePlanActivity extends AppCompatActivity implements AccountView {

    @BindView(R.id.labelEmail)
    TextView labelEmail;
    @BindView(R.id.labelAddress1)
    TextView labelAddress1;
    @BindView(R.id.labelAddress2)
    TextView labelAddress2;
    @BindView(R.id.labelCity)
    TextView labelBillingCity;
    @BindView(R.id.labelState)
    TextView labelBillingState;
    @BindView(R.id.labelZip)
    TextView labelZip;
    @BindView(R.id.labelCountry)
    TextView labelCountry;
    @BindView(R.id.textOrderSummary)
    TextView textOrderSummary;
    @BindView(R.id.labelAccountInformation)
    TextView labelAccountInformation;

    @BindView(R.id.textWarrantyName)
    TextView textWarrantyName;
    @BindView(R.id.textWarrantyPrice)
    TextView textWarrantyPrice;
    @BindView(R.id.textSalesTax)
    TextView textSalesTax;
    @BindView(R.id.textSalesTaxAmt)
    TextView textSalesTaxAmt;
    @BindView(R.id.textTotal)
    TextView textTotal;
    @BindView(R.id.totalAmount)
    TextView totalAmount;
    @BindView(R.id.labelBillingInformation)
    TextView labelBillingInformation;

    @BindView(R.id.editEmail)
    EditText editBillingEmail;
    @BindView(R.id.editAddress1)
    EditText editAddress1;
    @BindView(R.id.editAddress2)
    EditText editAddress2;
    @BindView(R.id.editCity)
    EditText editBillingCity;
    @BindView(R.id.editState)
    EditText editBillingState;
    @BindView(R.id.editZip)
    EditText editZip;
    @BindView(R.id.editCountry)
    EditText editCountry;
    @BindView(R.id.editCreditCardNo)
    EditText editCreditCardNo;
    @BindView(R.id.editExpiryMonth)
    MaterialBetterSpinner editExpiryMonth;
    @BindView(R.id.editExpiryYear)
    MaterialBetterSpinner editExpiryYear;
    @BindView(R.id.editCVV)
    EditText editCVV;
    @BindView(R.id.purchasePlan)
    Button purchasePlan;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageSave)
    ImageView imageSave;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private MyAccountPresenterImpl myAccountPresenter;
    private int selectedCountryId;
    private ActionBar actionBar;
    private int countryId;
    private List<SearchListItem> listCountry, stateList;
    private String planId;
    private List<String> month = new ArrayList<>();
    private List<String> year = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_plan);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        myAccountPresenter = new MyAccountPresenterImpl(this);
        myAccountPresenter.getBillingDetails();
        setTypeFace();
        planId = String.valueOf(getIntent().getLongExtra("PLAN_ID", 0));
        textWarrantyPrice.setText("$" + String.format("%.2f", getIntent().getDoubleExtra("WARANTY_PRICE", 0.00)));
        totalAmount.setText("$" + String.format("%.2f", getIntent().getDoubleExtra("WARANTY_PRICE", 0.00)));
        textWarrantyName.setText(getIntent().getStringExtra("COVERAGE_NAME"));
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");

        labelEmail.setTypeface(openSansRegular);
        labelAddress1.setTypeface(openSansRegular);
        labelAddress1.setTypeface(openSansRegular);
        labelAddress2.setTypeface(openSansRegular);
        labelAddress2.setTypeface(openSansRegular);
        labelBillingCity.setTypeface(openSansRegular);
        labelBillingState.setTypeface(openSansRegular);
        labelZip.setTypeface(openSansRegular);
        labelCountry.setTypeface(openSansRegular);

        editBillingEmail.setTypeface(openSansSemiBold);
        editAddress1.setTypeface(openSansSemiBold);
        editAddress2.setTypeface(openSansSemiBold);
        editBillingCity.setTypeface(openSansSemiBold);
        editBillingState.setTypeface(openSansSemiBold);
        editZip.setTypeface(openSansSemiBold);
        editCountry.setTypeface(openSansSemiBold);
        textOrderSummary.setTypeface(openSansSemiBold);
        labelAccountInformation.setTypeface(openSansSemiBold);
        textWarrantyName.setTypeface(openSansSemiBold);
        textWarrantyPrice.setTypeface(openSansSemiBold);
        textSalesTax.setTypeface(openSansSemiBold);
        textSalesTaxAmt.setTypeface(openSansSemiBold);
        textTotal.setTypeface(openSansSemiBold);
        totalAmount.setTypeface(openSansSemiBold);
        labelBillingInformation.setTypeface(openSansSemiBold);

        for (int i = 1; i <= 12; i++) {
            if (i < 10) {
                month.add("0" + i);
            } else {
                month.add("" + i);
            }
        }
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 20; i++) {
            year.add("" + (currentYear + i));
        }
        editExpiryMonth.setUnderlineColor(R.color.black_color);
        editExpiryYear.setUnderlineColor(R.color.black_color);
        ArrayAdapter<String> adapterMonth = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, month);
        editExpiryMonth.setAdapter(adapterMonth);
        ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, year);
        editExpiryYear.setAdapter(adapterYear);
    }

    @OnClick({R.id.imageSave, R.id.imageBackButton, R.id.editCountry, R.id.editState, R.id.purchasePlan, R.id.editCreditCardNo})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.purchasePlan:
                myAccountPresenter.initBuyPlan(
                        editBillingEmail.getText().toString().trim(),
                        editAddress1.getText().toString().trim(),
                        editAddress2.getText().toString().trim(),
                        editCountry.getText().toString().trim(),
                        editBillingState.getText().toString().trim(),
                        editBillingCity.getText().toString().trim(),
                        editZip.getText().toString().trim(),
                        "",
                        editExpiryMonth.getText().toString().trim(),
                        editExpiryYear.getText().toString().trim(),
                        editCVV.getText().toString().trim(),
                        planId,paymentProfileId
                );
                break;
            case R.id.imageBackButton:
                finish();
                break;
            case R.id.editCountry:
                if (listCountry == null) {
                    myAccountPresenter.initCountryList();
                } else {
                    displayCountry();
                }
                break;
            case R.id.editState:
                if (selectedCountryId != countryId || stateList == null) {
                    myAccountPresenter.initStatesList(editCountry.getText().toString().trim(), countryId);
                } else
                    displayState();
                break;

            case R.id.editCreditCardNo:
                startActivityForResult(new Intent(this, CreditCardListActivity.class).putExtra("FROM", "PURCHASE_PLAN"),
                        402);
                break;
        }
    }

    private void displayState() {
        final SearchableDialog searchableDialog = new SearchableDialog(PurchasePlanActivity.this, stateList, "Select State");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editBillingState.setText(searchListItem.getTitle());
            }
        });
    }

    @Override
    public void showEditView(int editorVisibility, int textVisibility) {
        editBillingEmail.setVisibility(editorVisibility);
        editAddress1.setVisibility(editorVisibility);
        editAddress2.setVisibility(editorVisibility);
        editBillingCity.setVisibility(editorVisibility);
        editBillingState.setVisibility(editorVisibility);
        editZip.setVisibility(editorVisibility);
        editCountry.setVisibility(editorVisibility);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    private CreditcardListPresenterImpl creditcardListPresenterImpl;
    private String paymentProfileId;

    @Override
    public void success(final Object obj) {
        if (obj instanceof BillingModel) {
            BillingModel.BillingProfile billingProfile = BillingModel.class.cast(obj).getBillingProfile();
            editBillingEmail.setText(billingProfile.getBillingEmail());
            editAddress1.setText(billingProfile.getBillingAddress1());
            editAddress2.setText(billingProfile.getBillingAddress2());
            editBillingCity.setText(billingProfile.getBillingCity());
            editBillingState.setText(billingProfile.getBillingState());
            editZip.setText(billingProfile.getBillingzip());
            editCountry.setText(billingProfile.getBillingCountry());
            setCountryId(billingProfile.getBillingCountry());
            creditcardListPresenterImpl = new CreditcardListPresenterImpl(this);
            creditcardListPresenterImpl.initCreditcardList();

        } else if (obj instanceof CreditCardList) {
            Log.i("CreditCardList", "CreditCardList");
            CreditCardList creditCardList = CreditCardList.class.cast(obj);
            for (CreditCardList.CreditCardListItem creditCardListItem : creditCardList.getCreditCardList()) {
                if (creditCardListItem.getPrimaryCard().equals("Yes")) {
                    paymentProfileId = String.valueOf(creditCardListItem.getId());
                    editCreditCardNo.setText("XXXX XXXX XXXX " + creditCardListItem.getLastFour());
                }
            }

        } else if (obj instanceof CountryList) {
            listCountry = new ArrayList<>();
            for (CountryList.CountriesItem countryList : CountryList.class.cast(obj).getCountries()) {
                listCountry.add(new SearchListItem(countryList.getId(), countryList.getName()));
            }
            displayCountry();
        } else if (obj instanceof StatesList) {
            selectedCountryId = countryId;
            stateList = new ArrayList<>();
            for (StatesList.State states : StatesList.class.cast(obj).getStates()) {
                stateList.add(new SearchListItem(0, states.getStateName()));
            }
            displayState();
        } else if (obj instanceof BuyPlan) {
            new iOSDialogBuilder(this)
                    .setTitle("Success")
                    .setSubtitle("Thank you for your purchase. Your policy number is: " + BuyPlan.class.cast(obj).getPolicyId())
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Ok", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            Intent intent = getIntent();
                            setResult(RESULT_OK, intent);
                            finish();
                            dialog.dismiss();
                        }
                    }).build().show();
        }
    }

    private void displayCountry() {
        final SearchableDialog searchableDialog = new SearchableDialog(PurchasePlanActivity.this, listCountry, "Select Country");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editCountry.setText(searchListItem.getTitle());
                countryId = searchListItem.getId();
                if (selectedCountryId != countryId) {
                    editBillingState.setText("");
                }
            }
        });
    }

    private void setCountryId(String billingCountry) {
        if (billingCountry != null && !billingCountry.isEmpty()) {
            switch (billingCountry) {
                case "United States":
                    countryId = 2;
                    selectedCountryId = 2;
                    break;
                case "Canada":
                    selectedCountryId = 3;
                    countryId = 3;
                    break;
                default:
                    countryId = 0;
            }
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void setTag(String account_state) {
    }

    @Override
    public void changeIcon(@DrawableRes int save_icon) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myAccountPresenter.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if (requestCode == 402) {
                    paymentProfileId = data.getStringExtra("PAYMENT_PROFILE_ID");
                    editCreditCardNo.setText("XXXX XXXX XXXX " + data.getStringExtra("CARD_NO"));
            }
            else if(requestCode==401)
            {
                myAccountPresenter.getBillingDetails();
            }
        }
        else
        {
            finish();
        }
    }
}
