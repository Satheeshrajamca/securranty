
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdPage {

    @SerializedName("Active")
    private Boolean active;
    @SerializedName("AdPagePlans")
    private List<AdPagePlan> adPagePlans;
    @SerializedName("Id")
    private Long id;
    @SerializedName("ImageUrl")
    private String imageUrl;
    @SerializedName("LayoutType")
    private String layoutType;
    @SerializedName("PageName")
    private String pageName;
    @SerializedName("PageUrl")
    private String pageUrl;
    @SerializedName("RenewalText")
    private String renewalText;
    @SerializedName("SEODescription")
    private String sEODescription;
    @SerializedName("SEODetailsImageALT")
    private String sEODetailsImageALT;
    @SerializedName("SEOKeyword")
    private String sEOKeyword;
    @SerializedName("SEOListImageALT")
    private String sEOListImageALT;
    @SerializedName("SEOTitleTag")
    private String sEOTitleTag;
    @SerializedName("Summary")
    private String summary;
    @SerializedName("Title")
    private String title;
    @SerializedName("Visible")
    private Boolean visible;

    public Boolean getActive() {
        return active;
    }

    public List<AdPagePlan> getAdPagePlans() {
        return adPagePlans;
    }

    public Long getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLayoutType() {
        return layoutType;
    }

    public String getPageName() {
        return pageName;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public String getRenewalText() {
        return renewalText;
    }

    public String getSEODescription() {
        return sEODescription;
    }

    public String getSEODetailsImageALT() {
        return sEODetailsImageALT;
    }

    public String getSEOKeyword() {
        return sEOKeyword;
    }

    public String getSEOListImageALT() {
        return sEOListImageALT;
    }

    public String getSEOTitleTag() {
        return sEOTitleTag;
    }

    public String getSummary() {
        return summary;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getVisible() {
        return visible;
    }

    public static class Builder {

        private Boolean active;
        private List<AdPagePlan> adPagePlans;
        private Long id;
        private String imageUrl;
        private String layoutType;
        private String pageName;
        private String pageUrl;
        private String renewalText;
        private String sEODescription;
        private String sEODetailsImageALT;
        private String sEOKeyword;
        private String sEOListImageALT;
        private String sEOTitleTag;
        private String summary;
        private String title;
        private Boolean visible;

        public AdPage.Builder withActive(Boolean active) {
            this.active = active;
            return this;
        }

        public AdPage.Builder withAdPagePlans(List<AdPagePlan> adPagePlans) {
            this.adPagePlans = adPagePlans;
            return this;
        }

        public AdPage.Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public AdPage.Builder withImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public AdPage.Builder withLayoutType(String layoutType) {
            this.layoutType = layoutType;
            return this;
        }

        public AdPage.Builder withPageName(String pageName) {
            this.pageName = pageName;
            return this;
        }

        public AdPage.Builder withPageUrl(String pageUrl) {
            this.pageUrl = pageUrl;
            return this;
        }

        public AdPage.Builder withRenewalText(String renewalText) {
            this.renewalText = renewalText;
            return this;
        }

        public AdPage.Builder withSEODescription(String sEODescription) {
            this.sEODescription = sEODescription;
            return this;
        }

        public AdPage.Builder withSEODetailsImageALT(String sEODetailsImageALT) {
            this.sEODetailsImageALT = sEODetailsImageALT;
            return this;
        }

        public AdPage.Builder withSEOKeyword(String sEOKeyword) {
            this.sEOKeyword = sEOKeyword;
            return this;
        }

        public AdPage.Builder withSEOListImageALT(String sEOListImageALT) {
            this.sEOListImageALT = sEOListImageALT;
            return this;
        }

        public AdPage.Builder withSEOTitleTag(String sEOTitleTag) {
            this.sEOTitleTag = sEOTitleTag;
            return this;
        }

        public AdPage.Builder withSummary(String summary) {
            this.summary = summary;
            return this;
        }

        public AdPage.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public AdPage.Builder withVisible(Boolean visible) {
            this.visible = visible;
            return this;
        }

        public AdPage build() {
            AdPage adPage = new AdPage();
            adPage.active = active;
            adPage.adPagePlans = adPagePlans;
            adPage.id = id;
            adPage.imageUrl = imageUrl;
            adPage.layoutType = layoutType;
            adPage.pageName = pageName;
            adPage.pageUrl = pageUrl;
            adPage.renewalText = renewalText;
            adPage.sEODescription = sEODescription;
            adPage.sEODetailsImageALT = sEODetailsImageALT;
            adPage.sEOKeyword = sEOKeyword;
            adPage.sEOListImageALT = sEOListImageALT;
            adPage.sEOTitleTag = sEOTitleTag;
            adPage.summary = summary;
            adPage.title = title;
            adPage.visible = visible;
            return adPage;
        }

    }

}
