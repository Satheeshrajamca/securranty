package com.android.securranty.view.activity.faq;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.android.securranty.R;
import com.android.securranty.adapter.FAQAdapter;
import com.android.securranty.model.FAQModel;
import com.android.securranty.presenter.implementor.FAQPresenterImpl;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satheeshr on 28-11-2017.
 */

public class FAQActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private ActionBar actionBar;
    private FAQAdapter faqAdapter;
    FAQPresenterImpl faqPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        faqPresenter = new FAQPresenterImpl(this);
        faqPresenter.intFAQ();
        groupExpandListener();
    }

    private void groupExpandListener() {
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                HashMap<Integer, HashMap<String, String>> hashMap1 = hashmapChildIdGroupIdWithAnswers.get(groupPosition);
                HashMap<String, String> hashMap2 = hashMap1.get(childPosition);
                for (Map.Entry<String, String> entry : hashMap2.entrySet()) {
                    startActivity(new Intent(FAQActivity.this, FAQDetailsActivity.class)
                            .putExtra("QUESTION", entry.getKey()).putExtra("ANSWER", entry.getValue()));
                }
                return false;
            }
        });
    }

    @OnClick({R.id.imageBackButton})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.imageBackButton:
                finish();
                break;
        }
    }


    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            finish();
            startActivity(new Intent(this, SignInActivity.class));
        }
    }

    List<String> listGroupTitle, listChildTitle;
    HashMap<String, List<String>> hashMapChild;
    HashMap<Integer, HashMap<String, String>> hashmapChildPosWithQuesAns;
    HashMap<String, String> hashMapQuesitonsAnswers;
    HashMap<Integer, HashMap<Integer, HashMap<String, String>>> hashmapChildIdGroupIdWithAnswers;

    @Override

    public void success(Object obj) {
        int groupPosition = 0;
        listGroupTitle = new ArrayList<>();
        hashMapChild = new HashMap<>();
        hashmapChildIdGroupIdWithAnswers = new HashMap<>();
        for (FAQModel.Faq faqModel1 : FAQModel.class.cast(obj).getFaqs()) {
            int childPosition = 0;
            listGroupTitle.add(faqModel1.getCategoryName());
            listChildTitle = new ArrayList<>();
            hashmapChildPosWithQuesAns = new HashMap<>();
            for (FAQModel.QuestionList questionList : faqModel1.getQuestionList()) {
                listChildTitle.add(questionList.getQuestion());
                hashMapQuesitonsAnswers = new HashMap<>();
                hashMapQuesitonsAnswers.put(questionList.getQuestion(), questionList.getAnswer());
                hashmapChildPosWithQuesAns.put(childPosition++, hashMapQuesitonsAnswers);
            }
            hashMapChild.put(faqModel1.getCategoryName(), listChildTitle);
            hashmapChildIdGroupIdWithAnswers.put(groupPosition++, hashmapChildPosWithQuesAns);
        }

        faqAdapter = new FAQAdapter(this, listGroupTitle, hashMapChild);
        expandableListView.setAdapter(faqAdapter);
    }


    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
