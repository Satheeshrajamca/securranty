
package com.android.securranty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DivisionModel {

    @SerializedName("divisions")
    private List<Division> Divisions;
    @SerializedName("status")
    private String Status;
    @SerializedName("statusCode")
    private Long StatusCode;

    public List<Division> getDivisions() {
        return Divisions;
    }

    public String getStatus() {
        return Status;
    }

    public Long getStatusCode() {
        return StatusCode;
    }

    public static class Builder {

        private List<Division> Divisions;
        private String Status;
        private Long StatusCode;

        public DivisionModel.Builder withDivisions(List<Division> divisions) {
            Divisions = divisions;
            return this;
        }

        public DivisionModel.Builder withStatus(String status) {
            Status = status;
            return this;
        }

        public DivisionModel.Builder withStatusCode(Long statusCode) {
            StatusCode = statusCode;
            return this;
        }

        public DivisionModel build() {
            DivisionModel DivisionModel = new DivisionModel();
            DivisionModel.Divisions = Divisions;
            DivisionModel.Status = Status;
            DivisionModel.StatusCode = StatusCode;
            return DivisionModel;
        }

    }

}
