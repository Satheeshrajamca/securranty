package com.android.securranty.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.adapter.ClaimsListAdapter;
import com.android.securranty.model.Claim;
import com.android.securranty.model.ClaimsModel;
import com.android.securranty.presenter.implementor.ClaimsListPresenterImpl;
import com.android.securranty.utility.DividerItemDecoration;
import com.android.securranty.utility.RecyclerItemClickListener;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.MainActivity;
import com.android.securranty.view.activity.claims.ClaimDetailsActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.MainActivity.CLAIMS;

/**
 * Created by satheeshraja on 4/7/18.
 */

public class ClaimFragment extends Fragment implements BaseView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.searchView)
    EditText searchView;
    @BindView(R.id.layoutClaims)
    RelativeLayout layoutClaims;

    private ActionBar actionBar;
    private ClaimsListAdapter claimsListAdapter;
    ClaimsListPresenterImpl ticketListPresenter;
    private MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
            Intent i = new Intent(getActivity(), SignInActivity.class);
            startActivityForResult(i, CLAIMS);
        }
    }

    private void recyclerviewItemClick() {
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        startActivityForResult(new Intent(getActivity(),
                                        ClaimDetailsActivity.class).putExtra("CLAIM_POSITION", position)
                                , 401);
                    }
                })
        );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_claims_list,
                container, false);
        ButterKnife.bind(this, view);
        if (SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
            ticketListPresenter = new ClaimsListPresenterImpl(this);
            ticketListPresenter.getClaimsList();
            search();
        }
        recyclerviewItemClick();
        return view;
    }

    private void search() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                claimsListAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(layoutClaims, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            mainActivity.changeLoginStatus("Login");
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            if (!SharePref.getInstance(getActivity()).isLoggedinCurrently()) {
                Intent i = new Intent(getActivity(), SignInActivity.class);
                startActivityForResult(i, CLAIMS);
            }
        }
    }

    public static List<Claim> claims = new ArrayList<>();

    @Override
    public void success(Object obj) {
        if (ClaimsModel.class.cast(obj).getClaims().size() > 0) {
            claims = ClaimsModel.class.cast(obj).getClaims();
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity());
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            claimsListAdapter = new ClaimsListAdapter();
            claimsListAdapter.setData(ClaimsModel.class.cast(obj).getClaims());
            recyclerView.setAdapter(claimsListAdapter);
        } else {
            new iOSDialogBuilder(getContext())
                    .setTitle("Claims")
                    .setSubtitle("No Active Claims Currently.")
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            dialog.dismiss();
                            mainActivity.backstackClaims();
                        }
                    }).build().show();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    public interface ListenerClaims {
        public void backstackClaims();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CLAIMS || requestCode == 401) {
            if (resultCode == RESULT_OK) {
                ticketListPresenter = new ClaimsListPresenterImpl(this);
                ticketListPresenter.getClaimsList();
                search();
                mainActivity.changeLoginStatus("Logout");
            }

        } else {
            mainActivity.backstackClaims();
        }
    }
}
