package com.android.securranty.view.activity.creditcard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.BuyPlan;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.CreditCardList;
import com.android.securranty.model.StatesList;
import com.android.securranty.presenter.implementor.AddCreditcardPresenterImpl;
import com.android.securranty.utility.PhoneNumberTextWatcher;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.creditcard.CreditCardListActivity.ISEditCreditCard;
import static com.android.securranty.view.activity.creditcard.CreditCardListActivity.listCreditCardList;

/**
 * Created by satheeshr on 04-12-2017.
 */

public class AddEditCreditcardActivity extends AppCompatActivity implements BaseView {
    @BindView(R.id.labelCreditCardNo)
    TextView labelCreditCardNo;
    @BindView(R.id.labelExpiryDate)
    TextView labelExpiryDate;
    @BindView(R.id.labelCVV)
    TextView labelCVV;
    @BindView(R.id.labelFirstName)
    TextView labelFirstName;
    @BindView(R.id.labelLastName)
    TextView labelLastName;
    @BindView(R.id.labelBillingEmail)
    TextView labelBillingEmail;
    @BindView(R.id.labelPhone)
    TextView labelPhone;
    @BindView(R.id.labelBillingAdd1)
    TextView labelBillingAdd1;
    @BindView(R.id.labelBillingAdd2)
    TextView labelBillingAdd2;
    @BindView(R.id.labelCity)
    TextView labelCity;
    @BindView(R.id.labelState)
    TextView labelState;
    @BindView(R.id.labelCountry)
    TextView labelCountry;
    @BindView(R.id.labelZip)
    TextView labelZip;
    @BindView(R.id.checkboxDefaultPayment)
    CheckBox checkboxDefaultPayment;
    @BindView(R.id.checkboxActive)
    CheckBox checkboxActive;
    /* @BindView(R.id.checkboxExpired)
     CheckBox checkboxExpired;*/
    @BindView(R.id.textToolbarTitle)
    TextView textToolbarTitle;


    @BindView(R.id.editCreditCardNo)
    EditText editCreditCardNo;
    @BindView(R.id.editExpiryMonth)
    MaterialBetterSpinner editExpiryMonth;
    @BindView(R.id.editExpiryYear)
    MaterialBetterSpinner editExpiryYear;
    @BindView(R.id.editCVV)
    EditText editCVV;
    @BindView(R.id.editFirstName)
    EditText editFirstName;
    @BindView(R.id.editLastName)
    EditText editLastName;
    @BindView(R.id.editBillingEmail)
    EditText editBillingEmail;
    @BindView(R.id.editPhone)
    EditText editPhone;
    @BindView(R.id.editBillingAdd1)
    EditText editBillingAdd1;
    @BindView(R.id.editBillingAdd2)
    EditText editBillingAdd2;
    @BindView(R.id.editCity)
    EditText editCity;
    @BindView(R.id.editState)
    EditText editState;
    @BindView(R.id.editCountry)
    EditText editCountry;
    @BindView(R.id.editZip)
    EditText editZip;

    @BindView(R.id.imageSave)
    ImageView imageSave;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private AddCreditcardPresenterImpl addCreditcardPresenter;
    private List<SearchListItem> listCountry, stateList;
    private int selectedCountryId, countryId, selectedCardPosition;

    private List<String> month = new ArrayList<>();
    private List<String> year = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_card);
        ButterKnife.bind(this);
        addCreditcardPresenter = new AddCreditcardPresenterImpl(this);
        setTypeFace();
        if (getIntent().getBooleanExtra(ISEditCreditCard, false)) {
            textToolbarTitle.setText("Edit Card");
            selectedCardPosition = Integer.parseInt(getIntent().getStringExtra("SelectedPosition"));
            fillCreditCardDetails(listCreditCardList.get(selectedCardPosition));
        }
        editPhone.addTextChangedListener(new PhoneNumberTextWatcher(editPhone));
        addMonthYear();
    }

    private void addMonthYear() {
        for (int i = 1; i <= 12; i++) {
            if (i < 10) {
                month.add("0" + i);
            } else {
                month.add("" + i);
            }
        }
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 20; i++) {
            year.add("" + (currentYear + i));
        }
        editExpiryMonth.setUnderlineColor(R.color.black_color);
        editExpiryYear.setUnderlineColor(R.color.black_color);
        ArrayAdapter<String> adapterMonth = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, month);
        editExpiryMonth.setAdapter(adapterMonth);
        ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, year);
        editExpiryYear.setAdapter(adapterYear);
    }

    private void fillCreditCardDetails(CreditCardList.CreditCardListItem creditCardListItem) {
        editCreditCardNo.setFocusable(false);
        checkboxActive.setChecked(creditCardListItem.isActive());
        checkboxDefaultPayment.setChecked(creditCardListItem.isDefault());
        editCreditCardNo.setText("**** **** **** " + creditCardListItem.getLastFour());
        editExpiryMonth.setText(creditCardListItem.getExMonth());
        editExpiryYear.setText(creditCardListItem.getExYear());
        editFirstName.setText(creditCardListItem.getFirstName());
        editLastName.setText(creditCardListItem.getLastName());
        editBillingEmail.setText(creditCardListItem.getEmail());
        //editPhone.setText(creditCardListItem.getPhone());
        editBillingAdd1.setText(creditCardListItem.getAddress1());
        editBillingAdd2.setText(creditCardListItem.getAddress2());
        editCountry.setText(creditCardListItem.getCountry());
        editState.setText(creditCardListItem.getState());
        editCity.setText(creditCardListItem.getCity());
        editZip.setText(creditCardListItem.getZip());
        String phoneNumber = creditCardListItem.getPhone();
        if (phoneNumber != null) {
            if (!phoneNumber.isEmpty() && !phoneNumber.contains("-")) {
                editPhone.setText(String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6)));
            } else {
                editPhone.setText(phoneNumber);
            }
        }

        setCountryId(creditCardListItem.getCountry());
    }

    @OnClick({R.id.imageSave, R.id.imageBackButton, R.id.editCountry, R.id.editState, R.id.btnSave})
    public void updateCreditCardDetails(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                saveCreditCard();
                break;
            case R.id.imageBackButton:
                finish();
                break;
            case R.id.editCountry:
                if (listCountry == null) {
                    addCreditcardPresenter.initCountryList();
                } else {
                    displayCountry();
                }
                break;
            case R.id.editState:
                if (selectedCountryId != countryId || stateList == null) {
                    addCreditcardPresenter.initStatesList(editCountry.getText().toString().trim(), countryId);
                } else
                    displayState();
                break;
        }

    }

    private void saveCreditCard() {
        addCreditcardPresenter.validateCredential(editCreditCardNo.getText().toString().trim(),
                editExpiryMonth.getText().toString().trim(),
                editExpiryYear.getText().toString().trim(),
                editCVV.getText().toString().trim(),
                editFirstName.getText().toString().trim(),
                editLastName.getText().toString().trim(),
                editBillingEmail.getText().toString().trim(),
                editPhone.getText().toString().trim(),
                editBillingAdd1.getText().toString().trim(),
                editBillingAdd2.getText().toString().trim(),
                editCity.getText().toString().trim(),
                editState.getText().toString().trim(),
                editCountry.getText().toString().trim(),
                editZip.getText().toString().trim(),
                checkboxDefaultPayment.isEnabled(),
                checkboxActive.isEnabled()
        );
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");
        labelCreditCardNo.setTypeface(openSansRegular);
        labelExpiryDate.setTypeface(openSansRegular);
        labelCVV.setTypeface(openSansRegular);
        labelFirstName.setTypeface(openSansRegular);
        labelLastName.setTypeface(openSansRegular);
        labelBillingEmail.setTypeface(openSansRegular);
        labelPhone.setTypeface(openSansRegular);
        labelBillingAdd1.setTypeface(openSansRegular);
        labelBillingAdd2.setTypeface(openSansRegular);
        labelCity.setTypeface(openSansRegular);
        labelState.setTypeface(openSansRegular);
        labelZip.setTypeface(openSansRegular);
        labelCountry.setTypeface(openSansRegular);
        checkboxDefaultPayment.setTypeface(openSansRegular);
        checkboxActive.setTypeface(openSansRegular);
        /*checkboxExpired.setTypeface(openSansRegular);*/

        editCreditCardNo.setTypeface(openSansSemiBold);
        editExpiryMonth.setTypeface(openSansSemiBold);
        editExpiryYear.setTypeface(openSansSemiBold);
        editCVV.setTypeface(openSansSemiBold);
        editFirstName.setTypeface(openSansSemiBold);
        editLastName.setTypeface(openSansSemiBold);
        editBillingEmail.setTypeface(openSansSemiBold);
        editPhone.setTypeface(openSansSemiBold);
        editBillingAdd1.setTypeface(openSansSemiBold);
        editBillingAdd2.setTypeface(openSansSemiBold);
        editCity.setTypeface(openSansSemiBold);
        editState.setTypeface(openSansSemiBold);
        editCountry.setTypeface(openSansSemiBold);
        editZip.setTypeface(openSansSemiBold);

    }


    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    @Override
    public void success(Object obj) {
        if (obj instanceof APISuccessModel) {
            /*APISuccessModel apiSuccessModel = APISuccessModel.class.cast(obj);
            Snackbar.make(coordinatorLayout, apiSuccessModel.getMessage(), Snackbar.LENGTH_LONG).show();*/

            new iOSDialogBuilder(this)
                    .setTitle("Success")
                    .setSubtitle(APISuccessModel.class.cast(obj).getMessage())
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            Intent intent = getIntent();
                            setResult(RESULT_OK, intent);
                            finish();
                            dialog.dismiss();
                        }
                    }).build().show();

        } else if (obj instanceof CountryList) {
            listCountry = new ArrayList<>();
            for (CountryList.CountriesItem countryList : CountryList.class.cast(obj).getCountries()) {
                listCountry.add(new SearchListItem(countryList.getId(), countryList.getName()));
            }
            displayCountry();
        } else if (obj instanceof StatesList) {
            selectedCountryId = countryId;
            stateList = new ArrayList<>();
            for (StatesList.State states : StatesList.class.cast(obj).getStates()) {
                stateList.add(new SearchListItem(0, states.getStateName()));
            }
            displayState();
        }
    }

    private void displayState() {
        final SearchableDialog searchableDialog = new SearchableDialog(AddEditCreditcardActivity.this, stateList, "Select State");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editState.setText(searchListItem.getTitle());
            }
        });
    }

    private void displayCountry() {
        final SearchableDialog searchableDialog = new SearchableDialog(AddEditCreditcardActivity.this, listCountry, "Select Country");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editCountry.setText(searchListItem.getTitle());
                countryId = searchListItem.getId();
                if (selectedCountryId != countryId) {
                    editState.setText("");
                }
            }
        });
    }

    private void setCountryId(String billingCountry) {
        if (!billingCountry.isEmpty()) {
            switch (billingCountry) {
                case "United States":
                    countryId = 2;
                    selectedCountryId = 2;
                    break;
                case "Canada":
                    selectedCountryId = 3;
                    countryId = 3;
                    break;
                default:
                    countryId = 0;
            }
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            saveCreditCard();
        } else {
            finish();
        }
    }
}
