package com.android.securranty.view.activity.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.model.APISuccessModel;
import com.android.securranty.model.BillingModel;
import com.android.securranty.model.CountryList;
import com.android.securranty.model.StatesList;
import com.android.securranty.presenter.implementor.MyAccountPresenterImpl;
import com.android.securranty.utility.PhoneNumberTextWatcher;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.AccountView;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.securranty.view.activity.account.AccountProfileActivity.ACCOUNT_EDIT;
import static com.android.securranty.view.activity.account.AccountProfileActivity.ACCOUNT_IDLE;

/**
 * Created by satheeshr on 27-11-2017.
 */

public class BillingProfileActivity extends AppCompatActivity implements AccountView {

    @BindView(R.id.labelBillingContactName)
    TextView labelBillingContactName;
    @BindView(R.id.labelBillingEmail)
    TextView labelBillingEmail;
    /*@BindView(R.id.labelBillingContactEmail)
    TextView labelBillingContactEmail;
    @BindView(R.id.labelPurchaseOrderNo)
    TextView labelPurchaseOrderNo;*/
    @BindView(R.id.labelContactPhone)
    TextView labelContactPhone;
    @BindView(R.id.labelAddress1)
    TextView labelAddress1;
    @BindView(R.id.labelAddress2)
    TextView labelAddress2;
    @BindView(R.id.labelBillingCity)
    TextView labelBillingCity;
    @BindView(R.id.labelBillingState)
    TextView labelBillingState;
    @BindView(R.id.labelZip)
    TextView labelZip;
    @BindView(R.id.labelCountry)
    TextView labelCountry;
    @BindView(R.id.labelBillingMethod)
    TextView labelBillingMethod;


    @BindView(R.id.textBillingContactName)
    TextView textBillingContactName;
    @BindView(R.id.textContactPhone)
    TextView textContactPhone;
    @BindView(R.id.textBillingEmail)
    TextView textBillingEmail;
    /*@BindView(R.id.textBillingContactEmail)
    TextView textBillingContactEmail;
    @BindView(R.id.textPurchaseOrderNo)
    TextView textPurchaseOrderNo;*/
    @BindView(R.id.textAddress1)
    TextView textAddress1;
    @BindView(R.id.textAddress2)
    TextView textAddress2;
    @BindView(R.id.textBillingCity)
    TextView textBillingCity;
    @BindView(R.id.textBillingState)
    TextView textBillingState;
    @BindView(R.id.textZip)
    TextView textZip;
    @BindView(R.id.textCountry)
    TextView textCountry;
    @BindView(R.id.textBillingMethod)
    TextView textBillingMethod;

    @BindView(R.id.editBillingContactName)
    EditText editBillingContactName;
    @BindView(R.id.editContactPhone)
    EditText editContactPhone;
    @BindView(R.id.editBillingEmail)
    EditText editBillingEmail;
    /*@BindView(R.id.editBillingContactEmail)
    EditText editBillingContactEmail;
    @BindView(R.id.editPurchaseOrderNo)
    EditText editPurchaseOrderNo;*/
    @BindView(R.id.editAddress1)
    EditText editAddress1;
    @BindView(R.id.editAddress2)
    EditText editAddress2;
    @BindView(R.id.editBillingCity)
    EditText editBillingCity;
    @BindView(R.id.editState)
    EditText editBillingState;
    @BindView(R.id.editZip)
    EditText editZip;
    @BindView(R.id.editCountry)
    EditText editCountry;
    @BindView(R.id.editBillingMethod)
    EditText editBillingMethod;
    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageEdit)
    ImageView imageEdit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private MyAccountPresenterImpl myAccountPresenter;
    private String tagBillingStatus = ACCOUNT_EDIT;
    private int selectedCountryId;
    private ActionBar actionBar;
    private int countryId;
    private List<SearchListItem> listCountry, stateList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        myAccountPresenter = new MyAccountPresenterImpl(this);
        myAccountPresenter.getBillingDetails();
        editContactPhone.addTextChangedListener(new PhoneNumberTextWatcher(editContactPhone));
        setTypeFace();
    }

    private void setTypeFace() {
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/opensans_semibold.ttf");

        labelBillingContactName.setTypeface(openSansRegular);
        labelBillingEmail.setTypeface(openSansRegular);
        labelAddress1.setTypeface(openSansRegular);
        labelAddress1.setTypeface(openSansRegular);
        labelContactPhone.setTypeface(openSansRegular);
        labelAddress2.setTypeface(openSansRegular);
        labelAddress2.setTypeface(openSansRegular);
        labelBillingCity.setTypeface(openSansRegular);
        labelBillingState.setTypeface(openSansRegular);
        labelZip.setTypeface(openSansRegular);
        labelCountry.setTypeface(openSansRegular);
        labelBillingMethod.setTypeface(openSansRegular);
        /*labelBillingContactEmail.setTypeface(openSansRegular);
        labelPurchaseOrderNo.setTypeface(openSansRegular);*/

        textBillingContactName.setTypeface(openSansSemiBold);
        textContactPhone.setTypeface(openSansSemiBold);
        textBillingEmail.setTypeface(openSansSemiBold);
        textAddress1.setTypeface(openSansSemiBold);
        textAddress2.setTypeface(openSansSemiBold);
        textBillingCity.setTypeface(openSansSemiBold);
        textBillingState.setTypeface(openSansSemiBold);
        textCountry.setTypeface(openSansSemiBold);
        textZip.setTypeface(openSansSemiBold);
        textCountry.setTypeface(openSansSemiBold);
        textBillingMethod.setTypeface(openSansSemiBold);
        /*textBillingContactEmail.setTypeface(openSansSemiBold);
        textPurchaseOrderNo.setTypeface(openSansSemiBold);*/

        editBillingContactName.setTypeface(openSansSemiBold);
        editContactPhone.setTypeface(openSansSemiBold);
        editBillingEmail.setTypeface(openSansSemiBold);
        editAddress1.setTypeface(openSansSemiBold);
        editAddress2.setTypeface(openSansSemiBold);
        editBillingCity.setTypeface(openSansSemiBold);
        editBillingState.setTypeface(openSansSemiBold);
        editZip.setTypeface(openSansSemiBold);
        editCountry.setTypeface(openSansSemiBold);
        editBillingMethod.setTypeface(openSansSemiBold);
        /*editBillingContactEmail.setTypeface(openSansSemiBold);
        editPurchaseOrderNo.setTypeface(openSansSemiBold);*/
    }

    @OnClick({R.id.imageEdit, R.id.imageBackButton, R.id.editCountry, R.id.editState, R.id.btnSave})
    public void onClickEditProfile(View view) {
        switch (view.getId()) {
            case R.id.imageEdit:
                myAccountPresenter.editBillingProfile(tagBillingStatus, editBillingContactName.getText().toString().trim(), editContactPhone.getText().toString().trim(),
                        "", editBillingEmail.getText().toString().trim(), "",
                        editAddress1.getText().toString().trim(),
                        editAddress2.getText().toString().trim(), editBillingCity.getText().toString().trim(), editBillingState.getText().toString().trim(),
                        editZip.getText().toString().trim(), editCountry.getText().toString().trim(), editBillingMethod.getText().toString().trim());
                break;
            case R.id.btnSave:
                String phoneNo = editContactPhone.getText().toString().trim().replaceAll("[-]", "");
                myAccountPresenter.editBillingProfile(tagBillingStatus, editBillingContactName.getText().toString().trim(), phoneNo,
                        "", editBillingEmail.getText().toString().trim(), "",
                        editAddress1.getText().toString().trim(),
                        editAddress2.getText().toString().trim(), editBillingCity.getText().toString().trim(), editBillingState.getText().toString().trim(),
                        editZip.getText().toString().trim(), editCountry.getText().toString().trim(), editBillingMethod.getText().toString().trim());
                break;
            case R.id.imageBackButton:
                onBackPressed();
                break;
            case R.id.editCountry:
                if (listCountry == null) {
                    myAccountPresenter.initCountryList();
                } else {
                    displayCountry();
                }
                break;
            case R.id.editState:
                if (selectedCountryId != countryId || stateList == null) {
                    myAccountPresenter.initStatesList(editCountry.getText().toString().trim(), countryId);
                } else
                    displayState();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (tagBillingStatus.equals(ACCOUNT_IDLE)) {
            btnSave.setVisibility(View.GONE);
            myAccountPresenter.backPressed(billingModel);
        } else {
            super.onBackPressed();
        }
    }

    private void displayState() {
        final SearchableDialog searchableDialog = new SearchableDialog(BillingProfileActivity.this, stateList, "Select State");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editBillingState.setText(searchListItem.getTitle());
            }
        });
    }

    @Override
    public void showEditView(int editorVisibility, int textVisibility) {
        editBillingContactName.setVisibility(editorVisibility);
        editContactPhone.setVisibility(editorVisibility);
        editBillingEmail.setVisibility(editorVisibility);
        editAddress1.setVisibility(editorVisibility);
        editAddress2.setVisibility(editorVisibility);
        editBillingCity.setVisibility(editorVisibility);
        editBillingState.setVisibility(editorVisibility);
        editZip.setVisibility(editorVisibility);
        editCountry.setVisibility(editorVisibility);
        editBillingMethod.setVisibility(editorVisibility);
        /*editBillingContactEmail.setVisibility(editorVisibility);
        editPurchaseOrderNo.setVisibility(editorVisibility);*/

        textBillingContactName.setVisibility(textVisibility);
        textContactPhone.setVisibility(textVisibility);
        textBillingEmail.setVisibility(textVisibility);
        textAddress1.setVisibility(textVisibility);
        textAddress2.setVisibility(textVisibility);
        textBillingCity.setVisibility(textVisibility);
        textBillingState.setVisibility(textVisibility);
        textZip.setVisibility(textVisibility);
        textCountry.setVisibility(textVisibility);
        textBillingMethod.setVisibility(textVisibility);
        /*textBillingContactEmail.setVisibility(textVisibility);
        textPurchaseOrderNo.setVisibility(textVisibility);*/
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(coordinatorLayout, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(this, SignInActivity.class), 401);
        }
    }

    private Object billingModel;

    @Override
    public void success(final Object obj) {
        if (obj instanceof BillingModel) {
            billingModel = obj;
            BillingModel.BillingProfile billingProfile = BillingModel.class.cast(obj).getBillingProfile();
            textBillingContactName.setText(billingProfile.getBillingContact());
            String phoneNumber = billingProfile.getBillingContactPhone();
            if (phoneNumber != null) {
                if (!phoneNumber.isEmpty() && !phoneNumber.contains("-")) {
                    textContactPhone.setText(String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6)));
                    editContactPhone.setText(String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6)));
                } else {
                    textContactPhone.setText(phoneNumber);
                    editContactPhone.setText(phoneNumber);
                }
            }
            textBillingEmail.setText(billingProfile.getBillingEmail());
            textAddress1.setText(billingProfile.getBillingAddress1());
            textAddress2.setText(billingProfile.getBillingAddress2());
            textBillingCity.setText(billingProfile.getBillingCity());
            textBillingState.setText(billingProfile.getBillingState());
            textZip.setText(billingProfile.getBillingzip());
            textCountry.setText(billingProfile.getBillingCountry());
            textBillingMethod.setText(billingProfile.getBillingMethod());
            /*textBillingContactEmail.setText(billingProfile.getBillingContactEmails());
            textPurchaseOrderNo.setText(billingProfile.getBillingPO());*/

            editBillingContactName.setText(billingProfile.getBillingContact());
            editBillingEmail.setText(billingProfile.getBillingEmail());
            editAddress1.setText(billingProfile.getBillingAddress1());
            editAddress2.setText(billingProfile.getBillingAddress2());
            editBillingCity.setText(billingProfile.getBillingCity());
            editBillingState.setText(billingProfile.getBillingState());
            editZip.setText(billingProfile.getBillingzip());
            editCountry.setText(billingProfile.getBillingCountry());
            editBillingMethod.setText(billingProfile.getBillingMethod());
            /*editBillingContactEmail.setText(billingProfile.getBillingContactEmails());
            editPurchaseOrderNo.setText(billingProfile.getBillingPO());*/
            setCountryId(billingProfile.getBillingCountry());

        } else if (obj instanceof APISuccessModel) {
            //Snackbar.make(coordinatorLayout, APISuccessModel.class.cast(obj).getMessage(), Snackbar.LENGTH_LONG).show();

            textBillingContactName.setText(editBillingContactName.getText().toString().trim());
            textContactPhone.setText(editContactPhone.getText().toString().trim());
            textBillingEmail.setText(editBillingEmail.getText().toString().trim());
            textAddress1.setText(editAddress1.getText().toString().trim());
            textAddress2.setText(editAddress2.getText().toString().trim());
            textBillingCity.setText(editBillingCity.getText().toString().trim());
            textBillingState.setText(editBillingState.getText().toString().trim());
            textZip.setText(editZip.getText().toString().trim());
            textCountry.setText(editCountry.getText().toString().trim());
            textBillingMethod.setText(editBillingMethod.getText().toString().trim());
            /*textBillingContactEmail.setText(billingProfile.getBillingContactEmails());
            textPurchaseOrderNo.setText(billingProfile.getBillingPO());*/
            new iOSDialogBuilder(BillingProfileActivity.this)
                    .setTitle("Billing Profile")
                    .setSubtitle(APISuccessModel.class.cast(obj).getMessage())
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            finish();
                            dialog.dismiss();
                        }
                    }).build().show();
        } else if (obj instanceof CountryList) {
            listCountry = new ArrayList<>();
            for (CountryList.CountriesItem countryList : CountryList.class.cast(obj).getCountries()) {
                listCountry.add(new SearchListItem(countryList.getId(), countryList.getName()));
            }
            displayCountry();
        } else if (obj instanceof StatesList) {
            selectedCountryId = countryId;
            stateList = new ArrayList<>();
            for (StatesList.State states : StatesList.class.cast(obj).getStates()) {
                stateList.add(new SearchListItem(0, states.getStateName()));
            }
            displayState();
        }
    }

    private void displayCountry() {
        final SearchableDialog searchableDialog = new SearchableDialog(BillingProfileActivity.this, listCountry, "Select Country");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                editCountry.setText(searchListItem.getTitle());
                countryId = searchListItem.getId();
                if (selectedCountryId != countryId) {
                    editBillingState.setText("");
                }
            }
        });
    }

    private void setCountryId(String billingCountry) {
        if (billingCountry != null && !billingCountry.isEmpty()) {
            switch (billingCountry) {
                case "United States":
                    countryId = 2;
                    selectedCountryId = 2;
                    break;
                case "Canada":
                    selectedCountryId = 3;
                    countryId = 3;
                    break;
                default:
                    countryId = 0;
            }
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void setTag(String account_state) {
        tagBillingStatus = account_state;
    }

    @Override
    public void changeIcon(@DrawableRes int save_icon) {
        if (save_icon == 0) {
            imageEdit.setImageResource(0);
            btnSave.setVisibility(View.VISIBLE);
        } else {
            imageEdit.setImageResource(save_icon);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myAccountPresenter.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 401 && resultCode == RESULT_OK) {
            myAccountPresenter.getBillingDetails();
        } else {
            finish();
        }
    }
}
