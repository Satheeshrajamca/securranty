package com.android.securranty.view.fragment.fileclaim;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.android.securranty.R;
import com.android.securranty.SecurrantyApp;
import com.android.securranty.presenter.implementor.FileClaimPresenter;
import com.android.securranty.utility.SharePref;
import com.android.securranty.view.BaseView;
import com.android.securranty.view.activity.claims.FileClaimsActivity;
import com.android.securranty.view.activity.shipping.AddShippingAddressActivity;
import com.android.securranty.view.activity.signs.SignInActivity;
import com.android.securranty.view.fragment.fileclaim.model.ShippingAddress;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.securranty.view.activity.plans.active_plan.ActivePlanListActivity.daoFileClaim;

/**
 * Created by satheeshraja on 4/13/18.
 */

public class ShippingAddresFragment extends Fragment implements BaseView {
    @BindView(R.id.btnShippingAddress)
    TextView btnShippingAddress;
    @BindView(R.id.textShippingAddress)
    TextView textShippingAddress;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.previous)
    TextView previous;
    @BindView(R.id.relativeClaimType)
    RelativeLayout relativeClaimType;

    private FileClaimPresenter fileclaimPresenter;
    private FileClaimsActivity context;
    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (FileClaimsActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.layout_shipping_address,
                    container, false);
            ButterKnife.bind(this, view);
            previous.setVisibility(View.VISIBLE);
            fileclaimPresenter = new FileClaimPresenter(this);
        }
        return view;
    }

    @OnClick({R.id.next, R.id.previous, R.id.btnShippingAddress, R.id.textAddAddress})
    public void onclickClaimType(View view) {
        switch (view.getId()) {
            case R.id.next:
                daoFileClaim.setShippingAddressId(shippingId);
                context.triggerClaim(new SubmitClaimFragment(), "Payment and Review");
                break;
            case R.id.previous:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                break;
            case R.id.btnShippingAddress:
                //if (listShippingAddress == null) {
                fileclaimPresenter.getShippingAddress();
                /*} else {
                    displayShippingAddress(listShippingAddress);
                }*/
                break;
            case R.id.textAddAddress:
                startActivityForResult(new Intent(getActivity(), AddShippingAddressActivity.class), 100);
                break;
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(int statusCode, String errorMsg) {
        Snackbar.make(relativeClaimType, errorMsg, Snackbar.LENGTH_LONG).show();
        if (statusCode == 401) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
            SharePref.getInstance(SecurrantyApp.context).clearCredential();
            SharePref.getInstance(SecurrantyApp.context).resetUserDetails();
            startActivityForResult(new Intent(getActivity(), SignInActivity.class), 401);
        }
    }

    private List<SearchListItem> listShippingAddress;

    @Override
    public void success(Object obj) {
        next.setVisibility(View.VISIBLE);
        listShippingAddress = new ArrayList<>();
        List<ShippingAddress> shippingAddresses = ShippingAddress.class.cast(obj).getShippingAddresses();
        for (ShippingAddress shippingAddress : shippingAddresses) {
            listShippingAddress.add(new SearchListItem(shippingAddress.getId(), shippingAddress.getDisplayText()));
        }
        if (!listShippingAddress.isEmpty()) {
            displayShippingAddress(listShippingAddress);
        } else {
            new iOSDialogBuilder(getContext())
                    .setTitle("Shipping Address")
                    .setSubtitle("No more shipping address. Please add shipping address.")
                    .setBoldPositiveLabel(true)
                    .setCancelable(false)
                    .setPositiveListener("Dismiss", new iOSDialogClickListener() {
                        @Override
                        public void onClick(iOSDialog dialog) {
                            dialog.dismiss();
                        }
                    }).build().show();
        }

    }

    @Override
    public void hideKeyboard() {

    }

    private int shippingId, selectedClaimId;

    private void displayShippingAddress(List<SearchListItem> listShippingAddress) {
        final SearchableDialog searchableDialog = new SearchableDialog(getActivity(), listShippingAddress, "Select Shipping Address");
        searchableDialog.show();
        searchableDialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int position, SearchListItem searchListItem) {
                shippingId = searchListItem.getId();
                if (shippingId != selectedClaimId) {
                    textShippingAddress.setText(searchListItem.getTitle());
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            Snackbar.make(relativeClaimType, data.getStringExtra("Key"), Snackbar.LENGTH_LONG).show();
        }
        if (requestCode == 401 && resultCode == RESULT_OK) {
            fileclaimPresenter.getShippingAddress();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
