
package com.android.securranty.view.activity.plans.active_plan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class UploadImageModel {

    @SerializedName("Files")
    private List<Long> files;
    @Expose
    private String message;
    @Expose
    private String status;
    @Expose
    private Long statusCode;

    public List<Long> getFiles() {
        return files;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private List<Long> files;
        private String message;
        private String status;
        private Long statusCode;

        public UploadImageModel.Builder withFiles(List<Long> files) {
            this.files = files;
            return this;
        }

        public UploadImageModel.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public UploadImageModel.Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public UploadImageModel.Builder withStatusCode(Long statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public UploadImageModel build() {
            UploadImageModel uploadImageModel = new UploadImageModel();
            uploadImageModel.files = files;
            uploadImageModel.message = message;
            uploadImageModel.status = status;
            uploadImageModel.statusCode = statusCode;
            return uploadImageModel;
        }

    }

}
